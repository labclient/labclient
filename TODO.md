# Notes

- [ ] Chomp ending '/' 308 errors
- Error / Response Handler? Response Object always?
- `project.create_milestone` Project Helper
- `milestone.close`, `milestone.activate`/`milestone.open`
- Issue Due Date Helper `Date.parse(issue.due_date).past?`
- Describe Shim process (custom methods for objects)
- Group/Project consistent helpers `full_path`, `path_with_namespace`, `full_path` (path_with_namespace best option?)
- Parameter searching for creating issues could be easier. Something like DataTables?
- Debug Setup/Parameters - Print HTTP Requests
- client.approvals.project.rules doesn't work
- Get a single project-level rule (13.7) - https://docs.gitlab.com/ee/api/merge_request_approvals.html#get-project-level-rules

project = client.projects.show(1)
project.approval_rules

- Wizard Issues?
  [2021-08-11 16:49:59 +0000] FATAL: Request Failed { code: 409, message: "Username has already been taken" }

# API Resources

See [Issues](https://gitlab.com/labclient/labclient/-/issues?label_name%5B%5D=feature+request)

Keeping information here to clear out / make issues for. Most of this is going to need to be GitLab Documentation.

# Documentation Fixes:

- [ ] Indentation weirdness - https://docs.gitlab.com/ee/api/users.html#get-user-activities-admin-only

YEAR DATE MONTH consistentcy
https://docs.gitlab.com/ee/api/users.html#get-user-activities-admin-only

- [ ] Clean up missing tables for Parameters

  ```
  Parameters:
  ```

  Example: https://docs.gitlab.com/ee/api/tags.html#create-a-new-tag

Sort by Required? https://docs.gitlab.com/ee/api/users.html#user-creation, required params are sprinkled throughout

# Group Badges

https://docs.gitlab.com/ee/api/group_badges.html#add-a-badge-to-a-group
Doesn't include parameter 'name', but is valid and acceoted

# Typo?

https://docs.gitlab.com/ee/api/pipeline_schedules.html#create-a-new-pipeline-schedule-variable
Create a new variable of a pipeline schedule.
