FROM ruby:3.0.3

COPY pkg/labclient-*.gem /

# RUN apk add --no-cache ruby ruby-dev build-base libxml2-dev libxslt-dev libcurl
RUN gem install /*.gem pry && rm *.gem

COPY docker/start.sh /usr/bin/
COPY docker/start.rb /root/

ENTRYPOINT ["start.sh"]