'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  let app = new EmberApp(defaults, {
    SRI: {
      enabled: false
    },
    sassOptions: {
      sourceMapEmbed: true,
      includePaths: [
        'node_modules/materialize-css/sass'
      ]
    },
    autoprefixer: {
      sourcemap: true
    }
  });

  app.import('node_modules/@fortawesome/fontawesome-free/css/all.min.css')
  app.import('node_modules/@fortawesome/fontawesome-free/js/all.min.js')
  
  app.import('node_modules/materialize-css/dist/js/materialize.min.js')  

  app.import('vendor/css/atom-dark.css')
  app.import('vendor/js/clipboard.min.js')


  return app.toTree();
};
