import { helper } from '@ember/component/helper';

export function itemTitle(params/*, hash*/) {
  return params.map((str) => {
    let re = new RegExp(' ', 'g');

    return str.replace(re, '_');
  }).join("-")
}

export default helper(itemTitle);
