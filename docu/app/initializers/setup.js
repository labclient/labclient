export function initialize(/* application */) {
  var clipboard = new ClipboardJS('.clip');

  clipboard.on('success', function(e) {
    M.toast({html: 'Copied!', displayLength: 1500})
    e.clearSelection();
  });
  
  $.fn.extend({
    animateCss: function (animationName, callback) {
      var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
      this.addClass('animated ' + animationName).one(animationEnd, function() {
        $(this).removeClass('animated ' + animationName);
        if (callback) {
          callback(this);
        }
      });
      return this;
    }
  });
}

export default {
  initialize
};
