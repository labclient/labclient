import Component from "@ember/component";

export default Component.extend({
  actions: {
    select: function (group) {
      this.set("group", group);
    },
  },

  filterDocs: Ember.computed("docs", "filter", function () {
    let docs = Object.keys(this.get("docs"));

    let filter = this.get("filter");

    if (filter) {
      let regex = new RegExp(filter, "i");

      let filtered = docs.filter((key) => key.match(regex));

      return this.order(filtered);
    }
    return docs;
  }),

  order: function (list) {
    return list.sort(function (a, b) {
      return a.length - b.length || a.localeCompare(b);
    });
  },
});
