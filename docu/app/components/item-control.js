import Component from "@ember/component";
import { empty } from "@ember/object/computed";

export default Component.extend({
  didInsertElement: function () {
    let elementId = this.get("elementId");
    Ember.run.scheduleOnce("afterRender", this, function () {
      document
        .querySelectorAll("div#" + elementId + " > pre code")
        .forEach((block) => {
          hljs.highlightBlock(block);
        });

      this.demo();
    });
  },

  title: Ember.computed("item.title", function () {
    let item = this.get("item");
    console.log("title", item.title);
    if (!item.title) {
      return "";
    }

    return item.title.replace(" ", "~");
  }),

  demo: function () {
    // Trigger Demo, Its Nasty, But No script tags for Ember
    let item = this.get("item");
    if (item.demo_url) {
      const script = document.createElement("script");
      script.src = item.demo_url;
      document.body.appendChild(script);
    }
  },
});
