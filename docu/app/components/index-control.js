import Component from '@ember/component';
import ENV from 'docu/config/environment';

export default Component.extend({
  date: ENV.date,
  docs: null,
  nav: true,
  didInsertElement() {
    Ember.$.getJSON("docs.json")
      .then((resp) => {
        this.set('docs', resp)
        this.set('group', 'Overview')
      })

    M.AutoInit();
  },

  navigate: function() {
    Ember.run.scheduleOnce('afterRender', this, function() {
      if(window.location.hash) {
        var hash = decodeURIComponent(window.location.hash.substring(1));
        var [group, header] = hash.split('-');
        this.set('group', group.replace("_", " "))

        var match = group + "-" + header
        
        // After Render isn't Complete
        Ember.run.later(() => {
          let elm = document.getElementById(match)
          if (elm) { elm.scrollIntoView() }
        })

      }

      this.toggleProperty('nav')
    })
  },

  data: Ember.computed('group', function() {

    Ember.run.scheduleOnce('afterRender', this, function() {
      $('.scrollspy').scrollSpy({scrollOffset: 175});
      // $('.scrollspy').scrollSpy();
    });


    let docs = this.get('docs')
    if (!docs) return false 

    if (this.get('nav')) { this.navigate() }

    let group = this.get('group')
    return docs[group]
  })
});
