# LabClient Release ChangeLog

[[_TOC_]]

# 0.7.0

- Added Project Templates
- Update docker dev environment helpers
  - Specific EmberCLI Version
  - Updated Ruby/Image
  - Sleep to avoid build conflicts

## Breaking Changes

- Assume/return LabClient::PaginatedArray when no class provided (Klass/LabStruct)

### Files

- Return LabStruct or LabFile depending on type of return (raw, blame etc)
- Fix Bad Encoding (Switch CGI to ERB Encode)

# 0.6.0

- Add Terraform methods and state
- LabClient::Job - Rename `artifacts method` => `download_artifacts` to prevent collision on accessing hash values
- Fix bad Issue Documentation for delete(copy / paste issue)
- Add/fix delete method for `RegistryRepository`

### LabFile Class

- New LabFile wrapper for file responses. This makes it possible
  - Check response status
  - Use other klass/response helpers (ex. curl)
  - Store output without immediately writing to disk

### Response Object

- Prior to 0.6.0 it was inconsistent if the http/response object was returned.
- Attempt to always send back the response object

  - LabFile Responses
  - Blank 200 or 204 responses

- Update Examples to show Response Object

# 0.5.1

- Return LabStruct Object for Repository Tree - Pagination
- Replace Testing for STDIN which is too inconsistent

# 0.5.0

- Added Project `merge_request_delete`
- Updated MergeRequest Delete documentation

## Ruby 3

- Bump to ruby 3.0.3
- Remove OpenStruct (no more lazy loading)
- Add lazy loading to LabStruct

# 0.4.2

- Add `LabClient.version` helper
- Retry
  - Retry Delay Order: response header (retry_after), `delay_factor`, `1`
  - Delay factor default is now 10
  - Debug logging for the header
  - Move retry update before exception on max retries.
- Ruby 2.6.6

# 0.4.1

- $stdin working differently on shared runner, safe chomp access

# 0.4.0

- Client method re-organization
  - Helpers, Meta, Setup
- Retry Logic
  - Wait / Combine `retry-after` and `delay_factor`
  - Additive back off with subtractive falloff.
  - Max Retries
- Gem Updates
  - Code formatting with updated rubocop
  - Lint Updates
- Fix warnings for initialized variables
- Better Quiet for `quiet` Settings
  - Waiting for, Import Status, Wizard, and Klass Helper
  - Improved output supression for Testing
- Adding Ougai and Custom Logger
- ENV['TESTING'] to ENV['LABCLIENT_TESTING'] for Labclient specific namespace and supression

```
[2021-02-26 12:34:51 -0700] FATAL: Request Failed { code: 404, message: "404 Project Not Found" }
```

# 0.3.5

- Fix issue where escaped group/project ID's were not unescaped in chained objects.

# 0.3.4

- Fix file encoding for Repository files API

# 0.3.3

- User Helpers `active?`, `inactive?`
- Additional pagination helpers, `total`, `page`, `total_pages`
- Bump to Ruby 2.6.6
- Update RubyGems
- Respect `quiet` setting on Wizard (Adding `client.quiet?` helper)
- Add `response` and `client` into `LabStruct`. Makes `curl` more widely available. Also allows for `success?`
- Add `curl` to `response` for failed responses (e.g. 404)

```
Using concurrent-ruby 1.1.8 (was 1.1.7)
Using minitest 5.14.3 (was 5.14.2)
Using oj 3.11.1 (was 3.10.16)
Using ruby-progressbar 1.11.0 (was 1.10.1)
Using parallel 1.20.1 (was 1.19.2)
Using regexp_parser 2.0.3 (was 1.7.1)
Using simplecov-html 0.12.3 (was 0.12.2)
Using simplecov_json_formatter 0.1.2
Using tzinfo 2.0.4 (was 1.2.8)
Using docile 1.3.5 (was 1.3.2)
Using ffi 1.14.2 (was 1.13.1)
Using simplecov 0.21.2 (was 0.19.0)
Using ast 2.4.2 (was 2.4.1)
Using i18n 1.8.7 (was 1.8.5)
Using crack 0.4.5 (was 0.4.4)
Using activesupport 6.1.1 (was 6.0.3.4)
Using faker 2.15.1 (was 2.14.0)
Using minitest-reporters 1.4.3 (was 1.4.2)
Using parser 3.0.0.0 (was 2.7.1.4)
Using webmock 3.11.1 (was 3.9.1)
Using rubocop-ast 0.8.0 (was 0.4.1)
```

# 0.3.2

- Fix environment stop API call
- Add OAuth Authorization Option

# 0.3.1

- Introducing FeatureFlags
- Fix for `collect_project_id` where paginated response didn't use a `base_url`
- Client now has a `base_url` reader method
- Updated Documentation

# 0.3.0

## General

- Bundled Gems update (Rubocop 0.91.0 has issues)
- Update docker to use CI Registry variables
- Developer docs rake update
- Additional rubocop lint styles
- Sorted `api_methods` for common
- Add `help` filtering to `client`.

## Group & Project Clusters

- New Endpoint
- List / Show / Add / Update / Delete

## Projects

- Docs fixes where Starrers.Forks were replacing all help

---

# 0.2.1

## General

- Added LabStruct `slice` method for collecting specific data from output

## Project

- Added `commit` helper method to "show" a specific commit via sha.

## Project Environment

- Added `show` helper method on environment collect issue new `show` API call
  - This collects additional information about the environment, eg. deployment
- Alias `environment_show` `environment`

## Files

- Documentation typo fix

---

# 0.2.0

## General

- Remove pry as a required gem (was already a development dependency)
- Updated Rubocop / Linting
  - String Concats
  - Access Levels, constant hash lookup
  - STDIN / \$stdin
  - Blocks
- Gemfile Lock update

## Client

- Add `quiet` open to suppress error message output

## Issue

Helper Methods

- `close` and `reopen` to update issue state

## Jobs

- Docs Updates for incorrect titles for jobs. Retry, Keep, and Play.
- Fix for trace not returning an actual value. Was previous only using puts

---

# 0.1.5

## Notes

- Examples for creating notes specifically via Merge Requests, Issues, and Epics

## Issue

- Add `project` helper method to execute project show API call.

## HTTP

- Expand headers check for anything 'text/plain'. Resolve issues with file/raw
- Better file/raw testing. Check Parsing, return content

---

# 0.1.4

- Added Changelog and TOC

## Docker

- Updated Entrypoint. Will accept and can execute first parameter.
- Support params entrypoint prompt for credentials

## Client

- Prompt for credentials if none are provided (URL and Token)

#### Named Profiles

- Introducing Named Profiles
- Environment Variable selector - LABCLIENT_PROFILE
- Client creation (`LabClient::Client.new profile: :profile`)

## User Membership

- Membership API calls do not default to a type ('Project' => nil). This will include both types by default
- `user.memberships` now returns a `Membership` object rather than a Hash
- Membership object helper methods
  - `level`: human readable name
  - `permission`? true/false
  - `greater_than` :level
  - `parent`: show project/group for permission

## Member

- `greater_than` Permission Helper

## Protected Environments

- Introducing Protected Environments

  - List
  - Protect/Unprotect
  - Show

- Project helpers
  - `protected_environments`
  - `protected_environment`
  - `protect_environment`
  - `unprotect_environment`

## Wizard

- User generation skip_confirmation parameter, default: true.
- Print generated default password

---

# 0.1.3

## Repository Files

- Add URL encoding for file path/file creation. `file/name.txt`
- Documentation update / table / required fields

## Events

- Add `events` list helper for User and Projects and support query options

## Notes

- Fix Epics being incorrectly pointed to Projects instead of Groups
- Documentation fixes (name changes)
- Add `notes` and `note_create` for Issue, ProjectSnippet, Epic, and MergeRequest

## Branches

- Add Create API call
- List Pipelines from `Branch`
- Wait helper for initial branch creation `wait_for_pipelines`
- Branch Help

## Merge Request

- Accept call accepts query parameters. `should_remove_source_branch`, `merge_when_pipeline_succeeds`, etc
- Create via project example
- `reload` Helper
- `wait_for_merge_status` waiting helper. Merge Request checking status is deferred in later version of Gitlab. Waiter to fail/except/continue execution

## Pipeline

- `reload` helper
- `wait_for_status` helper. Wait for running pipeline, block/sleep until pipeline completes. `skipped`,`manual`,`canceled`, and `success`

## Wizard

- New Template Group Helper (For all templates requiring a default group)
- `create_file` helper, `branch`, `commit_message`, `content`
- Default set templates `templates_default`
- Section debug output (Creating projects/users/groups etc)

### Templates

Pages

- Updated with new helpers

Environment

- On Stop: Create environment on new branch. Stop environment on merge

Pipeline Triggers

- Child Pipeline: Parent pipeline with artifact, that is then executed as a child pipeline

#### Template Helpers

Projects & Groups

- Additional names selectors
- Ensure uniqness and consistency with total names selected

## Project

- Add `snippets` and `snippet_create` helpers
- Add `parent` helper. Returns namespace object, either Group or User
- Add `branch_create`
- Add `merge_request_create`

## Clean Up

- TODO clean up

## Dependencies / Conflicts

- Switch from `awesome_print` to `amazing_print`
- Remove `LabStruct` modifications to `LabStruct`
- Update Gems (ffi,kdramdown,active_support, oj)

## Other

- Verbose output using ruby19 syntax / amazing_print

---

# 0.1.2

Introduction of Wizard and Template Creators

- Generate Groups/Projects/Users
- Template's for Specific Generation
- Gitlab Pages: Page Group and X pages projects/import/pipeline

Reload Helpers

- Reload Object (Additional API Call)
- Group, Project, Issue, and User

- Issue Query/Create Helper

Object help filter and api method list

- Klass.help(:filter)
- Klass.api_methods

API Status response included in all levels of object responses.

Project

- Looping `wait_for_import` helper

Dockerfile

- version safe
- add libcurl

---

# 0.1.1

Documentation Updates

- Better Sorting when searching, order output

Helpers

- Fix all entries being applied to all help output
- Access levels helper

General

- Better error handling on invalid URLs
