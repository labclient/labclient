# LabClient

Labclient is another Gitlab API Gem. A focus on ease of use and helpers and snippets.

See usage and user docs here: https://gitlab.com/labclient/labclient

[![asciicast](https://asciinema.org/a/350689.svg)](https://asciinema.org/a/350689)

## Project Goals

- Easy to use
  - IRB/PRY Shortcuts/Helper Methods
  - Inspect Helpers / Instructions
- Hosted on Gitlab
- Feature Parity
- [ ] Populator
  - [ ] User Seed
  - [ ] Example Group/Projects

## Troubleshooting

### libcurl

LabClient's HTTP Client uses libcurl, so you need to ensure its installed/available.

Ubuntu

```shell
apt-get install libcurl4-openssl-dev
```

Alpine

```shell
apk add libcurl
```

Centos

```shell
yum install libcurl
```

## Todos

See [Issues](https://gitlab.com/labclient/labclient/-/issues?label_name%5B%5D=feature+request)

#### Potential Todo's

- [ ] Commandline/Docker version?
- [ ] Include ActiveSupport?
- [ ] Reload Support? `project.reload`

```

require "active_support/all"
5.days

```

## Documentation Structure/Methods

Documentation is included in each API call/class. The doc site is organized by category (typically the class), and subcategory. The categories are searchable in the left sidenav, and subcategories within each category navigatable via the right scrollspy.

Each "class" is used as a category with the 'block' name as the 'subgroup' within the scrollspy (sidenav on the right).

`@group_name`, can be used to override the category (_left sidenav_).

Class variable 'category' override: `@group_name`

Available Methods (_in render order_)

- **title**: Divider + Subheading in scrollspy
- **subtitle**: Divider + Subheading (not in scrollspy)
- **desc**: Paragraph/Standard Text render
- **markdown**: Markdown parsing by kramdown
- **example**: Code snippet, will include a 'clipboard' button to copy
- **result**: Code color/formatting without clipboard

The [overview.rb](lib/labclient/overview.rb) has some of the best examples for each.

Multiple 'doc' blocks can be called. Each new block will be appended to its category and is rendered in order.

Quick Example:

````ruby
  @group_name = 'Avatar'
  doc 'Overview' do
    title 'Setup'
    desc 'Install from RubyGems'
    markdown <<~DOC
      # Header

      ```
      code block
      ```
    DOC
    result "code result here"
    example 'gem install labclient'
  end

  doc 'Overview' do
    title 'Second'
    desc 'More helpful text'
  end
````

## Development

Included `docker-compose.yml` to make it easier to consume/test docs.

[Helpful Assertions](http://mattsears.com/articles/2011/12/10/minitest-quick-reference/)

```bash
docker-compose up --build
```

```ruby
# Install Dependency/Gems
bundle install

# Install Gem
bundle exec rake install

# Run Tests
bundle exec rake

# Review Coverage/Test Results
# coverage/index.html
```

To install this gem onto your local machine, run `bundle exec rake install`.

To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

#### Running Tests in a Container

```
docker run -it -v ${PWD}:/app ruby:3.0.3 bash
cd /app
bundle install
bundle exec rake
```

## Docker

```
# Build and Run
docker build -t labclient .F
docker run -it labclient

# Run Directly
docker run -it registry.gitlab.com/labclient/labclient
# Drops into a pry session with the gem required. Review Authentication/Env setup
client = LabClient::Client.new
```

## Release Process

- Increment `version.rb`
- Create MR
- Merge to Master
- Create Tag with Release Notes

```
# Build and Push
bundle exec rake build
gem push pkg/labclient-0.x.x.gem
```

## Contributing

Bug reports and merge requests are welcome on Gitlab at https://gitlab.com/labclient/labclient. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the LabClient project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/LabClient/blob/master/CODE_OF_CONDUCT.md).
