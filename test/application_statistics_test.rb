require File.expand_path 'test_helper.rb', __dir__

class ApplicationStatisticsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_statistics
    stub_get('/application/statistics', 'application_statistics')
    result = @client.application_statistics.show
    assert_kind_of LabClient::LabStruct, result
  end
end
