require File.expand_path 'test_helper.rb', __dir__

# rubocop:disable Metrics/AbcSize
class ProjectReleaseLinksTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::ProjectReleaseLink
  end

  def test_project_release_link
    stub_get('/projects/16/releases/tag_name/assets/links', 'project_release_links')
    stub_get('/projects/16/releases/tag_name/assets/links/3', 'project_release_link')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/releases/tag_name', 'project_release')

    list = @client.projects.release_links.list(16, :tag_name)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2

    # Via Project
    project = @client.projects.show(16)
    link = project.release_links(:tag_name).first

    assert_kind_of String, link.inspect
    assert_kind_of LabClient::Project, link.project

    # Via Release
    release = @client.projects.releases.show(16, :tag_name)
    assert_kind_of kind, release.links.first
  end

  def test_project_release_links_show
    stub_get('/projects/16/releases/tag_name/assets/links/3', 'project_release_link')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/releases/tag_name', 'project_release')

    link = @client.projects.release_links.show(16, :tag_name, 3)
    assert_kind_of kind, link

    project = @client.projects.show(16)
    assert_kind_of kind, project.release_link_show(:tag_name, 3)
  end

  def test_project_release_links_create
    stub_post('/projects/16/releases/tag_name/assets/links', 'project_release_link')
    stub_get('/projects/16', 'project')

    link = @client.projects.release_links.create(16, :tag_name, {})
    assert_kind_of kind, link

    project = @client.projects.show(16)
    assert_kind_of kind, project.release_link_create(:tag_name, {})
  end

  def test_project_release_links_update
    stub_put('/projects/16/releases/tag_name/assets/links/3', 'project_release_link')
    stub_get('/projects/16/releases/tag_name/assets/links/3', 'project_release_link')

    stub_get('/projects/16/release_links/tag_name', 'project_release_link')
    stub_get('/projects/16', 'project')

    # Root
    link = @client.projects.release_links.update(16, :tag_name, 3, {})
    assert_kind_of kind, link

    # Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.release_link_update(:tag_name, 3, {})

    # Release
    link = @client.projects.release_links.show(16, :tag_name, 3)
    assert_kind_of kind, link.update({})
  end

  def test_project_release_links_delete
    stub_get('/projects/16/releases/tag_name/assets/links/3', 'project_release_link')
    stub_delete('/projects/16/releases/tag_name/assets/links/3', 'project_release_link')
    stub_get('/projects/16', 'project')

    # Root
    assert_kind_of kind, @client.projects.release_links.delete(16, :tag_name, 3)

    # Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.release_link_delete(:tag_name, 3)

    # Release
    link = @client.projects.release_links.show(16, :tag_name, 3)
    assert_kind_of kind, link.delete
  end
  # rubocop:enable Metrics/AbcSize
end
