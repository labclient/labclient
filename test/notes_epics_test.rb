require File.expand_path 'test_helper.rb', __dir__

class EpicNotesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::Note
  end

  def test_epic_note_show
    stub_get('/groups/1/epics/2/notes/21', 'epic_note')
    note = @client.notes.epics.show(1, 2, 21)

    assert_kind_of kind, note
    assert_kind_of String, note.inspect
  end

  def test_epic_note_list
    stub_get('/groups/5/epics/1/notes', 'epic_notes')
    stub_get('/groups/5/epics/1', 'epic')

    list = @client.notes.epics.list(5, 1)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of kind, list.first

    epic = @client.epics.show(5, 1)
    assert_kind_of kind, epic.notes.first
  end

  def test_epic_note_create
    stub_post('/groups/5/epics/1/notes', 'epic_note')
    stub_get('/groups/5/epics/1', 'epic')

    note = @client.notes.epics.create(5, 1, {})
    assert_kind_of kind, note

    # Test On Epic
    epic = @client.epics.show(5, 1)
    assert_kind_of kind, epic.note_create({})
  end

  def test_epic_note_update
    stub_put('/groups/1/epics/2/notes/21', 'epic_note')
    note = @client.notes.epics.update(1, 2, 21, :body)
    assert_kind_of kind, note
  end

  def test_epic_note_delete
    stub_delete('/groups/1/epics/2/notes/21')

    resp = @client.notes.epics.delete(1, 2, 21)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end
end
