require File.expand_path 'test_helper.rb', __dir__

class ProtectedEnvironmentsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/projects/16', 'project')
  end

  def test_environment_list
    stub_get('/projects/16/protected_environments', 'protected_environments')

    list = @client.protected_environments.list(16)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::ProtectedEnvironment, list.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::ProtectedEnvironment, project.protected_environments.first
  end

  def test_environment_show
    stub_get('/projects/16/protected_environments/master', 'protected_environment')

    environment = @client.protected_environments.show(16, :master)
    assert_kind_of LabClient::ProtectedEnvironment, environment
    assert_kind_of String, environment.inspect

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::ProtectedEnvironment, project.protected_environment(:master)
  end

  def test_environment_protect
    opts = { name: :prod, deploy_access_levels: [{ access_level: :developer }] }
    stub_post('/projects/16/protected_environments', 'protected_environment')

    environment = @client.protected_environments.protect(16, opts)
    assert_kind_of LabClient::ProtectedEnvironment, environment

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::ProtectedEnvironment, project.protect_environment(opts)
  end

  def test_environment_unprotect
    stub_get('/projects/16/protected_environments/prod', 'protected_environment')
    stub_delete('/projects/16/protected_environments/prod')

    resp = @client.protected_environments.unprotect(16, :prod)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Project
    project = @client.projects.show(16)
    assert_nil project.unprotect_environment(:prod).data

    # Via ProtectedEnvironment
    environment = @client.protected_environments.show(16, :prod)
    assert_nil environment.unprotect.data
  end
end
