require File.expand_path 'test_helper.rb', __dir__

class AppearanceTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_appearance
    stub_get('/application/appearance', 'appearance')
    obj = @client.appearance.show
    assert_kind_of LabClient::Appearance, obj
  end

  def test_update_appearance
    stub_put('/application/appearance', 'appearance', 200, title: 'Gitlaaaab')

    obj = @client.appearance.update(title: 'Gitlaaaab')
    assert_kind_of LabClient::Appearance, obj
  end
end
