require File.expand_path 'test_helper.rb', __dir__

class ProjectPushRulesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_list_rules
    stub_get('/projects/16/push_rule', 'push_rule')

    rule = @client.projects.push_rules.show(16)
    assert_kind_of LabClient::PushRule, rule

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::PushRule, project.push_rules

    assert_kind_of String, rule.inspect
    assert_kind_of LabClient::Project, rule.project
  end

  def test_delete_rule
    stub_delete('/projects/16/push_rule')

    resp = @client.projects.push_rules.delete(16)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_nil project.push_rules_delete.data

    # Via Push Rule
    stub_get('/projects/16/push_rule', 'push_rule')
    rule = project.push_rules
    assert_nil rule.delete.data
  end

  def test_create_rule
    stub_post('/projects/16/push_rule', 'push_rule')

    rule = @client.projects.push_rules.create(16, {})
    assert_kind_of LabClient::PushRule, rule

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::PushRule, project.push_rules_create({})
  end

  def test_update_rule
    stub_put('/projects/16/push_rule', 'push_rule')

    assert_kind_of LabClient::PushRule, @client.projects.push_rules.update(16, {})

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::PushRule, project.push_rules_update({})

    # Via Push Rule
    stub_get('/projects/16/push_rule', 'push_rule')
    rule = project.push_rules
    assert_kind_of LabClient::PushRule, rule.update({})
  end
end
