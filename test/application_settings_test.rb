require File.expand_path 'test_helper.rb', __dir__

class ApplicationSettingsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_application_setting
    stub_get('/application/settings', 'application_settings')
    settings = @client.application_settings.show

    assert_kind_of String, settings.inspect
  end

  def test_application_settings_show
    suppress_output do
      stub_get('/application/settings', 'application_settings')
      settings = @client.application_settings.show
      assert_nil settings.verbose
    end
  end

  def test_application_setting_update
    details = { sign_in_text: 'You Love GitLab' }
    stub_get('/application/settings', 'application_settings')
    stub_put('/application/settings', 'application_settings', 200, details)

    assert_kind_of LabClient::ApplicationSetting, @client.application_settings.update(details)
  end

  def test_application_settings_update
    details = { sign_in_text: 'We Love GitLab', signup_enabled: true }
    stub_get('/application/settings', 'application_settings')
    stub_put('/application/settings', 'application_settings', 200, details)

    assert_kind_of LabClient::ApplicationSetting, @client.application_settings.update(details)
  end
end
