require File.expand_path 'test_helper.rb', __dir__

class GroupShowTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_group
    stub_get('/groups/5', 'group')
    group = @client.groups.show(5)

    assert_kind_of LabClient::Group, group
    assert_kind_of String, group.path
    assert_kind_of String, group.inspect
  end

  # Not the best test, place for it. Needed for class_helpers
  def test_class_helpers
    stub_get('/groups/5', 'group')
    group = @client.groups.show(5)

    assert_kind_of Array, group.keys
    assert_kind_of Hash, group.raw
  end
end
