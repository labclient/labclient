require File.expand_path 'test_helper.rb', __dir__

class AuditEventsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_audit_event
    stub_get('/audit_events/1', 'audit_event')
    audit_event = @client.audit_events.show(1)

    assert_kind_of LabClient::AuditEvent, audit_event
    assert_kind_of String, audit_event.inspect

    %i[created_at].each do |kind|
      assert_kind_of DateTime, audit_event.send(kind)
    end
  end

  def test_audit_event_list
    stub_get('/audit_events', 'audit_events')
    audit_events = @client.audit_events.list

    assert_kind_of LabClient::PaginatedResponse, audit_events
    assert_equal audit_events.size, 2
    assert_kind_of LabClient::AuditEvent, audit_events.first
  end

  def test_audit_event_group_list
    stub_get('/groups/15/audit_events', 'audit_events')

    audit_events = @client.audit_events.group_list(15)

    assert_kind_of LabClient::PaginatedResponse, audit_events
    assert_equal audit_events.size, 2
    assert_kind_of LabClient::AuditEvent, audit_events.first
  end

  def test_audit_event_group
    stub_get('/groups/15/audit_events/1', 'audit_event')
    audit_event = @client.audit_events.group_show(15, 1)

    assert_kind_of LabClient::AuditEvent, audit_event
    assert_kind_of String, audit_event.inspect

    %i[created_at].each do |kind|
      assert_kind_of DateTime, audit_event.send(kind)
    end
  end
end
