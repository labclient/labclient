require File.expand_path 'test_helper.rb', __dir__
class MembershipTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_users_memberships
    stub_get('/users/5', 'user')
    stub_get('/users/5/memberships', 'memberships')
    stub_get('/groups/5', 'group')
    stub_get('/projects/16', 'project')

    # Top
    group = @client.users.memberships(5).first
    project = @client.users.memberships(5).last

    assert_kind_of LabClient::Group, group.parent
    assert_kind_of LabClient::Project, project.parent
  end

  def test_users_membership
    stub_get('/users/5/memberships', 'memberships')

    # Top
    membership = @client.users.memberships(5).last

    assert_equal :owner, membership.level
    refute membership.guest?
    refute membership.reporter?
    refute membership.developer?
    refute membership.maintainer?
    assert membership.owner?

    assert_kind_of String, membership.inspect
  end

  def test_members_greater
    stub_get('/users/5/memberships', 'memberships')
    membership = @client.users.memberships(5).first

    assert membership.greater_than(:developer)
    assert membership.greater_than(10)
  end
end
