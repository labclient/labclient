require File.expand_path 'test_helper.rb', __dir__

class ProjectTests < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token', quiet: true)
  end

  def test_projects_list
    stub_get('/users/1/starred_projects', 'projects')
    projects = @client.projects.starred(1)

    assert_kind_of LabClient::PaginatedResponse, projects
    assert_equal projects.size, 3
    assert_kind_of LabClient::Project, projects.first
  end

  def test_project_show
    stub_get('/projects/16', 'project')

    project = @client.projects.show(16)
    assert_kind_of LabClient::Project, project

    # Test Reload
    assert_kind_of LabClient::Project, project.reload
  end

  # rubocop:disable Metrics/MethodLength
  def test_project_wait_for_import
    # stub_get('/projects/16', 'project')

    first = { status: 200, body: load_fixture('project_importing'), headers: {
      'content-type' => ['application/json']
    } }

    second = { status: 200, body: load_fixture('project_importing'), headers: {
      'content-type' => ['application/json']
    } }

    third = { status: 200, body: load_fixture('project'), headers: {
      'content-type' => ['application/json']
    } }
    stub_request(:get, 'https://labclient/api/v4/projects/16')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
          'Private-Token' => 'token'
        }
      )
      .to_return([first, second, third])

    project = @client.projects.show(16)

    # project.import_status = 'scheduled'
    assert_nil project.wait_for_import(5, 0.1)
  end
  # rubocop:enable Metrics/MethodLength

  def test_projects_starred
    stub_get('/projects', 'projects')
    projects = @client.projects.list

    assert_kind_of LabClient::PaginatedResponse, projects
    assert_equal projects.size, 3
    assert_kind_of LabClient::Project, projects.first
  end

  def test_projects_users
    stub_get('/projects/16/users', 'users')
    users = @client.projects.users(16)

    assert_kind_of LabClient::PaginatedResponse, users
    assert_equal users.size, 2
    assert_kind_of LabClient::User, users.first

    # Through Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)

    assert_kind_of LabClient::PaginatedResponse, project.users
  end

  def test_projects_events
    stub_get('/projects/16/events', 'events')
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    events = project.events

    assert_kind_of LabClient::PaginatedResponse, events
    assert_equal events.size, 2
    assert_kind_of LabClient::Event, events.first
  end

  def test_create_project
    stub_post('/projects', 'project')
    project = @client.projects.create({ name: 'Hello', description: 'World' })

    assert_kind_of LabClient::Project, project
  end

  def test_create_project_group
    stub_get('/groups/5', 'group')
    stub_post('/projects', 'project')

    group = @client.groups.show(5)
    assert_kind_of LabClient::Group, group
    project = group.project_create({})
    assert_kind_of LabClient::Project, project
  end

  def test_create_project_for_user
    stub_post('/projects/user/7', 'project')
    project = @client.projects.create_for_user(7, { name: 'Hello', description: 'World' })

    assert_kind_of LabClient::Project, project
  end

  def test_projects_update
    stub_put('/projects/16', 'project')
    project = @client.projects.update(16, name: 'rawr')

    assert_kind_of LabClient::Project, project

    # Through Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    project.update(name: 'rawr')
  end

  def test_projects_fork
    stub_post('/projects/16/fork', 'project')
    project = @client.projects.fork(16, name: 'rawr')

    assert_kind_of LabClient::Project, project

    # Through Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    project.fork(name: 'rawr')
  end

  def test_projects_forks
    stub_get('/projects/16/forks', 'projects')
    projects = @client.projects.forks(16)

    assert_kind_of LabClient::PaginatedResponse, projects
    assert_equal projects.size, 3
    assert_kind_of LabClient::Project, projects.first

    # Through Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    list = project.forks

    assert_kind_of LabClient::Project, list.first
  end

  def test_project_star
    stub_post('/projects/16/star', 'project')
    project = @client.projects.star(16)

    assert_kind_of LabClient::Project, project

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::Project, project.star
  end

  def test_project_star_repeat
    stub_post('/projects/16/star', 'subscribe_repeat', 304)
    suppress_output do
      assert_kind_of Typhoeus::Response, @client.projects.star(16)
    end
  end

  def test_project_unstar
    stub_post('/projects/16/unstar', 'project')
    project = @client.projects.unstar(16)

    assert_kind_of LabClient::Project, project

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::Project, project.unstar
  end

  def test_project_unstar_repeat
    stub_post('/projects/16/unstar', 'subscribe_repeat', 304)
    suppress_output do
      assert_kind_of Typhoeus::Response, @client.projects.unstar(16)
    end
  end

  def test_project_starrers
    stub_get('/projects/16/starrers', 'starred_users')
    starrers = @client.projects.starrers(16)

    assert_kind_of Array, starrers
    assert_kind_of LabClient::User, starrers.first

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of Array, project.starrers
  end

  def test_project_starrers_search
    stub_get('/projects/16/starrers?search=root', 'starred_users')
    starrers = @client.projects.starrers(16, 'root')

    assert_kind_of Array, starrers
    assert_kind_of LabClient::User, starrers.first
  end

  def test_projects_languages
    stub_get('/projects/16/languages', 'languages')
    lang = @client.projects.languages(16)

    assert_kind_of LabClient::LabStruct, lang

    # Through Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.languages
  end

  def test_project_archive
    stub_post('/projects/16/archive', 'project')
    project = @client.projects.archive(16)

    assert_kind_of LabClient::Project, project

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::Project, project.archive
  end

  def test_project_unarchive
    stub_post('/projects/16/unarchive', 'project')
    project = @client.projects.unarchive(16)

    assert_kind_of LabClient::Project, project

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::Project, project.unarchive
  end

  def test_project_delete
    stub_delete('/projects/16', 'delete_schedule')
    result = @client.projects.delete(16)

    assert_kind_of LabClient::LabStruct, result

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.delete
  end

  def test_project_restore
    stub_post('/projects/16/restore', 'project')
    project = @client.projects.restore(16)

    assert_kind_of LabClient::Project, project

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::Project, project.restore
  end

  def test_project_upload
    stub_request(:any, 'https://labclient/api/v4/projects/16/uploads')
      .to_return(
        status: 200,
        body: File.new("#{File.dirname(__FILE__)}/fixtures/project_upload.json"),
        headers: { 'content-type' => ['application/json'] }
      )

    upload = @client.projects.upload(16, 'test/fixtures/upload.txt')
    assert_kind_of LabClient::LabStruct, upload

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.upload('test/fixtures/upload.txt')
  end

  def test_project_share
    stub_post('/projects/16/share', 'project_share')
    share = @client.projects.share(16, {})

    assert_kind_of LabClient::LabStruct, share

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.share({})
  end

  def test_project_unshare
    stub_delete('/projects/16/share/30')

    resp = @client.projects.unshare(16, 30)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_nil project.unshare(30).data
  end

  def test_project_hooks
    stub_get('/projects/16/hooks', 'hooks')
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    hooks = project.hooks

    assert_kind_of LabClient::PaginatedResponse, hooks
    assert_equal hooks.size, 2
    assert_kind_of LabClient::ProjectHook, hooks.first
  end

  def test_project_fork_existing
    stub_post('/projects/16/fork/20', 'project')
    stub_get('/projects/16', 'project')

    fork_project = @client.projects.fork_existing(16, 20)
    assert_kind_of LabClient::Project, fork_project

    project = @client.projects.show(16)
    assert_kind_of LabClient::Project, project.fork_existing(20)
  end

  def test_project_fork_remove
    stub_delete('/projects/16/fork')
    stub_get('/projects/16', 'project')

    resp = @client.projects.fork_remove(16)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    project = @client.projects.show(16)
    assert_nil project.fork_remove.data
  end

  def test_project_search_within_issues
    stub_get('/projects/16/search?scope=issues&search=params', 'issues')

    list = @client.projects.search_within(16, :issues, 'params')
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::Issue, list.first
  end

  def test_project_search_within_merge_requests
    stub_get('/projects/16/search?scope=merge_requests&search=params', 'merge_requests')

    list = @client.projects.search_within(16, :merge_requests, 'params')
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::MergeRequest, list.first
  end

  def test_project_search_within_commits
    stub_get('/projects/16/search?scope=commits&search=params', 'commits')

    list = @client.projects.search_within(16, :commits, 'params')
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::Commit, list.first
  end

  def test_project_search_within_users
    stub_get('/projects/16/search?scope=users&search=params', 'users')

    list = @client.projects.search_within(16, :users, 'params')
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::User, list.first
  end

  def test_project_search_within_notes
    stub_get('/projects/16/search?scope=notes&search=params', 'issue_notes')

    list = @client.projects.search_within(16, :notes, 'params')
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::Note, list.first

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    list = project.search(:notes, 'params')
    assert_kind_of LabClient::PaginatedResponse, list
  end

  def test_projects_search
    stub_get('/search?scope=projects&search=params', 'projects')

    list = @client.projects.search('params')
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 3
    assert_kind_of LabClient::Project, list.first
  end

  def housekeeping_stub
    stub_request(:post, 'https://labclient/api/v4/projects/16/housekeeping')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
          'Private-Token' => 'token'
        }
      )
      .to_return(status: 200, body: '123412341234', headers: {})
  end

  def test_project_housekeeping
    housekeeping_stub

    job = @client.projects.housekeeping(16)
    assert_kind_of Typhoeus::Response, job
    assert_equal 200, job.code

    assert_kind_of Integer, job.data.to_i

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of Integer, project.housekeeping.data.to_i
  end

  def test_project_transfer
    stub_put('/projects/16/transfer', 'project')
    project = @client.projects.transfer(16, 25)

    assert_kind_of LabClient::Project, project

    # Individual
    assert_kind_of LabClient::Project, project.transfer(25)
  end

  def stub_mirror_pull
    stub_request(:post, 'https://labclient/api/v4/projects/16/mirror/pull')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
          'Private-Token' => 'token'
        }
      )
      .to_return(status: 200, body: '200', headers: {})
  end

  def test_project_mirror_pull
    stub_mirror_pull
    resp = @client.projects.mirror_start(16)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_equal 200, project.mirror_start.code
  end

  def test_project_snapshot
    stub_get('/projects/16/snapshot', 'project')

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)

    File.stub(:write, 1000) do
      assert_equal 1000, @client.projects.snapshot(16, 'output.tar')
      assert_equal 1000, project.snapshot('output.tar')
    end
  end

  def test_project_parent_group
    stub_get('/projects/16', 'project')
    stub_get('/groups/9', 'group')

    project = @client.projects.show(16)
    assert_kind_of LabClient::Group, project.parent
  end

  def test_project_parent_user
    stub_get('/projects/16', 'project_user')
    stub_get('/users/9', 'user')

    project = @client.projects.show(16)
    assert_kind_of LabClient::User, project.parent
  end
end
