require File.expand_path 'test_helper.rb', __dir__

class DebugTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token', debug: true)

    @client.settings[:retry] = { max: 2, delay_factor: 2 }
  end

  def test_with_debug_on
    stub_get('/users/5', nil, 500)

    suppress_output do
      resp = @client.users.show(5)
      assert_equal resp.code, 500
    end
  end
end
