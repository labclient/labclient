require File.expand_path 'test_helper.rb', __dir__

class FeatureFlagTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_feature_flag
    stub_get('/features', 'feature_flags')
    feature = @client.feature_flags.list.first
    assert_kind_of String, feature.inspect

    # Helper is console ouput
    suppress_output do
      assert_nil feature.help
    end
  end

  def test_feature_flag_list
    stub_get('/features', 'feature_flags')

    feature_flags = @client.feature_flags.list

    assert_kind_of LabClient::PaginatedResponse, feature_flags
    assert_equal feature_flags.size, 2
    assert_kind_of LabClient::FeatureFlag, feature_flags.first

    # Off to On
    stub_post('/features/test_feature', 'feature_flag')
    assert_kind_of LabClient::FeatureFlag, feature_flags.last.toggle
  end

  def test_create_feature_flag
    stub_post('/features/create_eks_clusters', 'feature_flag')

    feature_flag = @client.feature_flags.create(:create_eks_clusters, { value: true })
    assert_kind_of LabClient::FeatureFlag, feature_flag
    assert_kind_of String, feature_flag.inspect
    assert_kind_of LabClient::FeatureFlag, feature_flag.toggle
  end

  def test_delete_feature_flag
    stub_get('/features', 'feature_flags')
    stub_delete('/features/create_eks_clusters')

    resp = @client.feature_flags.delete(:create_eks_clusters)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Feature Flag
    feature_flag = @client.feature_flags.list.first
    assert_nil feature_flag.delete.data
  end
end
