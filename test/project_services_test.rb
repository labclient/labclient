require(File.expand_path('test_helper.rb', __dir__))

class ProjectServicesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_project_service
    stub_get('/projects/16/services', 'project_services')
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    service = project.services.first

    assert_kind_of(String, service.inspect)
    assert_kind_of(LabClient::Project, service.project)
  end

  def test_project_services_show
    stub_get('/projects/16/services/asana', 'project_service')
    stub_get('/projects/16', 'project')

    service = @client.projects.services.show(16, :asana)
    assert_kind_of(LabClient::ProjectService, service)

    project = @client.projects.show(16)
    assert_kind_of(LabClient::ProjectService, project.service_show(:asana))
  end

  def test_project_services_update
    stub_put('/projects/16/services/asana', 'project_service')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/services/asana', 'project_service')

    # Root
    service = @client.projects.services.update(16, :asana, {})
    assert_kind_of(LabClient::ProjectService, service)

    # Project
    project = @client.projects.show(16)
    assert_kind_of(LabClient::ProjectService, project.service_update(:asana, {}))

    # Service
    service = @client.projects.services.show(16, :asana)
    assert_kind_of(LabClient::ProjectService, service.update({}))
  end

  def test_project_services_delete
    stub_delete('/projects/16/services/asana')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/services/asana', 'project_service')

    # Root
    resp = @client.projects.services.delete(16, :asana)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Project
    project = @client.projects.show(16)
    assert_nil project.service_delete(:asana).data

    # Service
    service = @client.projects.services.show(16, :asana)
    assert_nil service.delete.data
  end
end
