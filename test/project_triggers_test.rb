require File.expand_path 'test_helper.rb', __dir__

class ProjectTriggersTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::ProjectTrigger
  end

  def test_project_trigger
    stub_get('/projects/16/triggers', 'project_triggers')
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    trigger = project.triggers.first
    assert_kind_of String, trigger.inspect
    assert_kind_of LabClient::Project, trigger.project
  end

  def test_project_triggers_show
    stub_get('/projects/16/triggers/10', 'project_trigger')
    stub_get('/projects/16', 'project')

    trigger = @client.projects.triggers.show(16, 10)
    assert_kind_of kind, trigger

    project = @client.projects.show(16)
    assert_kind_of kind, project.trigger_show(10)
  end

  def test_project_triggers_create
    stub_post('/projects/16/triggers', 'project_trigger')
    stub_get('/projects/16', 'project')

    trigger = @client.projects.triggers.create(16, {})
    assert_kind_of kind, trigger

    project = @client.projects.show(16)
    assert_kind_of kind, project.trigger_create({})
  end

  def test_project_triggers_delete
    stub_delete('/projects/16/triggers/10')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/triggers/10', 'project_trigger')

    # Root
    resp = @client.projects.triggers.delete(16, 10)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Project
    project = @client.projects.show(16)
    assert_nil project.trigger_delete(10).data

    # Variable
    trigger = @client.projects.triggers.show(16, 10)
    assert_nil trigger.delete.data
  end
end
