require File.expand_path 'test_helper.rb', __dir__

class ProjectTemplatesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_project_templates_show
    stub_get('/projects/16/templates/issues/elastic', 'project_template')
    stub_get('/projects/16', 'project')

    template = @client.projects.templates.show(16, :issues, :elastic)
    assert_kind_of LabClient::LabStruct, template

    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.template_show(:issues, :elastic)
  end

  def test_project_templates_list
    stub_get('/projects/16/templates/issues', 'project_templates')
    stub_get('/projects/16', 'project')

    templates = @client.projects.templates.list(16, :issues)
    assert_kind_of LabClient::PaginatedResponse, templates
    assert_kind_of LabClient::Klass, templates.first
    assert_kind_of String, templates.first.name

    project = @client.projects.show(16)
    assert_kind_of LabClient::PaginatedResponse, project.templates(:issues)
  end
end
