require File.expand_path 'test_helper.rb', __dir__

class GroupBadgesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_group_badge
    stub_get('/groups/5/badges', 'group_badges')
    stub_get('/groups/5', 'group')
    group = @client.groups.show(5)
    badge = group.badges.first

    assert_kind_of String, badge.inspect
    assert_kind_of LabClient::Group, badge.group
  end

  def test_group_badges_show
    stub_get('/groups/5/badges/2', 'group_badge')
    stub_get('/groups/5', 'group')

    badge = @client.groups.badges.show(5, 2)
    assert_kind_of LabClient::GroupBadge, badge

    group = @client.groups.show(5)
    assert_kind_of LabClient::GroupBadge, group.badge_show(2)
  end

  def test_group_badges_create
    stub_post('/groups/5/badges', 'group_badge')
    stub_get('/groups/5', 'group')

    badge = @client.groups.badges.create(5, url: 'value')
    assert_kind_of LabClient::GroupBadge, badge

    group = @client.groups.show(5)
    assert_kind_of LabClient::GroupBadge, group.badge_create(url: 'value')
  end

  def test_group_badges_update
    stub_put('/groups/5/badges/2', 'group_badge')
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/badges/2', 'group_badge')

    # Root
    badge = @client.groups.badges.update(5, 2, url: 'value')
    assert_kind_of LabClient::GroupBadge, badge

    # Group
    group = @client.groups.show(5)
    assert_kind_of LabClient::GroupBadge, group.badge_update(2, url: 'value')

    # Badge
    badge = @client.groups.badges.show(5, 2)
    assert_kind_of LabClient::GroupBadge, badge.update(url: 'value')
  end

  def test_group_badges_delete
    stub_delete('/groups/5/badges/2')
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/badges/2', 'group_badge')

    # Root
    resp = @client.groups.badges.delete(5, 2)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Group
    group = @client.groups.show(5)
    assert_nil group.badge_delete(2).data

    # Badge
    badge = @client.groups.badges.show(5, 2)
    assert_nil badge.delete.data
  end

  def test_group_badges_preview
    stub_get('/groups/5/badges/2', 'group_badge')
    stub_get('/groups/5/badges/render', 'group_badge')

    # Nasty but based on fixture
    stub_get('/groups/5/badges/render?image_url=https://labclient/%25%7Bproject_path%7D/badges/%25%7Bdefault_branch%7D/pipeline.svg&link_url=https://labclient/%25%7Bproject_path%7D', 'group_badge')

    # Root
    assert_kind_of LabClient::LabStruct, @client.groups.badges.preview(5, {})

    # Badge
    badge = @client.groups.badges.show(5, 2)
    assert_kind_of LabClient::LabStruct, badge.preview
  end
end
