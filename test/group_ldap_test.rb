require File.expand_path 'test_helper.rb', __dir__

class GroupLdapTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_ldap_list
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/ldap_group_links', 'group_links')

    # Top
    list = @client.groups.ldap.list(5)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::GroupLink, list.first

    # Via Group
    group = @client.groups.show(5)
    list = group.ldap_list

    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::GroupLink, list.first
  end

  def test_ldap_link_test
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/ldap_group_links', 'group_links')

    link = @client.groups.ldap.list(5).first
    assert_kind_of String, link.inspect
    assert_kind_of Symbol, link.access_level
  end

  def test_ldap_sync
    stub_get('/groups/5', 'group')
    stub_post('/groups/5/ldap_sync')

    # Top
    resp = @client.groups.ldap.sync(5)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Group
    group = @client.groups.show(5)
    assert_nil group.ldap_sync.data
  end

  def test_ldap_create
    stub_get('/groups/5', 'group')
    stub_post('/groups/5/ldap_group_links', 'group_link')

    # Top
    assert_kind_of LabClient::GroupLink, @client.groups.ldap.create(5, {})

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of LabClient::GroupLink, group.ldap_create({})
  end

  def test_ldap_delete
    stub_get('/groups/5', 'group')
    stub_delete('/groups/5/ldap_group_links/name')
    stub_delete('/groups/5/ldap_group_links/provider/name')

    # Top
    resp = @client.groups.ldap.delete(5, :name)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Group w/ Provider
    group = @client.groups.show(5)
    assert_nil group.ldap_delete(:name, :provider).data
  end
end
