require File.expand_path 'test_helper.rb', __dir__

class EpicsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_epic_list
    stub_get('/groups/5/epics', 'epics')
    stub_get('/groups/5', 'group')

    list = @client.epics.list(5)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2

    epic = list.first
    assert_kind_of String, epic.inspect
    assert_kind_of LabClient::Epic, epic

    # Via Group
    group = epic.group
    assert_kind_of LabClient::PaginatedResponse, group.epics
  end

  def test_epic_epics_show
    stub_get('/groups/5/epics/1', 'epic')
    stub_get('/groups/5', 'group')

    epic = @client.epics.show(5, 1)
    assert_kind_of LabClient::Epic, epic

    group = @client.groups.show(5)
    assert_kind_of LabClient::Epic, group.epic_show(1)
  end

  def test_epic_epics_create
    stub_post('/groups/5/epics', 'epic')
    stub_get('/groups/5', 'group')

    epic = @client.epics.create(5, title: 'value')
    assert_kind_of LabClient::Epic, epic

    group = @client.groups.show(5)
    assert_kind_of LabClient::Epic, group.epic_create(title: 'value')
  end

  def test_epic_epics_update
    stub_put('/groups/5/epics/1', 'epic')
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/epics/1', 'epic')

    # Root
    epic = @client.epics.update(5, 1, title: 'value')
    assert_kind_of LabClient::Epic, epic

    # Group
    group = @client.groups.show(5)
    assert_kind_of LabClient::Epic, group.epic_update(1, title: 'value')

    # Epic
    epic = @client.epics.show(5, 1)
    assert_kind_of LabClient::Epic, epic.update(title: 'value')
  end

  def test_epic_epics_delete
    stub_delete('/groups/5/epics/1')
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/epics/1', 'epic')

    # Root
    resp = @client.epics.delete(5, 1)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Group
    group = @client.groups.show(5)
    assert_nil group.epic_delete(1).data

    # Hook
    epic = @client.epics.show(5, 1)
    assert_nil epic.delete.data
  end

  def test_epic_todo
    stub_post('/groups/5/epics/1/todo', 'epic')

    todo = @client.epics.todo(5, 1)
    assert_kind_of LabClient::Todo, todo

    # Individual Epic
    stub_get('/groups/5/epics/1', 'epic')
    epic = @client.epics.show(5, 1)
    assert_kind_of LabClient::Todo, epic.todo
  end

  def test_todo_repeats
    stub_post('/groups/5/epics/1/todo', 'subscribe_repeat', 304)

    suppress_output do
      assert_kind_of Typhoeus::Response, @client.epics.todo(5, 1)
    end
  end

  def test_epic_issues
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/epics/1', 'epic')
    stub_get('/groups/5/epics/1/issues', 'issues')

    list = @client.epics.issues(5, 1)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::Issue, list.first

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of LabClient::Issue, group.epic_issues(1).first

    # Via Epic
    epic = @client.epics.show(5, 1)
    assert_kind_of LabClient::Issue, epic.issues.first
  end

  def test_epic_issue_add
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/epics/1', 'epic')
    stub_post('/groups/5/epics/1/issues/64', 'epic_issue')

    assert_kind_of LabClient::LabStruct, @client.epics.issue_add(5, 1, 64)

    # Via Group
    group = @client.groups.show(5)

    assert_kind_of LabClient::LabStruct, group.epic_issue_add(1, 64)

    # Via Epic
    epic = @client.epics.show(5, 1)
    assert_kind_of LabClient::LabStruct, epic.issue_add(64)
  end

  def test_epic_issue_remove
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/epics/1', 'epic')
    stub_delete('/groups/5/epics/1/issues/64', 'epic_issue')

    assert_kind_of LabClient::LabStruct, @client.epics.issue_remove(5, 1, 64)

    # Via Group
    group = @client.groups.show(5)

    assert_kind_of LabClient::LabStruct, group.epic_issue_remove(1, 64)

    # Via Epic
    epic = @client.epics.show(5, 1)
    assert_kind_of LabClient::LabStruct, epic.issue_remove(64)
  end

  def test_epic_issue_update
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/epics/1', 'epic')
    stub_put('/groups/5/epics/1/issues/64', 'epic_issues')

    assert_kind_of LabClient::PaginatedResponse, @client.epics.issue_update(5, 1, 64, {})

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of LabClient::PaginatedResponse, group.epic_issue_update(1, 64, {})

    # Via Epic
    epic = @client.epics.show(5, 1)
    assert_kind_of LabClient::PaginatedResponse, epic.issue_update(64, {})
  end
end
