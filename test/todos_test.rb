require File.expand_path 'test_helper.rb', __dir__

class TodosTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_snippet
    stub_get('/todos', 'todos')
    todo = @client.todos.list.first

    assert_kind_of LabClient::Todo, todo
    assert_kind_of String, todo.inspect

    %i[created_at].each do |kind|
      assert_kind_of DateTime, todo.send(kind)
    end

    # Helper is console ouput
    suppress_output do
      assert_nil todo.help
    end
  end

  def test_todos_list
    stub_get('/todos', 'todos')
    todos = @client.todos.list

    assert_kind_of LabClient::PaginatedResponse, todos
    assert_equal todos.size, 2
    assert_kind_of LabClient::Todo, todos.first
  end

  def test_mark_as_done
    stub_get('/todos', 'todos')

    todo = @client.todos.list.first
    stub_post("/todos/#{todo.id}/mark_as_done", 'todo')

    assert_kind_of LabClient::Todo, @client.todos.mark_as_done(todo.id)
    assert_kind_of LabClient::Todo, todo.mark_as_done
  end

  def test_mark_all_done
    stub_post('/todos/mark_as_done')

    resp = @client.todos.mark_all_done
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end
end
