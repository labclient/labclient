require File.expand_path 'test_helper.rb', __dir__

class LabStructTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_keys
    assert_kind_of Array, LabClient::LabStruct.new(hello: :there).keys
  end

  def test_inspect
    assert_kind_of String, LabClient::LabStruct.new(hello: :there).inspect
  end

  def test_as_json
    assert_kind_of String, LabClient::LabStruct.new(hello: :there).to_json
  end

  def test_slice
    struct = LabClient::LabStruct.new(hello: :there, mr: :kenobi, bold: :one)
    assert_equal %i[hello mr], struct.slice(:hello, :mr).keys
  end

  def test_key?
    struct = LabClient::LabStruct.new(hello: :there, mr: :kenobi, bold: :one)
    assert struct.key?(:hello)
    refute struct.key?(:didntthinkso)
  end

  def test_client
    # Fake Response
    resp = Typhoeus::Response.new(
      body: { test: :test }.to_json,
      headers: { 'Content-Type' => 'application/json' },
      code: 200,
      return_code: :ok
    )

    # Save Client
    resp.instance_variable_set(:@client, @client)

    # Get LabStruct
    struct = resp.data

    # Validate
    assert_kind_of LabClient::Client, struct.client
    assert struct.success?
  end
end
