require File.expand_path 'test_helper.rb', __dir__

class AvatarMessagesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_avatar
    details = { email: 'jeanlucpicard@labclient', size: 512 }

    stub_get('/avatar?email=jeanlucpicard@labclient&size=512', 'avatar')
    avatar = @client.avatar.show(details)

    assert_kind_of LabClient::Avatar, avatar
    assert_kind_of String, avatar.inspect
    assert_kind_of String, avatar.url
  end
end
