require 'pry'

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

require 'simplecov'
SimpleCov.minimum_coverage 100
SimpleCov.start

ENV['LABCLIENT_TESTING'] = 'true'

require 'minitest/reporters'
require 'minitest/autorun'
require 'rack/test'
require 'webmock/minitest'

Minitest::Reporters.use! [Minitest::Reporters::SpecReporter.new(color: true), Minitest::Reporters::JUnitReporter.new]

require File.expand_path("#{File.dirname(__FILE__)}/../lib/labclient")

def load_fixture(name = nil)
  File.new(File.dirname(__FILE__) + "/fixtures/#{name}.json") if name
end

# https://gist.github.com/moertel/11091573
def suppress_output
  original_stderr = $stderr.clone
  original_stdout = $stdout.clone
  $stderr.reopen(File.new('/dev/null', 'w'))
  $stdout.reopen(File.new('/dev/null', 'w'))
  yield
ensure
  $stdout.reopen(original_stdout)
  $stderr.reopen(original_stderr)
end

# Stubing placeholders
headers = {
  'Private-Token' => 'token', 'Content-Type' => 'application/json', 'Accept' => 'application/json'
}

url = 'https://labclient/api/v4'
response_headers = {
  'content-type' => ['application/json']
}

%i[get post put patch delete].each do |method|
  define_method "stub_#{method}" do |path, fixture = nil, status_code = 200, body = {}|
    stub_request(method, "#{url}#{path}")
      .with(headers: headers, body: body)
      .to_return(
        body: load_fixture(fixture),
        status: status_code,
        headers: response_headers
      )
  end
end
