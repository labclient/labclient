require File.expand_path 'test_helper.rb', __dir__
class ProjectMirrorTests < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/projects/16', 'project')
  end

  def kind
    LabClient::ProjectMirror
  end

  def test_mirrors_list
    stub_get('/projects/16/remote_mirrors', 'remote_mirrors')

    assert_kind_of LabClient::PaginatedResponse, @client.projects.mirrors.list(16)

    # Via Project
    project = @client.projects.show(16)
    mirror = project.mirrors.first
    assert_kind_of kind, mirror

    # Mirror Methods
    assert_kind_of String, mirror.inspect
    assert_kind_of LabClient::Project, mirror.project
  end

  def test_mirror_create
    stub_post('/projects/16/remote_mirrors', 'remote_mirror')

    assert_kind_of kind, @client.projects.mirrors.create(16, {})

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.mirror_create({})
  end

  def test_mirrors_update
    stub_get('/projects/16/remote_mirrors', 'remote_mirrors')
    stub_put('/projects/16/remote_mirrors/2', 'remote_mirror')

    assert_kind_of kind, @client.projects.mirrors.update(16, 2, {})

    # Via Project
    project = @client.projects.show(16)
    mirror = project.mirror_update(2, {})
    assert_kind_of kind, mirror

    # Via Mirror
    mirror = @client.projects.mirrors.list(16).first
    mirror.update({})
  end
end
