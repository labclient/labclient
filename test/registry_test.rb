require File.expand_path 'test_helper.rb', __dir__

class ProjectRegistryTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind_repository
    LabClient::RegistryRepository
  end

  def kind_tag
    LabClient::RegistryTag
  end

  def test_registry_list
    stub_get('/projects/16/registry/repositories', 'project_repositories')
    stub_get('/projects/16', 'project')

    list = @client.registry.list(16)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size

    # Repository Methods
    repo = list.first
    assert_kind_of String, repo.inspect
    assert_kind_of kind_repository, repo
    assert_kind_of LabClient::Project, repo.project

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind_repository, project.registry_repositories.first
  end

  def test_registry_list_group
    stub_get('/groups/5/registry/repositories', 'group_repositories')
    stub_get('/groups/5', 'group')

    list = @client.registry.group(5)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size

    # Via Project
    group = @client.groups.show(5)
    assert_kind_of kind_repository, group.registry_repositories.first
  end

  def test_registry_repo_delete
    stub_delete('/projects/16/registry/repositories/1')
    stub_get('/projects/16/registry/repositories', 'project_repositories')
    stub_get('/projects/16', 'project')

    resp = @client.registry.delete(16, 1)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Registry Repository
    repo = @client.registry.list(16).first
    assert_nil repo.delete.data
  end

  def test_registry_tags_list
    stub_get('/projects/16/registry/repositories/1/tags', 'project_repositories_tags')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/registry/repositories', 'project_repositories')

    list = @client.registry.tags(16, 1)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size

    # Tag Methods
    tag = list.first
    assert_kind_of String, tag.inspect

    # Via Registry Repository
    repo = @client.registry.list(16).first
    assert_kind_of kind_tag, repo.tags.first
  end

  def test_registry_tag_details
    stub_get('/projects/16/registry/repositories/1/tags/name', 'project_repositories_tag')

    tag = @client.registry.details(16, 1, :name)
    assert_kind_of kind_tag, tag
  end

  def test_registry_tag_delete
    stub_get('/projects/16/registry/repositories/1/tags/name', 'project_repositories_tag')
    stub_delete('/projects/16/registry/repositories/1/tags/name')

    resp = @client.registry.delete_tag(16, 1, :name)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Registry Tag
    tag = @client.registry.details(16, 1, :name)
    assert_nil tag.delete.data
  end

  def test_registry_tag_delete_bulk
    stub_delete('/projects/16/registry/repositories/1/tags')

    resp = @client.registry.bulk(16, 1, name_regex_delete: '*')
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end
end
