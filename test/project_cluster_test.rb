require File.expand_path 'test_helper.rb', __dir__

class ProjectClustersTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::ProjectCluster
  end

  def test_cluster_show
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/clusters/18', 'project_cluster')

    cluster = @client.projects.clusters.show(16, 18)

    assert_kind_of kind, cluster
    assert_kind_of String, cluster.inspect
    assert_kind_of LabClient::Project, cluster.project

    # via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.cluster(18)
  end

  def test_cluster_list
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/clusters', 'project_clusters')

    clusters = @client.projects.clusters.list(16)

    assert_kind_of LabClient::PaginatedResponse, clusters
    assert_equal clusters.size, 2
    assert_kind_of kind, clusters.first

    # via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.clusters.first
  end

  def test_add_cluster
    stub_post('/projects/16/clusters/user', 'project_cluster')

    cluster = @client.projects.clusters.add(16, {})

    assert_kind_of kind, cluster
    assert_kind_of String, cluster.inspect
  end

  def test_update_cluster
    stub_get('/projects/16/clusters/18', 'project_cluster')
    stub_put('/projects/16/clusters/18', 'project_cluster')

    assert_kind_of kind, @client.projects.clusters.update(16, 18, {})

    cluster = @client.projects.clusters.show(16, 18)
    cluster.update({})
    assert_kind_of kind, cluster
  end

  def test_delete_cluster
    stub_get('/projects/16/clusters/18', 'project_cluster')
    stub_delete('/projects/16/clusters/18')

    resp = @client.projects.clusters.delete(16, 18)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # via Project Cluster
    cluster = @client.projects.clusters.show(16, 18)
    assert_nil cluster.delete.data
  end
end
