require File.expand_path 'test_helper.rb', __dir__

class CurlTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_curl_pagination
    stub_get('/users', 'users')

    users = @client.users.list

    suppress_output do
      assert_nil users.curl
    end
  end

  def test_curl_klass
    stub_get('/users/5', 'user')

    user = @client.users.show(5)

    suppress_output do
      assert_nil user.curl
    end
  end
end
