require File.expand_path 'test_helper.rb', __dir__

class ProtectedTagsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/projects/16', 'project')
  end

  def kind
    LabClient::Tag
  end

  def test_tag_list
    stub_get('/projects/16/protected_tags', 'tags')

    list = @client.protected_tags.list(16)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of kind, list.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.protected_tags.first
  end

  def test_tag_show
    stub_get('/projects/16/protected_tags/master', 'tag')

    tag = @client.protected_tags.show(16, :master)
    assert_kind_of kind, tag
    assert_kind_of String, tag.inspect
    assert_kind_of LabClient::Commit, tag.commit

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.protected_tag(:master)
  end

  def test_tag_protect
    stub_post('/projects/16/protected_tags', 'protected_tag')

    tag = @client.protected_tags.protect(16, {})
    assert_kind_of kind, tag

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.protect_tag({})
  end

  def test_tag_unprotect
    stub_delete('/projects/16/protected_tags/other')

    resp = @client.protected_tags.unprotect(16, :other)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Project
    project = @client.projects.show(16)
    assert_nil project.unprotect_tag(:other).data
  end
end
