require File.expand_path 'test_helper.rb', __dir__

class RepositoryTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/projects/16', 'project')
  end

  def test_tree
    stub_get('/projects/16/repository/tree', 'repo_tree')
    tree = @client.repo.tree(16)

    assert_kind_of LabClient::PaginatedResponse, tree
    assert_kind_of LabClient::LabStruct, tree.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::PaginatedResponse, project.tree
  end

  def test_blob
    stub_get('/projects/16/repository/blobs/abc', 'blob_sha')
    blob = @client.repo.blob(16, :abc)

    assert_kind_of LabClient::LabStruct, blob

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.blob(:abc)
  end

  def test_archive
    stub_get('/projects/16/repository/archive.tar.gz', 'blob_sha')

    # Project
    project = @client.projects.show(16)

    File.stub(:write, 1000) do
      assert_equal 1000, @client.repo.archive(16)
      assert_equal 1000, project.download_archive
    end
  end

  def test_compare
    stub_get('/projects/16/repository/compare', 'repo_compare')
    diff = @client.repo.compare(16, {})

    assert_kind_of LabClient::CommitDiff, diff

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.compare({})
  end

  def test_contributors
    stub_get('/projects/16/repository/contributors', 'contributors')
    list = @client.repo.contributors(16, {})

    assert_kind_of LabClient::PaginatedResponse, list

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::PaginatedResponse, project.contributors({})
  end

  def test_merge_base
    stub_get('/projects/16/repository/merge_base?refs%5B%5D=feature&refs%5B%5D=master', 'merge_base')

    list = @client.repo.merge_base(16, %i[master feature])

    assert_kind_of LabClient::LabStruct, list

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.merge_base(%i[master feature])
  end
end
