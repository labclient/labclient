require File.expand_path 'test_helper.rb', __dir__

class GroupTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_groups_list
    stub_get('/groups', 'groups')
    groups = @client.groups.list

    assert_kind_of LabClient::PaginatedResponse, groups
    assert_equal groups.size, 3
    assert_kind_of LabClient::Group, groups.first
  end

  # Not the best test, place for it. Needed for Common
  def test_groups_help
    suppress_output do
      assert_nil @client.groups.help
    end
  end

  def test_group_subgroups
    stub_get('/groups/5', 'group')
    group = @client.groups.show(5)

    stub_get('/groups/5/subgroups', 'groups')
    groups = group.subgroups

    assert_kind_of LabClient::PaginatedResponse, groups
    assert_equal groups.size, 3
    assert_kind_of LabClient::Group, groups.first

    # Test Reload
    assert_kind_of LabClient::Group, group.reload
  end

  def test_group_projects
    stub_get('/groups/5', 'group')
    group = @client.groups.show(5)

    stub_get('/groups/5/projects', 'projects')

    list = group.projects

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 3
    assert_kind_of LabClient::Project, list.first
  end

  def test_group_details
    stub_get('/groups/5', 'group_details')
    group = @client.groups.show(5)
    group.details

    assert_kind_of Array, group.projects
    assert_kind_of LabClient::Project, group.projects.first
  end

  def test_group_create
    stub_post('/groups', 'group')
    group = @client.groups.create({})

    assert_kind_of LabClient::Group, group
  end

  def test_group_transfer
    stub_get('/groups/5', 'group_details')
    stub_post('/groups/5/projects/10', 'project')

    group = @client.groups.show(5)

    assert_kind_of LabClient::Project, group.transfer(10)
    assert_kind_of LabClient::Project, @client.groups.transfer(5, 10)
  end

  def test_group_update
    stub_get('/groups/5', 'group_details')
    stub_put('/groups/5', 'group_details')
    group = @client.groups.show(5)

    assert_kind_of LabClient::Group, group.update(name: :rawr)
    assert_kind_of LabClient::Group, @client.groups.update(5, name: :rawr)
  end

  def test_group_delete
    stub_get('/groups/5', 'group_details')
    stub_delete('/groups/5', 'delete_group')

    group = @client.groups.show(5)
    assert_kind_of LabClient::LabStruct, group.delete
    assert_kind_of LabClient::LabStruct, @client.groups.delete(5)
  end

  def test_group_restore
    stub_get('/groups/5', 'group_details')
    stub_post('/groups/5/restore', 'group_details')

    group = @client.groups.show(5)
    assert_kind_of LabClient::Group, group.restore
    assert_kind_of LabClient::Group, @client.groups.restore(5)
  end

  def test_groups_search
    stub_get('/groups?search=params', 'groups')

    list = @client.groups.search('params')
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 3
    assert_kind_of LabClient::Group, list.first
  end

  def test_groups_audit_events
    stub_get('/groups/5', 'group_details')
    stub_get('/groups/5/audit_events/1', 'audit_event')
    stub_get('/groups/5/audit_events', 'audit_events')

    group = @client.groups.show(5)

    assert_kind_of LabClient::AuditEvent, group.audit_event(1)
    assert_kind_of LabClient::AuditEvent, group.audit_events.first
  end

  # rubocop:disable Metrics/AbcSize
  def test_groups_search_within
    stub_get('/groups/5', 'group_details')

    group = @client.groups.show(5)

    # Projects
    stub_get('/groups/5/search?scope=projects&search=string', 'projects')
    assert_kind_of LabClient::Project, group.search(:projects, :string).first

    # Issues
    stub_get('/groups/5/search?scope=issues&search=string', 'issues')
    assert_kind_of LabClient::Issue, group.search(:issues, :string).first

    # Merge Requests
    stub_get('/groups/5/search?scope=merge_requests&search=string', 'merge_requests')
    assert_kind_of LabClient::MergeRequest, group.search(:merge_requests, :string).first

    # Commits
    stub_get('/groups/5/search?scope=commits&search=string', 'commits')
    assert_kind_of LabClient::Commit, group.search(:commits, :string).first

    # Users
    stub_get('/groups/5/search?scope=users&search=string', 'users')
    assert_kind_of LabClient::User, group.search(:users, :string).first

    # Top Level
    @client.groups.search_within(5, :users, :string)
  end
  # rubocop:enable Metrics/AbcSize
end
