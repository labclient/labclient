require File.expand_path 'test_helper.rb', __dir__

class MemberMethodsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/projects/16', 'project')
  end

  def kind
    LabClient::Member
  end

  def test_members_object
    stub_get('/projects/16/members/40', 'member')
    member = @client.members.projects.show(16, 40)
    assert_kind_of kind, member
    assert_kind_of String, member.inspect

    refute member.greater_than(:developer)
    assert member.greater_than(10)
  end

  def test_members_greater
    stub_get('/projects/16/members/40', 'member')
    member = @client.members.projects.show(16, 40)

    refute member.greater_than(:developer)
    assert member.greater_than(10)
  end

  def test_members_assertions
    stub_get('/projects/16/members/40', 'member')
    member = @client.members.projects.show(16, 40)

    refute member.guest?
    refute member.reporter?
    assert member.developer?
    refute member.maintainer?
    refute member.owner?
  end

  def test_members_user
    stub_get('/projects/16/members/40', 'member')
    stub_get('/users/40', 'user')
    member = @client.members.projects.show(16, 40)
    assert_kind_of LabClient::User, member.user
  end
end
