require File.expand_path 'test_helper.rb', __dir__

class IssueTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_issues
    stub_get('/projects/30/issues/1', 'issue')
    stub_get('/projects/30', 'project')
    issue = @client.issues.show(30, 1)

    assert_kind_of LabClient::Issue, issue
    assert_kind_of String, issue.inspect

    issue.assignees.each do |user|
      assert_kind_of LabClient::User, user
    end

    assert_kind_of LabClient::Project, issue.project

    # Test Reload
    assert_kind_of LabClient::Issue, issue.reload
  end

  def test_issue_list
    stub_get('/issues', 'issues')
    list = @client.issues.list

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::Issue, list.first
  end

  def test_issue_create
    details = { title: 'Fancy!' }
    stub_post('/projects/5/issues', 'issue', 200, details)

    issue = @client.issues.create(5, details)
    assert_kind_of LabClient::Issue, issue
  end

  def test_issue_create_project
    stub_get('/projects/16', 'project')
    details = { title: 'Fancy!' }
    stub_post('/projects/16/issues', 'issue', 200, details)

    project = @client.projects.show(16)
    assert_kind_of LabClient::Project, project

    issue = project.issue_create(details)
    assert_kind_of LabClient::Issue, issue
  end

  def test_issue_update
    details = { title: 'Fancy!' }
    stub_put('/projects/30/issues/1', 'issue', 200, details)

    issue = @client.issues.update(30, 1, details)
    assert_kind_of LabClient::Issue, issue

    # Through Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)
    issue.update(title: 'Fancy!')
  end

  def test_issue_close_reopen
    stub_get('/projects/30/issues/1', 'issue')
    stub_put('/projects/30/issues/1', 'issue', 200, {})

    issue = @client.issues.show(30, 1)
    assert_kind_of LabClient::Issue, issue.close
    assert_kind_of LabClient::Issue, issue.reopen
  end

  def test_issue_delete
    stub_get('/projects/30/issues/1', 'issue')
    stub_delete('/projects/30/issues/1')

    resp = @client.issues.delete(30, 1)

    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    issue = @client.issues.show(30, 1)
    assert_nil issue.delete.data
  end

  def test_issue_move
    details = { to_project_id: 10 }
    stub_post('/projects/30/issues/1/move', 'issue', 200, details)

    issue = @client.issues.move(30, 1, 10)
    assert_kind_of LabClient::Issue, issue

    # Through Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)
    issue.move(10)
  end

  def test_issue_subscribe
    stub_post('/projects/30/issues/1/subscribe', 'issue')
    issue = @client.issues.subscribe(30, 1)

    assert_kind_of LabClient::Issue, issue

    # Individual Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)
    assert_kind_of LabClient::Issue, issue.subscribe
  end

  def test_sub_repeats
    stub_post('/projects/30/issues/1/subscribe', 'subscribe_repeat', 304)
    suppress_output do
      assert_kind_of Typhoeus::Response, @client.issues.subscribe(30, 1)
    end
  end

  def test_issue_unsubscribe
    stub_post('/projects/30/issues/1/unsubscribe', 'issue')
    issue = @client.issues.unsubscribe(30, 1)

    assert_kind_of LabClient::Issue, issue

    # Individual Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)
    assert_kind_of LabClient::Issue, issue.unsubscribe
  end

  def test_unsub_repeats
    stub_post('/projects/30/issues/1/unsubscribe', 'subscribe_repeat', 304)

    suppress_output do
      assert_kind_of Typhoeus::Response, @client.issues.unsubscribe(30, 1)
    end
  end

  def test_issue_todo
    stub_post('/projects/30/issues/1/todo', 'issue')
    todo = @client.issues.todo(30, 1)

    assert_kind_of LabClient::Todo, todo

    # Individual Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)
    assert_kind_of LabClient::Todo, issue.todo
  end

  def test_todo_repeats
    stub_post('/projects/30/issues/1/todo', 'subscribe_repeat', 304)

    suppress_output do
      assert_kind_of Typhoeus::Response, @client.issues.todo(30, 1)
    end
  end

  def test_issue_time_stats
    stub_get('/projects/30/issues/1/time_stats', 'issue_time_stats')
    stats = @client.issues.time_stats(30, 1)

    assert_kind_of LabClient::LabStruct, stats

    # Individual Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)
    assert_kind_of LabClient::LabStruct, issue.time_stats
  end

  def test_issue_add_spent_time
    details = { duration: '1d' }
    stub_post('/projects/30/issues/1/add_spent_time', 'issue_time_stats', 200, details)
    stats = @client.issues.add_spent_time(30, 1, 1.day)

    assert_kind_of LabClient::LabStruct, stats

    # Individual Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)
    assert_kind_of LabClient::LabStruct, issue.add_spent_time('1d')
    assert_kind_of LabClient::LabStruct, issue.add_spent_time(1.day)
  end

  def test_issue_reset_spent_time
    stub_post('/projects/30/issues/1/reset_spent_time', 'issue_time_stats')
    stats = @client.issues.reset_spent_time(30, 1)

    assert_kind_of LabClient::LabStruct, stats

    # Individual Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)
    assert_kind_of LabClient::LabStruct, issue.reset_spent_time
  end

  def test_issue_reset_time_estimate
    stub_post('/projects/30/issues/1/reset_time_estimate', 'issue_time_stats')
    stats = @client.issues.reset_time_estimate(30, 1)

    assert_kind_of LabClient::LabStruct, stats

    # Individual Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)
    assert_kind_of LabClient::LabStruct, issue.reset_time_estimate
  end

  def test_issue_time_estimate
    details = { duration: '1d' }
    stub_post('/projects/30/issues/1/time_estimate', 'issue_time_stats', 200, details)
    stats = @client.issues.time_estimate(30, 1, 1.day)

    assert_kind_of LabClient::LabStruct, stats

    # Individual Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)
    assert_kind_of LabClient::LabStruct, issue.time_estimate('1d')
    assert_kind_of LabClient::LabStruct, issue.time_estimate(1.day)
  end

  def test_issue_participants
    stub_get('/projects/30/issues/1/participants', 'issue_participants')
    list = @client.issues.participants(30, 1)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::User, list.first

    # Individual Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)
    assert_kind_of LabClient::User, issue.participants.first
  end

  def test_issue_notes
    stub_get('/projects/30/issues/1/notes', 'issue_notes')

    # Individual Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)

    assert_kind_of LabClient::PaginatedResponse, issue.notes
    assert_kind_of LabClient::Note, issue.notes.first
  end

  def test_issue_agent_detail
    stub_get('/projects/30/issues/1', 'issue')
    stub_get('/projects/30/issues/1/user_agent_detail', 'user_agent_detail')
    issue = @client.issues.show(30, 1)

    assert_kind_of LabClient::LabStruct, issue.agent_detail
    assert_kind_of LabClient::LabStruct, @client.issues.agent_detail(30, 1)
  end

  def test_issue_related_merge_requests
    stub_get('/projects/30/issues/1/related_merge_requests', 'merge_requests')

    # Individual Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)

    assert_kind_of LabClient::PaginatedResponse, issue.related_merge_requests
    assert_kind_of LabClient::MergeRequest, issue.related_merge_requests.first
  end

  def test_issue_related_closed_by
    stub_get('/projects/30/issues/1/closed_by', 'merge_requests')

    # Individual Issue
    stub_get('/projects/30/issues/1', 'issue')
    issue = @client.issues.show(30, 1)

    assert_kind_of LabClient::PaginatedResponse, issue.closed_by
    assert_kind_of LabClient::MergeRequest, issue.closed_by.first
  end

  def test_issue_project_issues
    stub_get('/projects/16/issues', 'issues')
    stub_get('/projects/16', 'project')

    list = @client.issues.project_issues(16)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::Issue, list.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Issue, project.issues.first
  end

  def test_issue_group_issues
    stub_get('/groups/5/issues', 'issues')
    stub_get('/groups/5', 'group')

    list = @client.issues.group_issues(5)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::Issue, list.first

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of LabClient::Issue, group.issues.first
  end
end
