require File.expand_path 'test_helper.rb', __dir__

class ProjectMergeRequestApprovalsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/projects/16', 'project')
  end

  def kind
    LabClient::ApprovalRule
  end

  def test_project_get
    stub_get('/projects/16/approvals', 'project_approvals')
    approval = @client.approvals.project.show(16)
    assert_kind_of LabClient::LabStruct, approval

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.approvals
  end

  def test_project_update
    stub_post('/projects/16/approvals', 'project_approvals')
    approval = @client.approvals.project.update(16, {})
    assert_kind_of LabClient::LabStruct, approval

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.approvals_update({})
  end

  def test_project_rules
    stub_get('/projects/16/approval_rules', 'approval_rules')
    rules = @client.approvals.project.rules(16)
    assert_kind_of LabClient::PaginatedResponse, rules
    assert_kind_of kind, rules.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.approvals_rules.first
  end

  def test_project_rule_create
    stub_post('/projects/16/approval_rules', 'approval_rule')
    rule = @client.approvals.project.create_rule(16, {})
    assert_kind_of kind, rule
    assert_kind_of String, rule.inspect

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.approvals_rule_create({})
  end

  def test_project_rule
    stub_post('/projects/16/approval_rules', 'approval_rule')
    rule = @client.approvals.project.create_rule(16, {})
    assert_kind_of LabClient::User, rule.eligible_approvers.first
    assert_kind_of LabClient::User, rule.users.first
    assert_kind_of LabClient::Group, rule.groups.first
    assert_kind_of LabClient::Branch, rule.protected_branches.first
  end

  def test_project_rule_update
    stub_put('/projects/16/approval_rules/2', 'approval_rule')
    rule = @client.approvals.project.update_rule(16, 2, {})
    assert_kind_of kind, rule
    assert_kind_of String, rule.inspect

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.approvals_rule_update(2, {})
  end

  def test_project_rule_delete
    stub_delete('/projects/16/approval_rules/2')

    resp = @client.approvals.project.delete_rule(16, 2)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Project
    project = @client.projects.show(16)
    assert_nil project.approvals_rule_delete(2).data
  end
end
