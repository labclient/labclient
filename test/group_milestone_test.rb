require File.expand_path 'test_helper.rb', __dir__

class GroupMilestonesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::GroupMilestone
  end

  def test_milestone
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/milestones/21', 'group_milestone')
    milestone = @client.groups.milestones.show(5, 21)

    assert_kind_of kind, milestone
    assert_kind_of String, milestone.inspect
    assert_kind_of LabClient::Group, milestone.group
  end

  def test_milestone_list
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/milestones', 'group_milestones')

    milestones = @client.groups.milestones.list(5)

    assert_kind_of LabClient::PaginatedResponse, milestones
    assert_equal milestones.size, 2
    assert_kind_of kind, milestones.first

    # via Group
    group = @client.groups.show(5)
    assert_kind_of kind, group.milestones.first
  end

  def test_create_milestone
    stub_post('/groups/5/milestones', 'group_milestone')

    milestone = @client.groups.milestones.create(5, {})

    assert_kind_of kind, milestone
    assert_kind_of String, milestone.inspect
  end

  def test_update_milestone
    stub_get('/groups/5/milestones/21', 'group_milestone')
    stub_put('/groups/5/milestones/21', 'group_milestone')

    assert_kind_of kind, @client.groups.milestones.update(5, 21, {})

    milestone = @client.groups.milestones.show(5, 21)
    milestone.update({})
    assert_kind_of kind, milestone
  end

  def test_milestone_issues
    stub_get('/groups/5/milestones/21', 'group_milestone')
    stub_get('/groups/5/milestones/21/issues', 'issues')

    list = @client.groups.milestones.issues(5, 21)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::Issue, list.first

    # via Milestone
    milestone = @client.groups.milestones.show(5, 21)
    assert_kind_of LabClient::Issue, milestone.issues.first
  end

  def test_milestone_merge_requests
    stub_get('/groups/5/milestones/21', 'group_milestone')
    stub_get('/groups/5/milestones/21/merge_requests', 'merge_requests')

    list = @client.groups.milestones.merge_requests(5, 21)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::MergeRequest, list.first

    # via Milestone
    milestone = @client.groups.milestones.show(5, 21)
    assert_kind_of LabClient::MergeRequest, milestone.merge_requests.first
  end

  def test_delete_milestone
    stub_get('/groups/5/milestones/21', 'group_milestone')
    stub_delete('/groups/5/milestones/21')

    resp = @client.groups.milestones.delete(5, 21)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # via Milestone
    milestone = @client.groups.milestones.show(5, 21)
    assert_nil milestone.delete.data
  end

  def test_burndown_milestone
    stub_get('/groups/5/milestones/21', 'group_milestone')
    stub_get('/groups/5/milestones/21/burndown_events', 'group_milestone_burndown')

    assert_kind_of LabClient::PaginatedResponse, @client.groups.milestones.burndown(5, 21)

    # via Milestone
    milestone = @client.groups.milestones.show(5, 21)
    assert_kind_of LabClient::PaginatedResponse, milestone.burndown
  end
end
