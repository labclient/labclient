require File.expand_path 'test_helper.rb', __dir__

class MrMergeRequestApprovalsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/projects/333/merge_requests/1', 'merge_request')
  end

  def kind
    LabClient::MergeApproval
  end

  def rule_kind
    LabClient::ApprovalRule
  end

  def test_merge_request_get
    stub_get('/projects/333/merge_requests/1/approvals', 'merge_request_approval')
    approval = @client.approvals.merge_request.show(333, 1)
    assert_kind_of kind, approval

    assert_kind_of LabClient::User, approval.approved_by.first
    assert_kind_of String, approval.inspect

    # Via Merge Request
    merge_request = @client.merge_requests.show(333, 1)
    assert_kind_of kind, merge_request.approvals
  end

  def test_merge_request_update
    stub_post('/projects/333/merge_requests/1/approvals', 'merge_request_approval')
    approval = @client.approvals.merge_request.update(333, 1, 5)
    assert_kind_of kind, approval

    # Via Merge Request
    merge_request = @client.merge_requests.show(333, 1)
    assert_kind_of kind, merge_request.approvals_update(5)
  end

  def test_merge_request_rules
    stub_get('/projects/333/merge_requests/1/approval_rules', 'approval_rules')

    rules = @client.approvals.merge_request.rules(333, 1)
    assert_kind_of LabClient::PaginatedResponse, rules
    assert_kind_of rule_kind, rules.first

    # Via Merge Request
    merge_request = @client.merge_requests.show(333, 1)
    assert_kind_of rule_kind, merge_request.approvals_rules.first
  end

  def test_merge_request_rule_create
    stub_post('/projects/333/merge_requests/1/approval_rules', 'approval_rule')
    rule = @client.approvals.merge_request.create_rule(333, 1, {})
    assert_kind_of rule_kind, rule

    # Via Merge Request
    merge_request = @client.merge_requests.show(333, 1)
    assert_kind_of rule_kind, merge_request.approvals_rule_create({})
  end

  def test_merge_request_rule_update
    stub_put('/projects/333/merge_requests/1/approval_rules/2', 'approval_rule')
    rule = @client.approvals.merge_request.update_rule(333, 1, 2, {})
    assert_kind_of rule_kind, rule

    # Via Merge Request
    merge_request = @client.merge_requests.show(333, 1)
    assert_kind_of rule_kind, merge_request.approvals_rule_update(2, {})
  end

  def test_merge_request_rule_delete
    stub_delete('/projects/333/merge_requests/1/approval_rules/2')

    resp = @client.approvals.merge_request.delete_rule(333, 1, 2)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Merge Request
    merge_request = @client.merge_requests.show(333, 1)
    assert_nil merge_request.approvals_rule_delete(2).data
  end

  def test_merge_request_rule_approve
    stub_post('/projects/333/merge_requests/1/approve', 'merge_request_approval')

    assert_kind_of kind, @client.approvals.merge_request.approve(333, 1, {})

    # Via Merge Request
    merge_request = @client.merge_requests.show(333, 1)

    # Query is optional and needs to be hash
    assert_kind_of kind, merge_request.approve
  end

  def test_merge_request_rule_unapprove
    stub_post('/projects/333/merge_requests/1/unapprove', 'merge_request_approval')
    assert_kind_of kind, @client.approvals.merge_request.unapprove(333, 1)

    # Via Merge Request
    merge_request = @client.merge_requests.show(333, 1)
    assert_kind_of kind, merge_request.unapprove
  end
end
