require File.expand_path 'test_helper.rb', __dir__

class PaginatedResponseTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token', paginate: false)
    @link = '<https://labclient/api/v4/users?active=false&blocked=false&external=false&order_by=id&page=2&per_page=20&skip_ldap=false&sort=desc&with_custom_attributes=false>; rel="next", <https://labclient/api/v4/users?active=false&blocked=false&external=false&order_by=id&page=1&per_page=20&skip_ldap=false&sort=desc&with_custom_attributes=false>; rel="first", <https://labclient/api/v4/users?active=false&blocked=false&external=false&order_by=id&page=3&per_page=20&skip_ldap=false&sort=desc&with_custom_attributes=false>; rel="last"'

    @headers = { 'Private-Token' => 'token' }
    @url = 'https://labclient/api/v4/users'
    @response_headers = {
      'content-type' => ['application/json'],
      'link' => @link,
      'x-total' => 5,
      'x-total-pages' => 2,
      'x-page' => 1
    }

    setup_stub
  end

  def setup_stub
    stub_request(:get, @url)
      .with(headers: @headers)
      .to_return(
        body: load_fixture('users'),
        status: 200,
        headers: @response_headers
      )

    second_page = '/users?active=false&blocked=false&external=false&order_by=id&page=2&per_page=20&skip_ldap=false&sort=desc&with_custom_attributes=false'
    stub_get(second_page, 'users')
  end

  def test_auto_paginate
    @client.users.list.auto_paginate do |page|
      assert_equal 2, page.length
    end
  end

  def test_each_page
    stub_get('/users', 'users')

    @client.users.list.each_page do |page|
      assert_equal 2, page.length
    end
  end

  def test_paginate_with_limit
    assert_kind_of Array, @client.users.list.paginate_with_limit(1)
    assert_kind_of Array, @client.users.list.paginate_with_limit(15)
  end

  def test_paginated_class
    stub_get('/users', 'users')
    assert_kind_of LabClient::PaginatedResponse, @client.users.list
    assert_kind_of Array, @client.users.list.inspect

    suppress_output do
      assert_nil @client.users.list.help
    end

    assert_raises NoMethodError do
      @client.users.list.rawr
    end
  end

  def test_pages_response
    stub_get('/users', 'users')

    # Response Status
    assert @client.users.list.success?
  end

  def test_pages_helpers
    list = @client.users.list

    assert_equal list.page, 1
    assert_equal list.total_pages, 2
    assert_equal list.total, 5
  end
end
