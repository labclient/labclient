require File.expand_path 'test_helper.rb', __dir__
class PipelineTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token', quiet: true)
  end

  def test_pipeline_list
    stub_get('/projects/16/pipelines', 'pipelines')
    stub_get('/projects/16', 'project')
    list = @client.pipelines.list(16)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::Pipeline, list.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Pipeline, project.pipelines.first
  end

  def test_pipeline_show
    stub_get('/projects/16/pipelines/4', 'pipeline')
    stub_get('/projects/16', 'project')

    pipeline = @client.pipelines.show(16, 4)
    assert_kind_of LabClient::Pipeline, pipeline
    assert_kind_of String, pipeline.inspect

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Pipeline, project.pipeline(4)

    # Test Reload
    assert_kind_of LabClient::Pipeline, pipeline.reload
  end

  def test_pipeline_variables
    stub_get('/projects/16/pipelines/4', 'pipeline')
    stub_get('/projects/16/pipelines/4/variables', 'pipeline_variables')
    stub_get('/projects/16', 'project')

    vars = @client.pipelines.variables(16, 4)
    assert_kind_of LabClient::LabStruct, vars.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::PaginatedResponse, project.pipeline_variables(4)

    # Via Pipeline
    pipeline = @client.pipelines.show(16, 4)
    assert_kind_of LabClient::PaginatedResponse, pipeline.variables
  end

  def test_pipeline_create
    stub_post('/projects/16/pipeline', 'pipeline')
    stub_get('/projects/16', 'project')

    pipeline = @client.pipelines.create(16, {})
    assert_kind_of LabClient::Pipeline, pipeline

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Pipeline, project.pipeline_create({})
  end

  def test_pipeline_retry
    stub_post('/projects/16/pipelines/4/retry', 'pipeline')
    stub_get('/projects/16/pipelines/4', 'pipeline')
    stub_get('/projects/16', 'project')

    pipeline = @client.pipelines.retry(16, 4)
    assert_kind_of LabClient::Pipeline, pipeline

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Pipeline, project.pipeline_retry(4)

    # Via Pipeline
    pipeline = @client.pipelines.show(16, 4)
    assert_kind_of LabClient::Pipeline, pipeline.retry
  end

  def test_pipeline_cancel
    stub_post('/projects/16/pipelines/4/cancel', 'pipeline')
    stub_get('/projects/16/pipelines/4', 'pipeline')
    stub_get('/projects/16', 'project')

    pipeline = @client.pipelines.cancel(16, 4)
    assert_kind_of LabClient::Pipeline, pipeline

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Pipeline, project.pipeline_cancel(4)

    # Via Pipeline
    pipeline = @client.pipelines.show(16, 4)
    assert_kind_of LabClient::Pipeline, pipeline.cancel
  end

  def test_pipeline_delete
    stub_delete('/projects/16/pipelines/4')
    stub_get('/projects/16/pipelines/4', 'pipeline')
    stub_get('/projects/16', 'project')

    resp = @client.pipelines.delete(16, 4)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Project
    project = @client.projects.show(16)
    assert_nil project.pipeline_delete(4).data

    # Via Pipeline
    pipeline = @client.pipelines.show(16, 4)
    assert_nil pipeline.delete.data
  end

  # rubocop:disable Metrics/MethodLength
  def test_merge_request_wait_for_merge_status
    first = { status: 200, body: load_fixture('pipeline_running'), headers: {
      'content-type' => ['application/json']
    } }

    second = { status: 200, body: load_fixture('pipeline_running'), headers: {
      'content-type' => ['application/json']
    } }

    third = { status: 200, body: load_fixture('pipeline'), headers: {
      'content-type' => ['application/json']
    } }

    stub_request(:get, 'https://labclient/api/v4/projects/16/pipelines/4')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
          'Private-Token' => 'token'
        }
      )
      .to_return([first, second, third])

    pipeline = @client.pipelines.show(16, 4)
    assert_nil pipeline.wait_for_status(5, 0.1)
  end
  # rubocop:enable Metrics/MethodLength
end
