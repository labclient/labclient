require File.expand_path 'test_helper.rb', __dir__

class SnippetsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_snippet
    stub_get('/snippets/1', 'snippet')
    snippet = @client.snippets.show(1)

    assert_kind_of LabClient::Snippet, snippet
    assert_kind_of String, snippet.inspect

    %i[updated_at created_at].each do |kind|
      assert_kind_of DateTime, snippet.send(kind)
    end

    # Helper is console ouput
    suppress_output do
      assert_nil snippet.help
    end
  end

  def test_snippet_list
    stub_get('/snippets', 'snippets')
    snippets = @client.snippets.list

    assert_kind_of LabClient::PaginatedResponse, snippets
    assert_equal snippets.size, 2
    assert_kind_of LabClient::Snippet, snippets.first
  end

  def test_snippet_public
    stub_get('/snippets/public', 'snippets')
    snippets = @client.snippets.public

    assert_kind_of LabClient::PaginatedResponse, snippets
    assert_equal snippets.size, 2
    assert_kind_of LabClient::Snippet, snippets.first
  end

  def test_create_snippet
    details = { description: 'Update', title: 'Update' }
    stub_post('/snippets', 'snippet', 200, details)

    snippet = @client.snippets.create(details)

    assert_kind_of LabClient::Snippet, snippet
    assert_kind_of String, snippet.inspect
  end

  def test_raw_snippet
    stub_get('/snippets/1', 'snippet')
    stub_get('/snippets/1/raw', 'string')
    snippet = @client.snippets.show(1)

    assert_kind_of LabClient::Snippet, snippet

    # Snippet Response Status
    resp = snippet.raw
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_kind_of String, resp.data

    assert_kind_of String, @client.snippets.raw(1).data
  end

  def test_agent_detail_snippet
    stub_get('/snippets/1', 'snippet')
    stub_get('/snippets/1/user_agent_detail', 'user_agent_detail')
    snippet = @client.snippets.show(1)

    assert_kind_of LabClient::Snippet, snippet
    assert_kind_of LabClient::LabStruct, snippet.agent_detail
    assert_kind_of LabClient::LabStruct, @client.snippets.agent_detail(1)
  end

  def test_update_snippet
    details = { description: 'Update', title: 'Update' }
    stub_get('/snippets/1', 'snippet')
    stub_put('/snippets/1', 'snippet', 200, details)

    snippet_direct = @client.snippets.update(1, details)

    snippet = @client.snippets.show(1)
    snippet.update(details)

    assert_kind_of LabClient::Snippet, snippet
    assert_kind_of LabClient::Snippet, snippet_direct
  end

  def test_delete_snippet
    stub_get('/snippets/1', 'snippet')
    stub_delete('/snippets/1')

    resp = @client.snippets.delete(1)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    snippet = @client.snippets.show(1)
    assert_nil snippet.delete.data
  end
end
