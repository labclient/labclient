require File.expand_path 'test_helper.rb', __dir__
class UserEmailTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_users_emails_list
    stub_get('/users/5', 'user')
    stub_get('/users/5/emails', 'emails')
    stub_get('/user/emails', 'emails')

    # Current User
    list = @client.users.emails.list
    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::Email, list.first

    # Specific User
    list = @client.users.emails.list(5)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::Email, list.first
    assert_kind_of String, list.first.inspect

    # Via User
    user = @client.users.show(5)
    assert_kind_of LabClient::Email, user.emails.first
  end

  def test_users_email_show
    stub_get('/user/emails/3', 'email')
    assert_kind_of LabClient::Email, @client.users.emails.show(3)
  end

  def test_users_email_create
    stub_get('/users/5', 'user')
    stub_post('/users/5/emails', 'email')
    stub_post('/user/emails', 'email')

    # Top
    assert_kind_of LabClient::Email, @client.users.emails.create({})
    assert_kind_of LabClient::Email, @client.users.emails.create({}, 5)

    # Via User
    user = @client.users.show(5)
    assert_kind_of LabClient::Email, user.email_create({})
  end

  def test_users_email_delete
    stub_get('/users/5', 'user')
    stub_delete('/users/5/emails/3')
    stub_delete('/user/emails/3')

    # Top
    resp = @client.users.emails.delete(3)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    resp = @client.users.emails.delete(3, 5)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via User
    user = @client.users.show(5)
    assert_nil user.email_delete(3).data
  end
end
