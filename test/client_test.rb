require File.expand_path 'test_helper.rb', __dir__

class ClientTest < Minitest::Test
  include Rack::Test::Methods

  def expected_settings
    {
      url: 'https://labclient_test',
      token: 'token_test',
      paginate: true,
      ssl_verify: true,
      quiet: false,
      debug: false,
      token_type: 'Private-Token',
      retry: {
        max: 5, delay_factor: 10, count: 0
      }
    }
  end

  def test_env_variables_expected_defaults
    client = File.stub(:exist?, false) do
      # Parser Requires HTTP/HTTPS://
      ENV['LABCLIENT_URL'] = 'https://labclient_test'
      ENV['LABCLIENT_TOKEN'] = 'token_test'
      LabClient::Client.new
    end

    assert_equal client.settings, expected_settings
  end

  def stub_file
    {
      url: 'https://labclient', token: 'token', ssl_verify: false, paginate: false,

      # Profiles
      prod: {
        url: 'https://labclient-prod',
        token: 'prod_secure',
        ssl_verify: true,
        paginate: false
      },
      dev: {
        url: 'https://labclient-dev',
        token: 'dev_secure',
        ssl_verify: false,
        paginate: true
      }
    }
  end

  def test_home_file_exists
    client = File.stub(:exist?, true) do
      Oj.stub(:load_file, stub_file) do
        LabClient::Client.new
      end
    end

    opts = %i[url token ssl_verify paginate]
    assert_equal client.settings.slice(opts), stub_file.slice(opts)
  end

  def test_explicit_profile
    client = File.stub(:exist?, true) do
      Oj.stub(:load_file, stub_file) do
        LabClient::Client.new(profile: :prod)
      end
    end

    assert_equal client.settings.slice(:url, :token, :ssl_verify, :paginate), stub_file[:prod]
  end

  def test_env_profile
    client = File.stub(:exist?, true) do
      Oj.stub(:load_file, stub_file) do
        ENV['LABCLIENT_PROFILE'] = 'dev'
        LabClient::Client.new
      end
    end

    assert_equal client.settings.slice(:url, :token, :ssl_verify, :paginate), stub_file[:dev]
    ENV['LABCLIENT_PROFILE'] = nil
  end

  # rubocop:disable Metrics/MethodLength
  def test_authorization_token_type
    client = File.stub(:exist?, false) do
      LabClient::Client.new(url: 'https://labclient_test', token: 'token_test', token_type: 'Authorization')
    end

    # Settings Object
    assert_equal client.settings[:token_type], 'Authorization'

    # Headers
    stub_request(:get, 'https://labclient_test/api/v4/user')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Authorization' => 'Bearer token_test'
        }
      )
      .to_return(
        body: load_fixture('user'),
        status: 200,
        headers: { 'content-type' => ['application/json'] }
      )

    assert_kind_of LabClient::User, client.users.current

    headers = client.users.current.response.request.options[:headers]
    assert headers.key?('Authorization')
    assert_equal headers['Authorization'], 'Bearer token_test'
  end

  # rubocop:enable Metrics/MethodLength

  def test_inspect
    ENV['LABCLIENT_URL'] = 'labclient_test'
    ENV['LABCLIENT_TOKEN'] = 'token_test'

    assert_kind_of String, LabClient::Client.new.inspect
  end

  def test_api_methods
    ENV['LABCLIENT_URL'] = 'labclient_test'
    ENV['LABCLIENT_TOKEN'] = 'token_test'
    assert_kind_of Array, LabClient::Client.new.api_methods
  end

  def test_help
    suppress_output do
      ENV['LABCLIENT_URL'] = 'labclient_test'
      ENV['LABCLIENT_TOKEN'] = 'token_test'
      assert_nil LabClient::Client.new.help
      assert_nil LabClient::Client.new.help :filter
    end
  end

  def test_missing
    ENV['LABCLIENT_URL'] = 'labclient_test'
    ENV['LABCLIENT_TOKEN'] = 'token_test'
    client = LabClient::Client.new
    suppress_output do
      assert_kind_of LabClient::Users, client.users
    end
  end

  def test_prompts
    ENV['LABCLIENT_URL'] = nil
    ENV['LABCLIENT_TOKEN'] = nil

    client = suppress_output do
      # Ensure No Home File Exists
      File.stub(:exist?, false) do
        LabClient::Client.new
      end
    end
    assert_equal client.settings[:url], 'testu'
    assert_equal client.settings[:token], 'testt'
  end

  def test_base_url
    ENV['LABCLIENT_URL'] = 'labclient_test'
    ENV['LABCLIENT_TOKEN'] = 'token_test'
    client = LabClient::Client.new
    assert_kind_of String, client.base_url
  end

  def test_retry_readers
    @client = LabClient::Client.new
    assert_kind_of Integer, @client.delay_factor
    assert_kind_of Integer, @client.retry_max
    refute @client.retry_max?
  end

  def test_main_client_version
    assert_kind_of String, LabClient.version
  end
end
