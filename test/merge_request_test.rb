require File.expand_path 'test_helper.rb', __dir__

class MergeRequestTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token', quiet: true)
  end

  def test_merge_request_attrs
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)

    %i[
      closed_at created_at first_deployed_to_production_at
      latest_build_finished_at latest_build_started_at merged_at updated_at
    ].each do |kind|
      assert_kind_of DateTime, mr.send(kind)
    end

    %i[author assignee merged_by closed_by].each do |kind|
      assert_kind_of LabClient::User, mr.send(kind)
    end

    mr.assignees.each do |kind|
      assert_kind_of LabClient::User, kind
    end
  end

  def test_merge_request_projects
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)

    stub_get('/projects/333', 'project')
    assert_kind_of LabClient::Project, mr.project
    assert_kind_of LabClient::Project, mr.source_project
    assert_kind_of LabClient::Project, mr.target_project
  end

  def test_merge_request
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)

    assert_kind_of LabClient::MergeRequest, mr
    assert_kind_of String, mr.inspect

    # Helper is console ouput
    suppress_output do
      assert_nil mr.help
    end

    # Test Reload
    assert_kind_of LabClient::MergeRequest, mr.reload
  end

  def test_merge_request_list
    stub_get('/merge_requests', 'merge_requests')
    list = @client.merge_requests.list

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::MergeRequest, list.first
  end

  def test_merge_request_participants
    stub_get('/projects/333/merge_requests/1/participants', 'mr_participants')
    list = @client.merge_requests.participants(333, 1)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::User, list.first

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::User, mr.participants.first
  end

  def test_merge_request_commits
    stub_get('/projects/333/merge_requests/1/commits', 'mr_commits')
    list = @client.merge_requests.commits(333, 1)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::Commit, list.first

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::Commit, mr.commits.first
  end

  def test_merge_request_changes
    stub_get('/projects/333/merge_requests/1/changes', 'changes')
    change = @client.merge_requests.changes(333, 1)

    assert_kind_of LabClient::Changes, change

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::Changes, mr.changes
  end

  def test_merge_request_time_stats
    stub_get('/projects/333/merge_requests/1/time_stats', 'mr_time_stats')
    stats = @client.merge_requests.time_stats(333, 1)

    assert_kind_of LabClient::LabStruct, stats

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::LabStruct, mr.time_stats
  end

  def test_merge_request_add_spent_time
    details = { duration: '1d' }
    stub_post('/projects/333/merge_requests/1/add_spent_time', 'mr_time_stats', 200, details)
    stats = @client.merge_requests.add_spent_time(333, 1, 1.day)

    assert_kind_of LabClient::LabStruct, stats

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::LabStruct, mr.add_spent_time('1d')
    assert_kind_of LabClient::LabStruct, mr.add_spent_time(1.day)
  end

  def test_merge_request_reset_spent_time
    stub_post('/projects/333/merge_requests/1/reset_spent_time', 'mr_time_stats')
    stats = @client.merge_requests.reset_spent_time(333, 1)

    assert_kind_of LabClient::LabStruct, stats

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::LabStruct, mr.reset_spent_time
  end

  def test_merge_request_reset_time_estimate
    stub_post('/projects/333/merge_requests/1/reset_time_estimate', 'mr_time_stats')
    stats = @client.merge_requests.reset_time_estimate(333, 1)

    assert_kind_of LabClient::LabStruct, stats

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::LabStruct, mr.reset_time_estimate
  end

  def test_merge_request_time_estimate
    details = { duration: '1d' }
    stub_post('/projects/333/merge_requests/1/time_estimate', 'mr_time_stats', 200, details)
    stats = @client.merge_requests.time_estimate(333, 1, 1.day)

    assert_kind_of LabClient::LabStruct, stats

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::LabStruct, mr.time_estimate('1d')
    assert_kind_of LabClient::LabStruct, mr.time_estimate(1.day)
  end

  def test_merge_request_pipelines
    stub_get('/projects/333/merge_requests/1/pipelines', 'pipelines')
    pipelines = @client.merge_requests.pipelines(333, 1)

    assert_kind_of LabClient::PaginatedResponse, pipelines
    assert_equal pipelines.size, 2
    assert_kind_of LabClient::Pipeline, pipelines.first

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::Pipeline, mr.pipelines.first
  end

  def test_merge_request_create_pipeline
    stub_post('/projects/333/merge_requests/1/pipelines', 'pipeline')
    pipeline = @client.merge_requests.create_pipeline(333, 1)

    assert_kind_of LabClient::Pipeline, pipeline

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::Pipeline, mr.create_pipeline
  end

  def test_create_merge_request
    stub_get('/projects/16', 'project')
    stub_post('/projects/16/merge_requests', 'merge_request')
    mr = @client.merge_requests.create(16, {})

    assert_kind_of LabClient::MergeRequest, mr

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::MergeRequest, project.merge_request_create({})
  end

  def test_merge_request_update
    details = { title: 'update' }
    stub_put('/projects/333/merge_requests/1', 'merge_request', 200, details)
    mr = @client.merge_requests.update(333, 1, title: 'update')

    assert_kind_of LabClient::MergeRequest, mr

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::MergeRequest, mr.update(details)
  end

  def test_delete_merge_request
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    stub_delete('/projects/333/merge_requests/1')

    resp = @client.merge_requests.delete(333, 1)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    mr = @client.merge_requests.show(333, 1)
    assert_nil mr.delete.data

    # Via Project
    stub_get('/projects/333', 'project_333')
    project = @client.projects.show(333)
    assert_nil project.merge_request_delete(1).data
  end

  def test_merge_request_accept
    stub_put('/projects/333/merge_requests/1/merge', 'merge_request')
    mr = @client.merge_requests.accept(333, 1)

    assert_kind_of LabClient::MergeRequest, mr

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::MergeRequest, mr.accept
  end

  def test_merge_request_ref
    stub_get('/projects/333/merge_requests/1/merge_ref', 'merge_ref')
    ref = @client.merge_requests.merge_ref(333, 1)

    assert_kind_of LabClient::LabStruct, ref

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::LabStruct, mr.merge_ref
  end

  def test_merge_request_cancel_auto_merge
    stub_post('/projects/333/merge_requests/1/cancel_merge_when_pipeline_succeeds', 'merge_request')
    mr = @client.merge_requests.cancel_auto_merge(333, 1)

    assert_kind_of LabClient::MergeRequest, mr

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::MergeRequest, mr.cancel_auto_merge
  end

  def test_merge_request_rebase
    details = { skip_ci: false }
    stub_put('/projects/333/merge_requests/1/rebase', 'merge_rebase', 200, details)
    rebase = @client.merge_requests.rebase(333, 1)

    assert_kind_of LabClient::LabStruct, rebase

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::LabStruct, mr.rebase
  end

  def test_merge_request_issues
    stub_get('/projects/333/merge_requests/1/closes_issues', 'issues')
    list = @client.merge_requests.closes_issues(333, 1)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::Issue, list.first

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)

    assert_kind_of LabClient::PaginatedResponse, mr.closes_issues
    assert_kind_of LabClient::Issue, mr.closes_issues.first
  end

  def test_merge_request_notes
    stub_get('/projects/333/merge_requests/1/notes', 'merge_request_notes')

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)

    assert_kind_of LabClient::PaginatedResponse, mr.notes
    assert_kind_of LabClient::Note, mr.notes.first
  end

  def test_merge_request_subscribe
    stub_post('/projects/333/merge_requests/1/subscribe', 'merge_request')
    mr = @client.merge_requests.subscribe(333, 1)

    assert_kind_of LabClient::MergeRequest, mr

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::MergeRequest, mr.subscribe
  end

  def test_sub_repeats
    stub_post('/projects/333/merge_requests/1/subscribe', 'subscribe_repeat', 304)
    suppress_output do
      assert_kind_of Typhoeus::Response, @client.merge_requests.subscribe(333, 1)
    end
  end

  def test_merge_request_unsubscribe
    stub_post('/projects/333/merge_requests/1/unsubscribe', 'merge_request')
    mr = @client.merge_requests.unsubscribe(333, 1)

    assert_kind_of LabClient::MergeRequest, mr

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::MergeRequest, mr.unsubscribe
  end

  def test_unsub_repeats
    stub_post('/projects/333/merge_requests/1/unsubscribe', 'subscribe_repeat', 304)

    suppress_output do
      assert_kind_of Typhoeus::Response, @client.merge_requests.unsubscribe(333, 1)
    end
  end

  def test_merge_request_todo
    stub_post('/projects/333/merge_requests/1/todo', 'merge_request')
    todo = @client.merge_requests.todo(333, 1)

    assert_kind_of LabClient::Todo, todo

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::Todo, mr.todo
  end

  def test_todo_repeats
    stub_post('/projects/333/merge_requests/1/todo', 'subscribe_repeat', 304)
    suppress_output do
      assert_kind_of Typhoeus::Response, @client.merge_requests.todo(333, 1)
    end
  end

  def test_merge_request_diff_versions
    stub_get('/projects/333/merge_requests/1/versions', 'diffs')
    list = @client.merge_requests.diff_versions(333, 1)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::MergeRequestDiff, list.first

    # Individual MR
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::MergeRequestDiff, mr.diff_versions.first
  end

  # Project
  def test_merge_request_project
    stub_get('/projects/16/merge_requests', 'merge_requests')
    stub_get('/projects/16', 'project')

    assert_kind_of LabClient::PaginatedResponse, @client.merge_requests.list_project(16)

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::MergeRequest, project.merge_requests.first
  end

  # Group
  def test_merge_request_group
    stub_get('/groups/5/merge_requests', 'merge_requests')
    stub_get('/groups/5', 'group')

    assert_kind_of LabClient::PaginatedResponse, @client.merge_requests.list_group(5)

    # Via Project
    group = @client.groups.show(5)
    assert_kind_of LabClient::MergeRequest, group.merge_requests.first
  end

  # rubocop:disable Metrics/MethodLength
  def test_merge_request_wait_for_merge_status
    first = { status: 200, body: load_fixture('merge_request_checking'), headers: {
      'content-type' => ['application/json']
    } }

    second = { status: 200, body: load_fixture('merge_request_checking'), headers: {
      'content-type' => ['application/json']
    } }

    third = { status: 200, body: load_fixture('merge_request'), headers: {
      'content-type' => ['application/json']
    } }

    stub_request(:get, 'https://labclient/api/v4/projects/333/merge_requests/1')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
          'Private-Token' => 'token'
        }
      )
      .to_return([first, second, third])

    merge_request = @client.merge_requests.show(333, 1)
    assert_nil merge_request.wait_for_merge_status(5, 0.1)
  end
  # rubocop:enable Metrics/MethodLength
end
