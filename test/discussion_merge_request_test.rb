require File.expand_path 'test_helper.rb', __dir__

class MergeRequestDiscussionsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::Discussion
  end

  def test_merge_request_discussion_show
    stub_get('/projects/1/merge_requests/2/discussions/21', 'merge_request_discussion')
    discussion = @client.discussions.merge_requests.show(1, 2, 21)

    assert_kind_of kind, discussion
    assert_kind_of String, discussion.inspect
  end

  def test_merge_request_discussion_list
    stub_get('/projects/1/merge_requests/2/discussions', 'merge_request_discussions')
    list = @client.discussions.merge_requests.list(1, 2)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of kind, list.first
  end

  def test_merge_request_discussion_create
    stub_post('/projects/1/merge_requests/2/discussions', 'merge_request_discussion')
    discussion = @client.discussions.merge_requests.create(1, 2, {})
    assert_kind_of kind, discussion
  end

  def test_merge_request_discussion_update
    stub_put('/projects/1/merge_requests/2/discussions/21', 'merge_request_discussion')
    discussion = @client.discussions.merge_requests.update(1, 2, 21, :body)
    assert_kind_of kind, discussion
  end

  def test_merge_request_discussion_delete
    stub_delete('/projects/1/merge_requests/2/discussions/21')
    resp = @client.discussions.merge_requests.delete(1, 2, 21)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end
end
