require File.expand_path 'test_helper.rb', __dir__

class ProjectMembersTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/projects/16', 'project')
  end

  def kind
    LabClient::Member
  end

  def test_members_list
    stub_get('/projects/16/members', 'members')
    list = @client.members.projects.list(16)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of kind, list.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.members.first
  end

  def test_members_list_all
    stub_get('/projects/16/members/all', 'members')
    list = @client.members.projects.list_all(16)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of kind, list.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.members_all.first
  end

  def test_members_show
    stub_get('/projects/16/members/40', 'member')
    show = @client.members.projects.show(16, 40)
    assert_kind_of kind, show
    assert_kind_of String, show.inspect

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.member(40)
  end

  def test_members_show_all
    stub_get('/projects/16/members/all/40', 'member')
    show = @client.members.projects.show_all(16, 40)
    assert_kind_of kind, show

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.member_all(40)
  end

  def test_members_add
    stub_post('/projects/16/members', 'member')
    show = @client.members.projects.add(16, 40, {})
    assert_kind_of kind, show

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.member_add(40, {})
  end

  def test_members_update
    stub_put('/projects/16/members/40', 'member')
    show = @client.members.projects.update(16, 40, {})
    assert_kind_of kind, show

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.member_update(40, {})
  end

  def test_members_delete
    stub_delete('/projects/16/members/40')

    resp = @client.members.projects.delete(16, 40)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Project
    project = @client.projects.show(16)
    assert_nil project.member_delete(40).data
  end
end
