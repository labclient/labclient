require File.expand_path 'test_helper.rb', __dir__

class ProtectedBranchesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/projects/16', 'project')
  end

  def test_branch_list
    stub_get('/projects/16/protected_branches', 'branches')

    list = @client.protected_branches.list(16)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::Branch, list.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Branch, project.protected_branches.first
  end

  def test_branch_search
    stub_get('/projects/16/protected_branches?search=search', 'branches')

    list = @client.protected_branches.list(16, 'search')
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::Branch, list.first
  end

  def test_branch_show
    stub_get('/projects/16/protected_branches/master', 'branch')

    branch = @client.protected_branches.show(16, :master)
    assert_kind_of LabClient::Branch, branch
    assert_kind_of String, branch.inspect
    assert_kind_of LabClient::Commit, branch.commit

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Branch, project.protected_branch(:master)
  end

  def test_branch_protect
    stub_post('/projects/16/protected_branches', 'protected_branch')

    branch = @client.protected_branches.protect(16, {})
    assert_kind_of LabClient::Branch, branch

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Branch, project.protect_branch({})
  end

  def test_branch_unprotect
    stub_delete('/projects/16/protected_branches/other')

    resp = @client.protected_branches.unprotect(16, :other)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Project
    project = @client.projects.show(16)
    assert_nil project.unprotect_branch(:other).data
  end

  def test_branch_code_owner_approval
    stub_patch('/projects/16/protected_branches/other', 'protected_branch')

    branch = @client.protected_branches.code_owner_approval(16, 'other', true)
    assert_kind_of LabClient::Branch, branch

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Branch, project.branch_code_owner_approval('other', true)
  end
end
