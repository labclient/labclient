require File.expand_path 'test_helper.rb', __dir__

class AwardMergeRequestTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::Award
  end

  def test_merge_request_award_show
    stub_get('/projects/1/merge_requests/2/award_emoji/21', 'award_merge_request')
    award = @client.awards.merge_requests.show(1, 2, 21)

    assert_kind_of kind, award
    assert_kind_of String, award.inspect
  end

  def test_merge_request_award_list
    stub_get('/projects/1/merge_requests/2/award_emoji', 'award_merge_requests')
    list = @client.awards.merge_requests.list(1, 2)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of kind, list.first
  end

  def test_merge_request_award_create
    stub_post('/projects/1/merge_requests/2/award_emoji', 'award_merge_request')
    award = @client.awards.merge_requests.create(1, 2, {})
    assert_kind_of kind, award
  end

  def test_merge_request_award_delete
    stub_delete('/projects/1/merge_requests/2/award_emoji/21')

    resp = @client.awards.merge_requests.delete(1, 2, 21)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end
end
