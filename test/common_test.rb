require File.expand_path 'test_helper.rb', __dir__

class CommonTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_human_access_level
    common = LabClient::Common.new(@client)

    assert_equal :none, common.human_access_level(0)
    assert_equal :guest, common.human_access_level(10)
    assert_equal :reporter, common.human_access_level(20)
    assert_equal :developer, common.human_access_level(30)
    assert_equal :maintainer, common.human_access_level(40)
    assert_equal :owner, common.human_access_level(50)
    assert_equal :admin, common.human_access_level(60)
  end

  def test_machine_access_level
    common = LabClient::Common.new(@client)

    assert_equal 0, common.machine_access_level(:none)
    assert_equal 10, common.machine_access_level(:guest)
    assert_equal 20, common.machine_access_level(:reporter)
    assert_equal 30, common.machine_access_level(:developer)
    assert_equal 40, common.machine_access_level(:maintainer)
    assert_equal 50, common.machine_access_level(:owner)
    assert_equal 60, common.machine_access_level(:admin)
  end

  def test_protected_human_access_level
    common = LabClient::Common.new(@client)

    assert_equal :none, common.human_protected_access_level(0)
    assert_equal :developer, common.human_protected_access_level(30)
    assert_equal :maintainer, common.human_protected_access_level(40)
    assert_equal :admin, common.human_protected_access_level(60)
  end

  def test_protected_machine_access_level
    common = LabClient::Common.new(@client)

    assert_equal 0, common.machine_protected_access_level(:none)
    assert_equal 30, common.machine_protected_access_level(:developer)
    assert_equal 40, common.machine_protected_access_level(:maintainer)
    assert_equal 60, common.machine_protected_access_level(:admin)
  end

  def test_query_access_level
    common = LabClient::Common.new(@client)

    symbol = { group_access: :owner }
    assert_equal 50, common.query_access_level(symbol, :group_access)
    assert_equal 50, symbol[:group_access]

    number = { group_access: 40 }
    assert_nil common.query_access_level(number, :group_access)
    assert_equal 40, number[:group_access]
  end

  def test_query_format_time
    common = LabClient::Common.new(@client)

    # Ignore Host Time, default to UTC
    query = { time:  Time.find_zone('UTC').parse('2020-02-27') }

    iso = '2020-02-27T00:00:00Z'

    assert_equal iso, common.query_format_time(query, :time)
    assert_equal iso, query[:time]
  end

  def test_query_format_id
    common = LabClient::Common.new(@client)

    # CGI Escape
    assert_equal 'group%2Fproject', common.format_id('group/project')

    # String ID
    assert_equal '1', common.format_id('1')

    # Integer ID
    assert_equal 3, common.format_id(3)

    # LabClient ID
    klass = LabClient::Klass.new(id: 19)
    assert_equal 19, common.format_id(klass)
  end

  def test_api_methods_help
    assert_kind_of Array, LabClient::Common.new(@client).api_methods
  end

  def test_klass
    assert_kind_of String, LabClient::Common.new(@client).klass
  end

  def test_group_name
    assert_kind_of String, LabClient::Common.new(@client).group_name

    suppress_output do
      common = LabClient::Common.new(nil)
      common.class.instance_variable_set(:@group_name, 'rawr')
      assert_equal 'rawr', common.group_name

      # Clean Up
      common.class.remove_instance_variable(:@group_name)
    end
  end

  def test_inspect
    suppress_output do
      assert_kind_of String, LabClient::Common.new(@client).inspect
    end
  end

  def test_common_help
    suppress_output do
      assert_nil LabClient::Common.new(@client).help
      assert_nil LabClient::Common.new(@client).help(:filter)
    end
  end
end
