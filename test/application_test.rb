require File.expand_path 'test_helper.rb', __dir__

class ApplicationsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_application_list
    stub_get('/applications', 'applications')
    applications = @client.applications.list

    assert_kind_of LabClient::PaginatedResponse, applications
    assert_equal applications.size, 2
    assert_kind_of LabClient::Application, applications.first

    suppress_output do
      assert_nil applications.first.help
    end
  end

  def test_create_application
    details = { name: 'Name' }
    stub_post('/applications', 'application', 200, details)

    application = @client.applications.create(details)

    assert_kind_of LabClient::Application, application
    assert_kind_of String, application.inspect
  end

  def test_delete_application
    stub_get('/applications', 'applications')
    stub_delete('/applications/1')

    resp = @client.applications.delete(1)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    application = @client.applications.list.first
    assert_nil application.delete.data
  end
end
