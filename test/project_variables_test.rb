require File.expand_path 'test_helper.rb', __dir__

class ProjectVariablesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::ProjectVariable
  end

  def test_project_variable
    stub_get('/projects/16/variables', 'project_variables')
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    variable = project.variables.first
    assert_kind_of String, variable.inspect
    assert_kind_of LabClient::Project, variable.project
  end

  def test_project_variables_show
    stub_get('/projects/16/variables/key', 'project_variable')
    stub_get('/projects/16', 'project')

    variable = @client.projects.variables.show(16, :key)
    assert_kind_of kind, variable
    assert_kind_of kind, variable.show

    project = @client.projects.show(16)
    assert_kind_of kind, project.variable_show(:key)
  end

  def test_project_variables_create
    stub_post('/projects/16/variables', 'project_variable')
    stub_get('/projects/16', 'project')

    variable = @client.projects.variables.create(16, {})
    assert_kind_of kind, variable

    project = @client.projects.show(16)
    assert_kind_of kind, project.variable_create({})
  end

  def test_project_variables_update
    stub_put('/projects/16/variables/key', 'project_variable')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/variables/key', 'project_variable')

    # Root
    variable = @client.projects.variables.update(16, :key, {})
    assert_kind_of kind, variable

    # Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.variable_update(:key, {})

    # Variable
    variable = @client.projects.variables.show(16, :key)
    assert_kind_of kind, variable.update({})
  end

  def test_project_variables_delete
    stub_delete('/projects/16/variables/key')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/variables/key', 'project_variable')

    # Root
    resp = @client.projects.variables.delete(16, :key)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Project
    project = @client.projects.show(16)
    assert_nil project.variable_delete(:key).data

    # Variable
    variable = @client.projects.variables.show(16, :key)
    assert_nil variable.delete.data
  end
end
