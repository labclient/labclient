require File.expand_path 'test_helper.rb', __dir__

class LabFileTest < Minitest::Test
  include Rack::Test::Methods

  def test_labfile_class
    klass = LabClient::LabFile.new(data: :rawr)
    assert_kind_of LabClient::LabFile, klass
    assert_equal klass.data, :rawr
    assert_kind_of String, klass.inspect
  end
end
