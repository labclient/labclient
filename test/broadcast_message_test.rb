require File.expand_path 'test_helper.rb', __dir__

class BroadcastMessagesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_broadcast_message
    stub_get('/broadcast_messages/1', 'broadcast_message')
    broadcast_message = @client.broadcast_messages.show(1)

    assert_kind_of LabClient::BroadcastMessage, broadcast_message
    assert_kind_of String, broadcast_message.inspect

    %i[ends_at starts_at].each do |kind|
      assert_kind_of DateTime, broadcast_message.send(kind)
    end

    # Helper is console ouput
    suppress_output do
      assert_nil broadcast_message.help
    end
  end

  def test_broadcast_message_list
    stub_get('/broadcast_messages', 'broadcast_messages')
    broadcast_messages = @client.broadcast_messages.list

    assert_kind_of LabClient::PaginatedResponse, broadcast_messages
    assert_equal broadcast_messages.size, 2
    assert_kind_of LabClient::BroadcastMessage, broadcast_messages.first
  end

  def test_create_broadcast_message
    details = { message: 'Alert' }
    stub_post('/broadcast_messages', 'broadcast_message', 200, details)

    broadcast_message = @client.broadcast_messages.create(details)

    assert_kind_of LabClient::BroadcastMessage, broadcast_message
    assert_kind_of String, broadcast_message.inspect
  end

  def test_update_broadcast_message
    details = { message: 'Update' }
    stub_get('/broadcast_messages/1', 'broadcast_message')
    stub_put('/broadcast_messages/1', 'broadcast_message', 200, details)

    broadcast_message_direct = @client.broadcast_messages.update(1, message: 'Update')

    broadcast_message = @client.broadcast_messages.show(1)
    broadcast_message.update(message: 'Update')

    assert_kind_of LabClient::BroadcastMessage, broadcast_message
    assert_kind_of LabClient::BroadcastMessage, broadcast_message_direct
  end

  def test_delete_broadcast_message
    stub_get('/broadcast_messages/1', 'broadcast_message')
    stub_delete('/broadcast_messages/1')

    resp = @client.broadcast_messages.delete(1)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    broadcast_message = @client.broadcast_messages.show(1)
    assert_nil broadcast_message.delete.data
  end
end
