require File.expand_path 'test_helper.rb', __dir__
class NotificationTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_list
    stub_get('/notification_settings', 'global_notification')
    stub_get('/projects/16/notification_settings', 'global_notification')
    stub_get('/groups/5/notification_settings', 'global_notification')

    # Global
    assert_kind_of LabClient::LabStruct, @client.notifications.list
    assert_kind_of LabClient::LabStruct, @client.notifications.list_global

    # Group
    assert_kind_of LabClient::LabStruct, @client.notifications.list_group(5)

    # Project
    assert_kind_of LabClient::LabStruct, @client.notifications.list_project(16)
  end

  def test_update
    stub_put('/notification_settings', 'global_notification')
    stub_put('/groups/5/notification_settings', 'global_notification')
    stub_put('/projects/16/notification_settings', 'global_notification')

    # Global
    assert_kind_of LabClient::LabStruct, @client.notifications.update({})
    assert_kind_of LabClient::LabStruct, @client.notifications.update_global({})

    # Group
    assert_kind_of LabClient::LabStruct, @client.notifications.update_group(5, {})

    # Project
    assert_kind_of LabClient::LabStruct, @client.notifications.update_project(16, {})
  end
end
