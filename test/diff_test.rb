require File.expand_path 'test_helper.rb', __dir__

class MergeRequestDiffTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_diff_versions
    stub_get('/projects/333/merge_requests/1/versions', 'diffs')
    list = @client.merge_requests.diff_versions(333, 1)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::MergeRequestDiff, list.first
    assert_kind_of String, list.first.inspect
  end

  def test_diff_details
    stub_get('/projects/333/merge_requests/1/versions', 'diffs')
    diff = @client.merge_requests.diff_versions(333, 1).first

    stub_get('/projects/333/merge_requests/1/versions/3', 'diff')

    diff.details
    assert diff.keys.include? :commits
  end
end
