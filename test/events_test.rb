require File.expand_path 'test_helper.rb', __dir__

class EventsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_events
    stub_get('/events', 'events')
    list = @client.events.list

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::Event, list.first
    assert_kind_of String, list.first.inspect
  end

  def test_user_events
    stub_get('/users/5/events', 'events')
    stub_get('/users/5', 'user')
    list = @client.events.user(5)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::Event, list.first

    user = @client.users.show(5)
    assert_kind_of LabClient::Event, user.events.first
  end

  def test_project_events
    stub_get('/projects/16/events', 'events')
    stub_get('/projects/16', 'project')
    list = @client.events.project(16)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::Event, list.first

    project = @client.projects.show(16)
    assert_kind_of LabClient::Event, project.events.first
  end
end
