require File.expand_path 'test_helper.rb', __dir__

class VersionTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_version
    stub_get('/version', 'version')
    assert_kind_of Hash, @client.version.show
  end
end
