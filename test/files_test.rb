require File.expand_path 'test_helper.rb', __dir__

class FilesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/projects/16', 'project')
  end

  def test_show
    stub_get('/projects/16/repository/files/file?ref=main', 'file_get')
    file = @client.files.show(16, 'file')
    assert_kind_of LabClient::LabStruct, file

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.file('file')
  end

  def test_show_blame
    stub_get('/projects/16/repository/files/file/blame?ref=main', 'file_blame')
    blame = @client.files.show(16, 'file', :main, :blame)
    assert_kind_of LabClient::PaginatedResponse, blame

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::PaginatedResponse, project.file('file', :main, :blame)
  end

  def test_show_raw
    # Return Headers for Parsing
    stub_request(:get, 'https://labclient/api/v4/projects/16/repository/files/file/raw?ref=main').to_return(status: 200, body: 'text', headers: { 'Content-Type' => 'text/plain; charset=utf-8' })

    resp = @client.files.show(16, 'file', :main, :raw)
    assert_kind_of LabClient::LabFile, resp
    assert_equal resp.data, 'text'

    # Via Project
    project = @client.projects.show(16)
    assert_equal 'text', project.file('file', :main, :raw).data
  end

  def test_create
    stub_post('/projects/16/repository/files/newfile', 'file_create')
    assert_kind_of LabClient::LabStruct, @client.files.create(16, 'newfile', {})

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.file_create('newfile', {})
  end

  def test_update
    stub_put('/projects/16/repository/files/newfile', 'file_create')
    assert_kind_of LabClient::LabStruct, @client.files.update(16, 'newfile', {})

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.file_update('newfile', {})
  end

  def test_delete
    stub_delete('/projects/16/repository/files/newfile')

    resp = @client.files.delete(16, 'newfile', {})
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Project
    project = @client.projects.show(16)
    assert_nil project.file_delete('newfile', {}).data
  end
end
