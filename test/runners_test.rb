require File.expand_path 'test_helper.rb', __dir__
class RunnerTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_runner_list
    stub_get('/runners', 'runners')
    list = @client.runners.list

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::Runner, list.first
    assert_kind_of String, list.first.inspect
  end

  def test_runner_all
    stub_get('/runners/all', 'runners')
    list = @client.runners.all

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::Runner, list.first
  end

  def test_runner_show
    stub_get('/runners/3', 'runner_details')
    runner = @client.runners.show(3)
    assert_kind_of LabClient::Runner, runner

    # Via Runner
    runner.details
    assert_kind_of LabClient::Runner, runner.details
  end

  def test_runner_update
    stub_put('/runners/3', 'runner_details')

    runner = @client.runners.update(3, {})
    assert_kind_of LabClient::Runner, runner

    # Via Runner
    assert_kind_of LabClient::Runner, runner.update({})
  end

  def test_runner_remove
    stub_delete('/runners/3')
    stub_get('/runners/3', 'runner_details')

    resp = @client.runners.remove(3)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Runner
    runner = @client.runners.show(3)
    assert_nil runner.remove.data
  end

  def test_runner_jobs
    stub_get('/runners/3', 'runner_details')
    stub_get('/runners/3/jobs', 'jobs')

    list = @client.runners.jobs(3)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::Job, list.first
    assert_kind_of String, list.first.inspect

    # Via Runner
    runner = @client.runners.show(3)
    assert_kind_of LabClient::PaginatedResponse, runner.jobs
  end

  def test_runner_register
    stub_post('/runners', 'runner_registration')

    runner = @client.runners.register({})
    assert_kind_of LabClient::LabStruct, runner
  end

  def test_runner_delete
    stub_delete('/runners')

    resp = @client.runners.delete({})
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end

  def test_runner_verify
    stub_get('/runners/3', 'runner_details')
    stub_post('/runners/verify')

    resp = @client.runners.verify(2)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Runner
    runner = @client.runners.show(3)
    assert_nil runner.verify.data
  end
end
