require File.expand_path 'test_helper.rb', __dir__
class TerraformTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_terraform_show
    stub_get('/projects/16/terraform/state/anotha', 'terraform')

    state = @client.terraform.show(16, :anotha)
    assert_kind_of LabClient::TerraformState, state
    assert_kind_of LabClient::TerraformState, state
    assert_kind_of String, state.inspect
    assert_kind_of Integer, state.serial
  end

  def test_terraform_create
    stub_post('/projects/16/terraform/state/anotha?ID=123')

    resp = @client.terraform.create(16, :anotha, 123, {})
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end

  def test_terraform_delete
    stub_delete('/projects/16/terraform/state/anotha')

    resp = @client.terraform.delete(16, :anotha)

    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end

  def test_terraform_lock
    stub_post('/projects/16/terraform/state/anotha/lock', 'terraform')
    assert_kind_of LabClient::LabStruct, @client.terraform.lock(16, :anotha, {})
  end

  def test_terraform_unlock
    stub_delete('/projects/16/terraform/state/anotha/lock?ID=123')

    resp = @client.terraform.unlock(16, :anotha, 123)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end
end
