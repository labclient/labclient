require File.expand_path 'test_helper.rb', __dir__

class ProjectLabelsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::ProjectLabel
  end

  def test_project_label
    stub_get('/projects/16/labels/7', 'project_label')
    stub_get('/projects/16', 'project')

    project = @client.projects.show(16)
    label = project.label(7)

    assert_kind_of String, label.inspect
    assert_kind_of LabClient::Project, label.project
  end

  def test_project_label_list
    stub_get('/projects/16/labels', 'project_labels')
    stub_get('/projects/16', 'project')

    # Root
    list = @client.projects.labels.list(16)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of kind, list.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::PaginatedResponse, project.labels
  end

  def test_project_labels_show
    stub_get('/projects/16/labels/7', 'project_label')
    stub_get('/projects/16', 'project')

    label = @client.projects.labels.show(16, 7)
    assert_kind_of kind, label

    project = @client.projects.show(16)
    assert_kind_of kind, project.label(7)
  end

  def test_project_labels_create
    stub_post('/projects/16/labels', 'project_label')
    stub_get('/projects/16', 'project')

    label = @client.projects.labels.create(16, {})
    assert_kind_of kind, label

    project = @client.projects.show(16)
    assert_kind_of kind, project.label_create({})
  end

  def test_project_labels_update
    stub_put('/projects/16/labels/7', 'project_label')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/labels/7', 'project_label')

    # Root
    label = @client.projects.labels.update(16, 7, {})
    assert_kind_of kind, label

    # Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.label_update(7, {})

    # Label
    label = @client.projects.labels.show(16, 7)
    assert_kind_of kind, label.update({})
  end

  def test_project_labels_promote
    stub_put('/projects/16/labels/7/promote', 'project_label')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/labels/7', 'project_label')

    # Root
    label = @client.projects.labels.promote(16, 7)
    assert_kind_of kind, label

    # Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.label_promote(7)

    # Label
    label = @client.projects.labels.show(16, 7)
    assert_kind_of kind, label.promote
  end

  def test_project_labels_delete
    stub_delete('/projects/16/labels/7')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/labels/7', 'project_label')

    # Root
    resp = @client.projects.labels.delete(16, 7)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Project
    project = @client.projects.show(16)
    assert_nil project.label_delete(7).data

    # Label
    label = @client.projects.labels.show(16, 7)
    assert_nil label.delete.data
  end

  def test_label_subscribe
    stub_post('/projects/16/labels/7/subscribe', 'project_label')
    stub_get('/projects/16', 'project')

    label = @client.projects.labels.subscribe(16, 7)
    assert_kind_of kind, label

    # Individual Label
    stub_get('/projects/16/labels/7', 'project_label')
    label = @client.projects.labels.show(16, 7)
    assert_kind_of kind, label.subscribe

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.label_subscribe(7)
  end

  def test_sub_repeats
    stub_post('/projects/16/labels/7/subscribe', 'subscribe_repeat', 304)
    suppress_output do
      assert_kind_of Typhoeus::Response, @client.projects.labels.subscribe(16, 7)
    end
  end

  def test_label_unsubscribe
    stub_post('/projects/16/labels/7/unsubscribe', 'project_label')
    stub_get('/projects/16', 'project')

    label = @client.projects.labels.unsubscribe(16, 7)
    assert_kind_of kind, label

    # Individual Label
    stub_get('/projects/16/labels/7', 'project_label')
    label = @client.projects.labels.show(16, 7)
    assert_kind_of kind, label.unsubscribe

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.label_unsubscribe(7)
  end

  def test_unsub_repeats
    stub_post('/projects/16/labels/7/unsubscribe', 'subscribe_repeat', 304)
    suppress_output do
      assert_kind_of Typhoeus::Response, @client.projects.labels.unsubscribe(16, 7)
    end
  end
end
