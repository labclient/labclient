require File.expand_path 'test_helper.rb', __dir__
class NameSpaceTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_list
    stub_get('/namespaces', 'namespaces')
    stub_get('/namespaces?search=test', 'namespaces')

    result = @client.namespaces.list
    assert_kind_of LabClient::PaginatedResponse, result
    assert_kind_of LabClient::Namespace, result.first

    # w/ search
    result = @client.namespaces.list('test')
    assert_kind_of LabClient::PaginatedResponse, result
    assert_kind_of LabClient::Namespace, result.first
  end

  def test_show
    stub_get('/namespaces/1', 'namespace')

    result = @client.namespaces.show(1)

    assert_kind_of LabClient::Namespace, result
    assert_kind_of String, result.inspect
  end
end
