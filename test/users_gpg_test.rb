require File.expand_path 'test_helper.rb', __dir__
class UserGpgTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_users_gpg_keys_list
    stub_get('/users/5', 'user')
    stub_get('/users/5/gpg_keys', 'gpg_keys')
    stub_get('/user/gpg_keys', 'gpg_keys')

    # Current User
    list = @client.users.gpg_keys.list
    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::GpgKey, list.first

    # Specific User
    list = @client.users.gpg_keys.list(5)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::GpgKey, list.first
    assert_kind_of String, list.first.inspect

    # Via User
    user = @client.users.show(5)
    assert_kind_of LabClient::GpgKey, user.gpg_keys.first
  end

  def test_users_gpg_key_show
    stub_get('/user/gpg_keys/3', 'gpg_key')
    assert_kind_of LabClient::GpgKey, @client.users.gpg_keys.show(3)
  end

  def test_users_gpg_key_create
    stub_get('/users/5', 'user')
    stub_post('/users/5/gpg_keys', 'gpg_key')
    stub_post('/user/gpg_keys', 'gpg_key')

    # Top
    assert_kind_of LabClient::GpgKey, @client.users.gpg_keys.create({})
    assert_kind_of LabClient::GpgKey, @client.users.gpg_keys.create({}, 5)

    # Via User
    user = @client.users.show(5)
    assert_kind_of LabClient::GpgKey, user.gpg_key_create({})
  end

  def test_users_gpg_key_delete
    stub_get('/users/5', 'user')
    stub_delete('/users/5/gpg_keys/3')
    stub_delete('/user/gpg_keys/3')

    # Top
    resp = @client.users.gpg_keys.delete(3)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    resp = @client.users.gpg_keys.delete(3, 5)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via User
    user = @client.users.show(5)
    assert_nil user.gpg_key_delete(3).data
  end
end
