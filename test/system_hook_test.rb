require File.expand_path 'test_helper.rb', __dir__
class SystemHookTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::SystemHook
  end

  def test_hook_list
    stub_get('/hooks', 'system_hooks')
    list = @client.system_hooks.list
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of kind, list.first
  end

  def test_hook_add
    stub_post('/hooks', 'system_hook')
    hook = @client.system_hooks.add({})

    assert_kind_of kind, hook
    assert_kind_of String, hook.inspect
  end

  def test_hook_test
    stub_get('/hooks/7', 'system_hook_test')
    assert_kind_of LabClient::LabStruct, @client.system_hooks.test(7)
  end

  def test_hook_delete
    stub_delete('/hooks/7')
    stub_get('/hooks', 'system_hooks')

    resp = @client.system_hooks.delete(7)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    resp = @client.system_hooks.list.first.delete
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end
end
