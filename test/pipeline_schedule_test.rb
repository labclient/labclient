require File.expand_path 'test_helper.rb', __dir__

class PipelineSchedulesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::PipelineSchedule
  end

  def test_schedule
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/pipeline_schedules/13', 'pipeline_schedule')

    schedule = @client.projects.pipeline_schedules.show(16, 13)

    assert_kind_of kind, schedule
    assert_kind_of String, schedule.inspect
    assert_kind_of LabClient::Project, schedule.project
  end

  def test_schedule_list
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/pipeline_schedules', 'pipeline_schedules')

    pipeline_schedules = @client.projects.pipeline_schedules.list(16)

    assert_kind_of LabClient::PaginatedResponse, pipeline_schedules
    assert_equal pipeline_schedules.size, 2
    assert_kind_of kind, pipeline_schedules.first

    # via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.pipeline_schedules.first
  end

  def test_create_schedule
    stub_post('/projects/16/pipeline_schedules', 'pipeline_schedule')

    schedule = @client.projects.pipeline_schedules.create(16, {})

    assert_kind_of kind, schedule
    assert_kind_of String, schedule.inspect
  end

  def test_update_schedule
    stub_get('/projects/16/pipeline_schedules/13', 'pipeline_schedule')
    stub_put('/projects/16/pipeline_schedules/13', 'pipeline_schedule')

    assert_kind_of kind, @client.projects.pipeline_schedules.update(16, 13, {})

    schedule = @client.projects.pipeline_schedules.show(16, 13)
    schedule.update({})
    assert_kind_of kind, schedule
  end

  def test_delete_schedule
    stub_get('/projects/16/pipeline_schedules/13', 'pipeline_schedule')
    stub_delete('/projects/16/pipeline_schedules/13')

    resp = @client.projects.pipeline_schedules.delete(16, 13)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # via Pipeline Schedule
    schedule = @client.projects.pipeline_schedules.show(16, 13)
    assert_nil schedule.delete.data
  end

  def test_schedule_play
    stub_get('/projects/16/pipeline_schedules/13', 'pipeline_schedule')
    stub_post('/projects/16/pipeline_schedules/13/play', 'pipeline_schedule')

    assert_kind_of LabClient::LabStruct, @client.projects.pipeline_schedules.play(16, 13)

    # via Pipeline Schedule
    schedule = @client.projects.pipeline_schedules.show(16, 13)
    assert_kind_of LabClient::LabStruct, schedule.play
  end

  def test_schedule_ownership
    stub_get('/projects/16/pipeline_schedules/13', 'pipeline_schedule')
    stub_post('/projects/16/pipeline_schedules/13/take_ownership', 'pipeline_schedule')

    assert_kind_of LabClient::LabStruct, @client.projects.pipeline_schedules.take_ownership(16, 13)

    # via Pipeline Schedule
    schedule = @client.projects.pipeline_schedules.show(16, 13)
    assert_kind_of LabClient::LabStruct, schedule.take_ownership
  end

  def test_schedule_var_create
    stub_get('/projects/16/pipeline_schedules/13', 'pipeline_schedule')
    stub_post('/projects/16/pipeline_schedules/13/variables', 'pipeline_schedule')

    var = @client.projects.pipeline_schedules.variables.create(16, 13, {})
    assert_kind_of LabClient::LabStruct, var

    # Via Pipeline Schedule
    pipeline_schedule = @client.projects.pipeline_schedules.show(16, 13)
    assert_kind_of LabClient::LabStruct, pipeline_schedule.create_variable({})
  end

  def test_schedule_var_update
    stub_get('/projects/16/pipeline_schedules/13', 'pipeline_schedule')
    stub_put('/projects/16/pipeline_schedules/13/variables/key', 'pipeline_schedule')

    var = @client.projects.pipeline_schedules.variables.update(16, 13, :key, {})
    assert_kind_of LabClient::LabStruct, var

    # Via Pipeline Schedule
    pipeline_schedule = @client.projects.pipeline_schedules.show(16, 13)
    assert_kind_of LabClient::LabStruct, pipeline_schedule.update_variable(:key, {})
  end

  def test_schedule_var_delete
    stub_get('/projects/16/pipeline_schedules/13', 'pipeline_schedule')
    stub_delete('/projects/16/pipeline_schedules/13/variables/key', 'pipeline_schedule')

    var = @client.projects.pipeline_schedules.variables.delete(16, 13, :key)
    assert_kind_of LabClient::LabStruct, var

    # Via Pipeline Schedule
    pipeline_schedule = @client.projects.pipeline_schedules.show(16, 13)
    assert_kind_of LabClient::LabStruct, pipeline_schedule.delete_variable(:key)
  end
end
