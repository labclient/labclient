require File.expand_path 'test_helper.rb', __dir__

class SnippetNotesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::Note
  end

  def test_snippet_note_show
    stub_get('/projects/1/snippets/2/notes/21', 'snippet_note')
    note = @client.notes.snippets.show(1, 2, 21)

    assert_kind_of kind, note
    assert_kind_of String, note.inspect
  end

  def test_snippet_note_list
    stub_get('/projects/1/snippets/2/notes', 'snippet_notes')
    list = @client.notes.snippets.list(1, 2)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of kind, list.first
  end

  def test_snippet_note_create
    stub_post('/projects/16/snippets/1/notes', 'snippet_note')
    stub_get('/projects/16/snippets/1', 'project_snippet')

    note = @client.notes.snippets.create(16, 1, {})
    assert_kind_of kind, note

    # Test On Snippet
    snippet = @client.projects.snippets.show(16, 1)
    assert_kind_of kind, snippet.note_create({})
  end

  def test_snippet_note_update
    stub_put('/projects/1/snippets/2/notes/21', 'snippet_note')
    note = @client.notes.snippets.update(1, 2, 21, :body)
    assert_kind_of kind, note
  end

  def test_snippet_note_delete
    stub_delete('/projects/1/snippets/2/notes/21')
    resp = @client.notes.snippets.delete(1, 2, 21)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end
end
