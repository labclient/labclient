require File.expand_path 'test_helper.rb', __dir__

class JobsTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_jobs_project_list
    stub_get('/projects/16/jobs', 'jobs')
    stub_get('/projects/16', 'project')
    list = @client.jobs.project(16)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::Job, list.first
    assert_kind_of LabClient::Project, list.first.project

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Job, project.jobs.first
  end

  def test_jobs_pipeline_list
    stub_get('/projects/16/pipelines/4/jobs', 'jobs')
    stub_get('/projects/16/pipelines/4', 'pipeline')

    list = @client.jobs.pipeline(16, 4)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::Job, list.first

    # Via Pipeline
    pipeline = @client.pipelines.show(16, 4)
    assert_kind_of LabClient::Job, pipeline.jobs.first
  end

  def test_job_show
    stub_get('/projects/16/jobs/1', 'job')
    stub_get('/projects/16', 'project')

    job = @client.jobs.show(16, 1)
    assert_kind_of LabClient::Job, job
    assert_kind_of LabClient::Pipeline, job.pipeline
    assert_kind_of String, job.inspect

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Job, project.job(1)
  end

  def test_job_artifacts
    stub_get('/projects/16/jobs/1/artifacts')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/jobs/1', 'job')

    # Project
    project = @client.projects.show(16)

    # Job
    job = @client.jobs.show(16, 1)

    File.stub(:write, 1000) do
      assert_kind_of Typhoeus::Response, @client.jobs.artifacts(16, 1)
      assert_kind_of Typhoeus::Response,  project.job_artifacts(1)
      assert_kind_of Typhoeus::Response,  project.job_download_artifacts(1)
      assert_kind_of Typhoeus::Response,  job.download_artifacts
      assert_kind_of Typhoeus::Response,  job.show_artifacts
    end
  end

  def test_job_latest_artifacts
    stub_get('/projects/16/jobs/artifacts/master/download?job=name')
    stub_get('/projects/16', 'project')

    # Project
    project = @client.projects.show(16)

    File.stub(:write, 1000) do
      assert_equal 1000, @client.jobs.artifacts_latest(16, :master, :name)
      assert_equal 1000, project.job_artifacts_latest(:master, :name)
    end
  end

  def test_job_artifacts_path
    stub_get('/projects/16/jobs/1/artifacts/name')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/jobs/1', 'job')

    # Job
    job = @client.jobs.show(16, 1)

    # Project
    project = @client.projects.show(16)

    File.stub(:write, 1000) do
      assert_equal 1000, @client.jobs.artifacts_path(16, 1, :name)
      assert_equal 1000, project.job_artifacts_path(1, :name)
      assert_equal 1000, job.artifacts_path(:name)
    end
  end

  # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
  def test_job_trace
    stub_get('/projects/16/jobs/1/trace')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/jobs/1', 'job')

    # Project
    project = @client.projects.show(16)

    # Job
    job = @client.jobs.show(16, 1)

    suppress_output do
      resp = @client.jobs.trace(16, 1)
      assert_kind_of Typhoeus::Response, resp
      assert_equal 200, resp.code
      assert_nil resp.data

      resp = project.job_trace(1)
      assert_kind_of Typhoeus::Response, resp
      assert_equal 200, resp.code
      assert_nil resp.data

      resp = job.trace
      assert_kind_of Typhoeus::Response, resp
      assert_equal 200, resp.code
      assert_nil resp.data
    end
  end
  # rubocop:enable Metrics/MethodLength, Metrics/AbcSize

  def test_job_cancel
    stub_post('/projects/16/jobs/1/cancel', 'job')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/jobs/1', 'job')

    # Project
    project = @client.projects.show(16)

    # Job
    job = @client.jobs.show(16, 1)

    assert_kind_of LabClient::Job, @client.jobs.cancel(16, 1)
    assert_kind_of LabClient::Job, project.job_cancel(1)
    assert_kind_of LabClient::Job, job.cancel
  end

  def test_job_retry
    stub_post('/projects/16/jobs/1/retry', 'job')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/jobs/1', 'job')

    # Project
    project = @client.projects.show(16)

    # Job
    job = @client.jobs.show(16, 1)

    assert_kind_of LabClient::Job, @client.jobs.retry(16, 1)
    assert_kind_of LabClient::Job, project.job_retry(1)
    assert_kind_of LabClient::Job, job.retry
  end

  def test_job_erase
    stub_post('/projects/16/jobs/1/erase', 'job')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/jobs/1', 'job')

    # Project
    project = @client.projects.show(16)

    # Job
    job = @client.jobs.show(16, 1)

    assert_kind_of LabClient::Job, @client.jobs.erase(16, 1)
    assert_kind_of LabClient::Job, project.job_erase(1)
    assert_kind_of LabClient::Job, job.erase
  end

  def test_job_keep
    stub_post('/projects/16/jobs/1/keep', 'job')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/jobs/1', 'job')

    # Project
    project = @client.projects.show(16)

    # Job
    job = @client.jobs.show(16, 1)

    assert_kind_of LabClient::Job, @client.jobs.keep(16, 1)
    assert_kind_of LabClient::Job, project.job_keep(1)
    assert_kind_of LabClient::Job, job.keep
  end

  def test_job_play
    stub_post('/projects/16/jobs/1/play', 'job')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/jobs/1', 'job')

    # Project
    project = @client.projects.show(16)

    # Job
    job = @client.jobs.show(16, 1)

    assert_kind_of LabClient::Job, @client.jobs.play(16, 1)
    assert_kind_of LabClient::Job, project.job_play(1)
    assert_kind_of LabClient::Job, job.play
  end

  # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
  def test_job_delete
    stub_delete('/projects/16/jobs/1/artifacts')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/jobs/1', 'job')

    # Project
    project = @client.projects.show(16)

    # Job
    job = @client.jobs.show(16, 1)

    resp = @client.jobs.delete(16, 1)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    resp = project.job_delete(1)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    resp = job.delete
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end
  # rubocop:enable Metrics/MethodLength, Metrics/AbcSize
end
