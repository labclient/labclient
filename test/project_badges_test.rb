require File.expand_path 'test_helper.rb', __dir__

class ProjectBadgesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_project_badge
    stub_get('/projects/16/badges', 'project_badges')
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    badge = project.badges.first

    assert_kind_of String, badge.inspect
    assert_kind_of LabClient::Project, badge.project
  end

  def test_project_badges_show
    stub_get('/projects/16/badges/5', 'project_badge')
    stub_get('/projects/16', 'project')

    badge = @client.projects.badges.show(16, 5)
    assert_kind_of LabClient::ProjectBadge, badge

    project = @client.projects.show(16)
    assert_kind_of LabClient::ProjectBadge, project.badge_show(5)
  end

  def test_project_badges_create
    stub_post('/projects/16/badges', 'project_badge')
    stub_get('/projects/16', 'project')

    badge = @client.projects.badges.create(16, url: 'value')
    assert_kind_of LabClient::ProjectBadge, badge

    project = @client.projects.show(16)
    assert_kind_of LabClient::ProjectBadge, project.badge_create(url: 'value')
  end

  def test_project_badges_update
    stub_put('/projects/16/badges/5', 'project_badge')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/badges/5', 'project_badge')

    # Root
    badge = @client.projects.badges.update(16, 5, url: 'value')
    assert_kind_of LabClient::ProjectBadge, badge

    # Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::ProjectBadge, project.badge_update(5, url: 'value')

    # Badge
    badge = @client.projects.badges.show(16, 5)
    assert_kind_of LabClient::ProjectBadge, badge.update(url: 'value')
  end

  def test_project_badges_delete
    stub_delete('/projects/16/badges/5')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/badges/5', 'project_badge')

    # Root
    resp = @client.projects.badges.delete(16, 5)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Project
    project = @client.projects.show(16)
    assert_nil project.badge_delete(5).data

    # Badge
    badge = @client.projects.badges.show(16, 5)
    assert_nil badge.delete.data
  end

  def test_project_badges_preview
    stub_get('/projects/16/badges/5', 'project_badge')
    stub_get('/projects/16/badges/render', 'project_badge')

    # Nasty but based on fixture
    stub_get('/projects/16/badges/render?image_url=https://labclient/%25%7Bproject_path%7D/badges/%25%7Bdefault_branch%7D/pipeline.svg&link_url=https://labclient/%25%7Bproject_path%7D', 'project_badge')

    # Root
    assert_kind_of LabClient::LabStruct, @client.projects.badges.preview(16, {})

    # Badge
    badge = @client.projects.badges.show(16, 5)
    assert_kind_of LabClient::LabStruct, badge.preview
  end
end
