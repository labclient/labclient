require File.expand_path 'test_helper.rb', __dir__

class GroupLabelsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::GroupLabel
  end

  def test_group_label
    stub_get('/groups/5/labels/7', 'group_label')
    stub_get('/groups/5', 'group')

    group = @client.groups.show(5)
    label = group.label(7)

    assert_kind_of String, label.inspect
    assert_kind_of LabClient::Group, label.group
  end

  def test_group_label_list
    stub_get('/groups/5/labels', 'group_labels')
    stub_get('/groups/5', 'group')

    # Root
    list = @client.groups.labels.list(5)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of kind, list.first

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of LabClient::PaginatedResponse, group.labels
  end

  def test_group_labels_show
    stub_get('/groups/5/labels/7', 'group_label')
    stub_get('/groups/5', 'group')

    label = @client.groups.labels.show(5, 7)
    assert_kind_of kind, label

    group = @client.groups.show(5)
    assert_kind_of kind, group.label(7)
  end

  def test_group_labels_create
    stub_post('/groups/5/labels', 'group_label')
    stub_get('/groups/5', 'group')

    label = @client.groups.labels.create(5, {})
    assert_kind_of kind, label

    group = @client.groups.show(5)
    assert_kind_of kind, group.label_create({})
  end

  def test_group_labels_update
    stub_put('/groups/5/labels/7', 'group_label')
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/labels/7', 'group_label')

    # Root
    label = @client.groups.labels.update(5, 7, {})
    assert_kind_of kind, label

    # Group
    group = @client.groups.show(5)
    assert_kind_of kind, group.label_update(7, {})

    # Label
    label = @client.groups.labels.show(5, 7)
    assert_kind_of kind, label.update({})
  end

  def test_group_labels_delete
    stub_delete('/groups/5/labels/7')
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/labels/7', 'group_label')

    # Root
    resp = @client.groups.labels.delete(5, 7)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Group
    group = @client.groups.show(5)
    assert_nil group.label_delete(7).data

    # Label
    label = @client.groups.labels.show(5, 7)
    assert_nil label.delete.data
  end

  def test_label_subscribe
    stub_post('/groups/5/labels/7/subscribe', 'group_label')
    stub_get('/groups/5', 'group')

    label = @client.groups.labels.subscribe(5, 7)
    assert_kind_of kind, label

    # Individual Label
    stub_get('/groups/5/labels/7', 'group_label')
    label = @client.groups.labels.show(5, 7)
    assert_kind_of kind, label.subscribe

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of kind, group.label_subscribe(7)
  end

  def test_sub_repeats
    stub_post('/groups/5/labels/7/subscribe', 'subscribe_repeat', 304)
    suppress_output do
      assert_kind_of Typhoeus::Response, @client.groups.labels.subscribe(5, 7)
    end
  end

  def test_label_unsubscribe
    stub_post('/groups/5/labels/7/unsubscribe', 'group_label')
    stub_get('/groups/5', 'group')

    label = @client.groups.labels.unsubscribe(5, 7)
    assert_kind_of kind, label

    # Individual Label
    stub_get('/groups/5/labels/7', 'group_label')
    label = @client.groups.labels.show(5, 7)
    assert_kind_of kind, label.unsubscribe

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of kind, group.label_unsubscribe(7)
  end

  def test_unsub_repeats
    stub_post('/groups/5/labels/7/unsubscribe', 'subscribe_repeat', 304)
    suppress_output do
      assert_kind_of Typhoeus::Response, @client.groups.labels.unsubscribe(5, 7)
    end
  end
end
