require File.expand_path 'test_helper.rb', __dir__

class CommitTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_commit
    stub_get('/projects/300/repository/commits/1', 'commit')
    commit = @client.commits.show(300, 1)

    assert_kind_of LabClient::Commit, commit
    assert_kind_of String, commit.inspect

    # Individual
    stub_get('/projects/300', 'project')
    assert_kind_of LabClient::Project, commit.project
    assert_kind_of String, commit.web_url
  end

  def test_commits_list
    stub_get('/projects/16/repository/commits', 'commits')

    list = @client.commits.list(16)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::Commit, list.first
  end

  def test_project_helper
    stub_get('/projects/16/repository/commits', 'commits')

    # Individual
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)

    assert_kind_of LabClient::PaginatedResponse, project.commits
    assert_kind_of LabClient::Commit, project.commits.first
  end

  def test_project_commit_show
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/repository/commits/1', 'commit')

    project = @client.projects.show(16)
    assert_kind_of LabClient::Commit, project.commit(1)
  end

  def test_commit_create
    stub_post('/projects/16/repository/commits', 'commit')

    assert_kind_of LabClient::Commit, @client.commits.create(16, {})

    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::Commit, project.commit_create({})
  end

  def test_commit_refs
    commit_id = 'b539e5d3179825f68f12e17ee1767f6093b055f3'
    stub_get("/projects/16/repository/commits/#{commit_id}/refs?scope=all", 'commit_refs')
    stub_get("/projects/16/repository/commits/#{commit_id}", 'commit')
    stub_get('/projects/16', 'project')

    # Root
    assert_kind_of LabClient::PaginatedResponse, @client.commits.refs(16, commit_id)
    assert_kind_of LabClient::LabStruct, @client.commits.refs(16, commit_id).first

    commit = @client.commits.show(16, commit_id)
    assert_kind_of LabClient::LabStruct, commit.refs.first

    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.commit_refs(commit_id).first
  end

  def test_commit_merge_requests
    commit_id = 'b539e5d3179825f68f12e17ee1767f6093b055f3'
    stub_get("/projects/16/repository/commits/#{commit_id}/merge_requests", 'merge_requests')
    stub_get("/projects/16/repository/commits/#{commit_id}", 'commit')
    stub_get('/projects/16', 'project')

    # Root
    assert_kind_of LabClient::PaginatedResponse, @client.commits.merge_requests(16, commit_id)
    assert_kind_of LabClient::MergeRequest, @client.commits.merge_requests(16, commit_id).first

    commit = @client.commits.show(16, commit_id)
    assert_kind_of LabClient::MergeRequest, commit.merge_requests.first

    project = @client.projects.show(16)
    assert_kind_of LabClient::MergeRequest, project.commit_merge_requests(commit_id).first
  end

  def test_commit_diff
    commit_id = 'b539e5d3179825f68f12e17ee1767f6093b055f3'
    stub_get("/projects/16/repository/commits/#{commit_id}/diff", 'commit_diffs')
    stub_get("/projects/16/repository/commits/#{commit_id}", 'commit')
    stub_get('/projects/16', 'project')

    # Root
    assert_kind_of LabClient::PaginatedResponse, @client.commits.diff(16, commit_id)
    assert_kind_of LabClient::CommitDiff, @client.commits.diff(16, commit_id).first

    commit = @client.commits.show(16, commit_id)
    assert_kind_of LabClient::CommitDiff, commit.diff.first

    project = @client.projects.show(16)
    assert_kind_of LabClient::CommitDiff, project.commit_diff(commit_id).first
  end

  def test_commit_comments
    commit_id = 'b539e5d3179825f68f12e17ee1767f6093b055f3'
    stub_get("/projects/16/repository/commits/#{commit_id}/comments", 'commit_comments')
    stub_get("/projects/16/repository/commits/#{commit_id}", 'commit')
    stub_get('/projects/16', 'project')

    # Root
    assert_kind_of LabClient::PaginatedResponse, @client.commits.comments(16, commit_id)
    assert_kind_of LabClient::CommitComment, @client.commits.comments(16, commit_id).first

    commit = @client.commits.show(16, commit_id)
    assert_kind_of LabClient::CommitComment, commit.comments.first

    project = @client.projects.show(16)
    assert_kind_of LabClient::CommitComment, project.commit_comments(commit_id).first
  end

  def test_commit_comment_create
    commit_id = 'b539e5d3179825f68f12e17ee1767f6093b055f3'
    stub_post("/projects/16/repository/commits/#{commit_id}/comments", 'commit_comment')
    stub_get("/projects/16/repository/commits/#{commit_id}", 'commit')
    stub_get('/projects/16', 'project')

    # Root
    assert_kind_of LabClient::CommitComment, @client.commits.comment_create(16, commit_id, {})

    # Commit
    commit = @client.commits.show(16, commit_id)
    assert_kind_of LabClient::CommitComment, commit.comment({})

    project = @client.projects.show(16)
    assert_kind_of LabClient::CommitComment, project.commit_comment_create(commit_id, {})
  end

  def test_commit_cherry_pick
    commit_id = 'b539e5d3179825f68f12e17ee1767f6093b055f3'
    stub_post("/projects/16/repository/commits/#{commit_id}/cherry_pick", 'commit')
    stub_get("/projects/16/repository/commits/#{commit_id}", 'commit')
    stub_get('/projects/16', 'project')

    # Root
    assert_kind_of LabClient::Commit, @client.commits.cherry_pick(16, commit_id, :master)

    commit = @client.commits.show(16, commit_id)
    assert_kind_of LabClient::Commit, commit.cherry_pick(:master)

    project = @client.projects.show(16)
    assert_kind_of LabClient::Commit, project.commit_cherry_pick(commit_id, :master)
  end

  def test_commit_revert
    commit_id = 'b539e5d3179825f68f12e17ee1767f6093b055f3'
    stub_post("/projects/16/repository/commits/#{commit_id}/revert", 'commit')
    stub_get("/projects/16/repository/commits/#{commit_id}", 'commit')
    stub_get('/projects/16', 'project')

    # Root
    assert_kind_of LabClient::Commit, @client.commits.revert(16, commit_id, :master)

    commit = @client.commits.show(16, commit_id)
    assert_kind_of LabClient::Commit, commit.revert(:master)

    project = @client.projects.show(16)
    assert_kind_of LabClient::Commit, project.commit_revert(commit_id, :master)
  end

  def test_commit_status
    commit_id = 'b539e5d3179825f68f12e17ee1767f6093b055f3'
    stub_get("/projects/16/repository/commits/#{commit_id}/statuses", 'commit_statuses')
    stub_get("/projects/16/repository/commits/#{commit_id}", 'commit')
    stub_get('/projects/16', 'project')

    # Root
    assert_kind_of LabClient::PaginatedResponse, @client.commits.status(16, commit_id)
    assert_kind_of LabClient::CommitStatus, @client.commits.status(16, commit_id).first

    commit = @client.commits.show(16, commit_id)
    status = commit.status.first
    assert_kind_of String, status.inspect
    assert_kind_of LabClient::CommitStatus, status

    project = @client.projects.show(16)
    assert_kind_of LabClient::CommitStatus, project.commit_status(commit_id).first
  end

  def test_commit_status_update
    commit_id = 'b539e5d3179825f68f12e17ee1767f6093b055f3'
    stub_post("/projects/16/statuses/#{commit_id}", 'commit_status')
    stub_get("/projects/16/repository/commits/#{commit_id}", 'commit')
    stub_get('/projects/16', 'project')

    # Root
    assert_kind_of LabClient::CommitStatus, @client.commits.status_update(16, commit_id, {})

    commit = @client.commits.show(16, commit_id)
    assert_kind_of LabClient::CommitStatus, commit.status_update({})

    project = @client.projects.show(16)
    assert_kind_of LabClient::CommitStatus, project.commit_status_update(commit_id, {})
  end
end
