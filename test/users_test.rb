require File.expand_path 'test_helper.rb', __dir__

class UserTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_user
    stub_get('/users/5', 'user')
    user = @client.users.show(5)

    assert_kind_of LabClient::User, user
    assert_kind_of String, user.email
    assert_kind_of String, user.inspect

    %i[confirmed_at current_sign_in_at last_activity_on created_at last_sign_in_at].each do |kind|
      assert_kind_of DateTime, user.send(kind)
    end
  end

  def test_user_show
    stub_get('/users/5', 'user')
    user = @client.users.show(5)

    assert_kind_of LabClient::User, user

    # Test Reload
    assert_kind_of LabClient::User, user.reload
  end

  def test_user_projects
    stub_get('/users/5', 'user')
    stub_get('/users/5/projects', 'projects')
    user = @client.users.show(5)

    projects = user.projects

    assert_kind_of LabClient::PaginatedResponse, projects
    assert_equal 3, projects.size
    assert_kind_of LabClient::Project, projects.first
  end

  def test_user_starred
    stub_get('/users/5', 'user')
    stub_get('/users/5/projects', 'projects')
    user = @client.users.show(5)

    projects = user.starred

    assert_kind_of LabClient::PaginatedResponse, projects
    assert_equal 3, projects.size
    assert_kind_of LabClient::Project, projects.first
  end

  def test_users_show
    stub_get('/users', 'users')
    users = @client.users.list

    assert_kind_of LabClient::PaginatedResponse, users
    assert_equal users.size, 2
    assert_kind_of LabClient::User, users.first
  end

  def test_users_list
    stub_get('/users', 'users')
    users = @client.users.list

    assert_kind_of LabClient::PaginatedResponse, users
    assert_equal users.size, 2
    assert_kind_of LabClient::User, users.first
  end

  def test_user_search
    stub_get('/users', 'users')
    stub_get('/search?scope=users&search=term', 'users')
    users = @client.users.search('term')

    assert_kind_of LabClient::PaginatedResponse, users
    assert_equal users.size, 2
    assert_kind_of LabClient::User, users.first
  end

  def test_users_create
    stub_post('/users', 'user')
    user = @client.users.create({})

    assert_kind_of LabClient::User, user
  end

  def test_users_update
    stub_get('/users/5', 'user')
    stub_put('/users/5', 'user')

    # Top
    assert_kind_of LabClient::User, @client.users.update(5, {})
    # Via User

    user = @client.users.show(5)
    assert_kind_of LabClient::User, user.update({})
  end

  def test_users_delete
    stub_get('/users/5', 'user')
    stub_delete('/users/5')

    # Top
    resp = @client.users.delete(5)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via User
    user = @client.users.show(5)
    assert_nil user.delete.data
  end

  def test_users_delete_identity
    stub_get('/users/5', 'user')
    stub_delete('/users/5/identities/ldapmain')

    # Top
    resp = @client.users.delete_identity(5, 'ldapmain')
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via User
    user = @client.users.show(5)
    assert_nil user.delete_identity('ldapmain').data
  end

  def test_users_current
    stub_get('/user', 'user')
    stub_get('/user?sudo=12', 'user')

    # Top
    assert_kind_of LabClient::User, @client.users.current
    assert_kind_of LabClient::User, @client.users.current(12)
  end

  def test_users_status
    stub_get('/users/5', 'user')
    stub_get('/user/status', 'user_status')
    stub_get('/users/5/status', 'user_status')

    # Top
    assert_kind_of LabClient::LabStruct, @client.users.status
    assert_kind_of LabClient::LabStruct, @client.users.status(5)

    # Via User
    user = @client.users.show(5)
    assert_kind_of LabClient::LabStruct, user.status
  end

  def test_users_status_helpers
    stub_get('/users/5', 'user')

    # Via User
    user = @client.users.show(5)
    assert user.active?
    refute user.inactive?
  end

  def test_users_set_status
    stub_put('/user/status', 'user_status')

    # Top
    assert_kind_of LabClient::LabStruct, @client.users.status_set({})
  end

  def test_users_counts
    stub_get('/user_counts', 'user_counts')

    # Top
    assert_kind_of LabClient::LabStruct, @client.users.counts
  end

  def test_users_keys_list
    stub_get('/users/5', 'user')
    stub_get('/users/5/keys', 'user_keys')
    stub_get('/user/keys', 'user_keys')

    # Current User
    list = @client.users.keys.list
    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::Key, list.first

    # Specific User
    list = @client.users.keys.list(5)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::Key, list.first

    assert_kind_of LabClient::User, list.first.user

    # Via User
    user = @client.users.show(5)
    assert_kind_of LabClient::Key, user.keys.first
  end

  def test_users_key_show
    stub_get('/user/keys/3', 'user_key')
    assert_kind_of LabClient::Key, @client.users.keys.show(3)
  end

  def test_users_key_create
    stub_get('/users/5', 'user')
    stub_post('/users/5/keys', 'user_key')
    stub_post('/user/keys', 'user_key')

    # Top
    assert_kind_of LabClient::Key, @client.users.keys.create({})
    assert_kind_of LabClient::Key, @client.users.keys.create({}, 5)

    # Via User
    user = @client.users.show(5)
    assert_kind_of LabClient::Key, user.key_create({})
  end

  def test_users_key_delete
    stub_get('/users/5', 'user')
    stub_delete('/users/5/keys/3')
    stub_delete('/user/keys/3')

    # Top
    resp = @client.users.keys.delete(3)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    resp = @client.users.keys.delete(3, 5)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via User
    user = @client.users.show(5)
    assert_nil user.key_delete(3).data
  end

  def test_users_block
    stub_get('/users/5', 'user')
    stub_post('/users/5/block')

    # Top
    resp = @client.users.block(5)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via User
    user = @client.users.show(5)
    assert_nil user.block.data
  end

  def test_users_unblock
    stub_get('/users/5', 'user')
    stub_post('/users/5/unblock')

    # Top
    resp = @client.users.unblock(5)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via User
    user = @client.users.show(5)
    assert_nil user.unblock.data
  end

  def test_users_activate
    stub_get('/users/5', 'user')
    stub_post('/users/5/activate')

    # Top
    resp = @client.users.activate(5)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via User
    user = @client.users.show(5)
    assert_nil user.activate.data
  end

  def test_users_deactivate
    stub_get('/users/5', 'user')
    stub_post('/users/5/deactivate')

    # Top
    resp = @client.users.deactivate(5)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via User
    user = @client.users.show(5)
    assert_nil user.deactivate.data
  end

  def test_users_activity
    stub_get('/user/activities?from=2020-05-19T00:00:00Z', 'user_activity')
    # Top
    # Better to have a Parameter / Each Client interprets this differently
    list = @client.users.activity('2020-05-19T00:00:00Z')
    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::LabStruct, list.first
  end

  def test_users_memberships
    stub_get('/users/5', 'user')
    stub_get('/users/5/memberships', 'memberships')

    # Top
    list = @client.users.memberships(5)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_kind_of LabClient::Membership, list.first

    # User
    user = @client.users.show(5)
    assert_kind_of LabClient::Membership, user.memberships.first
  end
end
