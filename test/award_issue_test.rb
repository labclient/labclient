require File.expand_path 'test_helper.rb', __dir__

class AwardIssueTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::Award
  end

  def test_issue_award_show
    stub_get('/projects/1/issues/2/award_emoji/21', 'award_issue')
    award = @client.awards.issues.show(1, 2, 21)

    assert_kind_of kind, award
    assert_kind_of String, award.inspect
  end

  def test_issue_award_list
    stub_get('/projects/1/issues/2/award_emoji', 'award_issues')
    list = @client.awards.issues.list(1, 2)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of kind, list.first
  end

  def test_issue_award_create
    stub_post('/projects/1/issues/2/award_emoji', 'award_issue')
    award = @client.awards.issues.create(1, 2, {})
    assert_kind_of kind, award
  end

  def test_issue_award_delete
    stub_delete('/projects/1/issues/2/award_emoji/21')

    resp = @client.awards.issues.delete(1, 2, 21)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end
end
