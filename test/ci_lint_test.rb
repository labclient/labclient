require File.expand_path 'test_helper.rb', __dir__

class CiLintTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_lint
    stub_post('/ci/lint', 'ci_lint', 200, content: 'rawr')
    result = @client.ci_lint.check('rawr')
    assert_kind_of LabClient::LabStruct, result
  end
end
