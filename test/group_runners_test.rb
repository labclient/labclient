require File.expand_path 'test_helper.rb', __dir__
class GroupRunnerTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_runner_list
    stub_get('/groups/5/runners', 'runners')
    stub_get('/groups/5', 'group')
    list = @client.groups.runners(5)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::Runner, list.first

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of LabClient::Runner, group.runners.first
  end
end
