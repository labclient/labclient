require File.expand_path 'test_helper.rb', __dir__
class ProjectAccessRequestTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/projects/16', 'project')
  end

  def kind
    LabClient::ProjectAccessRequest
  end

  def test_access_request_list
    stub_get('/projects/16/access_requests', 'access_requests')
    stub_get('/users/5', 'user')

    assert_kind_of LabClient::PaginatedResponse, @client.projects.access_requests.list(16)

    # Via Project
    project = @client.projects.show(16)
    request = project.access_requests.first
    assert_kind_of kind, request

    # Via Requests
    assert_kind_of String, request.inspect

    # Methods
    assert_kind_of LabClient::User, request.user
  end

  def test_access_request_create
    stub_post('/projects/16/access_requests', 'access_request')

    assert_kind_of kind, @client.projects.access_requests.create(16)

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.request_access
  end

  def test_access_request_approve
    stub_get('/projects/16/access_requests', 'access_requests')
    stub_put('/projects/16/access_requests/5/approve', 'access_request')

    assert_kind_of kind, @client.projects.access_requests.approve(16, 5)

    # Via Access Request
    request = @client.projects.access_requests.list(16).first
    assert_kind_of kind, request.approve
  end

  def test_access_request_deny
    stub_get('/projects/16/access_requests', 'access_requests')
    stub_delete('/projects/16/access_requests/5/deny')

    resp = @client.projects.access_requests.deny(16, 5)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Access Request
    request = @client.projects.access_requests.list(16).first
    assert_nil request.deny.data
  end
end
