require File.expand_path 'test_helper.rb', __dir__

class ProjectShowTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_project
    stub_get('/projects/5', 'project')
    project = @client.projects.show(5)

    assert_kind_of LabClient::Project, project
    assert_kind_of String, project.path
    assert_kind_of String, project.inspect

    %i[last_activity_at created_at].each do |kind|
      assert_kind_of DateTime, project.send(kind)
    end

    # Project Fixture doesn't include owner
    assert_nil project.owner
  end
end
