require File.expand_path 'test_helper.rb', __dir__

class ProjectMilestonesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::ProjectMilestone
  end

  def test_milestone
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/milestones/20', 'project_milestone')
    milestone = @client.projects.milestones.show(16, 20)

    assert_kind_of kind, milestone
    assert_kind_of String, milestone.inspect
    assert_kind_of LabClient::Project, milestone.project
  end

  def test_milestone_list
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/milestones', 'project_milestones')
    milestones = @client.projects.milestones.list(16)

    assert_kind_of LabClient::PaginatedResponse, milestones
    assert_equal milestones.size, 2
    assert_kind_of kind, milestones.first

    # via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.milestones.first
  end

  def test_create_milestone
    stub_post('/projects/16/milestones', 'project_milestone')

    milestone = @client.projects.milestones.create(16, {})

    assert_kind_of kind, milestone
    assert_kind_of String, milestone.inspect
  end

  def test_update_milestone
    stub_get('/projects/16/milestones/20', 'project_milestone')
    stub_put('/projects/16/milestones/20', 'project_milestone')

    assert_kind_of kind, @client.projects.milestones.update(16, 20, {})

    milestone = @client.projects.milestones.show(16, 20)
    milestone.update({})
    assert_kind_of kind, milestone
  end

  def test_milestone_issues
    stub_get('/projects/16/milestones/20', 'project_milestone')
    stub_get('/projects/16/milestones/20/issues', 'issues')

    list = @client.projects.milestones.issues(16, 20)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::Issue, list.first

    # via Milestone
    milestone = @client.projects.milestones.show(16, 20)
    assert_kind_of LabClient::Issue, milestone.issues.first
  end

  def test_milestone_merge_requests
    stub_get('/projects/16/milestones/20', 'project_milestone')
    stub_get('/projects/16/milestones/20/merge_requests', 'merge_requests')

    list = @client.projects.milestones.merge_requests(16, 20)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of LabClient::MergeRequest, list.first

    # via Milestone
    milestone = @client.projects.milestones.show(16, 20)
    assert_kind_of LabClient::MergeRequest, milestone.merge_requests.first
  end

  def test_delete_milestone
    stub_get('/projects/16/milestones/20', 'project_milestone')
    stub_delete('/projects/16/milestones/20')

    resp = @client.projects.milestones.delete(16, 20)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # via Milestone
    milestone = @client.projects.milestones.show(16, 20)
    assert_nil milestone.delete.data
  end

  def test_promote_milestone
    stub_get('/projects/16/milestones/20', 'project_milestone')
    stub_post('/projects/16/milestones/20/promote')

    resp = @client.projects.milestones.promote(16, 20)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # via Milestone
    milestone = @client.projects.milestones.show(16, 20)
    assert_nil milestone.promote.data
  end

  def test_burndown_milestone
    stub_get('/projects/16/milestones/20', 'project_milestone')
    stub_get('/projects/16/milestones/20/burndown_events', 'project_milestone_burndown')

    assert_kind_of LabClient::PaginatedResponse, @client.projects.milestones.burndown(16, 20)

    # via Milestone
    milestone = @client.projects.milestones.show(16, 20)
    assert_kind_of LabClient::PaginatedResponse, milestone.burndown
  end
end
