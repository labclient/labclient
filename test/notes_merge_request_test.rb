require File.expand_path 'test_helper.rb', __dir__

class MergeRequestNotesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::Note
  end

  def test_merge_request_note_show
    stub_get('/projects/1/merge_requests/2/notes/21', 'merge_request_note')
    note = @client.notes.merge_requests.show(1, 2, 21)

    assert_kind_of kind, note
    assert_kind_of String, note.inspect
  end

  def test_merge_request_note_list
    stub_get('/projects/1/merge_requests/2/notes', 'merge_request_notes')
    list = @client.notes.merge_requests.list(1, 2)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of kind, list.first
  end

  def test_merge_request_note_create
    stub_post('/projects/333/merge_requests/1/notes', 'merge_request_note')
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    note = @client.notes.merge_requests.create(333, 1, {})
    assert_kind_of kind, note

    # Test On MR
    mr = @client.merge_requests.show(333, 1)
    assert_kind_of kind, mr.note_create({})
  end

  def test_merge_request_note_update
    stub_put('/projects/1/merge_requests/2/notes/21', 'merge_request_note')
    note = @client.notes.merge_requests.update(1, 2, 21, :body)
    assert_kind_of kind, note
  end

  def test_merge_request_note_delete
    stub_delete('/projects/1/merge_requests/2/notes/21')

    resp = @client.notes.merge_requests.delete(1, 2, 21)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end
end
