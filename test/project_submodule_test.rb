require File.expand_path 'test_helper.rb', __dir__

class ProjectSubModuleTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_project_submodule
    stub_put('/projects/16/repository/submodules/module/path', 'repository_submodule')
    stub_get('/projects/16', 'project')

    assert_kind_of LabClient::Commit, @client.projects.submodule(16, 'module/path', {})

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Commit, project.submodule('module/path', {})
  end
end
