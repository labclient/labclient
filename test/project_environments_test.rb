require File.expand_path 'test_helper.rb', __dir__

class ProjectEnvironmentsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::ProjectEnvironment
  end

  def test_project_environment
    stub_get('/projects/16/environments', 'project_environments')
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    environment = project.environments.first

    assert_kind_of String, environment.inspect
    assert_kind_of LabClient::Project, environment.project
  end

  def test_environment_search
    stub_get('/projects/16/environments?search=search', 'project_environments')

    list = @client.projects.environments.list(16, 'search')
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of kind, list.first
  end

  def test_project_environments_show
    stub_get('/projects/16/environments/14', 'project_environment')
    stub_get('/projects/16', 'project')

    environment = @client.projects.environments.show(16, 14)
    assert_kind_of kind, environment
    assert_kind_of LabClient::Project, environment.project
    assert_kind_of String, environment.inspect

    # Collect Extra Details
    assert_kind_of kind, environment.show

    project = @client.projects.show(16)
    assert_kind_of kind, project.environment_show(14)
  end

  def test_project_environments_create
    stub_post('/projects/16/environments', 'project_environment')
    stub_get('/projects/16', 'project')

    environment = @client.projects.environments.create(16, url: 'value')
    assert_kind_of kind, environment

    project = @client.projects.show(16)
    assert_kind_of kind, project.environment_create(url: 'value')
  end

  def test_project_environments_update
    stub_put('/projects/16/environments/14', 'project_environment')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/environments/14', 'project_environment')

    # Root
    environment = @client.projects.environments.update(16, 14, url: 'value')
    assert_kind_of kind, environment

    # Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.environment_update(14, url: 'value')

    # Environment
    environment = @client.projects.environments.show(16, 14)
    assert_kind_of kind, environment.update(url: 'value')
  end

  def test_project_environments_delete
    stub_delete('/projects/16/environments/14')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/environments/14', 'project_environment')

    # Root
    resp = @client.projects.environments.delete(16, 14)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Project
    project = @client.projects.show(16)
    assert_nil project.environment_delete(14).data

    # Environment
    environment = @client.projects.environments.show(16, 14)
    assert_nil environment.delete.data
  end

  def test_project_environments_stop
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/environments/14', 'project_environment')
    stub_post('/projects/16/environments/14/stop')

    # Project
    project = @client.projects.show(16)
    resp = project.environment_stop(14)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Environment
    environment = @client.projects.environments.show(16, 14)
    assert_nil environment.stop.data
  end
end
