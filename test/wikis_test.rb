require File.expand_path 'test_helper.rb', __dir__
class WikiTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_wikis_list
    stub_get('/projects/16/wikis?with_content=false', 'wikis')
    list = @client.wikis.list(16)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::Wiki, list.first

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::Wiki, project.wikis.first
  end

  def test_wikis_show
    stub_get('/projects/16/wikis/home', 'wiki')
    stub_get('/projects/16', 'project')

    wiki = @client.wikis.show(16, :home)
    assert_kind_of LabClient::Wiki, wiki
    assert_kind_of String, wiki.inspect
    assert_kind_of LabClient::Project, wiki.project
    assert_kind_of String, wiki.web_url

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::Wiki, project.wiki(:home)
  end

  def test_wikis_create
    stub_post('/projects/16/wikis', 'wiki')
    wiki = @client.wikis.create(16, {})
    assert_kind_of LabClient::Wiki, wiki

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::Wiki, project.wiki_create({})
  end

  def test_wikis_update
    stub_put('/projects/16/wikis/home', 'wiki')
    wiki = @client.wikis.update(16, :home, {})
    assert_kind_of LabClient::Wiki, wiki

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::Wiki, project.wiki_update(:home, {})

    # Via Wiki
    stub_get('/projects/16/wikis/home', 'wiki')
    wiki = @client.wikis.show(16, :home)
    assert_kind_of LabClient::Wiki, wiki.update({})
  end

  def test_wikis_delete
    stub_delete('/projects/16/wikis/home')

    resp = @client.wikis.delete(16, :home)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_nil project.wiki_delete(:home).data

    # Via Wiki
    stub_get('/projects/16/wikis/home', 'wiki')
    wiki = @client.wikis.show(16, :home)
    assert_nil wiki.delete.data
  end

  def test_wiki_upload
    stub_request(:any, 'https://labclient/api/v4/projects/16/wikis/attachments')
      .to_return(
        status: 200,
        body: File.new("#{File.dirname(__FILE__)}/fixtures/wiki_upload.json"),
        headers: { 'content-type' => ['application/json'] }
      )

    upload = @client.wikis.upload(16, 'test/fixtures/upload.txt')
    assert_kind_of LabClient::LabStruct, upload

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.wiki_upload('test/fixtures/upload.txt')
  end
end
