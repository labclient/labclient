require File.expand_path 'test_helper.rb', __dir__

class ResourceLabelIssueTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::ResourceLabel
  end

  def test_issue_list
    stub_get('/projects/30/issues/1/resource_label_events', 'resource_labels')
    stub_get('/projects/30/issues/1', 'issue')

    list = @client.resource_labels.issues.list(30, 1)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size

    # Via Issue
    issue = @client.issues.show(30, 1)
    assert_kind_of LabClient::PaginatedResponse, issue.resource_labels
  end

  def test_issue_show
    stub_get('/projects/30/issues/1/resource_label_events/7', 'resource_label')
    stub_get('/projects/30/issues/1', 'issue')

    resource = @client.resource_labels.issues.show(30, 1, 7)
    assert_kind_of kind, resource
    assert_kind_of String, resource.inspect

    # Via Issue
    issue = @client.issues.show(30, 1)
    assert_kind_of kind, issue.resource_label(7)
  end
end
