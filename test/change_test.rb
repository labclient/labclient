require File.expand_path 'test_helper.rb', __dir__

class ChangeTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_changes
    stub_get('/projects/333/merge_requests/1/changes', 'changes')
    change = @client.merge_requests.changes(333, 1)

    assert_kind_of String, change.inspect

    change.assignees.each do |kind|
      assert_kind_of LabClient::User, kind
    end

    assert_kind_of LabClient::Changes, change
  end
end
