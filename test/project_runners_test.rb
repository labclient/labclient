require File.expand_path 'test_helper.rb', __dir__
class ProjectRunnerTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_runner_list
    stub_get('/projects/16/runners', 'runners')
    stub_get('/projects/16', 'project')
    list = @client.projects.runners.list(16)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::Runner, list.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Runner, project.runners.first
  end

  def test_runner_enable
    stub_post('/projects/16/runners', 'runner')
    stub_get('/projects/16', 'project')

    runner = @client.projects.runners.enable(16, 3)
    assert_kind_of LabClient::Runner, runner

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Runner, project.runner_enable(3)
  end

  def test_runner_disable
    stub_delete('/projects/16/runners/3')
    stub_get('/projects/16', 'project')

    resp = @client.projects.runners.disable(16, 3)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Project
    project = @client.projects.show(16)
    assert_nil project.runner_disable(3).data
  end
end
