require File.expand_path 'test_helper.rb', __dir__

class ResourceLabelMergeRequestTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::ResourceLabel
  end

  def test_merge_request_list
    stub_get('/projects/333/merge_requests/1/resource_label_events', 'resource_labels')
    stub_get('/projects/333/merge_requests/1', 'merge_request')

    list = @client.resource_labels.merge_requests.list(333, 1)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size

    # Via MergeRequest
    merge_request = @client.merge_requests.show(333, 1)
    assert_kind_of LabClient::PaginatedResponse, merge_request.resource_labels
  end

  def test_merge_request_show
    stub_get('/projects/333/merge_requests/1/resource_label_events/7', 'resource_label')
    stub_get('/projects/333/merge_requests/1', 'merge_request')

    resource = @client.resource_labels.merge_requests.show(333, 1, 7)
    assert_kind_of kind, resource
    assert_kind_of String, resource.inspect

    # Via MergeRequest
    merge_request = @client.merge_requests.show(333, 1)
    assert_kind_of kind, merge_request.resource_label(7)
  end
end
