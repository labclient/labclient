require File.expand_path 'test_helper.rb', __dir__

class ResourceLabelEpicTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::ResourceLabel
  end

  def test_epic_list
    stub_get('/groups/5/epics/6/resource_label_events', 'resource_labels')
    stub_get('/groups/5/epics/6', 'epic')

    list = @client.resource_labels.epics.list(5, 6)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size

    # Via Epic
    epic = @client.epics.show(5, 6)
    assert_kind_of LabClient::PaginatedResponse, epic.resource_labels
  end

  def test_epic_show
    stub_get('/groups/5/epics/6/resource_label_events/7', 'resource_label')
    stub_get('/groups/5/epics/6', 'epic')

    resource = @client.resource_labels.epics.show(5, 6, 7)
    assert_kind_of kind, resource
    assert_kind_of String, resource.inspect

    # Via Epic
    epic = @client.epics.show(5, 6)
    assert_kind_of kind, epic.resource_label(7)
  end
end
