require File.expand_path 'test_helper.rb', __dir__

class TagsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/projects/16', 'project')
  end

  def kind
    LabClient::Tag
  end

  def test_tags_list
    stub_get('/projects/16/repository/tags', 'tags')

    list = @client.tags.list(16)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of kind, list.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.tags.first
  end

  def test_tags_show
    stub_get('/projects/16/repository/tags/initial', 'tag')

    tag = @client.tags.show(16, :initial)
    assert_kind_of kind, tag
    assert_kind_of String, tag.inspect
    assert_kind_of LabClient::Commit, tag.commit

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.tag(:initial)
  end

  def test_tags_create
    stub_post('/projects/16/repository/tags', 'tag')

    tag = @client.tags.create(16, {})
    assert_kind_of kind, tag

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.tag_create({})
  end

  def test_tags_delete
    stub_delete('/projects/16/repository/tags/delete')

    resp = @client.tags.delete(16, :delete)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Project
    project = @client.projects.show(16)
    assert_nil project.tag_delete(:delete).data
  end

  def test_tags_release
    stub_post('/projects/16/repository/tags/youareit/release', 'tag_release')

    assert_kind_of LabClient::LabStruct, @client.tags.release(16, :youareit, :notouchbacks)

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.tag_release(:youareit, :notouchbacks)
  end

  def test_tags_release_update
    stub_put('/projects/16/repository/tags/youareit/release', 'tag_release')

    assert_kind_of LabClient::LabStruct, @client.tags.update(16, :youareit, :notouchbacks)

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.tag_update(:youareit, :notouchbacks)
  end
end
