require File.expand_path 'test_helper.rb', __dir__

class BranchesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token', quiet: true)
    stub_get('/projects/16', 'project')
  end

  def test_branch_list
    stub_get('/projects/16/repository/branches', 'branches')

    list = @client.branches.list(16)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::Branch, list.first

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Branch, project.branches.first
  end

  def test_branch_search
    stub_get('/projects/16/repository/branches?search=search', 'branches')

    list = @client.branches.list(16, 'search')
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::Branch, list.first
  end

  def test_branch_show
    stub_get('/projects/16/repository/branches/master', 'branch')

    branch = @client.branches.show(16, :master)
    assert_kind_of LabClient::Branch, branch
    assert_kind_of String, branch.inspect
    assert_kind_of LabClient::Commit, branch.commit

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Branch, project.branch(:master)
  end

  def test_branch_create
    stub_post('/projects/16/repository/branches', 'branch')

    branch = @client.branches.create(16, {})
    assert_kind_of LabClient::Branch, branch
    assert_kind_of String, branch.inspect

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::Branch, project.branch_create({})
  end

  def test_branch_delete
    stub_delete('/projects/16/repository/branches/master')

    resp = @client.branches.delete(16, :master)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Project
    project = @client.projects.show(16)
    assert_nil project.branch_delete(:master).data
  end

  def test_branch_delete_merged
    stub_delete('/projects/16/repository/merged_branches', 'delete_merged')

    assert_kind_of LabClient::LabStruct, @client.branches.delete_merged(16)

    # Via Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::LabStruct, project.branch_delete_merged
  end

  # rubocop:disable Metrics/MethodLength
  def test_merge_request_wait_for_merge_status
    stub_get('/projects/16/repository/branches/feature', 'branch')

    first = { status: 200, body: load_fixture('pipelines_empty'), headers: {
      'content-type' => ['application/json']
    } }

    second = { status: 200, body: load_fixture('pipelines_empty'), headers: {
      'content-type' => ['application/json']
    } }

    third = { status: 200, body: load_fixture('pipelines'), headers: {
      'content-type' => ['application/json']
    } }

    stub_request(:get, 'https://labclient/api/v4/projects/16/pipelines?ref=feature')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
          'Private-Token' => 'token'
        }
      )
      .to_return([first, second, third])

    branch = @client.branches.show(16, :feature)
    assert_nil branch.wait_for_pipelines(5, 0.1)
  end
  # rubocop:enable Metrics/MethodLength
end
