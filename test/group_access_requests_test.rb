require File.expand_path 'test_helper.rb', __dir__
class GroupAccessRequestTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/groups/5', 'group')
  end

  def kind
    LabClient::GroupAccessRequest
  end

  def test_access_request_list
    stub_get('/groups/5/access_requests', 'access_requests')
    stub_get('/users/5', 'user')

    assert_kind_of LabClient::PaginatedResponse, @client.groups.access_requests.list(5)

    # Via Group
    group = @client.groups.show(5)
    request = group.access_requests.first
    assert_kind_of kind, request

    # Via Requests
    assert_kind_of String, request.inspect

    # Methods
    assert_kind_of LabClient::User, request.user
  end

  def test_access_request_create
    stub_post('/groups/5/access_requests', 'access_request')

    assert_kind_of kind, @client.groups.access_requests.create(5)

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of kind, group.request_access
  end

  def test_access_request_approve
    stub_get('/groups/5/access_requests', 'access_requests')
    stub_put('/groups/5/access_requests/5/approve', 'access_request')

    assert_kind_of kind, @client.groups.access_requests.approve(5, 5)

    # Via Access Request
    request = @client.groups.access_requests.list(5).first
    assert_kind_of kind, request.approve
  end

  def test_access_request_deny
    stub_get('/groups/5/access_requests', 'access_requests')
    stub_delete('/groups/5/access_requests/5/deny')

    resp = @client.groups.access_requests.deny(5, 5)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Access Request
    request = @client.groups.access_requests.list(5).first
    assert_nil request.deny.data
  end
end
