require File.expand_path 'test_helper.rb', __dir__

class IssueNotesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::Note
  end

  def test_issue_note_show
    stub_get('/projects/1/issues/2/notes/21', 'issue_note')
    note = @client.notes.issues.show(1, 2, 21)

    assert_kind_of kind, note
    assert_kind_of String, note.inspect
  end

  def test_issue_note_list
    stub_get('/projects/1/issues/2/notes', 'issue_notes')
    list = @client.notes.issues.list(1, 2)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of kind, list.first
  end

  def test_issue_note_create
    stub_post('/projects/30/issues/1/notes', 'issue_note')
    stub_get('/projects/30/issues/1', 'issue')
    note = @client.notes.issues.create(30, 1, {})
    assert_kind_of kind, note

    # Test On Issue
    issue = @client.issues.show(30, 1)
    assert_kind_of kind, issue.note_create({})
  end

  def test_issue_note_update
    stub_put('/projects/1/issues/2/notes/21', 'issue_note')
    note = @client.notes.issues.update(1, 2, 21, :body)
    assert_kind_of kind, note
  end

  def test_issue_note_delete
    stub_delete('/projects/1/issues/2/notes/21')

    resp = @client.notes.issues.delete(1, 2, 21)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data
  end
end
