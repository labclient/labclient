require File.expand_path 'test_helper.rb', __dir__

class ProjectSnippetsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::ProjectSnippet
  end

  def test_snippet
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/snippets/1', 'project_snippet')
    stub_get('/projects/16/snippets/1', 'project_snippet')
    stub_get('/projects/16/snippets/1/notes', 'snippet_notes')
    snippet = @client.projects.snippets.show(16, 1)

    assert_kind_of kind, snippet
    assert_kind_of String, snippet.inspect
    assert_kind_of LabClient::Project, snippet.project
    assert_kind_of LabClient::Note, snippet.notes.first
  end

  def test_snippet_list
    stub_get('/projects/16/snippets', 'snippets')
    stub_get('/projects/16', 'project')
    snippets = @client.projects.snippets.list(16)

    assert_kind_of LabClient::PaginatedResponse, snippets
    assert_equal snippets.size, 2
    assert_kind_of kind, snippets.first

    project = @client.projects.show(16)
    assert_kind_of kind, project.snippets.first
  end

  def test_create_snippet
    stub_post('/projects/16/snippets', 'snippet', 200, {})
    stub_get('/projects/16', 'project')

    snippet = @client.projects.snippets.create(16, {})

    assert_kind_of kind, snippet
    assert_kind_of String, snippet.inspect

    project = @client.projects.show(16)
    assert_kind_of kind, project.snippet_create({})
  end

  def test_raw_snippet
    stub_get('/projects/16/snippets/1', 'snippet')
    stub_get('/projects/16/snippets/1/raw', 'string')

    snippet = @client.projects.snippets.show(16, 1)
    assert_kind_of kind, snippet
    assert_kind_of String, snippet.raw.data

    resp = @client.projects.snippets.raw(16, 1)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_kind_of String, resp.data
  end

  def test_agent_detail_snippet
    stub_get('/projects/16/snippets/1', 'snippet')
    stub_get('/projects/16/snippets/1/user_agent_detail', 'user_agent_detail')
    snippet = @client.projects.snippets.show(16, 1)

    assert_kind_of kind, snippet
    assert_kind_of LabClient::LabStruct, snippet.agent_detail
    assert_kind_of LabClient::LabStruct, @client.projects.snippets.agent_detail(16, 1)
  end

  def test_update_snippet
    details = { description: 'Update', title: 'Update' }
    stub_get('/projects/16/snippets/1', 'snippet')
    stub_put('/projects/16/snippets/1', 'snippet', 200, details)

    snippet_direct = @client.projects.snippets.update(16, 1, details)

    snippet = @client.projects.snippets.show(16, 1)
    snippet.update(details)

    assert_kind_of kind, snippet
    assert_kind_of kind, snippet_direct
  end

  def test_delete_snippet
    stub_get('/projects/16/snippets/1', 'snippet')
    stub_delete('/projects/16/snippets/1')

    resp = @client.projects.snippets.delete(16, 1)

    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    snippet = @client.projects.snippets.show(16, 1)
    assert_nil snippet.delete.data
  end
end
