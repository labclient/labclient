require File.expand_path 'test_helper.rb', __dir__

class GroupClustersTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::GroupCluster
  end

  def test_cluster_show
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/clusters/7', 'group_cluster')

    cluster = @client.groups.clusters.show(5, 7)

    assert_kind_of kind, cluster
    assert_kind_of String, cluster.inspect
    assert_kind_of LabClient::Group, cluster.group

    # via Group
    group = @client.groups.show(5)
    assert_kind_of kind, group.cluster(7)
  end

  def test_cluster_list
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/clusters', 'group_clusters')

    clusters = @client.groups.clusters.list(5)

    assert_kind_of LabClient::PaginatedResponse, clusters
    assert_equal clusters.size, 2
    assert_kind_of kind, clusters.first

    # via Group
    group = @client.groups.show(5)
    assert_kind_of kind, group.clusters.first
  end

  def test_add_cluster
    stub_post('/groups/5/clusters/user', 'group_cluster')

    cluster = @client.groups.clusters.add(5, {})

    assert_kind_of kind, cluster
    assert_kind_of String, cluster.inspect
  end

  def test_update_cluster
    stub_get('/groups/5/clusters/7', 'group_cluster')
    stub_put('/groups/5/clusters/7', 'group_cluster')

    assert_kind_of kind, @client.groups.clusters.update(5, 7, {})

    cluster = @client.groups.clusters.show(5, 7)
    cluster.update({})
    assert_kind_of kind, cluster
  end

  def test_delete_cluster
    stub_get('/groups/5/clusters/7', 'group_cluster')
    stub_delete('/groups/5/clusters/7')

    resp = @client.groups.clusters.delete(5, 7)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # via Project Cluster
    cluster = @client.groups.clusters.show(5, 7)
    assert_nil cluster.delete.data
  end
end
