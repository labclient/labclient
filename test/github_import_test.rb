require File.expand_path 'test_helper.rb', __dir__
class GithubImportTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_issues
    stub_post('/import/github', 'github_import')
    result = @client.projects.github_import({})

    assert_kind_of LabClient::LabStruct, result
  end
end
