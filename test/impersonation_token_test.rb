require File.expand_path 'test_helper.rb', __dir__
class UserImpersonationTokenTesting < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_impersonation_tokens_list
    stub_get('/users/5', 'user')
    stub_get('/users/5/impersonation_tokens?filter=all', 'impersonation_tokens')

    # Top
    list = @client.impersonation_tokens.list(5)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.length
    assert_kind_of LabClient::ImpersonationToken, list.first

    # Via User
    user = @client.users.show(5)
    assert_kind_of LabClient::ImpersonationToken, user.impersonation_tokens.first
  end

  def test_impersonation_tokens_show
    stub_get('/users/5', 'user')
    stub_get('/users/5/impersonation_tokens/2', 'impersonation_token')

    # Top
    token = @client.impersonation_tokens.show(5, 2)
    assert_kind_of LabClient::ImpersonationToken, token
    assert_kind_of String, token.inspect

    # Via User
    user = @client.users.show(5)
    assert_kind_of LabClient::ImpersonationToken, user.impersonation_token(2)
  end

  def test_impersonation_tokens_create
    stub_get('/users/5', 'user')
    stub_post('/users/5/impersonation_tokens', 'impersonation_token')

    # Top
    token = @client.impersonation_tokens.create(5, {})
    assert_kind_of LabClient::ImpersonationToken, token

    # Via User
    user = @client.users.show(5)
    assert_kind_of LabClient::ImpersonationToken, user.impersonation_tokens_create({})
  end

  def test_impersonation_tokens_revoke
    stub_get('/users/5', 'user')
    stub_delete('/users/5/impersonation_tokens/4')
    stub_get('/users/5/impersonation_tokens/4', 'impersonation_token')

    # Top
    resp = @client.impersonation_tokens.revoke(5, 4)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via User
    user = @client.users.show(5)
    assert_nil user.impersonation_tokens_revoke(4).data

    # Via Token
    token = @client.impersonation_tokens.show(5, 4)
    assert_nil token.revoke.data
  end
end
