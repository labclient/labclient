require File.expand_path 'test_helper.rb', __dir__

class KeyTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_key
    stub_get('/keys/7', 'key')
    key = @client.keys.show(7)

    assert_kind_of LabClient::Key, key
    assert_kind_of LabClient::User, key.user
    assert_kind_of String, key.inspect
  end

  def test_fingerprint
    stub_get('/keys?fingerprint=print', 'key')
    key = @client.keys.fingerprint('print')

    assert_kind_of LabClient::Key, key
    assert_kind_of LabClient::User, key.user
  end
end
