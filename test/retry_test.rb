require File.expand_path 'test_helper.rb', __dir__

class RetryTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token', quiet: false, debug: true)

    @client.settings[:retry] = { max: 3, delay_factor: 2 }
  end

  def test_retry
    stub_get('/users/5', nil, 429)

    resp = @client.users.show(5)

    assert_equal @client.retries, 3
    assert_equal resp.code, 429
  end
end
