require File.expand_path 'test_helper.rb', __dir__

class WizardTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token', quiet: true)
    @wizard = @client.wizard
  end

  def test_client
    assert_kind_of LabClient::Generator::Wizard, @wizard
    assert_kind_of String, @wizard.inspect
  end

  def test_template_class
    template = LabClient::Generator::TemplateHelper.new(nil, nil)
    assert_kind_of String, template.inspect
  end

  def test_generate_names
    # Setup
    @wizard.generate_names

    assert_kind_of Array, @wizard.instance_variable_get(:@user_names)
    assert_kind_of Array, @wizard.instance_variable_get(:@group_names)
    assert_kind_of Array, @wizard.instance_variable_get(:@project_names)

    assert_kind_of String, @wizard.gen_description
  end

  def test_generate_users
    # Setup
    stub_post('/users', 'user')
    @wizard.generate_names

    suppress_output do
      assert_kind_of Array, @wizard.generate_users
    end
  end

  def test_generate_groups
    # Setup
    stub_post('/groups', 'group')
    @wizard.generate_names

    suppress_output do
      assert_kind_of Array, @wizard.generate_groups
    end
  end

  def test_template_select
    assert_kind_of LabClient::Generator::Pages, @wizard.template(:pages)
  end

  # Additional Random Seeding
  def test_generate_projects
    stub_get('/groups/5', 'group')
    stub_post('/groups/5/members', 'member')
    stub_get('/groups/5/members', 'members')
    stub_post('/projects', 'project')
    stub_post('/projects/16/issues', 'issue')

    suppress_output do
      group = @client.groups.show(5)
      @wizard.generate_names
      assert_equal 5, @wizard.generate_projects(group).count
    end
  end

  def test_wizard_run
    stub_post('/users', 'user')
    stub_post('/groups', 'group')
    stub_post('/groups/5/members', 'member')
    stub_get('/groups/5/members', 'members')
    stub_post('/projects', 'project')
    stub_post('/projects/16/issues', 'issue')

    # Test the templates separately
    @wizard.templates = []

    suppress_output do
      assert_nil @wizard.run!
    end
  end

  def test_pages_run
    stub_post('/groups', 'group')
    stub_post('/projects', 'project')
    stub_get('/projects/16', 'project')
    stub_post('/projects/16/pipeline', 'pipeline')

    suppress_output do
      assert_kind_of Hash, @wizard.template(:pages).run!
    end
  end

  def test_pipeline_trigger_run
    stub_post('/groups', 'group')
    stub_post('/projects', 'project')

    stub_post('/projects/16/repository/files/child_pipeline.yml', 'file_create')
    stub_post('/projects/16/repository/files/.gitlab-ci.yml', 'file_create')

    suppress_output do
      assert_kind_of Hash, @wizard.template(:pipeline_trigger).run!
    end
  end

  def test_environments_run
    stub_post('/groups', 'group')
    stub_post('/projects', 'project')
    stub_get('/projects/16', 'project')
    stub_post('/projects/16/repository/files/README.md', 'file_create')
    stub_post('/projects/16/repository/files/.gitlab-ci.yml', 'file_create')
    stub_post('/projects/16/repository/branches', 'branch')
    stub_post('/projects/16/merge_requests', 'merge_request')
    stub_get('/projects/333/merge_requests/1', 'merge_request')
    stub_put('/projects/333/merge_requests/1/merge', 'merge_request')

    suppress_output do
      assert_kind_of Hash, @wizard.template(:environments).run!
    end
  end
end
