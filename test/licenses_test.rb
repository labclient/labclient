require File.expand_path 'test_helper.rb', __dir__

class LicensesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_license_current
    stub_get('/license', 'license')
    license = @client.license.current

    assert_kind_of String, license.inspect
  end

  def test_licenses_list
    stub_get('/licenses', 'licenses')
    licenses = @client.license.list

    assert_kind_of LabClient::PaginatedResponse, licenses
    assert_equal licenses.size, 2
    assert_kind_of LabClient::License, licenses.first
  end

  def test_license_verbose
    suppress_output do
      stub_get('/license', 'license')
      license = @client.license.current
      assert_nil license.verbose
    end
  end

  # Need guidance
  def test_license_add
    stub_get('/license', 'license_fake')
    stub_post('/license', 'license_fake')

    assert_kind_of LabClient::License, @client.license.add('eyJkYXRhIjoieHJIUU93NE9WM0hUVjMyY3pjemVtdzdZc==')
  end

  # Need Guidance
  def test_license_delete
    stub_get('/license', 'license')
    stub_delete('/license/8')

    resp = @client.license.delete(8)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    id = @client.license.current.id
    assert_nil @client.license.delete(id).data
  end
end
