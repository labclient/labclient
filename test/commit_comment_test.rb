require File.expand_path 'test_helper.rb', __dir__

class CommitCommentTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_comment
    commit_id = 'b539e5d3179825f68f12e17ee1767f6093b055f3'
    stub_get("/projects/16/repository/commits/#{commit_id}/comments", 'commit_comments')

    comment = @client.commits.comments(16, commit_id).first

    assert_kind_of LabClient::CommitComment, comment
    assert_kind_of String, comment.inspect
  end
end
