require File.expand_path 'test_helper.rb', __dir__

class ProjectHooksTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_project_hook
    stub_get('/projects/16/hooks', 'hooks')
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    hook = project.hooks.first

    assert_kind_of String, hook.inspect

    assert_kind_of LabClient::Project, hook.project
  end

  def test_project_hooks_show
    stub_get('/projects/16/hooks/1', 'hook')
    stub_get('/projects/16', 'project')

    hook = @client.projects.hooks.show(16, 1)
    assert_kind_of LabClient::ProjectHook, hook

    project = @client.projects.show(16)
    assert_kind_of LabClient::ProjectHook, project.hook_show(1)
  end

  def test_project_hooks_create
    stub_post('/projects/16/hooks', 'hook')
    stub_get('/projects/16', 'project')

    hook = @client.projects.hooks.create(16, url: 'value')
    assert_kind_of LabClient::ProjectHook, hook

    project = @client.projects.show(16)
    assert_kind_of LabClient::ProjectHook, project.hook_create(url: 'value')
  end

  def test_project_hooks_update
    stub_put('/projects/16/hooks/1', 'hook')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/hooks/1', 'hook')

    # Root
    hook = @client.projects.hooks.update(16, 1, url: 'value')
    assert_kind_of LabClient::ProjectHook, hook

    # Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::ProjectHook, project.hook_update(1, url: 'value')

    # Hook
    hook = @client.projects.hooks.show(16, 1)
    assert_kind_of LabClient::ProjectHook, hook.update(url: 'value')
  end

  def test_project_hooks_delete
    stub_delete('/projects/16/hooks/1')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/hooks/1', 'hook')

    # Root
    resp = @client.projects.hooks.delete(16, 1)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Project
    project = @client.projects.show(16)
    assert_nil project.hook_delete(1).data

    # Hook
    hook = @client.projects.hooks.show(16, 1)
    assert_nil hook.delete.data
  end
end
