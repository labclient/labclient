require File.expand_path 'test_helper.rb', __dir__

class ProjectReleasesTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_project_release
    stub_get('/projects/16/releases', 'project_releases')
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    release = project.releases.first

    assert_kind_of String, release.inspect
    assert_kind_of LabClient::Project, release.project
    assert_kind_of LabClient::Commit, release.commit
  end

  def test_project_releases_show
    stub_get('/projects/16/releases/tag_name', 'project_release')
    stub_get('/projects/16', 'project')

    release = @client.projects.releases.show(16, :tag_name)
    assert_kind_of LabClient::ProjectRelease, release

    project = @client.projects.show(16)
    assert_kind_of LabClient::ProjectRelease, project.release_show(:tag_name)
  end

  def test_project_releases_create
    stub_post('/projects/16/releases', 'project_release')
    stub_get('/projects/16', 'project')

    release = @client.projects.releases.create(16, url: 'value')
    assert_kind_of LabClient::ProjectRelease, release

    project = @client.projects.show(16)
    assert_kind_of LabClient::ProjectRelease, project.release_create(url: 'value')
  end

  def test_project_releases_update
    stub_put('/projects/16/releases/tag_name', 'project_release')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/releases/tag_name', 'project_release')

    # Root
    release = @client.projects.releases.update(16, :tag_name, url: 'value')
    assert_kind_of LabClient::ProjectRelease, release

    # Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::ProjectRelease, project.release_update(:tag_name, url: 'value')

    # Release
    release = @client.projects.releases.show(16, :tag_name)
    assert_kind_of LabClient::ProjectRelease, release.update(url: 'value')
  end

  def test_project_releases_delete
    stub_delete('/projects/16/releases/tag_name')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/releases/tag_name', 'project_release')

    # Root
    resp = @client.projects.releases.delete(16, :tag_name)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Project
    project = @client.projects.show(16)
    assert_nil project.release_delete(:tag_name).data

    # Release
    release = @client.projects.releases.show(16, :tag_name)
    assert_nil release.delete.data
  end

  def test_project_releases_evidence
    stub_post('/projects/16/releases/tag_name/evidence')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/releases/tag_name', 'project_release')

    # Root
    resp = @client.projects.releases.evidence(16, :tag_name)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Project
    project = @client.projects.show(16)
    assert_nil project.release_evidence(:tag_name).data

    # Release
    release = @client.projects.releases.show(16, :tag_name)
    assert_nil release.evidence.data
  end
end
