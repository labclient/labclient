require File.expand_path 'test_helper.rb', __dir__

class ProjectDeploymentsTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def kind
    LabClient::ProjectDeployment
  end

  def test_project_deployment
    stub_get('/projects/16/deployments', 'project_deployments')
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    deployment = project.deployments.first

    assert_kind_of String, deployment.inspect
    assert_kind_of LabClient::Project, deployment.project
  end

  def test_deployment_list
    stub_get('/projects/16/deployments', 'project_deployments')
    list = @client.projects.deployments.list(16)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of kind, list.first
  end

  def test_project_deployments_show
    stub_get('/projects/16/deployments/17', 'project_deployment')
    stub_get('/projects/16', 'project')

    deployment = @client.projects.deployments.show(16, 17)
    assert_kind_of kind, deployment
    assert_kind_of LabClient::Project, deployment.project
    assert_kind_of String, deployment.inspect

    project = @client.projects.show(16)
    assert_kind_of kind, project.deployment_show(17)
  end

  def test_project_deployments_create
    stub_post('/projects/16/deployments', 'project_deployment')
    stub_get('/projects/16', 'project')

    deployment = @client.projects.deployments.create(16, :running)
    assert_kind_of kind, deployment

    project = @client.projects.show(16)
    assert_kind_of kind, project.deployment_create(:running)
  end

  def test_project_deployments_update
    stub_put('/projects/16/deployments/17', 'project_deployment')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/deployments/17', 'project_deployment')

    # Root
    deployment = @client.projects.deployments.update(16, 17, :running)
    assert_kind_of kind, deployment

    # Project
    project = @client.projects.show(16)
    assert_kind_of kind, project.deployment_update(17, :running)

    # Environment
    deployment = @client.projects.deployments.show(16, 17)
    assert_kind_of kind, deployment.update(:running)
  end

  def test_project_deployments_merge_requests
    stub_get('/projects/16/deployments/17/merge_requests', 'merge_requests')
    stub_get('/projects/16', 'project')
    stub_get('/projects/16/deployments/17', 'project_deployment')

    # Root
    list = @client.projects.deployments.merge_requests(16, 17)
    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal 2, list.size
    assert_kind_of LabClient::MergeRequest, list.first

    # Project
    project = @client.projects.show(16)
    assert_kind_of LabClient::MergeRequest, project.deployment_merge_requests(17).first

    # Environment
    deployment = @client.projects.deployments.show(16, 17)
    assert_kind_of LabClient::MergeRequest, deployment.merge_requests.first
  end
end
