require File.expand_path 'test_helper.rb', __dir__

class KlassTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_default_help
    suppress_output do
      refute LabClient::Klass.new.help
      refute LabClient::Klass.new.api_methods
    end
  end

  # Need to have Valid Objects
  def test_klass_help
    stub_get('/users/5', 'user')
    user = @client.users.show(5)

    suppress_output do
      assert_nil user.help 'email'
    end

    # Response Forwarder
    assert user.success?

    # API Methods
    assert_kind_of Array, user.api_methods
  end

  def test_default_to_json
    assert_kind_of String, LabClient::Klass.new.to_json
  end

  def test_update_self
    klass = LabClient::Klass.new
    assert_kind_of LabClient::Klass, klass.update_self(LabClient::Klass.new)
  end

  def test_group_project_levels_list
    klass = LabClient::Klass.new
    assert_kind_of Array, klass.valid_group_project_levels
  end

  def test_collect_project_id_path
    klass = LabClient::Klass.new
    resp = Typhoeus::Response.new
    resp.instance_variable_set('@path', '/project_id/there')
    klass.instance_variable_set('@response', resp)
    assert_equal 'project_id', klass.collect_project_id
  end

  def test_collect_project_id_base_url
    # Fake Request
    resp = Typhoeus::Response.new
    resp.request = LabClient::LabStruct.new(base_url: 'https://labclient/api/v4/project_id')

    # Klass Object / Set @response and @client
    klass = LabClient::Klass.new
    klass.instance_variable_set('@response', resp)
    klass.instance_variable_set('@client', @client)

    # Verify collet_project_id is properly splitting URL
    assert_equal 'project_id', klass.collect_project_id
  end
end
