require File.expand_path 'test_helper.rb', __dir__

class GroupHooksTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_group_hook
    stub_get('/groups/5/hooks', 'hooks')
    stub_get('/groups/5', 'group')
    group = @client.groups.show(5)
    hook = group.hooks.first

    assert_kind_of String, hook.inspect
    assert_kind_of LabClient::Group, hook.group
  end

  def test_group_hooks_show
    stub_get('/groups/5/hooks/1', 'hook')
    stub_get('/groups/5', 'group')

    hook = @client.groups.hooks.show(5, 1)
    assert_kind_of LabClient::GroupHook, hook

    group = @client.groups.show(5)
    assert_kind_of LabClient::GroupHook, group.hook_show(1)
  end

  def test_group_hooks_create
    stub_post('/groups/5/hooks', 'hook')
    stub_get('/groups/5', 'group')

    hook = @client.groups.hooks.create(5, url: 'value')
    assert_kind_of LabClient::GroupHook, hook

    group = @client.groups.show(5)
    assert_kind_of LabClient::GroupHook, group.hook_create(url: 'value')
  end

  def test_group_hooks_update
    stub_put('/groups/5/hooks/1', 'hook')
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/hooks/1', 'hook')

    # Root
    hook = @client.groups.hooks.update(5, 1, url: 'value')
    assert_kind_of LabClient::GroupHook, hook

    # Group
    group = @client.groups.show(5)
    assert_kind_of LabClient::GroupHook, group.hook_update(1, url: 'value')

    # Hook
    hook = @client.groups.hooks.show(5, 1)
    assert_kind_of LabClient::GroupHook, hook.update(url: 'value')
  end

  def test_group_hooks_delete
    stub_delete('/groups/5/hooks/1')
    stub_get('/groups/5', 'group')
    stub_get('/groups/5/hooks/1', 'hook')

    # Root
    resp = @client.groups.hooks.delete(5, 1)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Group
    group = @client.groups.show(5)
    assert_nil group.hook_delete(1).data

    # Hook
    hook = @client.groups.hooks.show(5, 1)
    assert_nil hook.delete.data
  end
end
