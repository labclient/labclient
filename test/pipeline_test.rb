require File.expand_path 'test_helper.rb', __dir__

class PipelineTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_pipeline
    pipeline = LabClient::Pipeline.new JSON.parse load_fixture('pipeline').read

    assert_kind_of LabClient::Pipeline, pipeline
    assert_kind_of String, pipeline.inspect
  end
end
