require File.expand_path 'test_helper.rb', __dir__

class HttpTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_resp_obj
    stub_get('/users', 'error', 400)

    assert_kind_of Hash, @client.http.disable_ssl

    suppress_output do
      resp = @client.users.list
      assert_kind_of Typhoeus::Response, @client.users.list
      assert_equal resp.friendly_error, '400 - message'
      assert_kind_of String, resp.inspect
    end
  end

  def test_http_body
    resp = Typhoeus::Response.new(
      body: 'test',
      headers: { 'content-type' => 'text/plain' }
    )
    assert_kind_of String, resp.data
    assert_equal 'test', resp.data
  end

  def test_invalid_url_error
    resp = Typhoeus::Response.new(
      code: 0,
      body: '',
      return_code: :couldnt_resolve_host
    )

    assert_nil resp.data
    assert_equal resp.friendly_error, "0 - Couldn't resolve host name"
  end

  def test_friendly_error_raw
    resp = Typhoeus::Response.new
    resp.instance_variable_set(:@data, 'belarugh')
    assert_equal resp.friendly_error, '0 - belarugh'
  end

  # For CurlHelper
  def test_self_helper
    resp = Typhoeus::Response.new(
      code: 0,
      body: '',
      return_code: :couldnt_resolve_host
    )

    assert_kind_of Typhoeus::Response, resp.response
  end
end
