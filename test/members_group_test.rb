require File.expand_path 'test_helper.rb', __dir__

class GroupMembersTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
    stub_get('/groups/5', 'group')
  end

  def kind
    LabClient::Member
  end

  def test_members_list
    stub_get('/groups/5/members', 'members')
    list = @client.members.groups.list(5)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of kind, list.first

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of kind, group.members.first
  end

  def test_members_list_all
    stub_get('/groups/5/members/all', 'members')
    list = @client.members.groups.list_all(5)

    assert_kind_of LabClient::PaginatedResponse, list
    assert_equal list.size, 2
    assert_kind_of kind, list.first

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of kind, group.members_all.first
  end

  def test_members_show
    stub_get('/groups/5/members/40', 'member')
    show = @client.members.groups.show(5, 40)
    assert_kind_of kind, show
    assert_kind_of String, show.inspect

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of kind, group.member(40)
  end

  def test_members_show_all
    stub_get('/groups/5/members/all/40', 'member')
    show = @client.members.groups.show_all(5, 40)
    assert_kind_of kind, show

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of kind, group.member_all(40)
  end

  def test_members_add
    stub_post('/groups/5/members', 'member')
    show = @client.members.groups.add(5, 40, {})
    assert_kind_of kind, show

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of kind, group.member_add(40, {})
  end

  def test_members_update
    stub_put('/groups/5/members/40', 'member')
    show = @client.members.groups.update(5, 40, {})
    assert_kind_of kind, show

    # Via Group
    group = @client.groups.show(5)
    assert_kind_of kind, group.member_update(40, {})
  end

  def test_members_delete
    stub_delete('/groups/5/members/40')

    resp = @client.members.groups.delete(5, 40)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Group
    group = @client.groups.show(5)
    assert_nil group.member_delete(40).data
  end
end
