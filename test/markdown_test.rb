require File.expand_path 'test_helper.rb', __dir__

class MarkdownTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_md_render
    details = { text: '# Oh Hi!' }
    stub_post('/markdown', 'markdown', 200, details)
    result = @client.markdown.render(details)
    assert_kind_of LabClient::LabStruct, result
  end
end
