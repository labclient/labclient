require File.expand_path 'test_helper.rb', __dir__

class DeployKeyTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @client = LabClient::Client.new(url: 'https://labclient', token: 'token')
  end

  def test_deploy_key
    stub_get('/projects/5/deploy_keys/1', 'deploy_key')
    deploy_key = @client.deploy_keys.show(5, 1)

    assert_kind_of LabClient::DeployKey, deploy_key
    assert_kind_of String, deploy_key.inspect

    %i[created_at].each do |kind|
      assert_kind_of DateTime, deploy_key.send(kind)
    end

    # Helper is console ouput
    suppress_output do
      assert_nil deploy_key.help
    end
  end

  def test_deploy_keys_list
    stub_get('/deploy_keys', 'deploy_keys')
    deploy_keys = @client.deploy_keys.list

    assert_kind_of LabClient::PaginatedResponse, deploy_keys
    assert_equal deploy_keys.size, 2
    assert_kind_of LabClient::DeployKey, deploy_keys.first
  end

  def test_deploy_keys_list_project
    stub_get('/projects/16/deploy_keys', 'deploy_keys')
    deploy_keys = @client.deploy_keys.list(16)

    assert_kind_of LabClient::PaginatedResponse, deploy_keys
    assert_equal deploy_keys.size, 2
    assert_kind_of LabClient::DeployKey, deploy_keys.first

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    project_keys = project.deploy_keys
    assert_kind_of LabClient::PaginatedResponse, project_keys
    assert_kind_of LabClient::DeployKey, project_keys.first
  end

  def test_add_deploy_key
    details = { key: 'Key', title: 'Title' }
    stub_post('/projects/16/deploy_keys', 'deploy_key', 200, details)

    deploy_key = @client.deploy_keys.add(16, details)

    assert_kind_of LabClient::DeployKey, deploy_key
    assert_kind_of String, deploy_key.inspect

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    project_key = project.deploy_key_add(details)
    assert_kind_of LabClient::DeployKey, project_key
  end

  def test_show_deploy_key
    stub_get('/projects/16/deploy_keys/1', 'deploy_key')

    deploy_key = @client.deploy_keys.show(16, 1)

    assert_kind_of LabClient::DeployKey, deploy_key

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    project_key = project.deploy_key_show(1)
    assert_kind_of LabClient::DeployKey, project_key
  end

  def test_update_deploy_key
    details = { key: 'Key', title: 'Title' }
    stub_get('/projects/16/deploy_keys/3', 'deploy_key')
    stub_put('/projects/16/deploy_keys/3', 'deploy_key', 200, details)

    deploy_key_direct = @client.deploy_keys.update(16, 3, details)
    deploy_key = @client.deploy_keys.show(16, 3)

    deploy_key.update(details)

    assert_kind_of LabClient::DeployKey, deploy_key_direct
    assert_kind_of LabClient::DeployKey, deploy_key

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    project_key = project.deploy_key_update(3, details)
    assert_kind_of LabClient::DeployKey, project_key
  end

  def test_delete_deploy_key
    stub_get('/projects/16/deploy_keys/3', 'deploy_key')
    stub_delete('/projects/16/deploy_keys/3')

    resp = @client.deploy_keys.delete(16, 3)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    deploy_key = @client.deploy_keys.show(16, 3)
    assert_nil deploy_key.delete.data

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_nil project.deploy_key_delete(3).data
  end

  def test_enable_deploy_key
    stub_post('/projects/16/deploy_keys/3/enable')

    resp = @client.deploy_keys.enable(16, 3)
    assert_kind_of Typhoeus::Response, resp
    assert_equal 200, resp.code
    assert_nil resp.data

    # Via Project
    stub_get('/projects/16', 'project')
    project = @client.projects.show(16)
    assert_nil project.deploy_key_enable(3).data
  end
end
