require File.expand_path 'test_helper.rb', __dir__

class DocsTest < Minitest::Test
  include Rack::Test::Methods

  def test_to_json
    assert_kind_of String, LabClient::Docs.json
    assert_kind_of Hash, JSON.parse(LabClient::Docs.json)
  end
end
