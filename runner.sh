#!/bin/bash
# Used in docker-compose.yml to simplify dev environment / Avoid conflicts...
bundle install
rerun --no-notify --dir lib "sleep 1 && bundle exec rake install && ./create_docs.rb" 