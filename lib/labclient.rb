# Public Gems
require 'oj'
require 'typhoeus'
require 'active_support/all'
require 'amazing_print'
require 'gitlab_chronic_duration'
require 'ostruct'
require 'ougai'

# Escaping Paths - https://stackoverflow.com/questions/2824126/whats-the-difference-between-uri-escape-and-cgi-escape/13059657#13059657
require 'erb'

require 'labclient/version'
require 'labclient/logger'
require 'labclient/docs'
require 'labclient/curl'
require 'labclient/http'
require 'labclient/access_levels'
require 'labclient/common'
require 'labclient/lab_struct'
require 'labclient/klass'
require 'labclient/class_helpers'
require 'labclient/paginated_response'
require 'labclient/error'

# # The order here also determines the order of the sidenav in the docs
# # Overview / Docs / Landing Page
require 'labclient/overview'

# Project Helpers
require 'labclient/deploy_keys/project_helpers'
require 'labclient/commits/project_helpers'

# ===============================================
# Users
# ===============================================
require 'labclient/users/list'
require 'labclient/users/show'
require 'labclient/users/current'
require 'labclient/users/status'
require 'labclient/users/counts'
require 'labclient/users/activity'
require 'labclient/users/search'
require 'labclient/users/create'
require 'labclient/users/update'
require 'labclient/users/status_set'
require 'labclient/users/block'
require 'labclient/users/unblock'
require 'labclient/users/activate'
require 'labclient/users/deactivate'
require 'labclient/users/delete'
require 'labclient/users/delete_identity'
require 'labclient/users/memberships'
require 'labclient/users/membership'
require 'labclient/users/user'

# ===============================================
# Projects
# ===============================================
require 'labclient/projects/resources'
require 'labclient/projects/search'
require 'labclient/projects/list'
require 'labclient/projects/user'
require 'labclient/projects/show'

require 'labclient/projects/users'
require 'labclient/projects/events'
require 'labclient/projects/create'
require 'labclient/projects/update'
require 'labclient/projects/upload'
require 'labclient/projects/share'
require 'labclient/projects/unshare'
require 'labclient/projects/housekeeping'
require 'labclient/projects/submodule'
require 'labclient/projects/transfer'
require 'labclient/projects/mirror_start'
require 'labclient/projects/archive'
require 'labclient/projects/unarchive'
require 'labclient/projects/languages'
require 'labclient/projects/delete'
require 'labclient/projects/restore'
require 'labclient/projects/snapshot'
require 'labclient/projects/github_import'

# Group Stub / Docs ORder
require 'labclient/groups/stub'

# Project Forks
require 'labclient/projects/forks/list'
require 'labclient/projects/forks/fork'
require 'labclient/projects/forks/existing'
require 'labclient/projects/forks/remove'

# Project Stars
require 'labclient/projects/stars/star'
require 'labclient/projects/stars/starrers'
require 'labclient/projects/stars/starred_projects'

# Project Snippets
require 'labclient/projects/snippets/client'
require 'labclient/projects/snippets/list'
require 'labclient/projects/snippets/show'
require 'labclient/projects/snippets/content'
require 'labclient/projects/snippets/agent_detail'
require 'labclient/projects/snippets/create'
require 'labclient/projects/snippets/update'
require 'labclient/projects/snippets/delete'

require 'labclient/projects/snippets/project_snippet'

# Project Push Rules
require 'labclient/projects/push_rules/client'
require 'labclient/projects/push_rules/show'
require 'labclient/projects/push_rules/create'
require 'labclient/projects/push_rules/update'
require 'labclient/projects/push_rules/delete'
require 'labclient/projects/push_rules/push_rule'

# Project Clusters
require 'labclient/projects/clusters/client'
require 'labclient/projects/clusters/list'
require 'labclient/projects/clusters/show'
require 'labclient/projects/clusters/add'
require 'labclient/projects/clusters/update'
require 'labclient/projects/clusters/delete'
require 'labclient/projects/clusters/project_cluster'

# Project Hooks
require 'labclient/projects/hooks/client'
require 'labclient/projects/hooks/list'
require 'labclient/projects/hooks/show'
require 'labclient/projects/hooks/create'
require 'labclient/projects/hooks/update'
require 'labclient/projects/hooks/delete'
require 'labclient/projects/hooks/project_hook'

# Project Badges
require 'labclient/projects/badges/client'
require 'labclient/projects/badges/list'
require 'labclient/projects/badges/preview'
require 'labclient/projects/badges/show'
require 'labclient/projects/badges/create'
require 'labclient/projects/badges/update'
require 'labclient/projects/badges/delete'
require 'labclient/projects/badges/project_badge'

# Project Environments
require 'labclient/projects/environments/client'
require 'labclient/projects/environments/list'
require 'labclient/projects/environments/show'
require 'labclient/projects/environments/create'
require 'labclient/projects/environments/update'
require 'labclient/projects/environments/delete'
require 'labclient/projects/environments/stop'
require 'labclient/projects/environments/project_environment'

# Protected Environments
require 'labclient/protected_environments/list'
require 'labclient/protected_environments/show'
require 'labclient/protected_environments/protect'
require 'labclient/protected_environments/unprotect'
require 'labclient/protected_environments/protected_environment'

# Project Deployments
require 'labclient/projects/deployments/client'
require 'labclient/projects/deployments/list'
require 'labclient/projects/deployments/show'
require 'labclient/projects/deployments/merge_requests'
require 'labclient/projects/deployments/create'
require 'labclient/projects/deployments/update'
require 'labclient/projects/deployments/project_deployment'

# Project Variables
require 'labclient/projects/variables/client'
require 'labclient/projects/variables/list'
require 'labclient/projects/variables/show'
require 'labclient/projects/variables/create'
require 'labclient/projects/variables/update'
require 'labclient/projects/variables/delete'
require 'labclient/projects/variables/project_variable'

# Project Triggers
require 'labclient/projects/triggers/client'
require 'labclient/projects/triggers/list'
require 'labclient/projects/triggers/show'
require 'labclient/projects/triggers/create'
require 'labclient/projects/triggers/delete'
require 'labclient/projects/triggers/project_trigger'

# Project Labels
require 'labclient/projects/labels/client'
require 'labclient/projects/labels/list'
require 'labclient/projects/labels/show'
require 'labclient/projects/labels/create'
require 'labclient/projects/labels/update'
require 'labclient/projects/labels/delete'
require 'labclient/projects/labels/promote'
require 'labclient/projects/labels/subscribe'
require 'labclient/projects/labels/unsubscribe'
require 'labclient/projects/labels/project_label'

# Project Releases
require 'labclient/projects/releases/client'
require 'labclient/projects/releases/list'
require 'labclient/projects/releases/show'
require 'labclient/projects/releases/create'
require 'labclient/projects/releases/evidence'
require 'labclient/projects/releases/update'
require 'labclient/projects/releases/delete'
require 'labclient/projects/releases/project_release'

# Project Release Links
require 'labclient/projects/release_links/client'
require 'labclient/projects/release_links/list'
require 'labclient/projects/release_links/show'
require 'labclient/projects/release_links/create'
require 'labclient/projects/release_links/update'
require 'labclient/projects/release_links/delete'
require 'labclient/projects/release_links/project_release_link'

# Project Services
require 'labclient/projects/services/client'
require 'labclient/projects/services/list'
require 'labclient/projects/services/show'
require 'labclient/projects/services/update'
require 'labclient/projects/services/delete'
require 'labclient/projects/services/project_service'

# Project Milestones
require 'labclient/projects/milestones/client'
require 'labclient/projects/milestones/list'
require 'labclient/projects/milestones/show'
require 'labclient/projects/milestones/create'
require 'labclient/projects/milestones/update'
require 'labclient/projects/milestones/delete'
require 'labclient/projects/milestones/issues'
require 'labclient/projects/milestones/merge_requests'
require 'labclient/projects/milestones/promote'
require 'labclient/projects/milestones/burndown'
require 'labclient/projects/milestones/project_milestone'

# Pipeline Schedules
require 'labclient/projects/pipeline_schedules/client'
require 'labclient/projects/pipeline_schedules/list'
require 'labclient/projects/pipeline_schedules/show'
require 'labclient/projects/pipeline_schedules/create'
require 'labclient/projects/pipeline_schedules/update'
require 'labclient/projects/pipeline_schedules/delete'
require 'labclient/projects/pipeline_schedules/take_ownership'
require 'labclient/projects/pipeline_schedules/play'
require 'labclient/projects/pipeline_schedules/variables/client'
require 'labclient/projects/pipeline_schedules/variables/create'
require 'labclient/projects/pipeline_schedules/variables/update'
require 'labclient/projects/pipeline_schedules/variables/delete'
require 'labclient/projects/pipeline_schedules/pipeline_schedule'

# Project Mirrors
require 'labclient/projects/mirrors/client'
require 'labclient/projects/mirrors/list'
require 'labclient/projects/mirrors/create'
require 'labclient/projects/mirrors/update'
require 'labclient/projects/mirrors/project_mirror'

# Access Requests
require 'labclient/projects/access_requests/client'
require 'labclient/projects/access_requests/list'
require 'labclient/projects/access_requests/create'
require 'labclient/projects/access_requests/approve'
require 'labclient/projects/access_requests/deny'
require 'labclient/projects/access_requests/access_request'

# Project Templates
require 'labclient/projects/templates/client'
require 'labclient/projects/templates/show'
require 'labclient/projects/templates/list'

# ===========================================================
# Final Projects
# ===========================================================

require 'labclient/projects/methods'
require 'labclient/projects/reference'
require 'labclient/projects/project'

# Group Links
require 'labclient/groups/ldap/client'
require 'labclient/groups/ldap/sync'
require 'labclient/groups/ldap/list'
require 'labclient/groups/ldap/create'
require 'labclient/groups/ldap/delete'
require 'labclient/groups/ldap/link'

# Groups
require 'labclient/groups/list'
require 'labclient/groups/subgroups'
require 'labclient/groups/projects'
require 'labclient/groups/show'
require 'labclient/groups/create'
require 'labclient/groups/update'
require 'labclient/groups/transfer'
require 'labclient/groups/delete'
require 'labclient/groups/restore'
require 'labclient/groups/search'
require 'labclient/groups/runners'

# Group Hooks
require 'labclient/groups/hooks/client'
require 'labclient/groups/hooks/list'
require 'labclient/groups/hooks/show'
require 'labclient/groups/hooks/create'
require 'labclient/groups/hooks/update'
require 'labclient/groups/hooks/delete'
require 'labclient/groups/hooks/group_hook'

# Group Access Requests
require 'labclient/groups/access_requests/client'
require 'labclient/groups/access_requests/list'
require 'labclient/groups/access_requests/create'
require 'labclient/groups/access_requests/approve'
require 'labclient/groups/access_requests/deny'
require 'labclient/groups/access_requests/access_request'

# Group Labels
require 'labclient/groups/labels/client'
require 'labclient/groups/labels/list'
require 'labclient/groups/labels/show'
require 'labclient/groups/labels/create'
require 'labclient/groups/labels/update'
require 'labclient/groups/labels/delete'
require 'labclient/groups/labels/subscribe'
require 'labclient/groups/labels/unsubscribe'
require 'labclient/groups/labels/group_label'

# Group Milestones
require 'labclient/groups/milestones/client'
require 'labclient/groups/milestones/list'
require 'labclient/groups/milestones/show'
require 'labclient/groups/milestones/create'
require 'labclient/groups/milestones/update'
require 'labclient/groups/milestones/delete'
require 'labclient/groups/milestones/issues'
require 'labclient/groups/milestones/merge_requests'
require 'labclient/groups/milestones/burndown'
require 'labclient/groups/milestones/group_milestone'

# Epics
require 'labclient/epics/list'
require 'labclient/epics/show'
require 'labclient/epics/create'
require 'labclient/epics/update'
require 'labclient/epics/todo'
require 'labclient/epics/delete'

# Epic Issues
require 'labclient/epics/issues/list'
require 'labclient/epics/issues/add'
require 'labclient/epics/issues/remove'
require 'labclient/epics/issues/update'

require 'labclient/epics/epic'

# Group Badges
require 'labclient/groups/badges/client'
require 'labclient/groups/badges/list'
require 'labclient/groups/badges/preview'
require 'labclient/groups/badges/show'
require 'labclient/groups/badges/create'
require 'labclient/groups/badges/update'
require 'labclient/groups/badges/delete'
require 'labclient/groups/badges/group_badge'

# Group Clusters
require 'labclient/groups/clusters/client'
require 'labclient/groups/clusters/list'
require 'labclient/groups/clusters/show'
require 'labclient/groups/clusters/add'
require 'labclient/groups/clusters/update'
require 'labclient/groups/clusters/delete'
require 'labclient/groups/clusters/group_cluster'

require 'labclient/groups/group'

# Issues
require 'labclient/issues/list'
require 'labclient/issues/project_issues'
require 'labclient/issues/group_issues'
require 'labclient/issues/show'
require 'labclient/issues/create'
require 'labclient/issues/update'
require 'labclient/issues/delete'
require 'labclient/issues/move'
require 'labclient/issues/subscribe'
require 'labclient/issues/unsubscribe'
require 'labclient/issues/todo'
require 'labclient/issues/time_stats'
require 'labclient/issues/participants'
require 'labclient/issues/notes'
require 'labclient/issues/agent_detail'
require 'labclient/issues/related_merge_requests'
require 'labclient/issues/closed_by'
require 'labclient/issues/issue'

# Merge Requests
require 'labclient/merge_requests/list'
require 'labclient/merge_requests/show'
require 'labclient/merge_requests/pipelines'
require 'labclient/merge_requests/create'
require 'labclient/merge_requests/update'
require 'labclient/merge_requests/delete'
require 'labclient/merge_requests/accept'
require 'labclient/merge_requests/rebase'
require 'labclient/merge_requests/cancel_auto_merge'
require 'labclient/merge_requests/subscribe'
require 'labclient/merge_requests/unsubscribe'
require 'labclient/merge_requests/todo'
require 'labclient/merge_requests/participants'
require 'labclient/merge_requests/commits'
require 'labclient/merge_requests/changes'
require 'labclient/merge_requests/closes_issues'
require 'labclient/merge_requests/notes'
require 'labclient/merge_requests/time_stats'
require 'labclient/merge_requests/merge_ref'
require 'labclient/merge_requests/diff_versions'

# MergeRequest Result Objects
require 'labclient/merge_requests/merge_request'
require 'labclient/merge_requests/diff'
require 'labclient/merge_requests/change'

# Audit Events
require 'labclient/audit_events/audit_event'
require 'labclient/audit_events/instance_list'
require 'labclient/audit_events/instance_show'
require 'labclient/audit_events/group_list'
require 'labclient/audit_events/group_show'

# Events
require 'labclient/events/list'
require 'labclient/events/user'
require 'labclient/events/project'
require 'labclient/events/event'

# Search
require 'labclient/search/search'

# Appearance
require 'labclient/appearance/show'
require 'labclient/appearance/update'
require 'labclient/appearance/appearance'

# Applications
require 'labclient/applications/list'
require 'labclient/applications/create'
require 'labclient/applications/delete'
require 'labclient/applications/application'

# Avatar
require 'labclient/avatar/avatar'
require 'labclient/avatar/show'

# Broadcast Messages
require 'labclient/broadcast_messages/list'
require 'labclient/broadcast_messages/show'
require 'labclient/broadcast_messages/create'
require 'labclient/broadcast_messages/delete'
require 'labclient/broadcast_messages/update'
require 'labclient/broadcast_messages/broadcast_message'

# Snippets
require 'labclient/snippets/list'
require 'labclient/snippets/public'
require 'labclient/snippets/show'
require 'labclient/snippets/agent_detail'
require 'labclient/snippets/content'
require 'labclient/snippets/create'
require 'labclient/snippets/delete'
require 'labclient/snippets/update'
require 'labclient/snippets/snippet'

# Deploy Keys
require 'labclient/deploy_keys/list'
require 'labclient/deploy_keys/add'
require 'labclient/deploy_keys/show'
require 'labclient/deploy_keys/update'
require 'labclient/deploy_keys/delete'
require 'labclient/deploy_keys/enable'
require 'labclient/deploy_keys/deploy_key'

# Keys
require 'labclient/keys/key'
require 'labclient/keys/show'
require 'labclient/keys/fingerprint'

# Todos
require 'labclient/todos/todo'
require 'labclient/todos/list'
require 'labclient/todos/mark_done'
require 'labclient/todos/mark_all_done'

# Version
require 'labclient/version/show'

# Application settings
require 'labclient/application_settings/application_setting'
require 'labclient/application_settings/show'
require 'labclient/application_settings/update'

# GitLab License
require 'labclient/license/list'
require 'labclient/license/add'
require 'labclient/license/delete'
require 'labclient/license/license'

# Registry
require 'labclient/registry/list'
require 'labclient/registry/group'
require 'labclient/registry/delete'
require 'labclient/registry/tags/list'
require 'labclient/registry/tags/details'
require 'labclient/registry/tags/delete'
require 'labclient/registry/tags/bulk'

require 'labclient/registry/tags/tag'
require 'labclient/registry/repository'

# Commits
require 'labclient/commits/list'
require 'labclient/commits/show'
require 'labclient/commits/create'
require 'labclient/commits/refs'
require 'labclient/commits/cherry_pick'
require 'labclient/commits/revert'
require 'labclient/commits/diff'
require 'labclient/commits/merge_requests'
require 'labclient/commits/comments'
require 'labclient/commits/comment_create'

# Commit Status
require 'labclient/commits/status/list'
require 'labclient/commits/status/update'
require 'labclient/commits/status/status'

require 'labclient/commits/commit'
require 'labclient/commits/commit_diff'
require 'labclient/commits/commit_comment'

# CI Lint
require 'labclient/ci_lint/ci_lint'

# Markdown
require 'labclient/markdown/markdown'

# Application Statistics
require 'labclient/application_statistics/application_statistics'

# Wikis
require 'labclient/wikis/list'
require 'labclient/wikis/show'
require 'labclient/wikis/create'
require 'labclient/wikis/update'
require 'labclient/wikis/delete'
require 'labclient/wikis/upload'
require 'labclient/wikis/wiki'

# ================
# Discussions
# ================
# Issue
require 'labclient/discussions/issues/client'
require 'labclient/discussions/issues/list'
require 'labclient/discussions/issues/show'
require 'labclient/discussions/issues/create'
require 'labclient/discussions/issues/update'
require 'labclient/discussions/issues/delete'

# Snippet
require 'labclient/discussions/snippets/client'
require 'labclient/discussions/snippets/list'
require 'labclient/discussions/snippets/show'
require 'labclient/discussions/snippets/create'
require 'labclient/discussions/snippets/update'
require 'labclient/discussions/snippets/delete'

# Epic
require 'labclient/discussions/epics/client'
require 'labclient/discussions/epics/list'
require 'labclient/discussions/epics/show'
require 'labclient/discussions/epics/create'
require 'labclient/discussions/epics/update'
require 'labclient/discussions/epics/delete'

# Merge Request
require 'labclient/discussions/merge_requests/client'
require 'labclient/discussions/merge_requests/list'
require 'labclient/discussions/merge_requests/show'
require 'labclient/discussions/merge_requests/create'
require 'labclient/discussions/merge_requests/update'
require 'labclient/discussions/merge_requests/delete'

# Commit
require 'labclient/discussions/commits/client'
require 'labclient/discussions/commits/list'
require 'labclient/discussions/commits/show'
require 'labclient/discussions/commits/create'
require 'labclient/discussions/commits/update'
require 'labclient/discussions/commits/delete'

require 'labclient/discussions/discussion'

# ================
# Awards
# ================
# Issues
require 'labclient/awards/issues/client'
require 'labclient/awards/issues/list'
require 'labclient/awards/issues/show'
require 'labclient/awards/issues/delete'
require 'labclient/awards/issues/create'

# Merge Requests
require 'labclient/awards/merge_requests/client'
require 'labclient/awards/merge_requests/list'
require 'labclient/awards/merge_requests/show'
require 'labclient/awards/merge_requests/delete'
require 'labclient/awards/merge_requests/create'

# Snippet
require 'labclient/awards/snippets/client'
require 'labclient/awards/snippets/list'
require 'labclient/awards/snippets/show'
require 'labclient/awards/snippets/delete'
require 'labclient/awards/snippets/create'

require 'labclient/awards/award'

# ================
# Notes
# ================
# MR Notes
require 'labclient/notes/merge_requests/client'
require 'labclient/notes/merge_requests/list'
require 'labclient/notes/merge_requests/show'
require 'labclient/notes/merge_requests/create'
require 'labclient/notes/merge_requests/update'
require 'labclient/notes/merge_requests/delete'

# Issue Notes
require 'labclient/notes/issues/client'
require 'labclient/notes/issues/list'
require 'labclient/notes/issues/show'
require 'labclient/notes/issues/create'
require 'labclient/notes/issues/update'
require 'labclient/notes/issues/delete'

# Snippet Notes
require 'labclient/notes/snippets/client'
require 'labclient/notes/snippets/list'
require 'labclient/notes/snippets/show'
require 'labclient/notes/snippets/create'
require 'labclient/notes/snippets/update'
require 'labclient/notes/snippets/delete'

# Epic Notes
require 'labclient/notes/epics/client'
require 'labclient/notes/epics/list'
require 'labclient/notes/epics/show'
require 'labclient/notes/epics/create'
require 'labclient/notes/epics/update'
require 'labclient/notes/epics/delete'

# # require 'labclient/notes/issues/show'
# # require 'labclient/notes/issues/list'

require 'labclient/notes/note'
# -----------------------------------

# User Keys
require 'labclient/users/keys/client'
require 'labclient/users/keys/list'
require 'labclient/users/keys/show'
require 'labclient/users/keys/create'
require 'labclient/users/keys/delete'

# Notifications
require 'labclient/notifications/list'
require 'labclient/notifications/update'

# User GPG Keys
require 'labclient/users/gpg/client'
require 'labclient/users/gpg/list'
require 'labclient/users/gpg/show'
require 'labclient/users/gpg/create'
require 'labclient/users/gpg/delete'
require 'labclient/users/gpg/gpg_key'

# User Emails
require 'labclient/users/email/client'
require 'labclient/users/email/list'
require 'labclient/users/email/show'
require 'labclient/users/email/create'
require 'labclient/users/email/delete'
require 'labclient/users/email/email'

# Impersonation Tokens
require 'labclient/impersonation_tokens/impersonation_token'
require 'labclient/impersonation_tokens/list'
require 'labclient/impersonation_tokens/show'
require 'labclient/impersonation_tokens/create'
require 'labclient/impersonation_tokens/revoke'

# Namespaces
require 'labclient/namespaces/namespace'
require 'labclient/namespaces/list'
require 'labclient/namespaces/show'

# Runners
require 'labclient/runners/list'
require 'labclient/runners/all'
require 'labclient/runners/show'
require 'labclient/runners/jobs'
require 'labclient/runners/update'
require 'labclient/runners/register'
require 'labclient/runners/remove'
require 'labclient/runners/delete'
require 'labclient/runners/verify'
require 'labclient/runners/runner'

# Project Runners
require 'labclient/projects/runners/client'
require 'labclient/projects/runners/list'
require 'labclient/projects/runners/enable'
require 'labclient/projects/runners/disable'

# Jobs
require 'labclient/jobs/project_list'
require 'labclient/jobs/pipeline_list'
require 'labclient/jobs/show'
require 'labclient/jobs/trace'
require 'labclient/jobs/cancel'
require 'labclient/jobs/retry'
require 'labclient/jobs/erase'
require 'labclient/jobs/keep'
require 'labclient/jobs/delete'
require 'labclient/jobs/play'
require 'labclient/jobs/artifacts'
require 'labclient/jobs/artifacts_latest'
require 'labclient/jobs/artifacts_path'
require 'labclient/jobs/job'

# Members
require 'labclient/members/members'

# Projects
require 'labclient/members/projects/list'
require 'labclient/members/projects/show'
require 'labclient/members/projects/add'
require 'labclient/members/projects/update'
require 'labclient/members/projects/delete'

# Groups
require 'labclient/members/groups/list'
require 'labclient/members/groups/show'
require 'labclient/members/groups/add'
require 'labclient/members/groups/update'
require 'labclient/members/groups/delete'

require 'labclient/members/member'

# Pipelines
require 'labclient/pipelines/list'
require 'labclient/pipelines/show'
require 'labclient/pipelines/variables'
require 'labclient/pipelines/create'
require 'labclient/pipelines/retry'
require 'labclient/pipelines/cancel'
require 'labclient/pipelines/delete'
require 'labclient/pipelines/pipeline'

# Repository
require 'labclient/repository/alias'
require 'labclient/repository/tree'
require 'labclient/repository/repository_tree'
require 'labclient/repository/blob'
require 'labclient/repository/archive'
require 'labclient/repository/compare'
require 'labclient/repository/contributors'
require 'labclient/repository/merge_base'

# Branches
require 'labclient/branches/list'
require 'labclient/branches/show'
require 'labclient/branches/create'
require 'labclient/branches/delete'
require 'labclient/branches/delete_merged'
require 'labclient/branches/branch'

# Protected Branches
require 'labclient/protected_branches/list'
require 'labclient/protected_branches/show'
require 'labclient/protected_branches/protect'
require 'labclient/protected_branches/unprotect'
require 'labclient/protected_branches/code_owner_approval'

# Tags
require 'labclient/tags/list'
require 'labclient/tags/show'
require 'labclient/tags/create'
require 'labclient/tags/delete'
require 'labclient/tags/release'
require 'labclient/tags/update'
require 'labclient/tags/tag'

# Protected Tags
require 'labclient/protected_tags/list'
require 'labclient/protected_tags/show'
require 'labclient/protected_tags/protect'
require 'labclient/protected_tags/unprotect'

# Merge Request Approvals
# Project
require 'labclient/approvals/client'
require 'labclient/approvals/project/show'
require 'labclient/approvals/project/update'
require 'labclient/approvals/project/rules'
require 'labclient/approvals/project/create_rule'
require 'labclient/approvals/project/update_rule'
require 'labclient/approvals/project/delete_rule'

# MR
require 'labclient/approvals/merge_request/show'
require 'labclient/approvals/merge_request/update'
require 'labclient/approvals/merge_request/rules'
require 'labclient/approvals/merge_request/create_rule'
require 'labclient/approvals/merge_request/update_rule'
require 'labclient/approvals/merge_request/delete_rule'
require 'labclient/approvals/merge_request/approve'
require 'labclient/approvals/merge_request/unapprove'

require 'labclient/approvals/rule'
require 'labclient/approvals/merge_approval'

# Resource Labels
# Issue
require 'labclient/resource_labels/issues/client'
require 'labclient/resource_labels/issues/list'
require 'labclient/resource_labels/issues/show'

# Epics
require 'labclient/resource_labels/epics/client'
require 'labclient/resource_labels/epics/list'
require 'labclient/resource_labels/epics/show'

# System Hooks
require 'labclient/system_hooks/list'
require 'labclient/system_hooks/add'
require 'labclient/system_hooks/test'
require 'labclient/system_hooks/delete'
require 'labclient/system_hooks/system_hook'

# Merge Requests
require 'labclient/resource_labels/merge_requests/client'
require 'labclient/resource_labels/merge_requests/list'
require 'labclient/resource_labels/merge_requests/show'

# Feature Flags
require 'labclient/feature_flags/create'
require 'labclient/feature_flags/delete'
require 'labclient/feature_flags/list'
require 'labclient/feature_flags/feature_flag'

# Files
require 'labclient/files/file'
require 'labclient/files/show'
require 'labclient/files/create'
require 'labclient/files/update'
require 'labclient/files/delete'

# Terraform
require 'labclient/terraform/create'
require 'labclient/terraform/lock'
require 'labclient/terraform/unlock'
require 'labclient/terraform/delete'
require 'labclient/terraform/show'
require 'labclient/terraform/terraform_state'

require 'labclient/resource_labels/resource_label'

# Generators
require 'labclient/generator/names'
require 'labclient/generator/template_helper' # Default Include Template

# Wizard
require 'labclient/generator/generator'
require 'labclient/generator/wizard'

# Dynamically Require Templates (Simplify new template creation)
Dir["#{File.dirname(__FILE__)}/labclient/generator/templates/*.rb"].each { |file| require file }

# Load Client Files - I am Very Last!
require 'labclient/client/setup'
require 'labclient/client/helpers'
require 'labclient/client/meta'
require 'labclient/client'
