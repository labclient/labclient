# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Time Stats' do
      title 'Show'
      desc 'Get time tracking stats. [Project ID, mr iid]'
      example 'client.merge_requests.time_stats(333, 1)'
      result <<~DOC
        => {:time_estimate=>0, :total_time_spent=>0, :human_time_estimate=>nil, :human_total_time_spent=>nil}
      DOC
    end

    doc 'Time Stats' do
      desc 'Show through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(333, 1)'
        mr.time_stats
      DOC
    end

    def time_stats(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}/time_stats")
    end

    doc 'Time Stats' do
      title 'Add'
      desc 'Adds spent time for this merge request. [Project ID, mr iid, duration]'
      markdown 'Duration accepts Human Format, or ActiveSupport::Duration (e.g. 1.hour)'
    end

    doc 'Time Stats' do
      desc 'Duration Example'
      example 'client.merge_requests.add_spent_time(333, 1, 1.hour)'
    end

    doc 'Time Stats' do
      desc 'Human Format Example'
      example 'client.merge_requests.add_spent_time(333, 1, "1h")'
    end

    doc 'Time Stats' do
      desc 'Add through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(333, 1)
        mr.add_spent_time(1.hour)
      DOC

      result <<~DOC
        mr.add_spent_time(1.hour)
        => {:time_estimate=>0, :total_time_spent=>12978000, :human_time_estimate=>nil, :human_total_time_spent=>"22mo 2w 5h"}
        mr.add_spent_time('1h')
        => {:time_estimate=>0, :total_time_spent=>12978001, :human_time_estimate=>nil, :human_total_time_spent=>"22mo 2w 5h 1s"}
      DOC
    end

    def add_spent_time(project_id, merge_request_id, duration)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      duration = ChronicDuration.output(ChronicDuration.parse(duration.to_s), format: :short)
      client.request(:post, "projects/#{project_id}/merge_requests/#{merge_request_id}/add_spent_time", nil, duration: duration)
    end

    doc 'Time Stats' do
      title 'Estimate'
      desc 'Sets an estimated time of work for this merge request. [Project ID, mr iid, duration]'
      markdown 'Duration accepts Human Format, or ActiveSupport::Duration (e.g. 1.hour)'
    end

    doc 'Time Stats' do
      desc 'Duration Example'
      example 'client.merge_requests.time_estimate(333, 1, 1.hour)'
    end

    doc 'Time Stats' do
      desc 'Human Format Example'
      example 'client.merge_requests.time_estimate(333, 1, "1h")'
    end

    doc 'Time Stats' do
      desc 'Add through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(333, 1)
        mr.add_spent_time(1.hour)
      DOC

      result <<~DOC
        mr.add_spent_time(1.hour)
        => {:time_estimate=>0, :total_time_spent=>12978000, :human_time_estimate=>nil, :human_total_time_spent=>"22mo 2w 5h"}
        mr.add_spent_time('1h')
        => {:time_estimate=>0, :total_time_spent=>12978001, :human_time_estimate=>nil, :human_total_time_spent=>"22mo 2w 5h 1s"}
      DOC
    end

    def time_estimate(project_id, merge_request_id, duration)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      duration = ChronicDuration.output(ChronicDuration.parse(duration.to_s), format: :short)
      client.request(:post, "projects/#{project_id}/merge_requests/#{merge_request_id}/time_estimate", nil, duration: duration)
    end

    # Resets
    doc 'Time Stats' do
      title 'Reset'
      subtitle 'Estimate'
      desc 'Resets the estimated time for this merge request to 0 seconds. [Project ID, mr iid]'
      example 'client.merge_requests.reset_time_estimate(333, 1)'
      result <<~DOC
        => {:time_estimate=>0, :total_time_spent=>0, :human_time_estimate=>nil, :human_total_time_spent=>nil}
      DOC
    end

    doc 'Time Stats' do
      desc 'Show through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(333, 1)'
        mr.reset_time_estimate
      DOC
    end

    def reset_time_estimate(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:post, "projects/#{project_id}/merge_requests/#{merge_request_id}/reset_time_estimate")
    end

    doc 'Time Stats' do
      subtitle 'Time Spent'
      desc 'Resets the estimated time for this merge request to 0 seconds. [Project ID, mr iid]'
      example 'client.merge_requests.reset_spent_time(333, 1)'
      result <<~DOC
        => {:time_estimate=>0, :total_time_spent=>0, :human_time_estimate=>nil, :human_total_time_spent=>nil}
      DOC
    end

    doc 'Time Stats' do
      desc 'Show through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(333, 1)
        mr.reset_spent_time
      DOC
    end

    def reset_spent_time(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:post, "projects/#{project_id}/merge_requests/#{merge_request_id}/reset_spent_time")
    end
  end
end
