# Top namespace
module LabClient
  # Inspect Helper
  class MergeRequest < Klass
    include ClassHelpers
    # extend Docs

    def inspect
      "#<MergeRequest id: #{id}, title: #{title}, state: #{state}>"
    end

    # User Fields
    user_attrs %i[author assignee merged_by closed_by]

    # DateTime Fields
    date_time_attrs %i[
      closed_at created_at first_deployed_to_production_at
      latest_build_finished_at latest_build_started_at merged_at updated_at
    ]

    def assignees
      @table[:assignees].map { |x| User.new(x, response, client) } if has? :assignees
    end

    def participants
      client.merge_requests.participants(project_id, iid)
    end

    def changes
      client.merge_requests.changes(project_id, iid)
    end

    def commits
      client.merge_requests.commits(project_id, iid)
    end

    def pipelines
      client.merge_requests.pipelines(project_id, iid)
    end

    def create_pipeline
      client.merge_requests.create_pipeline(project_id, iid)
    end

    def update(query)
      update_self client.merge_requests.update(project_id, iid, query)
    end

    def delete
      client.merge_requests.delete(project_id, iid)
    end

    def accept(query = {})
      client.merge_requests.accept(project_id, iid, query)
    end

    def merge_ref
      client.merge_requests.merge_ref(project_id, iid)
    end

    def cancel_auto_merge
      client.merge_requests.cancel_auto_merge(project_id, iid)
    end

    def rebase(skip_ci = false)
      client.merge_requests.rebase(project_id, iid, skip_ci)
    end

    def add_spent_time(duration)
      client.merge_requests.add_spent_time(project_id, iid, duration)
    end

    def time_estimate(duration)
      client.merge_requests.time_estimate(project_id, iid, duration)
    end

    def reset_time_estimate
      client.merge_requests.reset_time_estimate(project_id, iid)
    end

    def reset_spent_time
      client.merge_requests.reset_spent_time(project_id, iid)
    end

    def closes_issues
      client.merge_requests.closes_issues(project_id, iid)
    end

    def subscribe
      client.merge_requests.subscribe(project_id, iid)
    end

    def unsubscribe
      client.merge_requests.unsubscribe(project_id, iid)
    end

    def notes
      client.notes.merge_requests.list(project_id, iid)
    end

    def note_create(query)
      client.notes.merge_requests.create(project_id, iid, query)
    end

    def todo
      client.merge_requests.todo(project_id, iid)
    end

    def time_stats
      client.merge_requests.time_stats(project_id, iid)
    end

    def diff_versions
      client.merge_requests.diff_versions(project_id, iid)
    end

    # Default to Source Project
    def project
      client.projects.show(source_project_id)
    end

    def source_project
      client.projects.show(source_project_id)
    end

    def target_project
      client.projects.show(target_project_id)
    end

    # Approvals
    def approvals
      client.approvals.merge_request.show(project_id, iid)
    end

    def approvals_update(approvals_required)
      client.approvals.merge_request.update(project_id, iid, approvals_required)
    end

    def approvals_rules
      client.approvals.merge_request.rules(project_id, iid)
    end

    def approvals_rule_create(query = {})
      client.approvals.merge_request.create_rule(project_id, iid, query)
    end

    def approvals_rule_update(approval_rule_id, query = {})
      client.approvals.merge_request.update_rule(project_id, iid, approval_rule_id, query)
    end

    def approvals_rule_delete(approval_rule_id)
      client.approvals.merge_request.delete_rule(project_id, iid, approval_rule_id)
    end

    def approve(query = {})
      client.approvals.merge_request.approve(project_id, iid, query)
    end

    def unapprove
      client.approvals.merge_request.unapprove(project_id, iid)
    end

    # Resource Labels
    def resource_labels
      client.resource_labels.merge_requests.list(project_id, iid)
    end

    def resource_label(resource_event_id)
      client.resource_labels.merge_requests.show(project_id, iid, resource_event_id)
    end

    # Reload
    def reload
      update_self client.merge_requests.show(project_id, iid)
    end

    # Wait for Import / Set a Hard Limit
    def wait_for_merge_status(total_time = 300, sleep_time = 15)
      # :unchecked
      # :cannot_be_merged_recheck
      # :checking
      # :cannot_be_merged_rechecking
      # :can_be_merged
      # :cannot_be_merged

      # Success
      # [unchecked, can_be_merged]

      # Fail
      # [cannot_be_merged cannot_be_merged_recheck]

      Timeout.timeout(total_time) do
        loop do
          reload
          logger.info "Waiting for Merge Status: #{merge_status}" unless quiet?
          break if %w[can_be_merged unchecked].include? merge_status
          raise "Cannot be merged! #{import_error}" if %w[cannot_be_merged cannot_be_merged_recheck].include? merge_status

          sleep sleep_time
        end
      end
    end

    # rubocop:disable Metrics/BlockLength
    help do
      subtitle 'MergeRequest'
      option 'accept', 'Merge changes submitted [Hash]'
      option 'add_time_spent', 'Adds spent time. Accepts Duration or Human Format. (1.hour or "1h")'
      option 'changes', 'Show merge request changes'
      option 'closes_issues', 'Show issues that this merge request will close'
      option 'commits', 'List associated commits.'
      option 'create_pipeline', 'Create new pipeline for this merge request.'
      option 'delete', 'Delete this merge request.'
      option 'merge_ref', 'Show Merge Ref'
      option 'reload', 'Reload this merge request object (New API Call)'
      option 'wait_for_merge_status', 'Looping, wait for merge request status [Timeout, Interval]'

      # Notes
      option 'notes', 'List notes/comments. [Hash]'
      option 'note_create', 'Creates a new note. [Hash]'

      option 'participants', 'List participants in this merge request.'
      option 'pipelines', 'List pipelines in this merge request.'
      option 'reset_spent_time', 'Resets the spent time'
      option 'reset_time_estimate', 'Resets the estimated time'
      option 'subscribe', 'Subscribe to notifications'
      option 'time_estimate', 'Sets an estimated time. Accepts Duration or Human Format. (1.hour or "1h")'
      option 'time_stats', 'Get time tracking stats.'
      option 'todo', 'Create todo'
      option 'unsubscribe', 'Unsubscribe to notifications'
      option 'update', 'Update merge request (accepts hash).'
      option 'project', 'Return associated Project (default to source project).'
      option 'source_project', 'MR Source Project'
      option 'target_project', 'MR Target Project'

      # Approvals
      option 'approvals', 'Show Merge Request approval configuration'
      option 'approvals_update', 'Change approval configuration [Integer]'
      option 'approvals_rules', 'Show Merge Request approval rules'

      option 'approvals_rule_create', 'Create Approval Rule [Hash]'
      option 'approvals_rule_update', 'Updating existing Approval Rule [Rule ID, Hash]'
      option 'approvals_rule_delete', 'Delete Approval Rule [Rule ID]'
      option 'approve', 'Approve Merge Request [Hash]'
      option 'unapprove', 'Unapprove Merge Request'

      # Resource Labels
      option 'resource_labels', 'List of all label events'
      option 'resource_label', 'Show single label event [Resource Event ID]'
    end

    # rubocop:enable Metrics/BlockLength
  end
end
