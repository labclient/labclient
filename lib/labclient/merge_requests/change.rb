# Top namespace
module LabClient
  # Inspect Helper, 'Changes' result for MR
  class Changes < Klass
    include ClassHelpers
    # extend Docs

    def inspect
      "#<Changes MR: #{id}, title: #{title}>"
    end

    # User Fields
    user_attrs %i[author assignee merged_by closed_by]

    # DateTime Fields
    date_time_attrs %i[
      closed_at created_at first_deployed_to_production_at
      latest_build_finished_at latest_build_started_at merged_at updated_at
    ]

    def assignees
      @table[:assignees].map { |x| User.new(x, response, client) } if has? :assignees
    end
  end
end
