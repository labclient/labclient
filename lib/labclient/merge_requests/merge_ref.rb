# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Show' do
      title 'Merge Ref'
      markdown <<~DOC
        - Merge the changes between the merge request source and target branches into `refs/merge-requests/:iid/merge` ref, of the target project repository, if possible. This ref will have the state the target branch would have if a regular merge action was taken.

        - This is not a regular merge action given it doesn't change the merge request target branch state in any manner.

        - This ref (`refs/merge-requests/:iid/merge`) isn't necessarily overwritten when submitting
        requests to this API, though it'll make sure the ref has the latest possible state.

        - If the merge request has conflicts, is empty or already merged, you'll get a `400` and a descriptive error message.

        - It returns the HEAD commit of `refs/merge-requests/:iid/merge` in the response body in case of `200`.

        - [Official Docs](https://docs.gitlab.com/ee/api/merge_requests.html#merge-to-default-merge-ref-path)

        ---
      DOC

      example <<~DOC
        client.merge_requests.merge_ref(343, 2)
      DOC

      result <<~DOC
        => {:commit_id=>"f9b62f6aaaabe2f0bd7563701670fcd74d694c37"}
      DOC
    end

    doc 'Show' do
      desc 'Through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(343,2)
        mr.merge_ref
      DOC
    end

    def merge_ref(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}/merge_ref")
    end
  end
end
