# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Create' do
      desc 'Creates a new merge request'

      example <<~DOC
        client.merge_requests.create(343, {
          title: 'New MR!',
          source_branch: 'sweet-release',
          target_branch: :master
        })
      DOC

      result <<~DOC
        => #<MergeRequest id: 2, title: New MR!>
      DOC
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute                  | Type           | Required | Description                                                                     |
        | ---------                  | ----           | -------- | -----------                                                                     |
        | id                         | integer/string | yes      | The ID or owned by the authenticated user |
        | source_branch              | string         | yes      | The source branch                                                               |
        | target_branch              | string         | yes      | The target branch                                                               |
        | title                      | string         | yes      | Title of MR                                                                     |
        | assignee_id                | integer        | no       | Assignee user ID                                                                |
        | assignee_ids               | integer array  | no       | The ID of the user(s) to assign the MR to. Set to 0 or provide an empty value to unassign all assignees.  |
        | description                | string         | no       | Description of MR. Limited to 1,048,576 characters. |
        | target_project_id          | integer        | no       | The target project (numeric id)                                                 |
        | labels                     | string         | no       | Labels for MR as a comma-separated list                                         |
        | milestone_id               | integer        | no       | The global ID of a milestone                                                           |
        | remove_source_branch       | boolean        | no       | Flag indicating if a merge request should remove the source branch when merging |
        | allow_collaboration        | boolean        | no       | Allow commits from members who can merge to the target branch                   |
        | allow_maintainer_to_push   | boolean        | no       | Deprecated, see allow_collaboration                                             |
        | squash                     | boolean        | no       | Squash commits into a single commit when merging                                |


      DOC
    end

    doc 'Create' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.merge_request_create(
          title: 'New MR!',
          source_branch: 'sweet-release',
          target_branch: :master
        )
      DOC
    end

    # Create
    def create(project_id, query)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/merge_requests", MergeRequest, query)
    end
  end
end
