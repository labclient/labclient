# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Show' do
      title 'Notes'
      desc 'Notes are handled via the Notes resource. Accessible via the object or notes'
      example <<~DOC
        mr = client.merge_requests.show(343,2)
        mr.notes
      DOC

      result <<~DOC
        => [#<Note id: 21, type: MergeRequest>, #<Note id: 20, type: MergeRequest>, .. ]
      DOC
    end
  end
end
