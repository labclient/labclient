# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Show' do
      title 'Changes'
      desc 'Shows information about the merge request including its files and changes. [Project ID, mr iid]'
      example 'client.merge_requests.changes(333,1)'
      result <<~DOC
        => #<Changes MR: 1, title: Update README.md>
      DOC
    end

    doc 'Show' do
      desc 'Show through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(333,1)'
        mr.changes
      DOC

      result <<~DOC
        => #<Changes MR: 1, title: Update README.md>
      DOC
    end

    # Show Specific
    def changes(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}/changes", Changes)
    end
  end
end
