# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Show' do
      desc 'Shows information about a single merge request. [Project ID, mr iid]'
      example 'client.merge_requests.show(333,1)'
      result <<~DOC
        => #<MergeRequest id: 37, title: Add new file>
      DOC
    end

    doc 'Show' do
      desc 'Parameters'
      markdown <<~DOC
        | Attribute                      | Description                                                         |
        | ------------------------------ | ------------------------------------------------------------------- |
        | render_html                    | If true response includes rendered HTML for title and description   |
        | include_diverged_commits_count | If true response includes the commits behind the target branch      |
        | include_rebase_in_progress     | If true response includes whether a rebase operation is in progress |
      DOC
    end

    # Show Specific
    def show(project_id, merge_request_id = nil, query = {})
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}", MergeRequest, query)
    end
  end
end
