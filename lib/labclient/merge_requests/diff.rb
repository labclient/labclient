# Top namespace
module LabClient
  # Inspect Helper
  class MergeRequestDiff < Klass
    include ClassHelpers
    # extend Docs

    def inspect
      "#<MergeRequestDiff id: #{id}, MR: #{merge_request_id}, state: #{state}>"
    end

    def details
      project_id = collect_project_id

      update_self client.merge_requests.diff_versions(project_id, merge_request_id, id)
    end

    # DateTime Fields
    date_time_attrs %i[created_at]

    help do
      @group_name = 'Merge Requests'
      subtitle 'Diff'
      desc 'Details is only needed if accessing a diff via the list. Diff loaded via `details` produces the same result'
      option 'details', 'Requests all Diff details and will reload.'
    end
  end
end
