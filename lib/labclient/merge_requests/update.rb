# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Update' do
      desc 'Updates an existing merge request. You can change the target branch, title, or even close the MR. [Project ID, merge request iid]'

      example <<~DOC
        client.merge_requests.update(343, 2, {
          title: 'Better Title'
        })
      DOC

      result <<~DOC
        => #<MergeRequest id: 2, title: Better Title>
      DOC
    end

    doc 'Update' do
      desc 'List through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(343,2)
        mr.update(title: 'Dew it')
      DOC
    end

    # Update
    def update(project_id, merge_request_id, query)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:put, "projects/#{project_id}/merge_requests/#{merge_request_id}", MergeRequest, query)
    end
  end
end
