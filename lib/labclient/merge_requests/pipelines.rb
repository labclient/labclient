# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Pipelines' do
      title 'List'
      desc 'Get a list of merge request pipelines. [Project ID, mr iid]'
      example 'client.merge_requests.pipelines(343,1)'
      result <<~DOC
        => [#<Pipeline id: 4, ref: branch, status: success>]
      DOC
    end

    doc 'Pipelines' do
      desc 'List through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(343,1)
        mr.pipelines
      DOC

      result <<~DOC
        => [#<Pipeline id: 4, ref: branch, status: success>]
      DOC
    end

    # Show Specific
    def pipelines(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}/pipelines", Pipeline)
    end

    doc 'Pipelines' do
      title 'Create'
      desc 'Create a new pipeline for a merge request. Requires .gitlab-ci.yml to be configured with only: [merge_requests] to create jobs. [Project ID, mr iid]'
      example 'client.merge_requests.create_pipeline(343, 1)'
      result <<~DOC
        => #<Pipeline id: 7, ref: refs/merge-requests/1/head, status: pending>
      DOC
    end

    doc 'Pipelines' do
      desc 'Create through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(343,1)
        mr.create_pipeline
      DOC

      result <<~DOC
        => #<Pipeline id: 8, ref: refs/merge-requests/1/head, status: pending>
      DOC
    end

    # Create New Pipeline
    def create_pipeline(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:post, "projects/#{project_id}/merge_requests/#{merge_request_id}/pipelines", Pipeline)
    end
  end
end
