# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Diff' do
      title 'Versions'
      desc 'Get a list of merge request diff versions. [Project ID, mr iid]'
      example 'client.merge_requests.diff_versions(333,1)'
      result <<~DOC
        => [#<MergeRequestDiff id: 3, MR: 1, state: collected>, #<MergeRequestDiff id: 2, MR: 1, state: collected>, ..]
      DOC
    end

    doc 'Diff' do
      desc 'List through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(343,1)
        mr.diff_versions
      DOC

      result <<~DOC
        mr = client.merge_requests.show(343,1)
        => #<MergeRequest id: 1, title: Update README.md, state: opened>
        mr.diff_versions
        => [#<MergeRequestDiff id: 3, MR: 1, state: collected>, #<MergeRequestDiff id: 2, MR: 1, state: collected>, ..]
      DOC
    end

    doc 'Diff' do
      title 'Details'
      example 'client.merge_requests.diff_versions(333,1, 3)'
      result <<~DOC
        => #<MergeRequestDiff id: 3, MR: 1, state: collected>
      DOC
    end

    doc 'Diff' do
      desc 'Details through MergeRequestDiff'
      example <<~DOC
        diff = client.merge_requests.diff_versions(343,1).first
        diff.details
      DOC

      result <<~DOC
        mr = client.merge_requests.show(343,1)
        => #<MergeRequest id: 1, title: Update README.md, state: opened>
        mr.diff_versions
        => [#<MergeRequestDiff id: 3, MR: 1, state: collected>, #<MergeRequestDiff id: 2, MR: 1, state: collected>, ..]
        diff = mr.diff_versions.first
        => #<DiffMergeRequestDiff id: 3, MR: 1, state: collected>
        # Commits and Diffs available with details
        diff.details.keys - mr.diff_versions.first.keys
        => [:commits, :diffs]
      DOC
    end

    # Show Specific
    def diff_versions(project_id, mr_id, diff_id = nil)
      project_id = format_id(project_id)
      mr_id = format_id(mr_id)
      diff_id = format_id(diff_id)

      if diff_id
        client.request(:get, "projects/#{project_id}/merge_requests/#{mr_id}/versions/#{diff_id}", MergeRequestDiff)
      else
        client.request(:get, "projects/#{project_id}/merge_requests/#{mr_id}/versions", MergeRequestDiff)
      end
    end
  end
end
