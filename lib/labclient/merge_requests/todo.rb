# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Update' do
      title 'Todo'
      desc 'Manually creates a todo for the current user on a merge request. [Project ID, merge request iid]'

      example 'client.merge_requests.todo(343, 7)'

      result <<~DOC
        => #<Todo id: 7513, state: pending>
      DOC
    end

    doc 'Update' do
      desc 'Create Todo through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(343,2)
        mr.todo
      DOC
    end

    def todo(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:post, "projects/#{project_id}/merge_requests/#{merge_request_id}/todo", Todo)
    end
  end
end
