# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'List' do
      desc 'Get all merge requests the authenticated user has access to. By default it returns only merge requests created by the current user. To get all merge requests, use parameter scope=all.'
      example 'client.merge_requests.list'
      result <<~DOC
        => [#<MergeRequest id: 37, title: Add new file>, ...]
      DOC
    end

    doc 'List' do
      desc 'List only opened'
      example 'client.merge_requests.list(state: :opened)'
      result <<~DOC
        => [#<MergeRequest id: 32, title: Just Opened>, ...]
      DOC
    end

    doc 'List' do
      desc 'Filter by date'
      example 'client.merge_requests.list(created_after: 10.day.ago)'
      result <<~DOC
        => [#<MergeRequest id: 37, title: Add new file>, #<MergeRequest id: 36, title: Update README.md>, ...]
      DOC
    end

    doc 'List' do
      desc 'Parameters'
      markdown <<~DOC
        | Attribute           | Type           | Required | Description                                                                                                            |
        | ------------------- | --------       | -------- | ---------------------------------------------------------------------------------------------------------------------- |
        | state             | string         | no       | Return all merge requests or just those that are opened, closed, locked, or merged                             |
        | order_by          | string         | no       | Return requests ordered by created_at or updated_at fields. Default is created_at                                |
        | sort              | string         | no       | Return requests sorted in asc or desc order. Default is desc                                                     |
        | milestone         | string         | no       | Return merge requests for a specific milestone. None returns merge requests with no milestone. Any returns merge requests that have an assigned milestone. |
        | view              | string         | no       | If simple, returns the iid, URL, title, description, and basic state of merge request                              |
        | labels            | string         | no       | Return merge requests matching a comma separated list of labels. None lists all merge requests with no labels. Any lists all merge requests with at least one label. No+Label (Deprecated) lists all merge requests with no labels. Predefined names are case-insensitive. |
        | with_labels_details | Boolean        | no         | If true, response will return more details for each label in labels field: :name, :color, :description, :description_html, :text_color. Default is false. Introduced in GitLab 12.7 |
        | created_after     | datetime       | no       | Return merge requests created on or after the given time                                                               |
        | created_before    | datetime       | no       | Return merge requests created on or before the given time                                                              |
        | updated_after     | datetime       | no       | Return merge requests updated on or after the given time                                                               |
        | updated_before    | datetime       | no       | Return merge requests updated on or before the given time                                                              |
        | scope             | string         | no       | Return merge requests for the given scope: created_by_me, assigned_to_me or all. Defaults to created_by_me<br> For versions before 11.0, use the now deprecated created-by-me or assigned-to-me scopes instead. |
        | author_id         | integer        | no       | Returns merge requests created by the given user id. Combine with scope=all or scope=assigned_to_me              |
        | assignee_id       | integer        | no       | Returns merge requests assigned to the given user id. None returns unassigned merge requests. Any returns merge requests with an assignee. |
        | approver_ids **(STARTER)** | integer array | no | Returns merge requests which have specified all the users with the given ids as individual approvers. None returns merge requests without approvers. Any returns merge requests with an approver. |
        | my_reaction_emoji | string         | no       | Return merge requests reacted by the authenticated user by the given emoji. None returns issues not given a reaction. Any returns issues given at least one reaction. Introduced in GitLab 10.0 |
        | source_branch     | string         | no       | Return merge requests with the given source branch                                                                     |
        | target_branch     | string         | no       | Return merge requests with the given target branch                                                                     |
        | search            | string         | no       | Search merge requests against their title and description                                                          |
        | in                | string         | no       | Modify the scope of the search attribute. title, description, or a string joining them with comma. Default is title,description |
        | wip               | string         | no       | Filter merge requests against their wip status. yes to return *only* WIP merge requests, no to return *non* WIP merge requests |
      DOC
    end

    doc 'List' do
      title 'Project'
      desc 'List project merge requests [Project ID, Hash]'
      example 'client.merge_requests.list_project(16, created_after: 10.day.ago)'
      result <<~DOC
        => [#<MergeRequest id: 37, title: Add new file>, #<MergeRequest id: 36, title: Update README.md>, ...]
      DOC
    end

    doc 'List' do
      title 'Group'
      desc 'List group merge requests [Group ID, Hash]'
      example 'client.merge_requests.list_group(5, created_after: 10.day.ago)'
      result <<~DOC
        => [#<MergeRequest id: 37, title: Add new file>, #<MergeRequest id: 36, title: Update README.md>, ...]
      DOC
    end

    # List
    def list(query = {})
      %i[created_after created_before updated_after updated_before].each do |field|
        query[field] = query[field].to_time.iso8601 if format_time?(query[field])
      end

      client.request(:get, 'merge_requests', MergeRequest, query)
    end

    # Project
    def list_project(project_id, query = {})
      project_id = format_id(project_id)
      %i[created_after created_before updated_after updated_before].each do |field|
        query[field] = query[field].to_time.iso8601 if format_time?(query[field])
      end

      client.request(:get, "projects/#{project_id}/merge_requests", MergeRequest, query)
    end

    # Group
    def list_group(group_id, query = {})
      group_id = format_id(group_id)
      %i[created_after created_before updated_after updated_before].each do |field|
        query[field] = query[field].to_time.iso8601 if format_time?(query[field])
      end

      client.request(:get, "groups/#{group_id}/merge_requests", MergeRequest, query)
    end
  end
end
