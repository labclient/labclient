# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Show' do
      title 'Participants'
      desc 'Get a list of merge request participants. [Project ID, mr iid]'
      example 'client.merge_requests.participants(333,1)'
      result <<~DOC
        => [#<User id: 1, username: braum>, #<User id: 52, username: anivia>]
      DOC
    end

    doc 'Show' do
      desc 'List through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(343,1)
        mr.participants
      DOC
      result <<~DOC
        mr = client.merge_requests.show(343,1)
        => #<MergeRequest id: 1, title: Update README.md>
        mr.participants
        => [#<User id: 1, username: root>]
      DOC
    end

    # Show Specific
    def participants(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}/participants", User)
    end
  end
end
