# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Update' do
      title 'Cancel Auto Merge'
      desc 'Cancel Automatic Merge When Pipeline Succeeds [Project ID, merge request iid]'
      markdown <<~DOC
        - If you don't have permissions to accept this merge request - you'll get a `401`

        - If the merge request is already merged or closed - you get `405` and error message 'Method Not Allowed'

        - In case the merge request is not set to be merged when the pipeline succeeds, you'll also get a `406` error.
      DOC

      example 'client.merge_requests.cancel_auto_merge(343, 3)'

      result <<~DOC
        => #<MergeRequest id: 3, title: Add new file>
      DOC
    end

    doc 'Update' do
      desc 'Cancel through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(343,2)
        mr.cancel_auto_merge
      DOC
    end

    def cancel_auto_merge(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:post, "projects/#{project_id}/merge_requests/#{merge_request_id}/cancel_merge_when_pipeline_succeeds", MergeRequest)
    end
  end
end
