# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Delete' do
      desc 'Only for admins and project owners. Deletes the merge request in question. [Project ID, merge request iid]'

      example 'client.merge_requests.delete(343, 2)'

      result <<~DOC
        => nil
      DOC
    end

    doc 'Delete' do
      desc 'Delete via MergeRequest'
      example <<~DOC
        mr = client.merge_requests.delete(343,2)
        mr.delete
      DOC
    end

    doc 'Delete' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(1)
        project.merge_requests_delete(12)
      DOC
    end

    # Delete
    def delete(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:delete, "projects/#{project_id}/merge_requests/#{merge_request_id}")
    end
  end
end
