# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Update' do
      title 'Rebase'
      desc 'Rebase a merge request [Project ID, merge request iid, skip_ci (true/false)]'
      markdown <<~DOC
        - Automatically rebase the `source_branch` of the merge request against its `target_branch`.

        - If you don't have permissions to push to the merge request's source branch you'll get a `403 Forbidden` response.
      DOC

      example 'client.merge_requests.rebase(343, 3)'

      result <<~DOC
        => {:rebase_in_progress=>true}
      DOC
    end

    doc 'Update' do
      desc 'Rebase skipping CI'

      example 'client.merge_requests.rebase(343, 3, true)'
      result <<~DOC
        => {:rebase_in_progress=>true}
      DOC
    end

    doc 'Update' do
      desc 'Rebase through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(343,2)
        mr.rebase
      DOC
    end

    def rebase(project_id, merge_request_id, skip_ci = false)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:put, "projects/#{project_id}/merge_requests/#{merge_request_id}/rebase", nil, skip_ci: skip_ci)
    end
  end
end
