# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Update' do
      title 'Accept'
      desc 'Merge changes submitted with MR. [Project ID, merge request iid]'

      markdown <<~DOC
          - <small>if merge request is unable to be accepted (ie: Work in Progress, Closed, Pipeline Pending Completion, or Failed while requiring Success) - you'll get a `405` and the error message 'Method Not Allowed'</small>
          - <small>If it has some conflicts and can not be merged - you'll get a `406` and the error message 'Branch cannot be merged'</small>
          - <small>If the `sha` parameter is passed and does not match the HEAD of the source - you'll get a `409` and the error message 'SHA </small>does not match HEAD of source branch'
          - <small>If you don't have permissions to accept this merge request - you'll get a `401`</small>

        | **Attribute**                | **Attribute**                                                                                     |
        | ---------------------------- | ------------------------------------------------------------------------------------------------- |
        | merge_commit_message         | Custom merge commit message                                                                       |
        | squash_commit_message        | Custom squash commit message                                                                      |
        | squash                       | if true the commits will be squashed into a single commit on merge                                |
        | should_remove_source_branch  | if true removes the source branch                                                                 |
        | merge_when_pipeline_succeeds | if true the MR is merged when the pipeline succeeds                                               |
        | sha                          | if present, then this SHA must match the HEAD of the source branch, otherwise the merge will fail |
      DOC

      example 'client.merge_requests.accept(343, 3)'

      result <<~DOC
        => #<MergeRequest id: 3, title: Add new file>
      DOC
    end

    doc 'Update' do
      desc 'Accept through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(343,2)
        mr.accept
      DOC
    end

    # Accept
    def accept(project_id, merge_request_id, query = {})
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:put, "projects/#{project_id}/merge_requests/#{merge_request_id}/merge", MergeRequest, query)
    end
  end
end
