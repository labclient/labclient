# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Show' do
      title 'Closes Issues'
      desc 'Get all the issues that would be closed by merging the provided merge request. [Project ID, mr iid]'
      example 'client.merge_requests.closes_issues(343, 1)'
      result <<~DOC
        => => [#<Issue id: 1, title: Problem!, state: opened>]
      DOC
    end

    doc 'Show' do
      desc 'List through MergeRequest'
      example <<~DOC
        mr = client.closes_issues.show(343,7)
        mr.participants
      DOC
      result <<~DOC
        mr = client.merge_requests.show(343,1)
        => #<MergeRequest id: 1, title: Update README.md>
        mr.closes_issues
        => [#<Issue id: 1, title: Problem!, state: opened>]
      DOC
    end

    def closes_issues(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}/closes_issues", Issue)
    end
  end
end
