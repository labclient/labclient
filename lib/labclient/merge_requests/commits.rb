# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Show' do
      title 'Commits'
      desc 'Get a list of merge request commits. [Project ID, mr iid]'
      example 'client.merge_requests.commits(333,1)'
      result <<~DOC
        => [#<Commit title: Update README.md, sha: 50c90734>, #<Commit title: Add new file, sha: 6534a685>]
      DOC
    end

    # Show Specific
    def commits(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}/commits", Commit)
    end
  end
end
