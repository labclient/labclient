# Top namespace
module LabClient
  # Specifics
  class MergeRequests < Common
    doc 'Update' do
      title 'Unsubscribe'
      desc 'Subscribes the authenticated user to a merge request to receive notification. [Project ID, merge request iid]'

      example 'client.merge_requests.unsubscribe(343, 7)'

      result <<~DOC
        => #<MergeRequest id: 3, title: Add new file>
      DOC
    end

    doc 'Update' do
      desc 'Unsubscribe through MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(343,2)
        mr.unsubscribe
      DOC
    end

    def unsubscribe(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:post, "projects/#{project_id}/merge_requests/#{merge_request_id}/unsubscribe", MergeRequest)
    end
  end
end
