# Top namespace
module LabClient
  # Specifics
  class SnippetNotes < Common
    doc 'Snippets' do
      title 'Create'
      desc 'Creates a new note for a single snippet [Project, Snippet ID]'
      example 'client.notes.snippets.create(16, 2, body: "Hello!")'
      result <<~DOC
        => #<Note id: 44, type: Snippet>
      DOC
    end

    doc 'Snippets' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
        | created_at | string | no | Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z |
      DOC
    end

    def create(project_id, snippet_id, query = {})
      project_id = format_id(project_id)
      snippet_id = format_id(snippet_id)

      query[:created_at] = query[:created_at].to_time.iso8601 if format_time?(query[:created_at])

      client.request(:post, "projects/#{project_id}/snippets/#{snippet_id}/notes", Note, query)
    end
  end
end
