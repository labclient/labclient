# Top namespace
module LabClient
  # Specifics
  class SnippetNotes < Common
    doc 'Snippets' do
      title 'Update'
      desc 'Get a single project snippet. [Project ID, Snippet ID, Note ID]'
      example 'client.notes.snippets.update(16, 1, 43, "Updaaate")'
      result <<~DOC
        => #<Note id: 43, type: Snippet>
      DOC
    end

    doc 'Snippets' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
      DOC
    end

    # Show
    def update(project_id, snippet_id, note_id, body)
      project_id = format_id(project_id)
      snippet_id = format_id(snippet_id)
      note_id = format_id(note_id)
      query = { body: body }

      client.request(:put, "projects/#{project_id}/snippets/#{snippet_id}/notes/#{note_id}", Note, query)
    end
  end
end
