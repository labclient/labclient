# Top namespace
module LabClient
  # Inspect Helper
  class SnippetNotes < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Notes < Common
    def snippets
      SnippetNotes.new(client)
    end
  end
end
