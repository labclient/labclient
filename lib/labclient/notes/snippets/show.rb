# Top namespace
module LabClient
  # Specifics
  class SnippetNotes < Common
    doc 'Snippets' do
      title 'Show'
      desc 'Returns a single note for a given snippet. [Project ID, Snippet ID, Note ID]'
      example 'client.notes.snippets.show(16, 1, 43)'
      result <<~DOC
        => #<Note id: 21, type: Snippet>
      DOC
    end

    def show(project_id, snippet_id, note_id)
      project_id = format_id(project_id)
      snippet_id = format_id(snippet_id)
      note_id = format_id(note_id)

      client.request(:get, "projects/#{project_id}/snippets/#{snippet_id}/notes/#{note_id}", Note)
    end
  end
end
