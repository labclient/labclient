# Top namespace
module LabClient
  # Specifics
  class SnippetNotes < Common
    @group_name = 'Notes'
    doc 'Snippets' do
      title 'List'
      desc 'Gets a list of all notes for a single snippet. [Project ID, Snippet ID]'
      markdown <<~DOC
        | Attribute | Type   | Required | Description                                                                          |
        | --------- | ------ | -------- | ------------------------------------------------------------------------------------ |
        | sort      | string | no       | Return snippet notes sorted in asc or desc order. Default is desc                      |
        | order_by  | string | no       | Return snippet notes ordered by created_at or updated_at fields. Default is created_at |
      DOC
    end

    doc 'Snippets' do
      example 'client.notes.snippets.list(16, 1)'
      result <<~DOC
        => [#<Note id: 21, type: Snippet>, #<Note id: 20, type: Snippet> .. ]
      DOC
    end

    doc 'Snippets' do
      desc 'Filter or Sort'
      example 'client.notes.snippets.list(16, 1, order_by: :created_at, sort: :asc)'
      result <<~DOC
        => [#<Note id: 21, type: Snippet>, #<Note id: 20, type: Snippet> .. ]
      DOC
    end

    def list(project_id, snippet_id, query = {})
      project_id = format_id(project_id)
      snippet_id = format_id(snippet_id)

      client.request(:get, "projects/#{project_id}/snippets/#{snippet_id}/notes", Note, query)
    end
  end
end
