# Top namespace
module LabClient
  # Specifics
  class SnippetNotes < Common
    doc 'Snippets' do
      title 'Delete'
      desc 'Get a single project snippet. [Project ID, Snippet ID, Note ID]'
      example 'client.notes.snippets.delete(16, 2, 44)'
    end

    # Delete
    def delete(project_id, snippet_id, note_id)
      project_id = format_id(project_id)
      snippet_id = format_id(snippet_id)
      note_id = format_id(note_id)

      client.request(:delete, "projects/#{project_id}/snippets/#{snippet_id}/notes/#{note_id}")
    end
  end
end
