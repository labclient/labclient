# Top namespace
module LabClient
  # Specifics
  class IssueNotes < Common
    doc 'Issues' do
      title 'Update'
      desc 'Get a single project issue. [Project ID, Issue IID, Note ID]'
      example 'client.notes.issues.update(16, 1, 43, "Updaaate")'
      result <<~DOC
        => #<Note id: 43, type: Issue>
      DOC
    end

    doc 'Issues' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
      DOC
    end

    # Show
    def update(project_id, issue_id, note_id, body)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)
      note_id = format_id(note_id)
      query = { body: body }

      client.request(:put, "projects/#{project_id}/issues/#{issue_id}/notes/#{note_id}", Note, query)
    end
  end
end
