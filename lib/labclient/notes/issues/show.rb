# Top namespace
module LabClient
  # Specifics
  class IssueNotes < Common
    doc 'Issues' do
      title 'Show'
      desc 'Returns a single note for a given issue. [Project ID, Issue IID, Note ID]'
      example 'client.notes.issues.show(16, 1, 43)'
      result <<~DOC
        => #<Note id: 21, type: Issue>
      DOC
    end

    def show(project_id, issue_id, note_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)
      note_id = format_id(note_id)

      client.request(:get, "projects/#{project_id}/issues/#{issue_id}/notes/#{note_id}", Note)
    end
  end
end
