# Top namespace
module LabClient
  # Specifics
  class IssueNotes < Common
    @group_name = 'Notes'

    doc 'Issues' do
      title 'List'
      desc 'Gets a list of all notes for a single issue. [Project ID, Issue IID]'
      markdown <<~DOC
        | Attribute | Type   | Required | Description                                                                          |
        | --------- | ------ | -------- | ------------------------------------------------------------------------------------ |
        | sort      | string | no       | Return issue notes sorted in asc or desc order. Default is desc                      |
        | order_by  | string | no       | Return issue notes ordered by created_at or updated_at fields. Default is created_at |
      DOC
    end

    doc 'Issues' do
      example 'client.notes.issues.list(16, 1)'
      result <<~DOC
        => [#<Note id: 21, type: Issue>, #<Note id: 20, type: Issue> .. ]
      DOC
    end

    doc 'Issues' do
      desc 'Sort or Filter'
      example 'client.notes.issues.list(16, 1, order_by: :created_at, sort: :asc)'
    end

    def list(project_id, issue_id, query = {})
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      client.request(:get, "projects/#{project_id}/issues/#{issue_id}/notes", Note, query)
    end
  end
end
