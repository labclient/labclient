# Top namespace
module LabClient
  # Specifics
  class IssueNotes < Common
    doc 'Issues' do
      title 'Create'
      desc 'Creates a new note for a single issue [Project, Issue IID]'
      example 'client.notes.issues.create(16, 2, body: "Hello!")'
      result <<~DOC
        => #<Note id: 44, type: Issue>
      DOC
    end

    doc 'Issues' do
      desc 'Via Issue'
      example <<~DOC
        issue = client.issues.show(16, 1)
        issue.note_create(body: 'There!')
      DOC
    end

    doc 'Issues' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
        | created_at | string | no | Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z |
      DOC
    end

    def create(project_id, issue_id, query = {})
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      query[:created_at] = query[:created_at].to_time.iso8601 if format_time?(query[:created_at])

      # POST /projects/:id/issues/:issue_iid/notes

      client.request(:post, "projects/#{project_id}/issues/#{issue_id}/notes", Note, query)
    end
  end
end
