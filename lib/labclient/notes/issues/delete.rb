# Top namespace
module LabClient
  # Specifics
  class IssueNotes < Common
    doc 'Issues' do
      title 'Delete'
      desc 'Get a single project issue. [Project ID, Issue IID, Note ID]'
      example 'client.notes.issues.delete(16, 2, 44)'
    end

    # Delete
    def delete(project_id, issue_id, note_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)
      note_id = format_id(note_id)

      client.request(:delete, "projects/#{project_id}/issues/#{issue_id}/notes/#{note_id}")
    end
  end
end
