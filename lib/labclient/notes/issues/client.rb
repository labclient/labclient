# Top namespace
module LabClient
  # Inspect Helper
  class IssueNotes < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Notes < Common
    def issues
      IssueNotes.new(client)
    end
  end
end
