# Top namespace
module LabClient
  # Specifics
  class EpicNotes < Common
    doc 'Epics' do
      title 'Show'
      desc 'Returns a single note for a given epic. [Project ID, Epic IID, Note ID]'
      example 'client.notes.epics.show(16, 1, 43)'
      result <<~DOC
        => #<Note id: 21, type: Epic>
      DOC
    end

    def show(group_id, epic_id, note_id)
      group_id = format_id(group_id)
      epic_id = format_id(epic_id)
      note_id = format_id(note_id)

      client.request(:get, "groups/#{group_id}/epics/#{epic_id}/notes/#{note_id}", Note)
    end
  end
end
