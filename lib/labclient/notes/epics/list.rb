# Top namespace
module LabClient
  # Specifics
  class EpicNotes < Common
    @group_name = 'Notes'
    doc 'Epics' do
      title 'List'
      desc 'Gets a list of all notes for a single epic. [Project ID, Epic IID]'
      markdown <<~DOC
        | Attribute | Type   | Required | Description                                                                          |
        | --------- | ------ | -------- | ------------------------------------------------------------------------------------ |
        | sort      | string | no       | Return epic notes sorted in asc or desc order. Default is desc                      |
        | order_by  | string | no       | Return epic notes ordered by created_at or updated_at fields. Default is created_at |
      DOC
    end

    doc 'Epics' do
      example 'client.notes.epics.list(16, 1)'
      result <<~DOC
        => [#<Note id: 21, type: Epic>, #<Note id: 20, type: Epic> .. ]
      DOC
    end

    doc 'Epics' do
      desc 'Filter or Sort'
      example 'client.notes.epics.list(16, 1, order_by: :created_at, sort: :asc)'
      result <<~DOC
        => [#<Note id: 21, type: Epic>, #<Note id: 20, type: Epic> .. ]
      DOC
    end

    def list(group_id, epic_id, query = {})
      group_id = format_id(group_id)
      epic_id = format_id(epic_id)

      client.request(:get, "groups/#{group_id}/epics/#{epic_id}/notes", Note, query)
    end
  end
end
