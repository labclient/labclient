# Top namespace
module LabClient
  # Specifics
  class EpicNotes < Common
    doc 'Epics' do
      title 'Create'
      desc 'Creates a new note for a single epic [Project, Epic IID]'
      example 'client.notes.epics.create(16, 2, body: "Hello!")'
      result <<~DOC
        => #<Note id: 44, type: Epic>
      DOC
    end

    doc 'Epics' do
      desc 'Via Epic'
      example <<~DOC
        epic = client.epics.show(16, 2)
        epic.note_create(body: 'There!')
      DOC
    end

    doc 'Epics' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
        | created_at | string | no | Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z |
      DOC
    end

    def create(group_id, epic_id, query = {})
      group_id = format_id(group_id)
      epic_id = format_id(epic_id)

      query[:created_at] = query[:created_at].to_time.iso8601 if format_time?(query[:created_at])

      client.request(:post, "groups/#{group_id}/epics/#{epic_id}/notes", Note, query)
    end
  end
end
