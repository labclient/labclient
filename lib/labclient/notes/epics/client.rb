# Top namespace
module LabClient
  # Inspect Helper
  class EpicNotes < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Notes < Common
    def epics
      EpicNotes.new(client)
    end
  end
end
