# Top namespace
module LabClient
  # Specifics
  class EpicNotes < Common
    doc 'Epics' do
      title 'Update'
      desc 'Get a single group epic. [Project ID, Epic IID, Note ID]'
      example 'client.notes.epics.update(16, 1, 43, "Updaaate")'
      result <<~DOC
        => #<Note id: 43, type: Epic>
      DOC
    end

    doc 'Epics' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
      DOC
    end

    # Show
    def update(group_id, epic_id, note_id, body)
      group_id = format_id(group_id)
      epic_id = format_id(epic_id)
      note_id = format_id(note_id)
      query = { body: body }

      client.request(:put, "groups/#{group_id}/epics/#{epic_id}/notes/#{note_id}", Note, query)
    end
  end
end
