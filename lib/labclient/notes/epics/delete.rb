# Top namespace
module LabClient
  # Specifics
  class EpicNotes < Common
    doc 'Epics' do
      title 'Delete'
      desc 'Get a single project epic. [Project ID, Epic IID, Note ID]'
      example 'client.notes.epics.delete(16, 2, 44)'
    end

    # Delete
    def delete(group_id, epic_id, note_id)
      group_id = format_id(group_id)
      epic_id = format_id(epic_id)
      note_id = format_id(note_id)

      client.request(:delete, "groups/#{group_id}/epics/#{epic_id}/notes/#{note_id}")
    end
  end
end
