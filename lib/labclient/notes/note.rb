# Top namespace
module LabClient
  # Inspect Helper
  class Note < Klass
    include ClassHelpers

    def inspect
      "#<Note id: #{id}, type: #{noteable_type}>"
    end

    # DateTime Fields
    date_time_attrs %i[created_at updated_at]

    # User Fields
    user_attrs %i[resolved_by author]
  end
end
