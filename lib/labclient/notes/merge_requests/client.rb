# Top namespace
module LabClient
  # Inspect Helper
  class MergeRequestNotes < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Notes < Common
    def merge_requests
      MergeRequestNotes.new(client)
    end
  end
end
