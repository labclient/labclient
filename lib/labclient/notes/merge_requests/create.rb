# Top namespace
module LabClient
  # Specifics
  class MergeRequestNotes < Common
    doc 'Merge Requests' do
      title 'Create'
      desc 'Creates a new note for a single merge request [Project, Merge Request IID]'
      example 'client.notes.merge_requests.create(16, 2, body: "Hello!")'
      result <<~DOC
        => #<Note id: 44, type: MergeRequest>
      DOC
    end

    doc 'Merge Request' do
      desc 'Via Merge Request'
      example <<~DOC
        merge_request = client.merge_requests.show(16, 2)
        merge_request.note_create(body: 'There!')
      DOC
    end

    doc 'Merge Requests' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
        | created_at | string | no | Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z |
      DOC
    end

    def create(project_id, merge_request_id, query = {})
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      query[:created_at] = query[:created_at].to_time.iso8601 if format_time?(query[:created_at])

      client.request(:post, "projects/#{project_id}/merge_requests/#{merge_request_id}/notes", Note, query)
    end
  end
end
