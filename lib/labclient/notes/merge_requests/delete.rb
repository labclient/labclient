# Top namespace
module LabClient
  # Specifics
  class MergeRequestNotes < Common
    doc 'Merge Requests' do
      title 'Delete'
      desc 'Get a single project issue. [Project ID, Merge Request IID, Note ID]'
      example 'client.notes.merge_requests.delete(16, 2, 44)'
    end

    # Delete
    def delete(project_id, merge_request_id, note_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)
      note_id = format_id(note_id)

      client.request(:delete, "projects/#{project_id}/merge_requests/#{merge_request_id}/notes/#{note_id}")
    end
  end
end
