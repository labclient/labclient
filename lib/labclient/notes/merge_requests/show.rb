# Top namespace
module LabClient
  # Specifics
  class MergeRequestNotes < Common
    doc 'Merge Requests' do
      title 'Show'
      desc 'Returns a single note for a given merge request. [Project ID, Merge Request IID, Note ID]'
      example 'client.notes.merge_requests.show(16, 1, 43)'
      result <<~DOC
        => #<Note id: 21, type: MergeRequest>
      DOC
    end

    def show(project_id, merge_request_id, note_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)
      note_id = format_id(note_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}/notes/#{note_id}", Note)
    end
  end
end
