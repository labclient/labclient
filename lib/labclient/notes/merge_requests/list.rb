# Top namespace
module LabClient
  # Specifics
  class MergeRequestNotes < Common
    @group_name = 'Notes'
    doc 'Merge Requests' do
      title 'List'
      desc 'Gets a list of all notes for a single merge request. [Project ID, Merge Request IID]'

      markdown <<~DOC
        | Attribute | Type   | Required | Description                                                                                  |
        | --------- | ------ | -------- | -------------------------------------------------------------------------------------------- |
        | sort      | string | no       | Return merge request notes sorted in asc or desc order. Default is desc                      |
        | order_by  | string | no       | Return merge request notes ordered by created_at or updated_at fields. Default is created_at |
      DOC
    end

    doc 'Merge Requests' do
      example 'client.notes.merge_requests.list(16, 1)'
      result <<~DOC
        => [#<Note id: 21, type: MergeRequest>, #<Note id: 20, type: MergeRequest> .. ]
      DOC
    end

    doc 'Merge Requests' do
      desc 'Sort or Filter'
      example 'client.notes.merge_requests.list(16, 1, order_by: :created_at, sort: :asc)'
      result <<~DOC
        => [#<Note id: 21, type: MergeRequest>, #<Note id: 20, type: MergeRequest> .. ]
      DOC
    end

    def list(project_id, merge_request_id, query = {})
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}/notes", Note, query)
    end
  end
end
