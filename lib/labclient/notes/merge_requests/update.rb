# Top namespace
module LabClient
  # Specifics
  class MergeRequestNotes < Common
    doc 'Merge Requests' do
      title 'Update'
      desc 'Get a single project issue. [Project ID, Merge Request IID, Note ID]'
      example 'client.notes.merge_requests.update(16, 1, 43, "Updaaate")'
      result <<~DOC
        => #<Note id: 43, type: MergeRequest>
      DOC
    end

    doc 'Merge Requests' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
      DOC
    end

    # Show
    def update(project_id, merge_request_id, note_id, body)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)
      note_id = format_id(note_id)
      query = { body: body }

      client.request(:put, "projects/#{project_id}/merge_requests/#{merge_request_id}/notes/#{note_id}", Note, query)
    end
  end
end
