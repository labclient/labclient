# Top Namesapce
module LabClient
  # Common Configuration for all Class Helpers
  class Klass < LabStruct
    include LabClient::Logger
    include CurlHelper

    attr_reader :client

    extend Docs

    # TODO: Awesome Print / Amazing Print Conflicts?
    def verbose
      ap @table, ruby19_syntax: true
    end

    # API Methods here have to be explicitly documented / custom helpers
    # Assume no methods by default
    def help(help_filter = nil)
      docs = LabClient::Docs.docs.dig(group_name, 'Reference')
      unless docs
        puts 'No Available Help'
        return false
      end

      puts klass
      docs.each do |doc|
        next unless doc[:options]

        doc[:options].each do |opt|
          next if help_filter && !(opt[:name] + opt[:text]).include?(help_filter.to_s)

          puts "  #{opt[:name]}"
          puts "    #{opt[:text]}\n"
        end
      end

      # Ignore Output
      nil
    end

    # Documented API Methods
    def api_methods
      docs = LabClient::Docs.docs.dig(group_name, 'Reference')

      unless docs
        puts 'No Available Help'
        return false
      end

      LabClient::Docs.docs.dig(group_name, 'Reference').map do |doc|
        doc[:options].map do |opt|
          opt[:name]
        end
      end.flatten.sort
    end

    def valid_group_project_levels
      %i[guest reporter developer maintainer owner]
    end

    # TODO: Combine all of these?
    def collect_project_id(position = 1)
      # Check if Path / Pagination will be blank
      if response.path.nil?
        response.request.base_url.split(@client.base_url, 2)[position]
      else
        CGI.unescape response.path.split('/')[position]
      end
    end

    alias collect_group_id collect_project_id
    alias collect_user_id collect_project_id

    def collect_release_id(position = 3)
      response.path.split('/')[position]
    end

    def collect_repository_id(position = 4)
      response.path.split('/')[position]
    end

    # Category and Primary Key for docs
    def group_name
      self.class.instance_variable_get('@group_name') || klass.pluralize
    end

    # Prevent stack level errors, but turning into has first
    def to_json(*_args)
      to_h.to_json
    end

    # Helper to get docs
    def klass
      self.class.name.split('::', 2).last.split(/(?=[A-Z])/).join(' ')
    end

    def update_self(obj)
      @table = obj.table
      self
    end

    # Quiet Reader Helper
    def quiet?
      client.quiet?
    end

    def initialize(table = nil, response = nil, client = nil)
      # @table = table unless table.nil?
      @client = client
      @response = response

      super(table)
    end

    # Forward response success
    def success?
      @response.success?
    end

    # Formatting Time Helper
    def format_time?(time)
      time.respond_to?(:to_time)
    end

    # Define a list of DateTime Attributes
    def self.date_time_attrs(list)
      list.each do |kind|
        define_method(kind) do
          DateTime.parse @table[kind] if has? kind
        end
      end
    end

    # Define a list of LabClient::User Attributes
    def self.user_attrs(list)
      list.each do |kind|
        define_method(kind) do
          User.new(@table[kind], response, client) if has? kind
        end
      end
    end
  end
end
