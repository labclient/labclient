# Top namespace
module LabClient
  # Specifics
  class Snippets < Common
    doc 'Delete' do
      markdown 'You can delete broadcast messages via the `snippets` method or via the `Snippet` object'
    end

    doc 'Delete' do
      desc 'Delete an existing snippet.'
      example <<~DOC
        client.snippets.delete(4)
      DOC
    end

    doc 'Delete' do
      desc 'Delete through Snippet object'
      example <<~DOC
        snippet.delete
      DOC

      result <<~DOC
        snippet = client.snippets.show(4)
        => #<Snippet id: 4, One Snippet to find them!>
        snippet.delete
        => nil
      DOC
    end

    # Delete
    def delete(snippet_id = nil)
      client.request(:delete, "snippets/#{snippet_id}")
    end
  end
end
