# Top namespace
module LabClient
  # Inspect Helper
  class Snippet < Klass
    include ClassHelpers

    def inspect
      "#<Snippet id: #{id}, title: #{title}>"
    end

    def update(query = {})
      update_self client.snippets.update(id, query)
    end

    def raw
      client.snippets.raw(id)
    end

    def agent_detail
      client.snippets.agent_detail(id)
    end

    date_time_attrs %i[updated_at created_at]

    user_attrs %i[author]

    def delete
      client.snippets.delete id
    end

    help do
      subtitle 'Snippet'
      option 'raw', 'Retrieves the raw snippet content.'
      option 'agent_detail', 'Hash of user agent details.'
      option 'delete', 'Delete this snippet.'
      option 'update', 'Update this this snippet.'
    end
  end
end
