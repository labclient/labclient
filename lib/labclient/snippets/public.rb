# Top namespace
module LabClient
  # Specifics
  class Snippets < Common
    doc 'List' do
      title 'List all public snippets.'

      markdown <<~DOC
        | Attribute  | Type    | Required | Description                            |
        | -----------| --------| ---------| ---------------------------------------|
        | per_page | integer | no       | Number of snippets to return per page. |
        | page     | integer | no       | Page to retrieve.                      |

      DOC

      example 'client.snippets.public'
      result <<~DOC
        => [#<Snippet id: 2, Special Title>]
      DOC
    end

    def public(query = {})
      client.request(:get, 'snippets/public', Snippet, query)
    end
  end
end
