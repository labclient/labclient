# Top namespace
module LabClient
  # Specifics
  class Snippets < Common
    doc 'Content' do
      desc 'Get a single snippet’s raw contents.'
      example 'client.snippets.raw(2)'
      result <<~DOC
        => "Here's the special content"
      DOC
    end

    doc 'Content' do
      markdown 'Display content from `Snippet` object.'
      example <<~DOC
        snippet = client.snippets.raw(2)
        snippet.raw
      DOC

      result <<~DOC
        snippet = client.snippets.show(2)
        => #<Snippet id: 2, Special Title>
        snippet.raw
        => "Here's the special content"
      DOC
    end

    # Show Specific
    def raw(snippet_id)
      client.request(:get, "snippets/#{snippet_id}/raw")
    end
  end
end
