# Top namespace
module LabClient
  # Specifics
  class Snippets < Common
    doc 'Create' do
      desc 'Create a new snippet.'
      markdown <<~DOC
        | Attribute    | Type   | Required | Description                                        |
        |--------------|------- |--------- |--------------------------------------------------- |
        | title        | string | yes      | Title of a snippet.                                |
        | file_name    | string | yes      | Name of a snippet file.                            |
        | content      | string | yes      | Content of a snippet.                              |
        | description  | string | no       | Description of a snippet.                          |
        | visibility   | string | no       | Snippet's [visibility](#snippet-visibility-level). |
      DOC
    end

    doc 'Create' do
      example <<~DOC
        client.snippets.create(
          title: "Sweet Code",
          file_name: 'script.rb',
          content: 'puts "All your snippet are belong to us!"',
          description: "The one snippet",
          visibility: :private
        )
      DOC
      result <<~DOC
        => #<Snippet id: 3, Sweet Code>
      DOC
    end

    # Create
    def create(query = {})
      client.request(:post, 'snippets', Snippet, query)
    end
  end
end
