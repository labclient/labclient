# Top namespace
module LabClient
  # Specifics
  class Snippets < Common
    doc 'List' do
      desc 'Get a list of the current user’s snippets.'
      example 'client.snippets.list'
      result <<~DOC
        => [#<Snippet id: 2, title: Special Title>, #<Snippet id: 1, title: HereWeGo>]
      DOC
    end

    # List
    def list
      client.request(:get, 'snippets', Snippet)
    end
  end
end
