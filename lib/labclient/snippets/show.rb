# Top namespace
module LabClient
  # Specifics
  class Snippets < Common
    doc 'Show' do
      desc 'Get a single snippet.'
      example 'client.snippets.show(2)'
      result <<~DOC
        => #<Snippet id: 2, Special Title>
      DOC
    end

    # Show Specific
    def show(snippet_id = nil)
      client.request(:get, "snippets/#{snippet_id}", Snippet)
    end
  end
end
