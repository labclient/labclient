# Top namespace
module LabClient
  # Specifics
  class Snippets < Common
    doc 'Show' do
      title 'Get user agent details'
      example 'client.snippets.agent_detail(2)'
      result <<~DOC
        => {:user_agent=>"Mozilla/5.0 (X11; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0", :ip_address=>"10.0.0.14", :akismet_submitted=>false}
      DOC
    end

    doc 'Show' do
      desc 'From Snippet Object'
      example 'snippet.agent_detail'
      result <<~DOC
        snippet = client.snippets.show(2)
        => #<Snippet id: 2, Grapthar's hammer>
        snippet.agent_detail
        => {:user_agent=>"Mozilla/5.0 (X11; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0", :ip_address=>"10.0.0.14", :akismet_submitted=>false}
      DOC
    end

    # Show Specific
    def agent_detail(project_id)
      project_id = format_id(project_id)

      client.request(:get, "snippets/#{project_id}/user_agent_detail")
    end
  end
end
