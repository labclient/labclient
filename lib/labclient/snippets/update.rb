# Top namespace
module LabClient
  # Specifics
  class Snippets < Common
    doc 'Update' do
      desc 'Update an existing snippet.'
      markdown <<~DOC
        | Attribute    | Type   | Required | Description                                        |
        |--------------|------- |--------- |--------------------------------------------------- |
        | title        | string | no       | Title of a snippet.                                |
        | file_name    | string | no       | Name of a snippet file.                            |
        | content      | string | no       | Content of a snippet.                              |
        | description  | string | no       | Description of a snippet.                          |
        | visibility   | string | no       | Snippet's [visibility](#snippet-visibility-level). |
      DOC
    end

    doc 'Update' do
      example <<~DOC
        client.snippets.update(4, title: 'One Snippet', description: "One Snippet to rule them all")
      DOC
      result <<~DOC
        => #<Snippet id: 4, title: One Snippet>
      DOC
    end

    doc 'Update' do
      desc 'Update through Snippet object'
      example <<~DOC
        snippet.update(title: 'One Snippet to find them!')
      DOC

      result <<~DOC
        snippet = client.snippets.show(4)
        => #<Snippet id: 4, One Snippet>
        snippet.update(title: 'Two Snippet!')
        => #<Snippet id: 4, title: Two Snippet!>
      DOC
    end

    # Update
    def update(snippet_id = nil, query = {})
      client.request(:put, "snippets/#{snippet_id}", Snippet, query)
    end
  end
end
