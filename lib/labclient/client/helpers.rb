# Top Namespace
module LabClient
  # Reader Methods / Accessor Helpers
  module ClientHelpers
    def api_methods
      subclasses.keys.sort
    end

    def help(help_filter = nil)
      puts 'Available Methods'

      shown_subclasses = if help_filter
                           api_methods.grep(/#{help_filter}/)
                         else
                           api_methods
                         end

      puts " - #{shown_subclasses.join(' ')}\n\n"
      puts "See help for each specific sub-category\n"
      puts "- client.users.help\n"
      puts "- client.users.api_methods\n"

      nil
    end

    def home_file
      "#{ENV['HOME']}/.gitlab-labclient"
    end

    # Easier Profile Name Access
    def profile
      if settings&.key? :profile
        settings[:profile].to_sym
      else
        ENV['LABCLIENT_PROFILE'].to_sym
      end
    end

    # Instance Variable Helpers
    def save_client
      resp.instance_variable_set(:@client, self)
    end

    def save_path
      resp.instance_variable_set(:@path, path)
    end

    def base_url
      "#{settings[:url]}/api/v4/"
    end

    def quiet?
      settings[:quiet]
    end

    def debug?
      settings[:debug]
    end

    def delay_factor
      settings[:retry][:delay_factor]
    end

    def retry_max
      settings[:retry][:max]
    end

    # Maximum Retries
    def retry_max?
      retries >= retry_max
    end

    # On Successfully response lower delay
    # Prevent multiple request / delays
    def retry_update
      self.delay = [delay - 1, 1].max
      self.retries = [retries - 1, 0].max
    end

    # Debug Print Output
    def debug_handler
      options = resp.request.options

      logger.debug(
        options[:method].to_s.upcase,
        code: resp.code,
        path: path,
        ssl_verify: options[:ssl_verifyhost],
        message: resp.return_message,
        klass: klass.to_s,
        base_url: resp.request.base_url
      )
    end

    # Helper for Accessing the Retry Headers
    def retry_after
      retry_header || delay_factor || 1
    end

    def retry_header
      resp.headers['retry-after']&.to_i
    end

    def retry_debug_headers
      resp.headers.select { |k, _v| k.include? 'ratelimit' }
    end

    # Handle Retry Logic
    # 1. If response merits a retry
    # 2. Retry is enabled
    # 3. Retry Sleep Max isn't hit
    def should_retry?
      resp.retry? && settings[:retry] && !retry_max?
    end
  end
end
