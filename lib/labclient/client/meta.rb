# Top Namespace
module LabClient
  # Variables / Meta
  class Client
    # include HTTParty
    attr_accessor :settings, :resp, :klass, :link, :http, :delay, :retries, :path

    def inspect
      "#<LabClient::Client url: \"#{@settings[:url]}\">"
    end

    # Helper to make subclasses directly accessible
    def subclasses
      self.class.instance_variable_get(:@subclasses)
    end

    # ----------
    # Alias List
    # approvals == merge_request_approvals
    # repo == repository
    # ----------
    @subclasses = {
      appearance: Appearances,
      application_settings: ApplicationSettings,
      application_statistics: ApplicationStatistics,
      applications: Applications,
      approvals: Approvals,
      audit_events: AuditEvents,
      avatar: Avatars,
      awards: Awards,
      branches: Branches,
      broadcast_messages: BroadcastMessages,
      ci_lint: CiLint,
      commits: Commits,
      deploy_keys: DeployKeys,
      discussions: Discussions,
      epics: Epics,
      events: Events,
      feature_flags: FeatureFlags,
      files: Files,
      groups: Groups,
      impersonation_tokens: ImpersonationTokens,
      issues: Issues,
      jobs: Jobs,
      keys: Keys,
      license: Licenses,
      markdown: Markdown,
      members: Members,
      merge_request_approvals: Approvals,
      merge_requests: MergeRequests,
      namespaces: Namespaces,
      notes: Notes,
      notifications: Notifications,
      pipelines: Pipelines,
      project_runners: ProjectRunners,
      projects: Projects,
      terraform: Terraform,
      protected_branches: ProtectedBranches,
      protected_environments: ProtectedEnvironments,
      protected_tags: ProtectedTags,
      registry: Registry,
      repo: Repositories,
      repository: Repositories,
      resource_labels: ResourceLabels,
      runners: Runners,
      snippets: Snippets,
      system_hooks: SystemHooks,
      tags: Tags,
      todos: Todos,
      users: Users,
      version: Version,
      wikis: Wikis,
      wizard: Generator::Wizard
    }

    @subclasses.each do |name, obj|
      define_method(name) do
        obj.new(self)
      end
    end
  end
end
