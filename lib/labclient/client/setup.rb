# Top Namespace
module LabClient
  # Methods for Initialization
  module ClientSetup
    # Load default profile
    def fill_configuration
      if File.exist? home_file
        Oj.load_file(home_file, { symbol_keys: true })
      else
        {
          url: ENV['LABCLIENT_URL'],
          token: ENV['LABCLIENT_TOKEN']
        }
      end
    end

    # If nothing entered Prompt for Input
    def prompt_for_url
      print 'Enter GitLab URL (e.g. https://gitlab.com): '
      @settings[:url] = ENV['LABCLIENT_TESTING'] ? 'testu' : $stdin.gets&.chomp
      raise 'LabClient Error - Missing URL!' if @settings[:url].blank?
    end

    # Check for Token
    def prompt_for_token
      print 'Enter Personal Access Token: '

      @settings[:token] = ENV['LABCLIENT_TESTING'] ? 'testt' : $stdin.gets&.chomp
    end

    # Fill Defaults
    def unspecified_defaults
      @settings[:paginate] = true if @settings[:paginate].nil?
      @settings[:ssl_verify] = true if @settings[:ssl_verify].nil?
      @settings[:quiet] = false if @settings[:quiet].nil?
      @settings[:debug] = false if @settings[:quiet].nil?
      @settings[:debug] = false if @settings[:debug].nil?
      @settings[:token_type] = 'Private-Token' if @settings[:token_type].nil?
      @settings[:retry] = { max: 5, delay_factor: 10, count: 0 } if @settings[:retry].nil?
    end

    # Support for Named Profiles
    def setup_profile
      return false unless File.exist? home_file

      config = Oj.load_file(home_file, { symbol_keys: true })
      return false unless config.key? profile

      self.settings ||= {}
      settings.merge! config[profile]
    end
  end
end
