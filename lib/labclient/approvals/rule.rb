# Top namespace
module LabClient
  # Inspect Helper
  class ApprovalRule < Klass
    @group_name = 'Approvals'
    include ClassHelpers

    def inspect
      "#<ApprovalRule id: #{id}, name: #{name}>"
    end

    def eligible_approvers
      @table[:eligible_approvers].map do |user_data|
        User.new(user_data, response, client)
      end
    end

    def protected_branches
      @table[:protected_branches].map do |branch_data|
        LabClient::Branch.new(branch_data, response, client)
      end
    end

    def users
      @table[:users].map do |user_data|
        User.new(user_data, response, client)
      end
    end

    def groups
      @table[:groups].map do |group_data|
        Group.new(group_data, response, client)
      end
    end
  end
end
