# Top namespace
module LabClient
  # Inspect Helper
  class MergeApproval < Klass
    @group_name = 'Approvals'
    include ClassHelpers

    def inspect
      "#<MergeApproval iid: #{iid}, title: #{title}>"
    end

    date_time_attrs %i[created_at updated_at]

    def approved_by
      @table[:approved_by].map do |user_data|
        User.new(user_data, response, client)
      end
    end
  end
end
