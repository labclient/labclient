# Top namespace
module LabClient
  # Specifics
  class MergeRequestApprovals < Common
    @group_name = 'Approvals'
    doc 'MergeRequest' do
      title 'Update Rule'
      desc 'Update Merge Request approval rule. [Project ID, MergeRequest ID, Rule ID, Hash]'
      example 'client.approvals.merge_request.update_rule(1, 2, name: "Checky Check", approvals_required: 3)'
      result '#<ApprovalRule id: 2, name: Checky Check>'
      markdown <<~DOC
        **Parameters:**

        | Attribute              | Type    | Required | Description                                                      |
        |------------------------|---------|----------|------------------------------------------------------------------|
        | approval_rule_id     | integer | yes      | The ID of a approval rule                                        |
        | name                 | string  | yes      | The name of the approval rule                                    |
        | approvals_required   | integer | yes      | The number of required approvals for this rule                   |
        | user_ids             | Array   | no       | The ids of users as approvers                                    |
        | group_ids            | Array   | no       | The ids of groups as approvers                                   |
        | protected_branch_ids | Array   | no       | The ids of protected branches to scope the rule by |
      DOC
    end

    doc 'MergeRequest' do
      desc 'Via MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(1,1)
        mr.approvals_rule_update(1, name: "Sanity Check", approvals_required: 2)
      DOC
    end

    def update_rule(project_id, merge_request_iid, approval_rule_id, query = {})
      project_id = format_id(project_id)
      merge_request_iid = format_id(merge_request_iid)
      approval_rule_id = format_id(approval_rule_id)
      client.request(:put, "projects/#{project_id}/merge_requests/#{merge_request_iid}/approval_rules/#{approval_rule_id}", ApprovalRule, query)
    end
  end
end
