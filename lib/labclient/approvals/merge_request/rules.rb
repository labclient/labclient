# Top namespace
module LabClient
  # Specifics
  class MergeRequestApprovals < Common
    @group_name = 'Approvals'
    doc 'MergeRequest' do
      title 'Rules'
      desc 'Show Merge Request approval rules. [Project ID, MergeRequest IIU]'
      example 'client.approvals.merge_request.rules(1,1)'
      result '[#<ApprovalRule id: 1, name: All Members>, #<ApprovalRule id: 2, name: Sanity Check>]'
    end

    doc 'MergeRequest' do
      desc 'Via MergeRequest'
      example <<~DOC
        mr = client.merge_requests.rules(1,1)
        mr.approvals
      DOC
    end

    def rules(project_id, merge_request_iid)
      project_id = format_id(project_id)
      merge_request_iid = format_id(merge_request_iid)
      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_iid}/approval_rules", ApprovalRule)
    end
  end
end
