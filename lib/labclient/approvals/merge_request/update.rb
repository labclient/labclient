# Top namespace
module LabClient
  # Specifics
  class MergeRequestApprovals < Common
    @group_name = 'Approvals'
    doc 'MergeRequest' do
      title 'Update'
      desc 'Change approval configuration [Project ID, MergeRequest IID, Approvals Required]'
      example 'client.approvals.merge_request.update(1, 1, 3)'
      result '#<MergeApproval iid: 1, title: Update README.md>'
    end

    doc 'MergeRequest' do
      desc 'Via MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(1,1)
        mr.approval_update(4)
      DOC
    end

    def update(project_id, merge_request_iid, approvals_required)
      project_id = format_id(project_id)
      merge_request_iid = format_id(merge_request_iid)
      query = { approvals_required: approvals_required }
      client.request(:post, "projects/#{project_id}/merge_requests/#{merge_request_iid}/approvals", MergeApproval, query)
    end
  end
end
