# Top namespace
module LabClient
  # Specifics
  class MergeRequestApprovals < Common
    @group_name = 'Approvals'
    doc 'MergeRequest' do
      title 'Show'
      desc 'Show Merge Request approval configuration. [Project ID, MergeRequest IID]'
      example 'client.approvals.merge_request.show(1, 1)'
      result '#<MergeApproval iid: 1, title: Update README.md>'
    end

    doc 'MergeRequest' do
      desc 'Via MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(1,1)
        mr.approvals
      DOC
    end

    def show(project_id, merge_request_iid)
      project_id = format_id(project_id)
      merge_request_iid = format_id(merge_request_iid)
      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_iid}/approvals", MergeApproval)
    end
  end
end
