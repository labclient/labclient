# Top namespace
module LabClient
  # Specifics
  class MergeRequestApprovals < Common
    @group_name = 'Approvals'
    doc 'MergeRequest' do
      title 'Delete Rule'
      desc 'Delete MergeRequest approval rule. [Project ID, MergeRequest ID, Rule ID]'
      example 'client.approvals.merge_request.delete_rule(1, 2)'
    end

    doc 'MergeRequest' do
      desc 'Via MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(1,1)
        merge_request.approvals_rule_delete(2)
      DOC
    end

    def delete_rule(project_id, merge_request_iid, approval_rule_id)
      project_id = format_id(project_id)
      merge_request_iid = format_id(merge_request_iid)
      approval_rule_id = format_id(approval_rule_id)
      client.request(:delete, "projects/#{project_id}/merge_requests/#{merge_request_iid}/approval_rules/#{approval_rule_id}")
    end
  end
end
