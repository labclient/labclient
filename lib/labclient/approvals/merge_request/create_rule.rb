# Top namespace
module LabClient
  # Specifics
  class MergeRequestApprovals < Common
    @group_name = 'Approvals'
    doc 'MergeRequest' do
      title 'Create Rule'
      desc 'Create Merge Request approval rules. [Project ID, MergeRequest ID, Hash]'
      example 'client.approvals.merge_request.create_rule(1,1, name: "Sanity Check", approvals_required: 2, user_ids: [2])'
      result '#<ApprovalRule id: 2, name: Sanity Check>'
      markdown <<~DOC

        **Parameters:**

        | Attribute              | Type    | Required | Description                                                      |
        |------------------------|---------|----------|------------------------------------------------------------------|
        | name                 | string  | yes      | The name of the approval rule                                    |
        | approvals_required   | integer | yes      | The number of required approvals for this rule                   |
        | user_ids             | Array   | no       | The ids of users as approvers                                    |
        | group_ids            | Array   | no       | The ids of groups as approvers                                   |
        | protected_branch_ids | Array   | no       | The ids of protected branches to scope the rule by |
      DOC
    end

    doc 'MergeRequest' do
      desc 'Via MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(1,1)
        mr.approvals_rule_create(name: "Sanity Check", approvals_required: 2)
      DOC
    end

    def create_rule(project_id, merge_request_iid, query = {})
      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/merge_requests/#{merge_request_iid}/approval_rules", ApprovalRule, query)
    end
  end
end
