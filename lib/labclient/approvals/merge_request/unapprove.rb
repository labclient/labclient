# Top namespace
module LabClient
  # Specifics
  class MergeRequestApprovals < Common
    @group_name = 'Approvals'
    doc 'MergeRequest' do
      title 'Unapprove'
      desc 'Unapprove Merge Request. [Project ID, MergeRequest ID, Hash]'
      example 'client.approvals.merge_request.approve(1,2)'
      result '=> #<MergeApproval iid: 2, title: Add new file>'
      markdown <<~DOC

        **Parameters:**

        | Attribute           | Type    | Required | Description             |
        |---------------------|---------|----------|-------------------------|
        | sha               | string  | no       | The HEAD of the MR      |
        | approval_password | string  | no      | Current user's password. Required if **Require user password to approve** is enabled in the project settings. |
      DOC
    end

    doc 'MergeRequest' do
      desc 'Via MergeRequest'
      example <<~DOC
        mr = client.merge_requests.show(1,1)
        mr.unapprove
      DOC
    end

    def unapprove(project_id, merge_request_iid)
      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/merge_requests/#{merge_request_iid}/unapprove", MergeApproval)
    end
  end
end
