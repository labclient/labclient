# Top namespace
module LabClient
  # Specifics
  class ProjectApprovals < Common
    @group_name = 'Approvals'
    doc 'Project' do
      title 'Rules'
      desc 'Show project approval rules. [Project ID]'
      example 'client.approvals.project.rules(1)'
      result '[#<ApprovalRule id: 1, name: Root>]'
    end

    doc 'Project' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(1)
        project.approval_rules
      DOC
    end

    def rules(project_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/approval_rules", ApprovalRule)
    end
  end
end
