# Top namespace
module LabClient
  # Specifics
  class ProjectApprovals < Common
    @group_name = 'Approvals'
    doc 'Project' do
      title 'Show'
      desc 'Show project’s approval configuration. [Project ID]'
      example 'client.approvals.project.show(1)'
      result <<~DOC
        {
                                  approvals_before_merge: 0,
                                         approver_groups: [],
                                               approvers: [],
          disable_overriding_approvers_per_merge_request: nil,
                          merge_requests_author_approval: nil,
              merge_requests_disable_committers_approval: nil,
                             require_password_to_approve: nil,
                                 reset_approvals_on_push: true
        }

      DOC
    end

    doc 'Project' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(1)
        project.approvals
      DOC
    end

    def show(project_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/approvals")
    end
  end
end
