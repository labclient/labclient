# Top namespace
module LabClient
  # Specifics
  class ProjectApprovals < Common
    @group_name = 'Approvals'
    doc 'Project' do
      title 'Create Rule'
      desc 'Create project approval rules. [Project ID, Hash]'
      example 'client.approvals.project.create_rule(1, name: "Sanity Check", approvals_required: 2)'
      result '#<ApprovalRule id: 2, name: Sanity Check>'
      markdown <<~DOC

        **Parameters:**

        | Attribute              | Type    | Required | Description                                                      |
        |------------------------|---------|----------|------------------------------------------------------------------|
        | name                 | string  | yes      | The name of the approval rule                                    |
        | approvals_required   | integer | yes      | The number of required approvals for this rule                   |
        | user_ids             | Array   | no       | The ids of users as approvers                                    |
        | group_ids            | Array   | no       | The ids of groups as approvers                                   |
        | protected_branch_ids | Array   | no       | The ids of protected branches to scope the rule by |
      DOC
    end

    doc 'Project' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(1)
        project.approvals_rule_create(name: "Sanity Check", approvals_required: 2)
      DOC
    end

    def create_rule(project_id, query = {})
      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/approval_rules", ApprovalRule, query)
    end
  end
end
