# Top namespace
module LabClient
  # Specifics
  class ProjectApprovals < Common
    @group_name = 'Approvals'
    doc 'Project' do
      title 'Update'
      desc 'Change approval configuration [Project ID, Hash]'
      example 'client.approvals.project.update(1, reset_approvals_on_push: true)'
      result <<~DOC
        {
                                  approvals_before_merge: 0,
                                         approver_groups: [],
                                               approvers: [],
          disable_overriding_approvers_per_merge_request: nil,
                          merge_requests_author_approval: nil,
              merge_requests_disable_committers_approval: nil,
                             require_password_to_approve: nil,
                                 reset_approvals_on_push: true
        }

      DOC

      markdown <<~DOC

        **Parameters:**

        | Attribute                                        | Type    | Required | Description                                                                                         |
        | ------------------------------------------------ | ------- | -------- | --------------------------------------------------------------------------------------------------- |
        | approvals_before_merge                         | integer | no       | How many approvals are required before an MR can be merged. Deprecated in 12.0 in favor of Approval Rules API. |
        | reset_approvals_on_push                        | boolean | no       | Reset approvals on a new push                                                                       |
        | disable_overriding_approvers_per_merge_request | boolean | no       | Allow/Disallow overriding approvers per MR                                                          |
        | merge_requests_author_approval                 | boolean | no       | Allow/Disallow authors from self approving merge requests; true means authors cannot self approve |
        | merge_requests_disable_committers_approval     | boolean | no       | Allow/Disallow committers from self approving merge requests                                        |
        | require_password_to_approve                    | boolean | no       | Require approver to enter a password in order to authenticate before adding the approval         |

      DOC
    end

    doc 'Project' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(1)
        project.approvals_update(require_password_to_approve: true)
      DOC
    end

    def update(project_id, query = {})
      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/approvals", nil, query)
    end
  end
end
