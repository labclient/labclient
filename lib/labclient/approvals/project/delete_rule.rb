# Top namespace
module LabClient
  # Specifics
  class ProjectApprovals < Common
    @group_name = 'Approvals'
    doc 'Project' do
      title 'Delete Rule'
      desc 'Delete project approval rule. [Project ID, Rule ID]'
      example 'client.approvals.project.delete_rule(1, 2)'
    end

    doc 'Project' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(1)
        project.approvals_rule_delete(2)
      DOC
    end

    def delete_rule(project_id, approval_rule_id)
      project_id = format_id(project_id)
      approval_rule_id = format_id(approval_rule_id)
      client.request(:delete, "projects/#{project_id}/approval_rules/#{approval_rule_id}")
    end
  end
end
