# Top namespace
module LabClient
  # Inspect Helper
  class Approvals < Common
    @group_name = 'Approvals'
    include ClassHelpers

    def project
      ProjectApprovals.new(client)
    end

    def merge_request
      MergeRequestApprovals.new(client)
    end
  end
end
