# Top namespace
module LabClient
  # Specifics
  class ApplicationStatistics < Common
    doc 'Show' do
      desc 'List the current statistics of the GitLab instance. You have to be an administrator in order to perform this action.'
      example 'client.markdown.render(text: "# Oh Hi!")'
      result <<~DOC
        => {:forks=>"0", :issues=>"6,683", :merge_requests=>"0", :notes=>"0", :snippets=>"0", :ssh_keys=>"1", :milestones=>"0", :users=>"56", :projects=>"298", :groups=>"48", :active_users=>"56"}
      DOC
    end

    def show
      client.request(:get, 'application/statistics')
    end
  end
end
