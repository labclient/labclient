# Top namespace
module LabClient
  # Specifics
  class ResourceLabels < Common
    def issues
      ResourceLabelIssues.new(client)
    end
  end
end
