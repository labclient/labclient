# Top namespace
module LabClient
  # Specifics
  class ResourceLabelIssues < Common
    @group_name = 'Resource Labels'
    doc 'Issues' do
      title 'Show'
      desc 'Returns a single label event for a specific project issue [Project ID, Issue IID, Resource Event ID]'
      example 'client.resource_labels.issues.show(16,3,2)'
      result <<~DOC
        => #<ResourceLabel Issue id: 2>
      DOC
    end

    doc 'Issues' do
      desc 'Via Issue'
      example <<~DOC
        issue = client.issues.show(16,3)
        issue.resource_label(2)
      DOC
    end

    def show(project_id, issue_iid, resource_event_id)
      project_id = format_id(project_id)
      issue_iid = format_id(issue_iid)
      resource_event_id = format_id(resource_event_id)

      client.request(:get, "projects/#{project_id}/issues/#{issue_iid}/resource_label_events/#{resource_event_id}", ResourceLabel)
    end
  end
end
