# Top namespace
module LabClient
  # Specifics
  class ResourceLabelIssues < Common
    @group_name = 'Resource Labels'
    doc 'Issues' do
      title 'List'
      desc 'Gets a list of all label events for a single issue. [Project ID, Issue IID]'
      example 'client.resource_labels.issues.list(16,2)'
      result <<~DOC
        => [#<ResourceLabel Issue id: 2>, #<ResourceLabel Issue id: 3>]
      DOC
    end

    doc 'Issues' do
      desc 'Via Issue'
      example <<~DOC
        issue = client.issues.show(16,2)
        issue.resource_labels
      DOC
    end

    def list(project_id, issue_iid)
      project_id = format_id(project_id)
      issue_iid = format_id(issue_iid)

      client.request(:get, "projects/#{project_id}/issues/#{issue_iid}/resource_label_events", ResourceLabel)
    end
  end
end
