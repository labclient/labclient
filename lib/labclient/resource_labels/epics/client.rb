# Top namespace
module LabClient
  # Specifics
  class ResourceLabels < Common
    def epics
      ResourceLabelEpics.new(client)
    end
  end
end
