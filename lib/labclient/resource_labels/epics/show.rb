# Top namespace
module LabClient
  # Specifics
  class ResourceLabelEpics < Common
    @group_name = 'Resource Labels'
    doc 'Epics' do
      title 'Show'
      desc 'Returns a single label event for a specific group epic [Group ID, Epic ID, Resource Event ID]'
      example 'client.resource_labels.epics.show(59,6,5)'
      result <<~DOC
        => #<ResourceLabel Epic id: 5>
      DOC
    end

    doc 'Epics' do
      desc 'Via Epic'
      example <<~DOC
        epic = client.epics.show(59,3)
        epic.resource_label(5)
      DOC
    end

    def show(group_id, epic_id, resource_event_id)
      group_id = format_id(group_id)
      epic_id = format_id(epic_id)
      resource_event_id = format_id(resource_event_id)

      client.request(:get, "groups/#{group_id}/epics/#{epic_id}/resource_label_events/#{resource_event_id}", ResourceLabel)
    end
  end
end
