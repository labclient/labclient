# Top namespace
module LabClient
  # Specifics
  class ResourceLabelEpics < Common
    @group_name = 'Resource Labels'
    doc 'Epics' do
      title 'List'
      desc 'Gets a list of all label events for a single epic. [Group ID, Epic ID]'
      example 'client.resource_labels.epics.list(59,6)'
      result <<~DOC
        => [#<ResourceLabel Epic id: 4>, #<ResourceLabel Epic id: 5>]
      DOC
    end

    doc 'Epics' do
      desc 'Via Epic'
      example <<~DOC
        epic = client.epics.show(59,3)
        epic.resource_labels
      DOC
    end

    def list(group_id, epic_id)
      group_id = format_id(group_id)
      epic_id = format_id(epic_id)

      client.request(:get, "groups/#{group_id}/epics/#{epic_id}/resource_label_events", ResourceLabel)
    end
  end
end
