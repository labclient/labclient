# Top namespace
module LabClient
  # Inspect Helper
  class ResourceLabel < Klass
    include ClassHelpers

    def inspect
      "#<ResourceLabel #{resource_type} id: #{id}>"
    end

    date_time_attrs %i[created_at]
    user_attrs %i[user]
  end
end

# Top namespace
module LabClient
  # Inspect Helper
  class ResourceLabels < Common
    include ClassHelpers
  end
end
