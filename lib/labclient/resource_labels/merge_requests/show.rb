# Top namespace
module LabClient
  # Specifics
  class ResourceLabelMergeRequests < Common
    @group_name = 'Resource Labels'
    doc 'Merge Requests' do
      title 'Show'
      desc 'Returns a single label event for a specific group merge_requests [Project ID, MergeRequest IID, Resource Event ID]'
      example 'client.resource_labels.merge_requests.show(59,6,5)'
      result <<~DOC
        => #<ResourceLabel MergeRequest id: 5>
      DOC
    end

    doc 'Merge Requests' do
      desc 'Via MergeRequest'
      example <<~DOC
        merge_requests = client.merge_requests.show(59,3)
        merge_requests.resource_label(5)
      DOC
    end

    def show(project_id, merge_requests_iid, resource_event_id)
      project_id = format_id(project_id)
      merge_requests_iid = format_id(merge_requests_iid)
      resource_event_id = format_id(resource_event_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_requests_iid}/resource_label_events/#{resource_event_id}", ResourceLabel)
    end
  end
end
