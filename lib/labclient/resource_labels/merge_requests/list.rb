# Top namespace
module LabClient
  # Specifics
  class ResourceLabelMergeRequests < Common
    @group_name = 'Resource Labels'
    doc 'Merge Requests' do
      title 'List'
      desc 'Gets a list of all label events for a single merge_request. [Project ID, MergeRequest IID]'
      example 'client.resource_labels.merge_requests.list(16,1)'
      result <<~DOC
        => [#<ResourceLabel MergeRequest id: 4>, #<ResourceLabel MergeRequest id: 5>]
      DOC
    end

    doc 'Merge Requests' do
      desc 'Via MergeRequest'
      example <<~DOC
        merge_request = client.merge_requests.show(16,1)
        merge_request.resource_labels
      DOC
    end

    def list(project_id, merge_request_iid)
      project_id = format_id(project_id)
      merge_request_iid = format_id(merge_request_iid)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_iid}/resource_label_events", ResourceLabel)
    end
  end
end
