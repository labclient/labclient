# Top namespace
module LabClient
  # Specifics
  class ResourceLabels < Common
    def merge_requests
      ResourceLabelMergeRequests.new(client)
    end
  end
end
