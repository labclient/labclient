# Top namespace
module LabClient
  # Specifics
  class Terraform < Common
    doc 'Lock' do
      desc 'Unlock State [Project ID, StateName Lock ID]'
      example 'client.terraform.lock(5, :state_name, 123)'
    end

    def unlock(project_id, name, xid)
      project_id = format_id(project_id)

      client.request(:delete, "projects/#{project_id}/terraform/state/#{name}/lock?ID=#{xid}")
    end
  end
end
