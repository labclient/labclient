# Top namespace
module LabClient
  # Specifics
  class Terraform < Common
    doc 'Create' do
      desc 'Creates a new State. [Project ID, StateName, LockID, Hash]'
      example 'client.terraform.create(5, {})'
      result '#<TerraformState serial: 4>'

      markdown <<~DOC
        Attributes for Hash

        ID,Operation,Info,Who,Version,Created,Path
      DOC
    end

    def create(project_id, name, xid, body)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/terraform/state/#{name}?ID=#{xid}", nil, body)
    end
  end
end
