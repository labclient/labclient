# Top namespace
module LabClient
  # Specifics
  class Terraform < Common
    doc 'Lock' do
      desc 'Locks a new State. [Project ID, Hash]'
      example 'client.wikis.Lock(5, content: "Lots of Stuff", title: "Title!")'
      result '#<TerraformState slug: Title!>'
    end

    def lock(project_id, name, query)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/terraform/state/#{name}/lock", nil, query)
    end
  end
end
