# Top namespace
module LabClient
  # Specifics
  class Terraform < Common
    doc 'Delete' do
      desc 'Delete Terraform State. [Project ID, StateName]'
      example 'client.terraform.delete(5, :state_name)'
    end

    def delete(project_id, name)
      project_id = format_id(project_id)

      client.request(:delete, "projects/#{project_id}/terraform/state/#{name}")
    end
  end
end
