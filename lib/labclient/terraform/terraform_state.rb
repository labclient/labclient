# Top namespace
module LabClient
  # Inspect Helper
  class TerraformState < Klass
    include ClassHelpers

    def inspect
      "#<TerraformState serial: #{serial}>"
    end
  end
end
