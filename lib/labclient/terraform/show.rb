# Top namespace
module LabClient
  # Specifics
  class Terraform < Common
    doc 'Show' do
      desc 'Collect Terraform State Information'
      example 'client.terraform.show(264, "README.md")'

      markdown <<~DOC
        Ref will default to `main`

        Kind can be left empty or set to either :raw or :blame
      DOC
    end

    def show(project_id, name)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/terraform/state/#{name}", TerraformState)
    end
  end
end
