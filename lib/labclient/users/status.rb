# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Show' do
      title 'Status'
      desc 'Get the status of the current or target user [Optional User ID]'
      example 'client.users.status'
      result '{:emoji=>"smiling_imp", :message=>"Making GitLab Magic", :message_html=>"Making GitLab Magic"}'
    end

    doc 'Show' do
      desc 'Get specific user status'
      example 'client.users.status(12)'
    end

    def status(user_id = nil)
      if user_id
        user_id = format_id(user_id)
        client.request(:get, "users/#{user_id}/status")
      else
        client.request(:get, 'user/status')
      end
    end
  end
end
