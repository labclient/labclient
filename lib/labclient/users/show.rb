# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Show' do
      desc 'Display a single user'
      example 'client.users.show(5)'
      result <<~DOC
        user = client.users.show(5)
        => #<User id: 5, username: "princehumperdinck">
        user.email
        => "princehumperdinck@florin"
      DOC
    end

    # Display single user
    def show(user_id)
      user_id = format_id(user_id)
      client.request(:get, "users/#{user_id}", User)
    end
  end
end
