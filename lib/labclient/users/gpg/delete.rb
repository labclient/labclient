# Top namespace
module LabClient
  # Specifics
  class UserGpgKeys < Common
    doc 'Delete' do
      desc 'Delete gpg key for current or target user [GPG Key ID, User ID (Optional)]'
      example 'client.users.gpg_keys.delete(2)'
    end

    doc 'Delete' do
      desc 'Delete specific user gpg key. [GPG Key ID, User ID]'
      example 'client.users.gpg_keys.delete(3,5)'
    end

    doc 'Delete' do
      desc 'Via User [GPG Key ID]'
      example <<~DOC
        user = client.users.show(5)
        user.gpg_key_delete(2)
      DOC
    end

    def delete(key_id, user_id = nil)
      key_id = format_id(key_id)
      if user_id
        user_id = format_id(user_id)
        client.request(:delete, "users/#{user_id}/gpg_keys/#{key_id}")
      else
        client.request(:delete, "user/gpg_keys/#{key_id}")
      end
    end
  end
end
