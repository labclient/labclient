# Top namespace
module LabClient
  # Inspect Helper
  class GpgKey < Klass
    include ClassHelpers

    def inspect
      "#<GpGKey id: #{id}>"
    end

    date_time_attrs %i[created_at]
  end
end
