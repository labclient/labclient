# Top namespace
module LabClient
  # Specifics
  class UserGpgKeys < Common
    doc 'Create' do
      desc 'Create new key for current or target user [String, User ID (Optional)]'
      example 'lientc.users.gpg_keys.create("-----BEGIN PGP ...")'
      result '#<GpgKey id: 3>'
    end

    doc 'Create' do
      desc 'Create for Specific User'
      example <<~DOC
        client.users.gpg_keys.create("-----BEGIN PGP...",5)
      DOC
    end

    doc 'Create' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(5)
        user.gpg_key_create("keeeydata")
      DOC
    end

    def create(key_data, user_id = nil)
      query = { key: key_data }

      if user_id
        user_id = format_id(user_id)
        client.request(:post, "users/#{user_id}/gpg_keys", GpgKey, query)
      else
        client.request(:post, 'user/gpg_keys', GpgKey, query)
      end
    end
  end
end
