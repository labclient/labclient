# Top namespace
module LabClient
  # Inspect Helper
  class UserGpgKeys < Common
    @group_name = 'GPG Keys'
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Users < Common
    def gpg_keys
      UserGpgKeys.new(client)
    end
  end
end
