# Top namespace
module LabClient
  # Specifics
  class UserGpgKeys < Common
    doc 'Show' do
      desc 'Get current user single gpg key. [GPG Key ID]'
      example 'client.users.gpg_keys.show(1)'
      result '#<GpgKey id: 1>'
    end

    def show(key_id)
      key_id = format_id(key_id)

      client.request(:get, "user/gpg_keys/#{key_id}", GpgKey)
    end
  end
end
