# Top namespace
module LabClient
  # Specifics
  class UserGpgKeys < Common
    doc 'List' do
      desc 'List current or target user GPG keys. [User ID (Optional)]'
      example 'client.users.gpg_keys.list'
      result '[#<GpGKey id: 1>, ...]'
    end

    doc 'List' do
      desc 'List specific user keys.'
      example 'client.users.gpg_keys.list(1)'
    end

    doc 'List' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(5)
        user.gpg_keys
      DOC
    end

    def list(user_id = nil)
      if user_id
        user_id = format_id(user_id)
        client.request(:get, "users/#{user_id}/gpg_keys", GpgKey)
      else
        client.request(:get, 'user/gpg_keys', GpgKey)
      end
    end
  end
end
