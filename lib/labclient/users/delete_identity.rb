# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Delete' do
      title 'Identity'
      desc 'Deletes a user’s authentication identity using the provider name associated with that identity. [User ID, Provider]'
      example 'client.users.delete_identity(57, "ldapmain")'
      result '=> nil'
    end

    doc 'Delete' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(57)
        user.delete_identity(name: 'Lorenzo')
      DOC
    end

    def delete_identity(user_id, provider)
      user_id = format_id(user_id)
      client.request(:delete, "users/#{user_id}/identities/#{provider}")
    end
  end
end
