# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Update' do
      title 'Block'
      desc 'Activates the specified user [User ID]'
      example 'client.users.activate(57)'
    end

    doc 'Update' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(57)
        user.activate
      DOC
    end

    def activate(user_id)
      user_id = format_id(user_id)

      client.request(:post, "users/#{user_id}/activate")
    end
  end
end
