# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Update' do
      title 'Unblock'
      desc 'Unblocks the specified user [User ID]'
      example 'client.users.unblock(57)'
    end

    doc 'Update' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(57)
        user.unblock
      DOC
    end

    def unblock(user_id)
      user_id = format_id(user_id)

      client.request(:post, "users/#{user_id}/unblock")
    end
  end
end
