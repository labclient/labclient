# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'List' do
      # title 'List & Search'
      desc 'List and Search for users'

      markdown <<~DOC
        | Parameter    | Description |
        | ------------ | ----------- |
        | username   | Lookup by username |
        | search     | Search by name or primary email |
        | active     | Filter by user state. true or false |
        | blocked    | Filter by user state. true or false |
        | order_by   | Return users ordered by id, name, username, created_at, or updated_at fields. Default is id |
        | sort       | Return users sorted in asc or desc order. Default is desc |
        | two_factor | Filter users by Two-factor authentication. Filter values are enabled or disabled. By default it returns all users |

      DOC
    end

    doc 'List' do
      title 'Examples'
      example 'client.users.list'
      result <<~DOC
        => [
          #<User id: 56, username: "rakan">,
          #<User id: 55, username: "peregrintook">,
          #<User id: 54, username: "khazix">
        ]
      DOC
    end

    doc 'List' do
      desc 'Username'
      example 'client.users.list(username: "rakan")'
      result <<~DOC
        => [#<User id: 56, username: "rakan">]
      DOC
    end

    doc 'List' do
      desc 'Search'
      example 'client.users.list(search: "Sid")'
      result <<~DOC
        => [#<User id: 56, username: "rakan">]
      DOC
    end

    doc 'List' do
      desc 'Active'
      example 'client.users.list(active: true)'
      result <<~DOC
        => [#<User id: 56, username: "peregrintook">]
      DOC
    end

    # List/Search users
    def list(query = {})
      client.request(:get, 'users', User, query)
    end
  end
end
