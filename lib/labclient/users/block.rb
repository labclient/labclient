# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Update' do
      title 'Block'
      desc 'Blocks the specified user [User ID]'
      example 'client.users.block(57)'
    end

    doc 'Update' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(57)
        user.block
      DOC
    end

    def block(user_id)
      user_id = format_id(user_id)

      client.request(:post, "users/#{user_id}/block")
    end
  end
end
