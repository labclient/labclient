# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Show' do
      title 'Counts'
      desc 'Get the counts of the authenticated user.'
      example 'client.users.counts'
      result '{:merge_requests=>0}'
    end

    def counts
      client.request(:get, 'user_counts')
    end
  end
end
