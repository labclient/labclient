# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Update' do
      title 'Block'
      desc 'Deactivates the specified user [User ID]'
      example 'client.users.deactivate(57)'
    end

    doc 'Update' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(57)
        user.deactivate
      DOC
    end

    def deactivate(user_id)
      user_id = format_id(user_id)

      client.request(:post, "users/#{user_id}/deactivate")
    end
  end
end
