# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Show' do
      title 'Current'
      desc 'Gets currently authenticated user'
      example 'client.users.current'
      result '#<User id: 1, username: root>'
    end

    doc 'Show' do
      desc 'Sudo make call user place [User ID]'
      example 'client.users.current(12)'
      result '#<User id: 12, username: antonidas>'
    end

    # Display single user
    def current(sudo_id = nil)
      if sudo_id
        sudo_id = format_id(sudo_id)
        client.request(:get, 'user', User, sudo: sudo_id)
      else
        client.request(:get, 'user', User)
      end
    end
  end
end
