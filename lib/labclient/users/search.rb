# Top namespace
# Uses the '/search?scope=user' endpoint
module LabClient
  # Specifics
  class Users < Common
    doc 'Search' do
      desc 'Search within users scope'

      example "client.users.search 'speed'"
      result '[#<User id: 57, username: speed>]'
    end

    # Search users
    def search(search_string = '')
      query = {
        scope: :users,
        search: search_string
      }
      client.request(:get, 'search', User, query)
    end
  end
end
