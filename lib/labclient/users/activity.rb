# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Show' do
      title 'Activity'
      desc 'Get the last activity date for all users, sorted from oldest to newest. [User ID, Date (YYYY-MM-DD)]'

      example 'client.users.activity(1.year.ago)'
      result <<~DOC
        [
          {:username=>"shelob", :last_activity_on=>"2020-03-27", :last_activity_at=>"2020-03-27"}, {:username=>"root", :last_activity_on=>"2020-03-27", :last_activity_at=>"2020-03-27"}
        ]
      DOC
    end

    def activity(from_date)
      query = { from: from_date }
      query_format_time(query, :from)
      client.request(:get, 'user/activities', nil, query)
    end
  end
end
