# Top namespace
module LabClient
  # Inspect Helper
  class User < Klass
    include ClassHelpers
    def inspect
      "#<User id: #{id}, username: #{username}>"
    end

    date_time_attrs %i[confirmed_at current_sign_in_at created_at last_activity_on last_sign_in_at]

    def projects(query = {})
      client.projects.user(id, query)
    end

    def starred(query = {})
      client.projects.user(id, query)
    end

    def update(query = {})
      update_self client.users.update(id, query)
    end

    def delete
      client.users.delete(id)
    end

    def delete_identity(provider)
      client.users.delete_identity(id, provider)
    end

    def status
      client.users.status(id)
    end

    def keys
      client.users.keys.list(id)
    end

    def key_create(query)
      client.users.keys.create(query, id)
    end

    def key_delete(key_id)
      client.users.keys.delete(key_id, id)
    end

    def gpg_keys
      client.users.gpg_keys.list(id)
    end

    def gpg_key_create(key_data)
      client.users.gpg_keys.create(key_data, id)
    end

    def gpg_key_delete(key_id)
      client.users.gpg_keys.delete(key_id, id)
    end

    def emails
      client.users.emails.list(id)
    end

    def email_create(email_address)
      client.users.emails.create(email_address, id)
    end

    def email_delete(email_id)
      client.users.emails.delete(email_id, id)
    end

    def block
      client.users.block(id)
    end

    def unblock
      client.users.unblock(id)
    end

    def activate
      client.users.activate(id)
    end

    def active?
      state == 'active'
    end

    def inactive?
      state == 'inactive'
    end

    def deactivate
      client.users.deactivate(id)
    end

    def impersonation_tokens(filter = :all)
      client.impersonation_tokens.list(id, filter)
    end

    def impersonation_tokens_create(query)
      client.impersonation_tokens.create(id, query)
    end

    def impersonation_tokens_revoke(token_id)
      client.impersonation_tokens.revoke(id, token_id)
    end

    def impersonation_token(token_id)
      client.impersonation_tokens.show(id, token_id)
    end

    def memberships(type = nil)
      client.users.memberships(id, type)
    end

    # Events
    def events(query = {})
      client.events.user(id, query)
    end

    # Reload Helper
    def reload
      update_self client.users.show(id)
    end

    help do
      # @group_name = 'Users'
      subtitle 'User'
      option 'projects', 'List user owned projects [Hash]'
      option 'starred', 'List user starred projects [Hash]'
      option 'update', 'Update this user [Hash]'
      option 'block', 'Block this user'
      option 'unblock', 'Unblock this user'
      option 'activate', 'Activate this user'
      option 'deactivate', 'Deactivate this user'
      option 'delete', 'Delete this user'
      option 'delete_identity', 'Delete identity from user [Provider]'
      option 'status', 'Show user status'
      option 'keys', 'List user keys'
      option 'key_create', 'Create new key for user [Hash]'
      option 'key_delete', 'Delete key for user [Key ID]'
      option 'status', 'Show user status'
      option 'gpg_keys', 'List user gpg keys'
      option 'gpg_key_create', 'Create new gpg key for user [Key Data]'
      option 'gpg_key_delete', 'Delete gpg key for user [Key ID]'
      option 'emails', 'List user emails'
      option 'email_create', 'Create email for user [Email Address]'
      option 'email_delete', 'Delete email for user [Email ID]'
      option 'events', 'List user events [Hash]'
      option 'impersonation_tokens', 'List impersonation tokesn for user [Filter(Optional)]'
      option 'impersonation_token_create', 'Create impersonation tokesn [Hash]'
      option 'impersonation_token_revoke', 'Revoke impersonation tokesn [Token ID]'
      option 'impersonation_token', 'Show impersonation tokesn [Token ID]'
      option 'memberships', 'Lists all projects and groups a user is a member of [String/Type]'
      option 'reload', 'Reload this object (New API Call)'
      option 'active?', 'Boolean return if user is active'
      option 'inactive?', 'Boolean return if user is inactive'
    end
  end
end
