# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Show' do
      title 'Memberships'
      desc 'Lists all projects and groups a user is a member of. [User ID, Type]'

      markdown <<~DOC
        | Attribute | Type   | Required | Description                                                    |
        | --------- | ------ | -------- | -------------------------------------------------------------- |
        | type      | string | no       | Filter memberships by type. Can be either Project or Namespace |
      DOC

      example 'client.users.memberships(1)'
      result <<~DOC
        [
          {:source_id=>57, :source_name=>"MountTargon", :source_type=>"Namespace", :access_level=>50}, ...
        ]
      DOC
    end

    doc 'Show' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(10)
        user.memberships
      DOC
    end

    def memberships(user_id, type = nil)
      user_id = format_id(user_id)

      query = nil
      query = { type: type } if type
      client.request(:get, "users/#{user_id}/memberships", Membership, query)
    end
  end
end
