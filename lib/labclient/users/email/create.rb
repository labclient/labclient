# Top namespace
module LabClient
  # Specifics
  class UserEmails < Common
    doc 'Create' do
      desc 'Create new key for current or target user [String, User ID (Optional)]'
      example 'client.users.emails.create("user@labclient")'
      result '#<Email id: 3>'
    end

    doc 'Create' do
      desc 'Create for Specific User'
      example <<~DOC
        client.users.emails.create("email@labclient",5)
      DOC
    end

    doc 'Create' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(5)
        user.email_create("email@labclient")
      DOC
    end

    def create(email, user_id = nil)
      query = { email: email }

      if user_id
        user_id = format_id(user_id)
        client.request(:post, "users/#{user_id}/emails", Email, query)
      else
        client.request(:post, 'user/emails', Email, query)
      end
    end
  end
end
