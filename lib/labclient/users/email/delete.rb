# Top namespace
module LabClient
  # Specifics
  class UserEmails < Common
    doc 'Delete' do
      desc 'Delete email for current or target user [Email ID, User ID (Optional)]'
      example 'client.users.emails.delete(2)'
    end

    doc 'Delete' do
      desc 'Delete specific user email. [Email ID, User ID]'
      example 'client.users.emails.delete(3,5)'
    end

    doc 'Delete' do
      desc 'Via User [Email ID]'
      example <<~DOC
        user = client.users.show(5)
        user.email_deleteemail_delete(2)
      DOC
    end

    def delete(key_id, user_id = nil)
      key_id = format_id(key_id)
      if user_id
        user_id = format_id(user_id)
        client.request(:delete, "users/#{user_id}/emails/#{key_id}")
      else
        client.request(:delete, "user/emails/#{key_id}")
      end
    end
  end
end
