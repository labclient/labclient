# Top namespace
module LabClient
  # Inspect Helper
  class Email < Klass
    include ClassHelpers

    def inspect
      "#<Email id: #{id}, #{email}>"
    end
  end
end
