# Top namespace
module LabClient
  # Specifics
  class UserEmails < Common
    doc 'Show' do
      desc 'Get current user single gpg key. [Email Address]'
      example 'client.users.emails.show(1)'
      result '#<Email id: 1>'
    end

    def show(email_id)
      email_id = format_id(email_id)

      client.request(:get, "user/emails/#{email_id}", Email)
    end
  end
end
