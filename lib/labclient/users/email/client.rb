# Top namespace
module LabClient
  # Inspect Helper
  class UserEmails < Common
    @group_name = 'User Emails'
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Users < Common
    def emails
      UserEmails.new(client)
    end
  end
end
