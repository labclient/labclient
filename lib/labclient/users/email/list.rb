# Top namespace
module LabClient
  # Specifics
  class UserEmails < Common
    doc 'List' do
      desc 'List current or target user emails. [User ID (Optional)]'
      example 'client.users.emails.list'
      result '[#<GpGKey id: 1>, ...]'
    end

    doc 'List' do
      desc 'List specific user keys.'
      example 'client.users.emails.list(1)'
    end

    doc 'List' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(5)
        user.emails
      DOC
    end

    def list(user_id = nil)
      if user_id
        user_id = format_id(user_id)
        client.request(:get, "users/#{user_id}/emails", Email)
      else
        client.request(:get, 'user/emails', Email)
      end
    end
  end
end
