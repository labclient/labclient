# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Update' do
      desc 'Modifies an existing user. Only administrators can change attributes of a user. [User ID, Hash]'
      example 'client.users.update(57, name: "Lorenzo")'
      result '#<User id: 57, username: speed>'

      markdown <<~DOC
        See [docs](https://docs.gitlab.com/ee/api/users.html#user-modification) for all available parameters
      DOC
    end

    doc 'Update' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(57)
        user.update(name: 'Lorenzo')
      DOC
    end

    def update(user_id, query)
      user_id = format_id(user_id)
      client.request(:put, "users/#{user_id}", User, query)
    end
  end
end
