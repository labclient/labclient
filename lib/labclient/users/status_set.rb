# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Update' do
      title 'Status'
      desc 'Set the status of the current user. [Hash]'
      example 'client.users.status_set(message: "Maaaagic")'
      result '{:emoji=>"speech_balloon", :message=>"Maaagic", :message_html=>"Maaagic"}'

      markdown <<~DOC
        | Attribute | Type   | Required | Description |
        | --------- | ------ | -------- | ----------- |
        | emoji   | string | no     | The name of the emoji to use as status, if omitted speech_balloon is used. Emoji name can be one of the specified names in the [Gemojione index](https://github.com/bonusly/gemojione/blob/master/config/index.json). |
        | message | string | no     | The message to set as a status. It can also contain emoji codes. |
      DOC
    end

    def status_set(query)
      client.request(:put, 'user/status', nil, query)
    end
  end
end
