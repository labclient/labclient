# Top namespace
module LabClient
  # Specifics
  class UserKeys < Common
    doc 'List' do
      desc 'Get a list of a user SSH keys. [User ID (Optional)]'
      example 'client.users.keys.list'
      result '[#<Key id: 1>, ...]'
    end

    doc 'List' do
      desc 'List specific user keys.'
      example 'client.users.keys.list(1)'
    end

    doc 'List' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(5)
        user.keys
      DOC
    end

    def list(user_id = nil)
      if user_id
        user_id = format_id(user_id)
        client.request(:get, "users/#{user_id}/keys", Key)
      else
        client.request(:get, 'user/keys', Key)
      end
    end
  end
end
