# Top namespace
module LabClient
  # Inspect Helper
  class UserKeys < Common
    @group_name = 'SSH Keys'
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Users < Common
    def keys
      UserKeys.new(client)
    end
  end
end
