# Top namespace
module LabClient
  # Specifics
  class UserKeys < Common
    doc 'Create' do
      desc 'Create new key for current or target user [Hash, User ID (Optional)]'
      example 'client.users.keys.create(title: "New Key!", key: "ssh-rsa AAAAB3NzaC1...")'
      result '#<Key id: 3>'

      markdown <<~DOC
        Parameters:

        - `title` (required) - new SSH Key's title
        - `key` (required) - new SSH key
      DOC
    end

    doc 'Create' do
      desc 'Create for Specific User'
      example <<~DOC
        client.users.keys.create(title: "Another!", key: "ssh-rsa +nxI4w9F33LsCgFm0R...",5)
      DOC
    end

    doc 'Create' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(5)
        user.key_create(title: "User Key!", key: "ssh-rsa AAA...")
      DOC
    end

    def create(query, user_id = nil)
      if user_id
        user_id = format_id(user_id)
        client.request(:post, "users/#{user_id}/keys", Key, query)
      else
        client.request(:post, 'user/keys', Key, query)
      end
    end
  end
end
