# Top namespace
module LabClient
  # Specifics
  class UserKeys < Common
    doc 'Delete' do
      desc 'Delete key for current or target user [Key ID, User ID (Optional)]'
      example 'client.users.keys.delete(2)'
    end

    doc 'Delete' do
      desc 'Delete Specific User Key. [Key ID, User ID]'
      example 'client.users.keys.delete(3,5)'
    end

    doc 'Delete' do
      desc 'Via User [Key ID]'
      example <<~DOC
        user = client.users.show(5)
        user.key_delete(2)
      DOC
    end

    def delete(key_id, user_id = nil)
      key_id = format_id(key_id)
      if user_id
        user_id = format_id(user_id)
        client.request(:delete, "users/#{user_id}/keys/#{key_id}")
      else
        client.request(:delete, "user/keys/#{key_id}")
      end
    end
  end
end
