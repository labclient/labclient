# Top namespace
module LabClient
  # Specifics
  class UserKeys < Common
    doc 'Show' do
      desc 'Get current user single key. [Key ID]'
      example 'client.users.keys.show(1)'
      result '#<Key id: 1>'
    end

    def show(key_id)
      key_id = format_id(key_id)

      client.request(:get, "user/keys/#{key_id}", Key)
    end
  end
end
