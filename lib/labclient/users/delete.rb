# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Delete' do
      desc 'Deletes a user. Available only for administrators. [User ID]'
      example 'client.users.delete(57)'
      result '=> nil'

      markdown <<~DOC
        See [docs](https://docs.gitlab.com/ee/api/users.html#user-modification) for all available parameters
      DOC
    end

    doc 'Delete' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(57)
        user.delete
      DOC
    end

    def delete(user_id)
      user_id = format_id(user_id)
      client.request(:delete, "users/#{user_id}")
    end
  end
end
