# Top namespace
module LabClient
  # Inspect Helper
  class Membership < Klass
    include ClassHelpers
    include AccessLevel

    def inspect
      "#<Membership name: #{source_name}, access: #{level}>"
    end

    def parent
      case source_type
      when 'Project'
        client.projects.show(source_id)
      when 'Namespace'
        client.groups.show(source_id)
      end
    end

    def level
      human_access_level(access_level)
    end

    def guest?
      level == :guest
    end

    def reporter?
      level == :reporter
    end

    def developer?
      level == :developer
    end

    def maintainer?
      level == :maintainer
    end

    def owner?
      level == :owner
    end

    def greater_than(leveler)
      case leveler
      when Symbol
        access_level > machine_access_level(leveler)
      when Integer
        access_level > leveler
      end
    end

    help do
      subtitle 'Membership'
      option 'level', 'Humanized symbol of access level. e.g. :developer'
      option 'parent', 'Collect project/namespace for this membership'
      option '<permission>?', 'True/False evaluation each guest?, developer? etc'
      option 'greater_than', 'True/False if user has greater than [Permission Name]'
    end
  end
end
