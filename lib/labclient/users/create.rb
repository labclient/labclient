# Top namespace
module LabClient
  # Specifics
  class Users < Common
    doc 'Create' do
      desc 'Create a new user. Available only for administrators [Hash]'
      example <<~DOC
        client.users.create(
          name:  "Lorenzo Trudgealong",
          email:"speed@labclient",
          username: "speed", password:"supersecure"
        )
      DOC
      result '#<User id: 57, username: speed>'

      markdown <<~DOC
        Required Parameters: `email`, `name`, and `username`. See [docs](https://docs.gitlab.com/ee/api/users.html#user-creation) for all available parameters
      DOC
    end

    def create(query)
      client.request(:post, 'users', User, query)
    end
  end
end
