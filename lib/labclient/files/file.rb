# Top namespace
module LabClient
  # General File Wrapper
  class LabFile < Klass
    include ClassHelpers

    def inspect
      "#<LabFile size: #{data&.size}>"
    end
  end
end
