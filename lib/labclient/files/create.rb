# Top namespace
module LabClient
  # Specifics
  class Files < Common
    doc 'Create' do
      desc 'This allows you to create a single file [Project ID, File Path, Hash]'
      example <<~DOC
        client.files.create(
          264, "new.md",
          branch: :master,
          commit_message: "New File!",
          content: "Content!"
        )
      DOC

      result '{:file_path=>"new.md", :branch=>"master"}'

      markdown <<~DOC
        Parameters

        | **Attribute**  | **Required** | **Description**                                 |
        | -------------- | ------------ | ----------------------------------------------- |
        | branch         | yes          | Name of the branch                              |
        | content        | yes          | File content                                    |
        | start_branch   | no           | Name of the branch to start the new commit from |
        | encoding       | no           | Change encoding to ‘base64’. Default is text.   |
        | author_email   | no           | Specify the commit author’s email address       |
        | author_name    | no           | Specify the commit author’s name                |
        | commit_message | no           | Commit message                                  |


      DOC
    end

    doc 'Create' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.file_create('other.md',
          branch: :master,
          commit_message: "New File!",
          content: "Content!")
      DOC
    end

    def create(project_id, file_path, query)
      project_id = format_id(project_id)

      # Path Name Encoding
      file_path = ERB::Util.url_encode(file_path)

      client.request(:post, "projects/#{project_id}/repository/files/#{file_path}", nil, query)
    end
  end
end
