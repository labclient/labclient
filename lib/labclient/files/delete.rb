# Top namespace
module LabClient
  # Specifics
  class Files < Common
    doc 'Delete' do
      desc 'This allows you to update a single file [Project ID, File Path, Hash]'
      example <<~DOC
        client.files.delete(
          264, "new.md",
          branch: :master,
          commit_message: "Delete File!",
        )
      DOC

      markdown <<~DOC
        **Parameters**:

        * branch (required) - Name of the branch
        * start_branch (optional) - Name of the branch to start the new commit from
        * author_email (optional) - Specify the commit author’s email address
        * author_name (optional) - Specify the commit author’s name
        * commit_message (required) - Commit message
        * last_commit_id (optional) - Last known file commit id
      DOC
    end

    doc 'Delete' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.file_delete('other.md',
          branch: :master,
          commit_message: "Remove File!",)
      DOC
    end

    def delete(project_id, file_path, query)
      project_id = format_id(project_id)

      # Path Name Encoding
      file_path = ERB::Util.url_encode(file_path)

      client.request(:delete, "projects/#{project_id}/repository/files/#{file_path}", nil, query)
    end
  end
end
