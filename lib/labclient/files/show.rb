# Top namespace
module LabClient
  # Specifics
  class Files < Common
    doc 'Show' do
      desc 'Allows you to receive information about file in repository like name, size, content [Project ID, File Path, Ref (main default), kind]'
      example 'client.files.show(264, "README.md")'

      markdown <<~DOC
        Ref will default to `main`

        Kind can be left empty or set to either :raw or :blame
      DOC
    end

    doc 'Show' do
      desc 'Raw Content'
      example 'client.files.show(264, "README.md", :main, :raw)'
    end

    doc 'Show' do
      desc 'Blame'
      example 'client.files.show(264, "README.md", :main, :blame)'
    end

    doc 'Show' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.file('README.md')
      DOC
    end

    def show(project_id, file_path, ref = :main, kind = nil)
      # LabStruct vs LabFile
      return_kind = kind == :raw ? LabFile : nil

      kind = case kind
             when :raw
               '/raw'
             when :blame
               '/blame'
             end

      project_id = format_id(project_id)

      # Path Name Encoding
      file_path = ERB::Util.url_encode(file_path)

      client.request(:get, "projects/#{project_id}/repository/files/#{file_path}#{kind}", return_kind, ref: ref)
    end
  end
end
