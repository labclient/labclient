# Top namespace
module LabClient
  # Specifics
  class Files < Common
    doc 'Update' do
      desc 'This allows you to update a single file [Project ID, File Path, Hash]'
      example <<~DOC
        client.files.update(
          264, "new.md",
          branch: :master,
          commit_message: "New File!",
          content: "Content!"
        )
      DOC

      result '{:file_path=>"new.md", :branch=>"master"}'

      markdown <<~DOC
        **Parameters**:

        * branch (required) - Name of the branch
        * start_branch (optional) - Name of the branch to start the new commit from
        * encoding (optional) - Change encoding to ‘base64’. Default is text.
        * author_email (optional) - Specify the commit author’s email address
        * author_name (optional) - Specify the commit author’s name
        * content (required) - New file content
        * commit_message (required) - Commit message
        * last_commit_id (optional) - Last known file commit id
      DOC
    end

    doc 'Update' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.file_update('other.md',
          branch: :master,
          commit_message: "New File!",
          content: "Content!")
      DOC
    end

    def update(project_id, file_path, query)
      project_id = format_id(project_id)

      # Path Name Encoding
      file_path = ERB::Util.url_encode(file_path)

      client.request(:put, "projects/#{project_id}/repository/files/#{file_path}", nil, query)
    end
  end
end
