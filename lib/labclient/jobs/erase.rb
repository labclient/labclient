# Top namespace
module LabClient
  # Specifics
  class Jobs < Common
    doc 'Update' do
      title 'Erase'
      desc 'Erase a single job of a project [Project ID, Job ID]'
      example 'client.jobs.erase(264, 14)'
    end

    doc 'Update' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.job_erase(1)
      DOC
    end

    doc 'Update' do
      desc 'via Job'
      example <<~DOC
        job = client.jobs.show(264,1)
        job.erase
      DOC
    end

    def erase(project_id, job_id)
      job_id = format_id(job_id)
      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/jobs/#{job_id}/erase", Job)
    end
  end
end
