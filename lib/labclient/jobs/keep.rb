# Top namespace
module LabClient
  # Specifics
  class Jobs < Common
    doc 'Update' do
      title 'Keep'
      desc 'Prevents artifacts from being deleted when expiration is set. [Project ID, Job ID]'
      example 'client.jobs.keep(264, 14)'
    end

    doc 'Update' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.job_keep(1)
      DOC
    end

    doc 'Update' do
      desc 'via Job'
      example <<~DOC
        job = client.jobs.show(264,1)
        job.keep
      DOC
    end

    def keep(project_id, job_id)
      job_id = format_id(job_id)
      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/jobs/#{job_id}/keep", Job)
    end
  end
end
