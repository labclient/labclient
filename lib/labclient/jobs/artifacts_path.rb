# Top namespace
module LabClient
  # Specifics
  class Jobs < Common
    doc 'Artifacts' do
      title 'Path'
      desc 'Download a single artifact file from a job with a specified ID from within the job’s artifacts zipped archive. The file is extracted from the archive and streamed to the client. [Project ID, Job ID, Artifact Path, File Output]'
      example 'client.jobs.artifacts_path(264, 1, "test.txt")'

      markdown <<~DOC
        File Output will default to the current directory + job id + file extension. 14.zip

        artifact_path: 	Path to a file inside the artifacts archive.
      DOC
    end

    doc 'Artifacts' do
      desc 'Specific output location'
      example <<~DOC
        client.jobs.artifacts(264, 1, '/tmp/output.zip')
      DOC
    end

    doc 'Artifacts' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.job_artifacts_path(7, 'path/file.txt')
      DOC
    end

    doc 'Artifacts' do
      desc 'via Job'
      example <<~DOC
        job = client.jobs.show(264, 7)
        job.artifacts_path('path/file.txt')
      DOC
    end

    def artifacts_path(project_id, job_id, artifact_path, file_path = nil)
      job_id = format_id(job_id)
      project_id = format_id(project_id)
      file_path ||= "#{Dir.pwd}/#{artifact_path}"

      output = client.request(:get, "projects/#{project_id}/jobs/#{job_id}/artifacts/#{artifact_path}", nil)

      File.write(file_path, output)
    end
  end
end
