# Top namespace
module LabClient
  # Specifics
  class Jobs < Common
    doc 'Update' do
      title 'Play'
      desc 'Triggers a manual action to start a job. [Project ID, Job ID]'
      example 'client.jobs.play(264, 14)'
    end

    doc 'Update' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.job_play(1)
      DOC
    end

    doc 'Update' do
      desc 'via Job'
      example <<~DOC
        job = client.jobs.show(264,1)
        job.play
      DOC
    end

    def play(project_id, job_id)
      job_id = format_id(job_id)
      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/jobs/#{job_id}/play", Job)
    end
  end
end
