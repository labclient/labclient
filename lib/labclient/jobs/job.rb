# Top namespace
module LabClient
  # Inspect Helper
  class Job < Klass
    include ClassHelpers

    def inspect
      "#<Job id: #{id}, status: #{status}, stage: #{stage}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show(project_id)
    end

    def pipeline
      Pipeline.new(@table[:pipeline], response, client)
    end

    # File Write Response
    def download_artifacts(file_path = nil, job_token = nil)
      project_id = collect_project_id
      client.jobs.download_artifacts(project_id, id, job_token, file_path)
    end

    # LabFile Response
    def show_artifacts(job_token = nil)
      project_id = collect_project_id
      client.jobs.artifacts(project_id, id, job_token)
    end

    def artifacts_path(artifacts_path, file_path = nil)
      project_id = collect_project_id
      client.jobs.artifacts_path(project_id, id, artifacts_path, file_path)
    end

    def trace
      project_id = collect_project_id
      client.jobs.trace(project_id, id)
    end

    def cancel
      project_id = collect_project_id
      client.jobs.cancel(project_id, id)
    end

    def retry
      project_id = collect_project_id
      client.jobs.retry(project_id, id)
    end

    def erase
      project_id = collect_project_id
      client.jobs.erase(project_id, id)
    end

    def keep
      project_id = collect_project_id
      client.jobs.keep(project_id, id)
    end

    def delete
      project_id = collect_project_id
      client.jobs.delete(project_id, id)
    end

    def play
      project_id = collect_project_id
      client.jobs.play(project_id, id)
    end

    help do
      @group_name = 'Jobs'
      subtitle 'Job'
      option 'download_artifacts', 'Download Job artifacts. [File Path, Job Token]'
      option 'artifacts_path', 'Download Job artifacts path. [File Path, Output Path]'
      option 'trace', 'Get Job Output'
      option 'cancel', 'Cancel Job'
      option 'retry', 'Retry Job'
      option 'erase', 'Erase Job'
      option 'keep', 'Prevents artifacts from being deleted when expiration is set.'
      option 'delete', 'Delete artifacts of a job.'
      option 'play', 'Triggers a manual action to start a job.'
    end
  end
end
