# Top namespace
module LabClient
  # Specifics
  class Jobs < Common
    doc 'Artifacts' do
      title 'Latest'
      desc 'Download the artifacts zipped archive from the latest successful pipeline for the given reference name and job, provided the job finished successfully [Project ID, Branch Name, Job Name, File Path, Job Token]'
      example 'client.jobs.artifacts_latest(264, :master, :artifacts)'
      result '[#<Runner id: 1, status: online>, ... ]'

      markdown <<~DOC

        File Output will default to the current directory + job name + file extension. job_name.zip


          Job Token: To be used with triggers for multi-project pipelines. It should be invoked only inside `.gitlab-ci.yml`. Its value is always `$CI_JOB_TOKEN`.
      DOC
    end

    doc 'Artifacts' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.job_artifacts_latest(:master, :artifacts)
      DOC
    end

    def artifacts_latest(project_id, branch_name, job_name, file_path = nil, job_token = nil)
      query = { job: job_name }
      query[:job_token] = job_token if job_token
      file_path ||= "#{Dir.pwd}/#{job_name}.zip"

      project_id = format_id(project_id)
      output = client.request(:get, "projects/#{project_id}/jobs/artifacts/#{branch_name}/download", nil, query)

      File.write(file_path, output)
    end
  end
end
