# Top namespace
module LabClient
  # Specifics
  class Jobs < Common
    doc 'Artifacts' do
      desc 'Get the job’s artifacts zipped archive of a project. [Project ID, Job ID, Job Token, File Path]'
      example 'client.jobs.artifacts(264, 1)'

      markdown <<~DOC
        ---

        Two methods are available. `download_artifacts` will download and write to the local directory (or file path) and `artifacts`/`show_artifacts` which will return a `LabFile` with the key of `data` for the response result.

        File Output will default to the current directory + job id + file extension. 14.zip (Still returning `LabFile`)

        Job Token: To be used with triggers for multi-project pipelines. It should be invoked only inside `.gitlab-ci.yml`. Its value is always `$CI_JOB_TOKEN`.
      DOC
    end

    doc 'Artifacts' do
      desc 'Specific output location'
      example <<~DOC
        client.jobs.download_artifacts(264, 1, '/tmp/output.zip')
      DOC
    end

    doc 'Artifacts' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.job_download_artifacts(7)
        project.job_artifacts(7)
      DOC
    end

    doc 'Artifacts' do
      desc 'via Job'
      example <<~DOC
        job = client.jobs.show(264, 7)
        job.download_artifacts
        job.show_artifacts
      DOC
    end

    def artifacts(project_id, job_id, job_token = nil)
      job_id = format_id(job_id)
      project_id = format_id(project_id)
      query = { job_token: job_token } if job_token

      # Don't try to process output
      client.request(:get, "projects/#{project_id}/jobs/#{job_id}/artifacts", LabFile, query)
    end

    # Helper to write immediately to a file
    def download_artifacts(project_id, job_id, file_path = nil, job_token = nil)
      file_path ||= "#{Dir.pwd}/#{job_id}.zip"
      output = artifacts(project_id, job_id, job_token)

      File.write(file_path, output.data) if output.success?

      # Return LabFile
      output
    end
  end
end
