# Top namespace
module LabClient
  # Specifics
  class Jobs < Common
    doc 'Show' do
      desc 'Get a list of jobs for a pipeline. [Project ID, Job ID]'
      example 'client.jobs.show(264, 1)'
      result '#<Job id: 13, status: success, stage: build>'
    end

    doc 'Show' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.job(1)
      DOC
    end

    def show(project_id, job_id)
      job_id = format_id(job_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/jobs/#{job_id}", Job)
    end
  end
end
