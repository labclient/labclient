# Top namespace
module LabClient
  # Specifics
  class Jobs < Common
    doc 'List' do
      title 'Pipeline'
      desc 'Get a list of jobs for a pipeline. [Project ID, Pipeline ID, Scope]'
      example 'client.jobs.pipeline(264, 7)'
      result '[#<Job id: 13, status: success, stage: build>, #<Job id: 14, status: success, stage: build>]'

      markdown <<~DOC
        Scope of jobs to show. Either one of or an array of the following: created, pending, running, failed, success, canceled, skipped, or manual. All jobs are returned if scope is not provided.
      DOC
    end

    doc 'List' do
      desc 'via Pipeline'
      example <<~DOC
        pipeline = client.pipelines.show(264, 7)
        pipeline.jobs
      DOC
    end

    def pipeline(project_id, pipeline_id, scope = nil)
      pipeline_id = format_id(pipeline_id)
      project_id = format_id(project_id)

      query = { scope: scope } if scope

      client.request(:get, "projects/#{project_id}/pipelines/#{pipeline_id}/jobs", Job, query)
    end
  end
end
