# Top namespace
module LabClient
  # Specifics
  class Jobs < Common
    doc 'Update' do
      title 'Retry'
      desc 'Retry a single job of a project [Project ID, Job ID]'
      example 'client.jobs.retry(264, 14)'
    end

    doc 'Update' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.job_retry(1)
      DOC
    end

    doc 'Update' do
      desc 'via Job'
      example <<~DOC
        job = client.jobs.show(264,1)
        job.retry
      DOC
    end

    def retry(project_id, job_id)
      job_id = format_id(job_id)
      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/jobs/#{job_id}/retry", Job)
    end
  end
end
