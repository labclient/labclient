# Top namespace
module LabClient
  # Specifics
  class Jobs < Common
    doc 'Show' do
      title 'Trace'
      desc 'Get a log (trace) of a specific job of a project [Project ID, Job ID]'
      example 'client.jobs.show(264, 14)'
    end

    doc 'Show' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.job_trace(1)
      DOC
    end

    doc 'Show' do
      desc 'via Job'
      example <<~DOC
        job = client.jobs.show(264,1)
        job.trace #=> String
      DOC
    end

    def trace(project_id, job_id)
      job_id = format_id(job_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/jobs/#{job_id}/trace")
    end
  end
end
