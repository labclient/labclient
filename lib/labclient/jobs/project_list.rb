# Top namespace
module LabClient
  # Specifics
  class Jobs < Common
    doc 'List' do
      title 'Project'
      desc 'Get a list of jobs in a project. Jobs are sorted in descending order of their IDs. [Project ID, Scope]'
      example 'client.jobs.project(264)'
      result '[#<Job id: 10, status: failed>, ...]'

      markdown <<~DOC
        Scope of jobs to show. Either one of or an array of the following: created, pending, running, failed, success, canceled, skipped, or manual. All jobs are returned if scope is not provided.
      DOC
    end

    doc 'List' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.jobs
      DOC
    end

    def project(project_id, scope = nil)
      project_id = format_id(project_id)
      query = { scope: scope } if scope
      client.request(:get, "projects/#{project_id}/jobs", Job, query)
    end
  end
end
