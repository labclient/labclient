# Top namespace
module LabClient
  # Specifics
  class Jobs < Common
    doc 'Update' do
      title 'Delete'
      desc 'Delete artifacts of a job. [Project ID, Job ID]'
      example 'client.jobs.delete(264, 14)'
    end

    doc 'Update' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.job_delete(1)
      DOC
    end

    doc 'Update' do
      desc 'via Job'
      example <<~DOC
        job = client.jobs.show(264,1)
        job.delete
      DOC
    end

    def delete(project_id, job_id)
      job_id = format_id(job_id)
      project_id = format_id(project_id)
      client.request(:delete, "projects/#{project_id}/jobs/#{job_id}/artifacts")
    end
  end
end
