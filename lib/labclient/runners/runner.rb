# Top namespace
module LabClient
  # Inspect Helper
  class Runner < Klass
    include ClassHelpers

    def inspect
      "#<Runner id: #{id}, status: #{status}>"
    end

    def details
      update_self client.runners.show(id)
    end

    def remove
      client.runners.remove(id)
    end

    def jobs(query = {})
      client.runners.jobs(id, query)
    end

    def update(query)
      update_self client.runners.update(id, query)
    end

    def verify
      details if token.nil? # Not included in List
      client.runners.verify(token)
    end

    help do
      subtitle 'Runner'
      option 'details', 'Collect runner details'
      option 'update', 'Update this runner [Hash]'
      option 'update', 'Remove runner'
      option 'jobs', 'Show runner jobs [Hash]'
      option 'verify', 'Verify authentication token'
    end
  end
end
