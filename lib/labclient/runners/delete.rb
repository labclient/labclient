# Top namespace
module LabClient
  # Specifics
  class Runners < Common
    doc 'Delete' do
      desc 'Delete a registered runner. [Authentication Token]'
      example 'client.runners.delete("TokenInformation")'
    end

    def delete(token)
      client.request(:delete, 'runners', nil, token: token)
    end
  end
end
