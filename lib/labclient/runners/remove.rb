# Top namespace
module LabClient
  # Specifics
  class Runners < Common
    doc 'Remove' do
      desc 'Remove a runner. [Runner ID]'
      example 'client.runners.delete(5)'
    end

    doc 'Remove' do
      desc 'Via Runner'
      example <<~DOC
        runner = client.runners.show(5)
        runner.delete
      DOC
    end

    def remove(runner_id)
      runner_id = format_id(runner_id)

      client.request(:delete, "runners/#{runner_id}")
    end
  end
end
