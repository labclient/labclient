# Top namespace
module LabClient
  # Specifics
  class Runners < Common
    doc 'List' do
      title 'Owned'
      desc 'Get a list of specific runners available to the user. [Hash]'
      example 'client.runners.list'
      result '[#<Runner id: 1, status: online>, ... ]'

      markdown <<~DOC
        | Attribute   | Type           | Required | Description         |
        |-------------|----------------|----------|---------------------|
        | type      | string         | no       | The type of runners to show, one of: instance_type, group_type, project_type |
        | status    | string         | no       | The status of runners to show, one of: active, paused, online, offline |
        | tag_list  | string array   | no       | List of of the runner's tags |
      DOC
    end

    def list(query = {})
      client.request(:get, 'runners', Runner, query)
    end
  end
end
