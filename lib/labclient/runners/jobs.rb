# Top namespace
module LabClient
  # Specifics
  class Runners < Common
    doc 'Jobs' do
      desc 'List jobs that are being processed or were processed by specified Runner. [Runner ID]'
      example 'client.runners.jobs(1)'
      result '[#<Runner id: 1, status: online>, ... ]'

      markdown <<~DOC
        | Attribute | Type    | Required | Description         |
        |-----------|---------|----------|---------------------|
        | status  | string  | no       | Status of the job; one of: running, success, failed, canceled |
        | order_by| string  | no       | Order jobs by id. |
        | sort    | string  | no       | Sort jobs in asc or desc order (default: desc) |

      DOC
    end

    def jobs(runner_id, query = {})
      runner_id = format_id(runner_id)
      client.request(:get, "runners/#{runner_id}/jobs", Job, query)
    end
  end
end
