# Top namespace
module LabClient
  # Specifics
  class Runners < Common
    doc 'Show' do
      desc 'Get details of a runner. [Runner ID]'
      example 'client.runners.show(3)'
      result '#<Runner id: 3, status: online>'
    end

    def show(runner_id)
      runner_id = format_id(runner_id)
      client.request(:get, "runners/#{runner_id}", Runner)
    end
  end
end
