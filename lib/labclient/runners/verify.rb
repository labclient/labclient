# Top namespace
module LabClient
  # Specifics
  class Runners < Common
    doc 'Verify' do
      desc 'Validates authentication credentials for a registered Runner. [Authentication Token]'
      example 'client.runners.verify("TokenInformation")'
    end

    doc 'Verify' do
      desc 'Via Runner'
      example <<~DOC
        runner = client.runners.show(3)
        runner.verify
      DOC
    end

    def verify(token)
      client.request(:post, 'runners/verify', nil, token: token)
    end
  end
end
