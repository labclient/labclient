# Top namespace
module LabClient
  # Specifics
  class Runners < Common
    doc 'Register' do
      desc 'Register a new Runner for the instance. [Hash]'
      example 'client.runners.register(token: "Token!")'
      result '{:id=>"12345", :token=>"6337ff461c94fd3fa32ba3b1ff4125"}'

      markdown <<~DOC

        | Attribute    | Type    | Required | Description         |
        |--------------|---------|----------|---------------------|
        | token      | string  | yes      | Registration token |
        | description| string  | no       | Runner's description |
        | info       | hash    | no       | Runner's metadata   |
        | active     | boolean | no       | Whether the Runner is active   |
        | locked     | boolean | no       | Whether the Runner should be locked for current project |
        | run_untagged | boolean | no     | Whether the Runner should handle untagged jobs |
        | tag_list   | string array | no  | List of Runner's tags |
        | access_level    | string | no   | The access_level of the runner; not_protected or ref_protected |
        | maximum_timeout | integer | no  | Maximum timeout set when this Runner will handle the job |
      DOC
    end

    def register(query)
      client.request(:post, 'runners', nil, query)
    end
  end
end
