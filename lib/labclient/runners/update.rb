# Top namespace
module LabClient
  # Specifics
  class Runners < Common
    doc 'Update' do
      desc 'Update details of a runner. [Runner ID, Hash]'
      example 'client.runners.update(3, description: :rawr)'
      result '#<Runner id: 3, status: online>'
      markdown <<~DOC

        | Attribute     | Type    | Required | Description         |
        |---------------|---------|----------|---------------------|
        | description | string  | no       | The description of a runner |
        | active      | boolean | no       | The state of a runner; can be set to true or false |
        | tag_list    | array   | no       | The list of tags for a runner; put array of tags, that should be finally assigned to a runner |
        | run_untagged| boolean | no       | Flag indicating the runner can execute untagged jobs |
        | locked      | boolean | no       | Flag indicating the runner is locked |
        | access_level | string | no       | The access_level of the runner; not_protected or ref_protected |
        | maximum_timeout | integer | no   | Maximum timeout set when this Runner will handle the job |
      DOC
    end

    doc 'Update' do
      desc 'Via Runner'
      example <<~DOC
        runner = client.runners.show(5)
        runner.update(description: 'Hello!')
      DOC
    end

    doc 'Update' do
      desc 'Pausing a Runner'
      example <<~DOC
        client.runners.update(3, active: false)
        => #<Runner id: 3, status: paused>
      DOC
    end

    def update(runner_id, query)
      runner_id = format_id(runner_id)

      client.request(:put, "runners/#{runner_id}", Runner, query)
    end
  end
end
