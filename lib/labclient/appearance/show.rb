# Top namespace
module LabClient
  # Specifics
  class Appearances < Common
    @group_name = 'Appearance'

    doc 'Show' do
      desc 'List the current appearance configuration of the GitLab instance.'
      example 'client.appearance.show'
      result <<~DOC
        => => {:title=>"", :description=>"", :logo=>nil, :header_logo=>nil, :favicon=>nil, :new_project_guidelines=>"", :header_message=>"", :footer_message=>"", :message_background_color=>"#E75E40", :message_font_color=>"#FFFFFF", :email_header_and_footer_enabled=>false}
      DOC
    end

    def show
      client.request(:get, 'application/appearance', Appearance)
    end
  end
end
