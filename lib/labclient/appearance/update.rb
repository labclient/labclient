# Top namespace
module LabClient
  # Specifics
  class Appearances < Common
    @group_name = 'Appearance'

    doc 'Update' do
      desc 'Use an API call to modify GitLab instance appearance configuration.'
      example 'client.appearance.update(title: "Gitlaaaab!")'
      result <<~DOC
        => {:title=>"Gitlaaaab!", :description=>"", :logo=>nil, :header_logo=>nil, :favicon=>nil, :new_project_guidelines=>"", :header_message=>"", :footer_message=>"", :message_background_color=>"#E75E40", :message_font_color=>"#FFFFFF", :email_header_and_footer_enabled=>false}
      DOC
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute                         | Type    | Required | Description |
        | --------------------------------- | ------- | -------- | ----------- |
        | title                           | string  | no       | Instance title on the sign in / sign up page
        | description                     | string  | no       | Markdown text shown on the sign in / sign up page
        | logo                            | mixed   | no       | Instance image used on the sign in / sign up page
        | header_logo                     | mixed   | no       | Instance image used for the main navigation bar
        | favicon                         | mixed   | no       | Instance favicon in .ico/.png format
        | new_project_guidelines          | string  | no       | Markdown text shown on the new project page
        | header_message                  | string  | no       | Message within the system header bar
        | footer_message                  | string  | no       | Message within the system footer bar
        | message_background_color        | string  | no       | Background color for the system header / footer bar
        | message_font_color              | string  | no       | Font color for the system header / footer bar
        | email_header_and_footer_enabled | boolean | no       | Add header and footer to all outgoing emails if enabled
      DOC
    end

    def update(query = {})
      client.request(:put, 'application/appearance', Appearance, query)
    end
  end
end
