# Extensions for LabStruct specific to LabClient
module LabClient
  # Unique inherited class to not override top level LabStruct
  class LabStruct
    include CurlHelper
    attr_reader :response, :table

    def initialize(hash = {})
      @table = if hash.instance_of?(LabClient::LabStruct)
                 hash.to_h
               else
                 hash
               end
    end

    def to_h
      @table
    end

    def keys
      @table.keys.sort
    end

    def inspect
      @table.inspect
    end

    def as_json(*args)
      super.as_json['table']
    end

    def slice(*opts)
      @table.slice(*opts)
    end

    def client
      response.client
    end

    # Forward response success
    def success?
      @response.success?
    end

    def method_missing(method, *_args)
      @table[method] if @table.keys.include?(method)
    end

    def respond_to_missing?(method_name, include_private = false)
      @table.keys.include?(method_name) || super
    end

    def key?(idx)
      @table.key? idx
    end

    def []=(name, value)
      @table[name] = value
    end

    def [](name)
      @table[name.to_sym]
    end
  end
end
