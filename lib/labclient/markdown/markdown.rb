# Top namespace
module LabClient
  # Specifics
  class Markdown < Common
    doc 'Render' do
      desc 'Render an arbitrary Markdown document.'
      example 'client.markdown.render(text: "# Oh Hi!")'
      result <<~DOC
        =>  { :html=>"<h1 data-sourcepos=\"1:1-1:8\">Oh Hi!</h1>"}
      DOC
    end

    doc 'Render' do
      markdown <<~DOC
        | Attribute | Type    | Required      | Description                                |
        | --------- | ------- | ------------- | ------------------------------------------ |
        | text    | string  | yes           | The Markdown text to render                |
        | gfm     | boolean | no (optional) | Render text using GitLab Flavored Markdown. Default is false |
        | project | string  | no (optional) | Use project as a context when creating references using GitLab Flavored Markdown. Authentication is required if a project is not public.  |

      DOC
    end

    def render(query)
      client.request(:post, 'markdown', nil, query)
    end
  end
end
