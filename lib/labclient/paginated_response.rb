module LabClient
  # Additional Pagination Support
  # First page is always request prior to this class being instantiated
  class PaginatedResponse
    include CurlHelper

    attr_accessor :client, :response, :array

    def initialize(klass, response, client)
      @klass = klass
      @response = response
      @client = client

      @array = response.data.map { |entry| process_entry(entry, response) }

      auto_paginate if client.settings[:paginate]
    end

    def inspect
      @array
    end

    def method_missing(name, *args, &block)
      if @array.respond_to?(name)
        @array.send(name, *args, &block)
      else
        super
      end
    end

    def help
      puts <<~DOC
        Pagination Helper Methods
          auto_paginate
            Automatically collect and return all results for a query. Accepts block
          paginate_with_limit
            Iterate through pages, but end when a certain limit is reached
          each_page
            Similar with auto_paginate, you can call each_page directly
      DOC
    end

    def respond_to_missing?(method_name, include_private = false); end

    def each_page(&block)
      yield @array # This will eventually be the whole list
      auto_paginate(&block)
    end

    def auto_paginate(&block)
      yield @array if block_given?

      loop do
        break unless next_page?

        next_page(&block)
      end

      @array
    end

    # Paginate to a limit
    def paginate_with_limit(limit, &block)
      yield @array if block_given?

      loop do
        break unless next_page?
        break if @array.length >= limit

        next_page(&block)
      end

      @array
    end

    # Check for and store next page
    def next_page?
      text = @response.headers['link']&.split(',')&.find { |x| x.include? 'next' }
      return nil if text.nil?

      @link = link_regex.match(text.strip)[1]

      return false if @link.nil?

      true
    end

    def link_regex
      /<([^>]+)>; rel="([^"]+)"/
    end

    def next_page
      return false unless next_page?

      @response = client.http.request(:get, @link)

      raise LabClient::Error.new(@response), @response.friendly_error unless @response.success?

      results = process
      @array.concat results
      yield results if block_given?
    end

    # Create Class Objects
    def process_entry(entry, entry_response)
      @klass ? @klass.new(entry, entry_response, client) : entry
    end

    def process
      @response.data.map do |entry|
        process_entry(entry, @response)
      end
    end

    # Forward response success
    def success?
      @response.success?
    end

    def total_pages
      response.headers['x-total-pages'].to_i
    end

    def total
      response.headers['x-total'].to_i
    end

    def page
      response.headers['x-page'].to_i
    end
  end
end
