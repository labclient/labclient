# Top namespace
module LabClient
  # Specifics
  class Applications < Common
    doc 'Delete' do
      markdown 'Can delete via the `applications` or via the `Application` object'
    end

    doc 'Delete' do
      desc 'Delete through applications'
      example <<~DOC
        client.applications.delete(11)
      DOC
    end

    doc 'Delete' do
      desc 'Delete through result object'
      example <<~DOC
        application.delete
      DOC

      result <<~DOC
        application = client.applications.list.last
        => #<Application id: 8, Grafana>
        application.delete
        => nil
      DOC
    end

    # Delete
    def delete(application_id = nil)
      client.request(:delete, "applications/#{application_id}")
    end
  end
end
