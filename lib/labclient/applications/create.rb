# Top namespace
module LabClient
  # Specifics
  class Applications < Common
    doc 'Create' do
      desc 'Register a new Application'
      markdown <<~DOC
        | Attribute      | Description                      |
        |--------------- |--------------------------------- |
        | name         | Name of the application.         |
        | redirect_uri | Redirect URI of the application. |
        | scope        | Scope of the application.       |

        > **Scopes** - api, email, openid, profile, read_registry, read_repository, read_user, sudo, write_repository
      DOC
    end

    doc 'Create' do
      example <<~DOC
        client.applications.create(
          name: "Grafana",
          redirect_uri: "https://gitlab.com/-/grafana/login/gitlab",
          scopes: "email"
        )
      DOC
      result <<~DOC
        #<Application id: 11, Grafana>
      DOC
    end

    # Create
    def create(query = {})
      client.request(:post, 'applications', Application, query)
    end
  end
end
