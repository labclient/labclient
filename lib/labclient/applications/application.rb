# Top namespace
module LabClient
  # Inspect Helper
  class Application < Klass
    include ClassHelpers
    def inspect
      "#<Application id: #{id}, #{application_name}>"
    end

    def delete
      client.applications.delete id
    end

    help do
      subtitle 'Application'
      option 'delete', 'Delete this specific application.'
    end
  end
end
