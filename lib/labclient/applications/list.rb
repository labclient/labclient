# Top namespace
module LabClient
  # Specifics
  class Applications < Common
    doc 'List' do
      desc 'List'
      example 'client.applications.list'
      result <<~DOC
        => [#<Application id: 1, GitLab Grafana>, #<Application id: 4, OAuth>]
      DOC
    end

    # List
    def list(query = {})
      client.request(:get, 'applications', Application, query)
    end
  end
end
