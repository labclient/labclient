# Top namespace
module LabClient
  # Specifics
  class CiLint < Common
    doc 'Lint' do
      desc 'Checks if your .gitlab-ci.yml file is valid.'
      example <<~DOC
        client.ci_lint.check <<~YAML
          image: busybox:latest

          build:
            script:
              - echo "Do your build here"

        YAML
      DOC
      result <<~DOC
        =>  {:status=>"valid", :errors=>[]}
      DOC
    end

    doc 'Lint' do
      desc 'Invalid Output'
      example <<~DOC
        client.ci_lint.check("baddddd")
      DOC
      result <<~DOC
        => {:status=>"invalid", :errors=>["Invalid configuration format"]}
      DOC
    end

    doc 'Lint' do
      desc 'Read from File'
      example <<~DOC
        client.ci_lint.check File.read('.gitlab-ci.yml')
      DOC
    end

    def check(content)
      client.request(:post, 'ci/lint', nil, content: content)
    end
  end
end
