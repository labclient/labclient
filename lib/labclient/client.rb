# Top Namespace
module LabClient
  # API Specifics
  class Client
    include ClientHelpers
    include ClientSetup
    include Logger

    # Default setup, pull in settings
    def initialize(user_settings = nil)
      @settings = user_settings
      setup_profile if user_settings&.key?(:profile) || ENV['LABCLIENT_PROFILE']
      @settings ||= fill_configuration

      # Set Unspecified Defaults
      unspecified_defaults

      prompt_for_url if @settings[:url].blank?

      # Only prompt if explicitly set to nil
      prompt_for_token if @settings[:token].nil?

      # Initial Delay / Retry Value
      self.delay = 0
      self.retries = 0

      # Request Configuration
      self.http = HTTP.new(@settings)
    end

    # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
    def request(method, path, klass = nil, body = {}, dump_json = true)
      self.klass = klass
      self.resp = http.request(method, path, body, dump_json)
      self.path = path

      debug_handler if debug?

      post_request_handlers
      process resp
    rescue LabClient::Error => e
      logger.fatal('Request Failed', e.error_details) unless quiet?
      resp
    rescue LabClient::Retry
      self.retries += 1

      # Assume Retry After by Default
      logger.debug('Retry After', value: retry_after, retry_debug_headers: retry_debug_headers) if debug?
      self.delay = retry_after if resp.headers.key? 'retry-after'

      self.delay += delay_factor
      logger.warn "Received #{resp.code}. Retry in #{delay}", limit: retry_max, retries: retries unless quiet?

      sleep delay unless ENV['LABCLIENT_TESTING']

      retry
    end
    # rubocop:enable Metrics/AbcSize, Metrics/MethodLength

    # Handling Details for after the request
    # Debug,Retry, Instance Variables, Error Handling
    def post_request_handlers
      # Handle Retry Logic
      raise LabClient::Retry if should_retry?

      # Save Client
      save_client

      # Reset Delay/Retry Factor
      retry_update if resp.success?

      # Exit on Max Retries
      raise LabClient::Error.new(resp), resp.friendly_error if retry_max?

      raise LabClient::Error.new(resp), resp.friendly_error unless resp.success?

      # Drop in raw path
      save_path
    end

    # Assume we want LabStruct if @klass is ever nil
    def process(resp)
      case resp.data
      when LabStruct
        klass ? klass.new(resp.data, resp, self) : resp.data
      when Array
        if klass.nil?
          PaginatedResponse.new(Klass, resp, self)
        else
          PaginatedResponse.new(klass, resp, self)
        end
        # Return Response/Status Object if there is no output otherwise
      when NilClass
        resp
      # Default handler / if klass defined store plain response in data
      else
        klass ? klass.new({ data: resp.data, response: resp }, resp, self) : resp
      end
    end
  end
end
