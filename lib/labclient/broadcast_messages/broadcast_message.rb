# Top namespace
module LabClient
  # Inspect Helper
  class BroadcastMessage < Klass
    include ClassHelpers

    def inspect
      active_text = active ? 'Active' : 'Inactive'
      "#<BroadcastMessage id: #{id}, #{active_text}>"
    end

    def update(query = {})
      query[:starts_at] = query[:starts_at].to_time.iso8601 if format_time?(query[:starts_at])
      query[:ends_at] = query[:ends_at].to_time.iso8601 if format_time?(query[:ends_at])

      update_self client.broadcast_messages.update(id, query)
    end

    date_time_attrs %i[ends_at starts_at]

    def delete
      client.broadcast_messages.delete id
    end

    help do
      subtitle 'Broadcast Message'
      option 'delete', 'Delete this broadcast message.'
      option 'update', 'Update this broadcast message.'
    end
  end
end
