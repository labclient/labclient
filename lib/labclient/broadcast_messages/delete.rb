# Top namespace
module LabClient
  # Specifics
  class BroadcastMessages < Common
    doc 'Delete' do
      desc 'Delete a broadcast message.'
      markdown 'You can delete broadcast messages via the `broadcast_messages` method or via the `BroadcastMessage` object'
    end

    doc 'Delete' do
      desc 'Delete through broadcast_messages'
      example <<~DOC
        client.broadcast_messages.delete(5)
      DOC
    end

    doc 'Delete' do
      desc 'Delete through BroadcastMessage object'
      example <<~DOC
        broadcast.delete
      DOC

      result <<~DOC
        broadcast = client.broadcast_messages.list.last
        => #<BroadcastMessage id: 2, Active>
        broadcast.delete
        => nil
      DOC
    end

    # Delete
    def delete(broadcast_message_id = nil)
      client.request(:delete, "broadcast_messages/#{broadcast_message_id}")
    end
  end
end
