# Top namespace
module LabClient
  # Specifics
  class BroadcastMessages < Common
    doc 'Update' do
      desc 'Update an existing broadcast message.'
      markdown <<~DOC
        | Attribute  | Type     | Required | Description                                           |
        |------------| ---------|----------|-------------------------------------------------------|
        | message    | string   | yes      | Message to display.                                   |
        | starts_at  | datetime | no       | Starting time (defaults to current time).             |
        | ends_at    | datetime | no       | Ending time (defaults to one hour from current time). |
        | color      | string   | no       | Background color hex code.                            |
        | font       | string   | no       | Foreground color hex code.                            |
      DOC
    end

    doc 'Update' do
      example <<~DOC
        client.broadcast_messages.update(4, message: "Even more alert!")
      DOC
      result <<~DOC
        => #<BroadcastMessage id: 4, Active>
      DOC
    end

    doc 'Update' do
      title 'Date Time Helpers'
      markdown 'Both `:starts_at` and `:ends_at` can be used with Date or Time objects.   <small>_responds_to: `to_time`_</small>'

      example <<~DOC
        result = client.broadcast_messages.update(4, message: "Alert!", starts_at: Date.today, ends_at: 15.days.from_now)
      DOC
      result <<~DOC
        => #<BroadcastMessage id: 4, Active>
        result.ends_at
        => Thu, 09 Jan 2020 16:44:27 -0700
      DOC
    end

    doc 'Update' do
      title 'Methods'
      markdown 'You can update broadcast messages via the `broadcast_messages` method or via the `BroadcastMessage` object'
    end

    doc 'Update' do
      desc 'Update through broadcast_messages'
      example <<~DOC
        client.broadcast_messages.update(5, message: 'Update here!')
      DOC
    end

    doc 'Update' do
      desc 'Update through BroadcastMessage object'
      example <<~DOC
        broadcast.update(message: 'Another here!')
      DOC

      result <<~DOC
        broadcast = client.broadcast_messages.list.last
        => #<BroadcastMessage id: 2, Active>
        broadcast.update(message: 'Another here!')
        => #<BroadcastMessage id: 2, Active>
      DOC
    end

    # Update
    def update(broadcast_message_id = nil, query = {})
      query[:starts_at] = query[:starts_at].to_time.iso8601 if format_time?(query[:starts_at])
      query[:ends_at] = query[:ends_at].to_time.iso8601 if format_time?(query[:ends_at])

      client.request(:put, "broadcast_messages/#{broadcast_message_id}", BroadcastMessage, query)
    end
  end
end
