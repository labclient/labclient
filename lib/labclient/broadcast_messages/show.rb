# Top namespace
module LabClient
  # Specifics
  class BroadcastMessages < Common
    doc 'Show' do
      desc 'Get a specific broadcast message.'
      example 'client.broadcast_messages.show(2)'
      result <<~DOC
        => #<BroadcastMessage id: 2, Inactive>
      DOC
    end

    # Show Specific
    def show(broadcast_message_id = nil)
      client.request(:get, "broadcast_messages/#{broadcast_message_id}", BroadcastMessage)
    end
  end
end
