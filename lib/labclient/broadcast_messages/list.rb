# Top namespace
module LabClient
  # Specifics
  class BroadcastMessages < Common
    doc 'List' do
      desc 'List all broadcast messages.'
      example 'client.broadcast_messages.list'
      result <<~DOC
        => [#<BroadcastMessage id: 2, Active>, #<BroadcastMessage id: 1, Inactive>]
      DOC
    end

    # List
    def list
      client.request(:get, 'broadcast_messages', BroadcastMessage)
    end
  end
end
