# Top namespace
module LabClient
  # Specifics
  class BroadcastMessages < Common
    doc 'Create' do
      desc 'Create a new broadcast message'
      markdown <<~DOC
        | Attribute  | Type     | Required | Description                                           |
        |------------| ---------|----------|-------------------------------------------------------|
        | message    | string   | yes      | Message to display.                                   |
        | starts_at  | datetime | no       | Starting time (defaults to current time).             |
        | ends_at    | datetime | no       | Ending time (defaults to one hour from current time). |
        | color      | string   | no       | Background color hex code.                            |
        | font       | string   | no       | Foreground color hex code.                            |
      DOC
    end

    doc 'Create' do
      example <<~DOC
        client.broadcast_messages.create(message: "Alert!")
      DOC
      result <<~DOC
        => #<BroadcastMessage id: 3, Active>
      DOC
    end

    doc 'Create' do
      title 'DateTime Helpers'
      markdown 'Both `:starts_at` and `:ends_at` can be used with Date or Time objects.   <small>_responds_to: `to_time`_</small>'

      example <<~DOC
        result = client.broadcast_messages.create(message: "Alert!", starts_at: Date.today, ends_at: 15.days.from_now)
      DOC
      result <<~DOC
        => #<BroadcastMessage id: 4, Active>
        result.ends_at
        => Thu, 09 Jan 2020 16:44:27 -0700
      DOC
    end

    doc 'Create' do
      title 'All options'

      example <<~DOC
        client.broadcast_messages.create(
          message: "Scheduled Maintenance!",
          starts_at: 2.days.from_now,
          ends_at: 2.month.from_now,
          color: "#ffffff",
          font: "#34495E"
        )
      DOC
      result <<~DOC
        => #<BroadcastMessage id: 6, Inactive>
      DOC
    end

    # Create
    def create(query = {})
      query[:starts_at] = query[:starts_at].to_time.iso8601 if format_time?(query[:starts_at])
      query[:ends_at] = query[:ends_at].to_time.iso8601 if format_time?(query[:ends_at])

      client.request(:post, 'broadcast_messages', BroadcastMessage, query)
    end
  end
end
