# Top namespace
module LabClient
  # Specifics
  class FeatureFlags < Common
    doc 'Create' do
      title 'Set or create a feature'
      example 'client.feature_flags.create(:create_eks_clusters, value: true)'
      result <<~DOC
        => #<FeatureFlag name: create_eks_clusters, state: on
      DOC
    end

    doc 'Attributes' do
      desc 'Register a new Application'
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | name | string | yes | Name of the feature to create or update |
        | value | integer/string | yes | `true` or `false` to enable/disable, or an integer for percentage of time |
        | feature_group | string | no | A Feature group name |
        | user | string | no | A GitLab username |
        | group | string | no | A GitLab group's path, for example gitlab-org |
        | project | string | no | A projects path, for example gitlab-org/gitlab-foss |

         Note that you can enable or disable a feature for a `feature_group`, a `user`,
         a `group`, and a `project` in a single API call.
      DOC
    end

    doc 'Create' do
      example <<~DOC
        client.applications.create(
          name: "Grafana",
          redirect_uri: "https://gitlab.com/-/grafana/login/gitlab",
          scopes: "email"
        )
      DOC
      result <<~DOC
        #<Application id: 11, Grafana>
      DOC
    end

    # Create
    def create(name, query = {})
      client.request(:post, "features/#{name}", FeatureFlag, query)
    end
  end
end
