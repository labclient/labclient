# Top namespace
module LabClient
  # Specifics
  class FeatureFlags < Common
    doc 'Delete' do
      title 'Delete a feature'
      example 'client.feature_flags.delete(:name)'
    end

    doc 'Delete' do
      desc 'Delete via FeatureFlag'
      example <<~DOC
        feature = client.feature_flags.list.first
        feature.delete
      DOC
    end

    # Delete
    def delete(name)
      client.request(:delete, "features/#{name}")
    end
  end
end
