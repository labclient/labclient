# Top namespace
module LabClient
  # Specifics
  class FeatureFlags < Common
    doc 'List' do
      title 'List all features'
      desc 'Get a list of all persisted features, with its gate values.'
      example 'client.feature_flags.list'
      result <<~DOC
        =>  [#<Feature name: create_eks_clusters, state: on>]
      DOC
    end

    doc 'List' do
      title 'View detail of features'
      desc 'Retrieve detailed information of feature flags'
      example 'client.feature_flags.list.each(&:verbose)'
      result <<~DOC
        LabClient::Feature {
                            :name => "create_eks_clusters",
                            :state => "on",
                            :gates => [
                              [0] LabStruct {
                                    :key => "boolean",
                                    :value => true
                              }
                            ]
        }
        => [#<Feature name: create_eks_clusters, state: on>]
      DOC
    end

    doc 'List' do
      title 'Toggle all features on or off'
      example <<~DOC
        client.feature_flags.list.each(&:toggle)
      DOC
      result <<~DOC
        [#<Feature name: create_eks_clusters, state: off>,
         #<Feature name: merge_train1, state: off>,
         #<Feature name: merge_train2, state: off>]
      DOC
    end

    def list
      client.request(:get, 'features', FeatureFlag)
    end
  end
end
