# Top namespace
module LabClient
  # Inspect Helper
  class FeatureFlag < Klass
    include ClassHelpers
    def inspect
      "#<FeatureFlag name: #{name}, state: #{state}>"
    end

    def delete
      client.feature_flags.delete name
    end

    def update(query)
      update_self client.feature_flags.create(name, query)
    end

    def toggle
      if state == 'off'
        update(value: true)
      else
        update(value: false)
      end
    end

    date_time_attrs %i[created_at]

    help do
      subtitle 'Feature Flag'
      option 'delete', 'Delete this specific feature flag'
      option 'toggle', 'Disable / Turn on feature'
      option 'update', 'Create flag / Update with values [Hash]'
    end
  end
end
