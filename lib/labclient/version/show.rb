# Top namespace
module LabClient
  # Specifics
  class Version < Common
    doc 'Version' do
      desc 'Retrieve version information for this GitLab instance. Responds 200 OK for authenticated users.'
      example 'client.version.show'
      result <<~DOC
        => {:version=>"12.6.4-ee", :revision=>"cc6b787e7b0"}
      DOC
    end

    # Show
    def show
      client.request(:get, 'version')&.to_h
    end
  end
end
