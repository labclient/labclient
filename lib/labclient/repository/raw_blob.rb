# Top namespace
module LabClient
  # Specifics
  class Repositories < Common
    @group_name = 'Repository'
    doc 'Blob' do
      title 'Raw'
      desc 'Allows you to receive information about blob in repository like size and content [Project ID, Sha]'
      example 'client.repository.blob(264, "a3c86df3f")'
      result '{:size=>6, :encoding=>"base64", :content=>"123", :sha=>"a3c86df3f"}'
    end

    doc 'Blob' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.blob('a3c86df3f')
      DOC
    end

    def blob(project_id, sha)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/repository/blobs/#{sha}", nil)
    end
  end
end
