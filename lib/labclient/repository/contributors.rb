# Top namespace
module LabClient
  # Specifics
  class Repositories < Common
    @group_name = 'Repository'
    doc 'Contributors' do
      desc 'Get repository contributors list [Project ID, Hash]'
      example 'client.repository.contributors(264)'
      result <<~DOC
        [{
          "name": "Example User",
          "email": "example@labclient",
          "commits": 117,
          "additions": 2097,
          "deletions": 517
        }, {
          "name": "Sample User",
          "email": "sample@labclient",
          "commits": 33,
          "additions": 338,
          "deletions": 244
        }]
      DOC

      markdown <<~DOC
        **Parameters:**

        * order_by (optional) - Return contributors ordered by name, email, or commits (orders by commit date) fields. Default is commits
        * sort (optional) - Return contributors sorted in asc or desc order. Default is asc
      DOC
    end

    doc 'Contributors' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.contributors
      DOC
    end

    def contributors(project_id, query = {})
      client.request(:get, "projects/#{project_id}/repository/contributors", nil, query)
    end
  end
end
