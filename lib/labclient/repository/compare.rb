# Top namespace
module LabClient
  # Specifics
  class Repositories < Common
    @group_name = 'Repository'
    doc 'Compare' do
      desc 'Compare branches, tags or commits [Project ID, Hash]'
      example 'client.repository.archive(264)'

      markdown <<~DOC
        **Parameters:**

        * from (required) - the commit SHA or branch name
        * to (required) - the commit SHA or branch name
        * straight (optional) - comparison method, true for direct comparison between from and to (from..to), false to compare using merge base (from…to)’. Default is false.
      DOC
    end

    doc 'Compare' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.compare(from: :master, to: :feature)
      DOC
    end

    def compare(project_id, query)
      client.request(:get, "projects/#{project_id}/repository/compare", CommitDiff, query)
    end
  end
end
