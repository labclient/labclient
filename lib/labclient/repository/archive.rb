# Top namespace
module LabClient
  # Specifics
  class Repositories < Common
    @group_name = 'Repository'
    doc 'Archive' do
      desc 'Get an archive of the repository [Project ID, File Path, Format]'
      example 'client.repository.archive(264)'

      markdown <<~DOC
        File Path defaults to local directory '\#{project_id}\#{format}'

        format is an optional suffix for the archive format. Default is tar.gz. Options are tar.gz, tar.bz2, tbz, tbz2, tb2, bz2, tar, and zip. For example, specifying archive.zip would send an archive in ZIP format.
      DOC
    end

    doc 'Archive' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.download_archive
      DOC
    end

    def archive(project_id, file_path = nil, format = '.tar.gz')
      project_id = format_id(project_id)
      file_path ||= "#{Dir.pwd}/#{project_id}#{format}"

      output = client.request(:get, "projects/#{project_id}/repository/archive#{format}", nil)

      File.write(file_path, output)
    end
  end
end
