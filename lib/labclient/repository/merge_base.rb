# Top namespace
module LabClient
  # Specifics
  class Repositories < Common
    @group_name = 'Repository'
    doc 'Merge Base' do
      desc 'Get the common ancestor for 2 or more refs [Project ID, Array(Refs)]'
      example 'client.repository.merge_base(264, [:master, :feature])'
      result <<~DOC
        [{
          "name": "Example User",
          "email": "example@labclient",
          "commits": 117,
          "additions": 2097,
          "deletions": 517
        }, {
          "name": "Sample User",
          "email": "sample@labclient",
          "commits": 33,
          "additions": 338,
          "deletions": 244
        }]
      DOC
    end

    doc 'Merge Base' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.merge_base(:master, :feature)
      DOC
    end

    def merge_base(project_id, refs)
      client.request(:get, "projects/#{project_id}/repository/merge_base", nil, refs: refs)
    end
  end
end
