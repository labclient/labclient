# Top namespace
module LabClient
  # Specifics
  class Repositories < Common
    @group_name = 'Repository'
    doc '' do
      markdown <<~DOC
        ##### Aliases

        `repository` and `repo` are aliases and be inter-exchanged
      DOC
    end
  end
end
