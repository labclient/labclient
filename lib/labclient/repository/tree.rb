# Top namespace
module LabClient
  # Specifics
  class Repositories < Common
    @group_name = 'Repository'
    doc 'Tree' do
      desc 'GetGet a list of repository files and directories in a project [Project ID, Hash]'
      example 'client.repository.tree(264)'
      result '[{:id=>"a3c86dff3a547733fd3d91432785386211481842", :name=>"README", :type=>"blob", :path=>"README", :mode=>"100644"}]'

      markdown <<~DOC
        **Parameters**:

        * path (optional) - The path inside repository. Used to get content of subdirectories
        * ref (optional) - The name of a repository branch or tag or if not given the default branch
        * recursive (optional) - Boolean value used to get a recursive tree (false by default)

      DOC
    end

    doc 'Tree' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.tree(ref: :master)
      DOC
    end

    def tree(project_id, query = {})
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/repository/tree", RepositoryTree, query)
    end
  end
end
