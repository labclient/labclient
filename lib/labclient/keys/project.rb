# Top namespace
module LabClient
  # Specifics
  class Events < Common
    doc 'Project' do
      desc 'Get a list of visible events for a particular project. [ project id ]'
      example 'client.events.project(1)'
      result <<~DOC
        => [#<Event type: imported>, #<Event type: created>, #<Event type: pushed to>, ...]
      DOC
    end

    doc 'Project' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | action | string | no | Include only events of a particular action type |
        | target_type | string | no | Include only events of a particular target type] |
        | before | date | no |  Include only events created before a particular date. |
        | after | date | no |  Include only events created after a particular date. |
        | scope | string | no | Include all events across a user's projects. |
        | sort | string | no | Sort events in asc or desc order by created_at. Default is desc |



        **Available parameters for the `target_type` and `action`**:

        | Type        | Parameters |
        | ----------- | ---------- |
        | action      | issue, merge_request, milestone, note, project, snippet, user |
        | target_type | closed, commented, created, destroyed, expired, joined, left, merged, pushed, reopened, updated |


      DOC
    end

    doc 'Project' do
      desc 'Filter by Action'
      example 'client.events.project(1, action: :created)'
    end

    doc 'Project' do
      desc 'Filter by Target Type'
      example 'client.events.project(1, target_type: :issue)'
    end

    doc 'Project' do
      markdown <<~DOC
        ##### DateTime Helpers

        Both :after and :before can be used with Date or Time objects. (responds_to: to_time)
      DOC

      example <<~DOC
        client.events.project(1, after: 2.day.ago)
      DOC
    end

    def project(project_id, query = {})
      project_id = format_id(project_id)

      query[:before] = query[:before].to_time.iso8601 if format_time?(query[:before])
      query[:after] = query[:after].to_time.iso8601 if format_time?(query[:after])

      client.request(:get, "projects/#{project_id}/events", Event, query)
    end
  end
end
