# Top namespace
module LabClient
  # Specifics
  class Keys < Common
    doc 'Show' do
      title 'Fingerprint'
      desc 'Get user by fingerprint of SSH key'
      example 'client.keys.fingerprint("05:50:87:08:...9:7d:37:30:fe")'
      result '# => #<Key id: 2, username: root>'

      markdown <<~DOC
        | Attribute    | Type  | Required  | Description                   |
        |--------------|-------|---------- |------------------------------|
        | fingerprint  | string | yes      | The fingerprint of an SSH key |


        Deploy keys are bound to the creating user, so if you query with a deploy key fingerprint you get additional information about the projects using that key.
      DOC
    end

    def fingerprint(key_id)
      query = { fingerprint: key_id }

      client.request(:get, 'keys', Key, query)
    end
  end
end
