# Top namespace
module LabClient
  # Specifics
  class Keys < Common
    doc 'Show' do
      desc 'Get SSH key with user by ID of an SSH key. Note only administrators can lookup SSH key with user by ID of an SSH key.'
      example 'client.keys.show(1)'
      result '=> #<Key id: 1, username: root>'
    end

    def show(key_id)
      client.request(:get, "keys/#{key_id}", Key)
    end
  end
end
