# Top namespace
module LabClient
  # Inspect Helper
  class Key < Klass
    include ClassHelpers

    def inspect
      "#<Key id: #{id}#{inspect_username}>"
    end

    def inspect_username
      ", username: #{@table[:user].username}" if has? :user
    end

    def user
      if has? :user
        LabClient::User.new(@table[:user], response, client)
      else
        user_id = collect_user_id
        client.users.show(user_id)
      end
    end

    date_time_attrs %i[created_at]
  end
end
