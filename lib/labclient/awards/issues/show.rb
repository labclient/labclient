# Top namespace
module LabClient
  # Specifics
  class IssueAwards < Common
    doc 'Issues' do
      title 'Show'
      desc 'Get a single award emoji [Project ID, Issue IID, Award ID]'
      example 'client.awards.issues.show(16, 1, 1)'
      result <<~DOC
        => #<Award id: 1 name: badminton>
      DOC
    end

    def show(project_id, issue_id, award_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)
      award_id = format_id(award_id)

      client.request(:get, "projects/#{project_id}/issues/#{issue_id}/award_emoji/#{award_id}", Award)
    end
  end
end
