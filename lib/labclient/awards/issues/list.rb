# Top namespace
module LabClient
  # Specifics
  class IssueAwards < Common
    @group_name = 'Awards'
    doc 'Issues' do
      title 'List'
      desc 'Get a list of all award emoji for a specified awardable. [Project ID, Issue IID]'
      example 'client.awards.issues.list(16, 1)'
      result <<~DOC
        => [#<Award id: 2dc2a8d>, .. ]
      DOC
    end

    def list(project_id, issue_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      client.request(:get, "projects/#{project_id}/issues/#{issue_id}/award_emoji", Award)
    end
  end
end
