# Top namespace
module LabClient
  # Specifics
  class IssueAwards < Common
    doc 'Issues' do
      title 'Delete'
      desc 'Sometimes it’s just not meant to be and you’ll have to remove the award. [Project ID, Issue IID, Award ID]'
      example 'client.awards.issues.delete(16, 2, 2)'
    end

    # Delete
    def delete(project_id, issue_id, award_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)
      award_id = format_id(award_id)

      client.request(:delete, "projects/#{project_id}/issues/#{issue_id}/award_emoji/#{award_id}")
    end
  end
end
