# Top namespace
module LabClient
  # Specifics
  class IssueAwards < Common
    doc 'Issues' do
      title 'Create'
      desc 'Create an award emoji [Project, Issue IID, Name]'
      example 'client.awards.issues.create(16, 1, :blowfish)'
      result <<~DOC
        => #<Award id: 2 name: blowfish>
      DOC
    end

    def create(project_id, issue_id, name)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      query = { name: name }

      client.request(:post, "projects/#{project_id}/issues/#{issue_id}/award_emoji", Award, query)
    end
  end
end
