# Top namespace
module LabClient
  # Inspect Helper
  class IssueAwards < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Awards < Common
    def issues
      IssueAwards.new(client)
    end
  end
end
