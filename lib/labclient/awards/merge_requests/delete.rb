# Top namespace
module LabClient
  # Specifics
  class MergeRequestAwards < Common
    doc 'MergeRequests' do
      title 'Delete'
      desc 'Sometimes it’s just not meant to be and you’ll have to remove the award. [Project ID, MergeRequest IID, Award ID]'
      example 'client.awards.merge_requests.delete(16, 2, 2)'
    end

    # Delete
    def delete(project_id, merge_request_id, award_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)
      award_id = format_id(award_id)

      client.request(:delete, "projects/#{project_id}/merge_requests/#{merge_request_id}/award_emoji/#{award_id}")
    end
  end
end
