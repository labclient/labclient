# Top namespace
module LabClient
  # Specifics
  class MergeRequestAwards < Common
    doc 'MergeRequests' do
      title 'Show'
      desc 'Get a single award emoji [Project ID, MergeRequest IID, Award ID]'
      example 'client.awards.merge_requests.show(16, 1, 1)'
      result <<~DOC
        => #<Award id: 1 name: badminton>
      DOC
    end

    def show(project_id, merge_request_id, award_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)
      award_id = format_id(award_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}/award_emoji/#{award_id}", Award)
    end
  end
end
