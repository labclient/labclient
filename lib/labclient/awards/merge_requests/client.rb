# Top namespace
module LabClient
  # Inspect Helper
  class MergeRequestAwards < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Awards < Common
    def merge_requests
      MergeRequestAwards.new(client)
    end
  end
end
