# Top namespace
module LabClient
  # Specifics
  class MergeRequestAwards < Common
    @group_name = 'Awards'
    doc 'MergeRequests' do
      title 'List'
      desc 'Get a list of all award emoji for a specified awardable. [Project ID, MergeRequest IID]'
      example 'client.awards.merge_requests.list(16, 1)'
      result <<~DOC
        => [#<Award id: 2dc2a8d>, .. ]
      DOC
    end

    def list(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}/award_emoji", Award)
    end
  end
end
