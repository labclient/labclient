# Top namespace
module LabClient
  # Specifics
  class MergeRequestAwards < Common
    doc 'MergeRequests' do
      title 'Create'
      desc 'Create an award emoji [Project, MergeRequest IID, Name]'
      example 'client.awards.merge_requests.create(16, 1, :blowfish)'
      result <<~DOC
        => #<Award id: 2 name: blowfish>
      DOC
    end

    def create(project_id, merge_request_id, name)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      query = { name: name }

      client.request(:post, "projects/#{project_id}/merge_requests/#{merge_request_id}/award_emoji", Award, query)
    end
  end
end
