# Top namespace
module LabClient
  # Inspect Helper
  class Award < Klass
    include ClassHelpers

    def inspect
      "#<Award id: #{id} name: #{name}>"
    end

    # DateTime Fields
    date_time_attrs %i[created_at updated_at]

    # User Fields
    user_attrs %i[user]
  end
end
