# Top namespace
module LabClient
  # Specifics
  class SnippetAwards < Common
    doc 'Snippets' do
      title 'Create'
      desc 'Create an award emoji [Project, Snippet ID, Name]'
      example 'client.awards.snippets.create(16, 1, :blowfish)'
      result <<~DOC
        => #<Award id: 2 name: blowfish>
      DOC
    end

    def create(project_id, snippet_id, name)
      project_id = format_id(project_id)
      snippet_id = format_id(snippet_id)

      query = { name: name }

      client.request(:post, "projects/#{project_id}/snippets/#{snippet_id}/award_emoji", Award, query)
    end
  end
end
