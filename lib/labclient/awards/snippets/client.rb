# Top namespace
module LabClient
  # Inspect Helper
  class SnippetAwards < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Awards < Common
    def snippets
      SnippetAwards.new(client)
    end
  end
end
