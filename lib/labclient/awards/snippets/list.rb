# Top namespace
module LabClient
  # Specifics
  class SnippetAwards < Common
    @group_name = 'Awards'
    doc 'Snippets' do
      title 'List'
      desc 'Get a list of all award emoji for a specified awardable. [Project ID, Snippet ID]'
      example 'client.awards.snippets.list(16, 1)'
      result <<~DOC
        => [#<Award id: 2dc2a8d>, .. ]
      DOC
    end

    def list(project_id, snippet_id)
      project_id = format_id(project_id)
      snippet_id = format_id(snippet_id)

      client.request(:get, "projects/#{project_id}/snippets/#{snippet_id}/award_emoji", Award)
    end
  end
end
