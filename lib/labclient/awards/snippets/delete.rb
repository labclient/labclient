# Top namespace
module LabClient
  # Specifics
  class SnippetAwards < Common
    doc 'Snippets' do
      title 'Delete'
      desc 'Sometimes it’s just not meant to be and you’ll have to remove the award. [Project ID, Snippet ID, Award ID]'
      example 'client.awards.snippets.delete(16, 2, 2)'
    end

    # Delete
    def delete(project_id, snippet_id, award_id)
      project_id = format_id(project_id)
      snippet_id = format_id(snippet_id)
      award_id = format_id(award_id)

      client.request(:delete, "projects/#{project_id}/snippets/#{snippet_id}/award_emoji/#{award_id}")
    end
  end
end
