# Top namespace
module LabClient
  # Specifics
  class SnippetAwards < Common
    doc 'Snippets' do
      title 'Show'
      desc 'Get a single award emoji [Project ID, Snippet ID, Award ID]'
      example 'client.awards.snippets.show(16, 1, 1)'
      result <<~DOC
        => #<Award id: 1 name: badminton>
      DOC
    end

    def show(project_id, snippet_id, award_id)
      project_id = format_id(project_id)
      snippet_id = format_id(snippet_id)
      award_id = format_id(award_id)

      client.request(:get, "projects/#{project_id}/snippets/#{snippet_id}/award_emoji/#{award_id}", Award)
    end
  end
end
