# Top Namesapce
module LabClient
  # Shared Methods
  module Docs
    # include ActionView::Helpers

    @docs = {}

    def self.json
      @docs.to_json
    end

    def self.docs
      @docs
    end

    # -------------------------------------------------------------
    # Include Rails Sanitizer
    # def safe_list_sanitizer
    #   Rails::Html::SafeListSanitizer.new
    # end

    # def sanitize(html, options = {})
    #   safe_list_sanitizer.sanitize(html, options)&.html_safe
    # end
    # ---------------------------------

    # Klass Helper
    def help(&block)
      # require 'active_support/inflector'
      @group_name ||= group_name.pluralize
      doc 'Reference' do
        block.call
      end
    end

    # -------------------------------------------------------------
    # DSL Helpers
    def title(value)
      @result[:title] = value
    end

    def subtitle(value)
      @result[:subtitle] = value
    end

    def markdown(value)
      require 'kramdown' unless defined? Kramdown
      @result[:markdown] = Kramdown::Document.new(value).to_html
    end

    def desc(value)
      @result[:desc] = value
    end

    def demo(value)
      @result[:demo_url] = "https://asciinema.org/a/#{value}.js"
      @result[:demo] = "<script id='asciicast-#{value}' src='#{@result[:demo_url]}' data-autoplay='true' data-loop='true' async></script>"
    end

    def example(value)
      @result[:example] = value
    end

    def result(value)
      @result[:result] = value
    end

    def option(name, text)
      @result[:options] ||= []
      @result[:options].push(name: name, text: text)
    end

    # Allow for Custom Group Name Overrides
    # Ruby warning of uninitialized variables
    def group_name
      if defined? @group_name
        @group_name
      else
        name.split('::', 2).last.split(/(?=[A-Z])/).join(' ')
      end
    end

    # Helper to Make navigation rendered out once rather than evaluated on Ember
    def navigation(subgroup)
      # Group
      nav = @group.gsub(' ', '_')

      # Subgroup
      subnav = subgroup.gsub(' ', '_')

      # Title
      subnav += "_#{@result[:title].gsub(' ', '_')}" if @result.key? :title

      @result[:nav] = nav
      @result[:nav] += "-#{subnav}" if subnav
    end

    def doc(subgroup)
      @result = {}
      yield

      # Turn Class name into friendly name. Split multiple words
      @group = group_name

      # Docs Navigation Helper
      navigation(subgroup)

      # @result[:name] = name
      Docs.docs[@group] ||= {}
      Docs.docs[@group][subgroup] ||= []
      Docs.docs[@group][subgroup].push @result
    end
    # -------------------------------------------------------------
  end
end
