# Top Namespace
module LabClient
  # Helper to Unify Log Output
  module Logger
    def logger
      LabClient::Logger.logger
    end

    def self.logger_setup
      logger = Ougai::Logger.new($stdout)
      logger.formatter = Ougai::Formatters::LabClient.new

      logger
    end

    def self.logger
      @logger ||= logger_setup
    end
  end
end

# Log Formatter for Readable Inline (Timestamp)
# https://github.com/eropple/ougai-formatters-inline_readable/blob/master/lib/ougai/formatters/inline_readable.rb
module Ougai
  module Formatters
    # LabClient Specific
    class LabClient < Ougai::Formatters::Readable
      # For Amazing Print
      def ai_settings
        { ruby19_syntax: true, multiline: false }
      end

      def time_format
        '%Y-%m-%e %k:%M:%S %z'
      end

      def call(severity, time, _progname, data)
        msg = data.delete(:msg)
        @excluded_fields.each { |f| data.delete(f) }

        level = @plain ? severity : colored_level(severity)
        output = "[#{time.strftime(time_format)}] #{level}: #{msg}"

        output += " #{data.ai(ai_settings)}" unless data.empty?

        "#{output}\n"
      end
    end
  end
end
