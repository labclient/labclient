# Top namespace
module LabClient
  # Specifics
  class Pipelines < Common
    doc 'Show' do
      title 'Variables'
      desc 'Get variables of a pipeline. [Project ID, Pipeline ID]'
      example 'client.pipelines.variables(264,1)'
      result <<~DOC
        [
          {
            "key": "RUN_NIGHTLY_BUILD",
            "variable_type": "env_var",
            "value": "true"
          },
          {
            "key": "foo",
            "value": "bar"
          }
        ]
      DOC
    end

    doc 'Show' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.pipeline_variables(7)
      DOC
    end

    doc 'Show' do
      desc 'Via Pipeline'
      example <<~DOC
        pipeline = client.pipelines.show(264,7)
        pipeline.variables
      DOC
    end

    def variables(project_id, pipeline_id)
      project_id = format_id(project_id)
      pipeline_id = format_id(pipeline_id)

      client.request(:get, "projects/#{project_id}/pipelines/#{pipeline_id}/variables")
    end
  end
end
