# Top namespace
module LabClient
  # Specifics
  class Pipelines < Common
    doc 'Cancel' do
      desc 'Cancel jobs in a pipeline. [Project ID, Pipeline ID]'
      example 'client.pipelines.cancel(264, 7)'
      result '#<Pipeline id: 10, ref: master, status: canceled>'
    end

    doc 'Cancel' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(5)
        project.pipeline_cancel(7)
      DOC
    end

    doc 'Cancel' do
      desc 'Via Pipeline'
      example <<~DOC
        pipeline = client.pipelines.show(264,7)
        pipeline.cancel
      DOC
    end

    def cancel(project_id, pipeline_id)
      project_id = format_id(project_id)
      pipeline_id = format_id(pipeline_id)

      client.request(:post, "projects/#{project_id}/pipelines/#{pipeline_id}/cancel", Pipeline)
    end
  end
end
