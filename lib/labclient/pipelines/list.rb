# Top namespace
module LabClient
  # Specifics
  class Pipelines < Common
    doc 'List' do
      desc 'List project pipelines. [Project ID, Hash]'
      example 'client.pipelines.list(5)'
      result '[#<Pipeline id: 6, ref: master, status: success>, ... ]'

      markdown <<~DOC
        | Attribute | Type    | Required | Description         |
        |-----------|---------|----------|---------------------|
        | scope   | string  | no       | The scope of pipelines, one of: running, pending, finished, branches, tags |
        | status  | string  | no       | The status of pipelines, one of: running, pending, success, failed, canceled, skipped, created |
        | ref     | string  | no       | The ref of pipelines |
        | sha     | string  | no       | The SHA of pipelines |
        | yaml_errors| boolean  | no       | Returns pipelines with invalid configurations |
        | name| string  | no       | The name of the user who triggered pipelines |
        | username| string  | no       | The username of the user who triggered pipelines |
        | updated_after | datetime | no | Return pipelines updated after the specified date. Format: ISO 8601 YYYY-MM-DDTHH:MM:SSZ |
        | updated_before | datetime | no | Return pipelines updated before the specified date. Format: ISO 8601 YYYY-MM-DDTHH:MM:SSZ |
        | order_by| string  | no       | Order pipelines by id, status, ref, updated_at or user_id (default: id) |
        | sort    | string  | no       | Sort pipelines in asc or desc order (default: desc) |
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(5)
        project.pipelines
      DOC
    end

    doc 'List' do
      desc 'Filter by date'
      example 'client.pipelines.list(264, updated_before: 10.day.ago)'
    end

    def list(project_id, query = {})
      project_id = format_id(project_id)

      %i[updated_after updated_before].each do |field|
        query[field] = query[field].to_time.iso8601 if format_time?(query[field])
      end

      client.request(:get, "projects/#{project_id}/pipelines", Pipeline, query)
    end
  end
end
