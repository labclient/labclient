# Top namespace
module LabClient
  # Specifics
  class Pipelines < Common
    doc 'Delete' do
      desc 'Delete a pipeline. [Project ID, Pipeline ID]'
      example 'client.pipelines.delete(264, 7)'
    end

    doc 'Delete' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(5)
        project.pipeline_delete(7)
      DOC
    end

    doc 'Delete' do
      desc 'Via Pipeline'
      example <<~DOC
        pipeline = client.pipelines.show(264,7)
        pipeline.delete
      DOC
    end

    def delete(project_id, pipeline_id)
      project_id = format_id(project_id)
      pipeline_id = format_id(pipeline_id)

      client.request(:delete, "projects/#{project_id}/pipelines/#{pipeline_id}")
    end
  end
end
