# Top namespace
module LabClient
  # Specifics
  class Pipelines < Common
    doc 'Show' do
      desc 'Get a single pipeline. [Project ID, Pipeline ID]'
      example 'client.pipelines.show(264,6)'
      result '#<Pipeline id: 6, ref: master, status: success>'
    end

    doc 'Show' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(5)
        project.pipeline(6)
      DOC
    end

    def show(project_id, pipeline_id)
      project_id = format_id(project_id)
      pipeline_id = format_id(pipeline_id)

      client.request(:get, "projects/#{project_id}/pipelines/#{pipeline_id}", Pipeline)
    end
  end
end
