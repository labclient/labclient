# Top namespace
module LabClient
  # Specifics
  class Pipelines < Common
    doc 'Retry' do
      desc 'Retry jobs in a pipeline. [Project ID, Pipeline ID]'
      example 'client.pipelines.retry(264, 7)'
      result '#<Pipeline id: 7, ref: master, status: running>'
    end

    doc 'Retry' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(5)
        project.pipeline_retry(7)
      DOC
    end

    doc 'Retry' do
      desc 'Via Pipeline'
      example <<~DOC
        pipeline = client.pipelines.show(264,7)
        pipeline.retry
      DOC
    end

    def retry(project_id, pipeline_id)
      project_id = format_id(project_id)
      pipeline_id = format_id(pipeline_id)

      client.request(:post, "projects/#{project_id}/pipelines/#{pipeline_id}/retry", Pipeline)
    end
  end
end
