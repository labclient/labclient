# Top namespace
module LabClient
  # Specifics
  class Pipelines < Common
    doc 'Create' do
      desc 'Create a new pipeline. [Project ID]'
      example 'client.pipelines.create(264, ref: :master)'
      result '#<Pipeline id: 7, ref: master, status: success>'

      markdown <<~DOC
        | Attribute   | Type    | Required | Description         |
        | ref       | string  | yes      | Reference to commit |
        | variables | array   | no       | An array containing the variables available in the pipeline, matching the structure [{ 'key' => 'UPLOAD_TO_S3', 'variable_type' => 'file', 'value' => 'true' }] |
      DOC
    end

    doc 'Create' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(5)
        project.pipeline_create(ref: :master)
      DOC
    end

    def create(project_id, query)
      client.request(:post, "projects/#{project_id}/pipeline", Pipeline, query)
    end
  end
end
