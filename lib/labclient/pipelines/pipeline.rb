# Top namespace
module LabClient
  # Inspect Helper
  class Pipeline < Klass
    include ClassHelpers

    def inspect
      "#<Pipeline id: #{id}, ref: #{ref}, status: #{status}>"
    end

    def variables
      project_id = collect_project_id
      client.pipelines.variables(project_id, id)
    end

    def retry
      project_id = collect_project_id
      client.pipelines.retry(project_id, id)
    end

    def cancel
      project_id = collect_project_id
      update_self client.pipelines.cancel(project_id, id)
    end

    def delete
      project_id = collect_project_id
      client.pipelines.delete(project_id, id)
    end

    def jobs(scope = nil)
      project_id = collect_project_id
      client.jobs.pipeline(project_id, id, scope)
    end

    def reload
      project_id = collect_project_id
      update_self client.pipelines.show(project_id, id)
    end

    # The status of pipelines, one of: running, pending, success, failed, canceled, skipped, created, manual

    # Wait for Import / Set a Hard Limit
    def wait_for_status(total_time = 300, sleep_time = 15)
      # running
      # pending
      # success
      # failed
      # canceled
      # skipped
      # manual
      # created

      # Wait
      # [running created pending]

      # Success
      # [skipped manual canceled success]

      # Fail
      # [failed]

      Timeout.timeout(total_time) do
        loop do
          reload
          logger.info('Waiting for Pipeline', status: status) unless quiet?
          break if %w[skipped manual canceled success].include? status
          raise 'Pipeline failed' if status == 'failed'

          sleep sleep_time
        end
      end
    end

    # DateTime Fields
    date_time_attrs %i[created_at updated_at finished_at started_at]
    user_attrs %i[user]

    help do
      subtitle 'Pipeline'
      option 'variables', 'Show variables from this pipeline'
      option 'retry', 'Retry jobs in this pipeline'
      option 'cancel', 'Cancel jobs in this pipeline'
      option 'delete', 'Delete this pipeline'
      option 'reload', 'Reload this pipeline object (New API Call)'
      option 'wait_for_status', 'Looping, wait for merge request status [Timeout, Interval]'
    end
  end
end
