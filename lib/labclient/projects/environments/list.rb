# Top namespace
module LabClient
  # Specifics
  class ProjectEnvironments < Common
    doc 'List' do
      desc 'Get all environments for a given project. [Project ID, Search (Optional)]'
      example 'client.projects.environments.list(16)'

      result <<~DOC
        => [#<ProjectEnvironment id: 1, name: rawr>, #<ProjectEnvironment id: 2, name: rawred>, #<ProjectEnvironment id: 3, name: drawred>]
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.environments
      DOC
    end

    def list(project_id, search = nil)
      project_id = format_id(project_id)

      query = if search.nil?
                nil
              else
                { search: search }
              end

      client.request(:get, "projects/#{project_id}/environments", ProjectEnvironment, query)
    end
  end
end
