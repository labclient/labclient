# Top namespace
module LabClient
  # Specifics
  class ProjectEnvironments < Common
    doc 'Update' do
      desc 'Updates an existing environment’s name and/or external_url. [Project ID, Environment ID, Params]'
      example 'client.projects.environments.update(16, 3, name: "planet bob")'

      result '=>  #<ProjectEnvironment id: 3, name: planet bob external_url: https://labclient>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute     | Type    | Required | Description                           |
        | ------------- | ------- | -------- | ------------------------------------- |
        | name          | string  | no       | The name of the environment           |
        | external_url  | string  | no       | Place to link to for this environment |
      DOC
    end

    doc 'Update' do
      desc 'Via Project [Environment ID, Params]'
      example <<~DOC
        project = client.projects.show(16)
        environment = project.environment_update(4, name: "another")
      DOC
    end

    doc 'Update' do
      desc 'Via ProjectEnvironment [Params]'
      example <<~DOC
        environment = client.projects.environments.show(16, 4)
        environment.update(name: "switchy")
      DOC
    end

    def update(project_id, environment_id, query)
      project_id = format_id(project_id)
      environment_id = format_id(environment_id)

      client.request(:put, "projects/#{project_id}/environments/#{environment_id}", ProjectEnvironment, query)
    end
  end
end
