# Top namespace
module LabClient
  # Inspect Helper
  class ProjectEnvironments < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def environments
      ProjectEnvironments.new(client)
    end
  end
end
