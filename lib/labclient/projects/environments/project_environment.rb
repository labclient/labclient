# Top namespace
module LabClient
  # Inspect Helper
  class ProjectEnvironment < Klass
    include ClassHelpers
    def inspect
      "#<ProjectEnvironment id: #{id}, name: #{name} external_url: #{external_url}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show project_id
    end

    def update(query)
      project_id = collect_project_id

      update_self client.projects.environments.update(project_id, id, query)
    end

    def delete
      project_id = collect_project_id

      client.projects.environments.delete(project_id, id)
    end

    def show
      project_id = collect_project_id

      update_self client.projects.environments.show(project_id, id)
    end

    alias reload show

    def stop
      project_id = collect_project_id

      client.projects.environments.stop(project_id, id)
    end

    date_time_attrs %i[created_at]

    help do
      subtitle 'ProjectEnvironment'
      option 'project', 'Show Project'
      option 'show', 'Collect/reload environment'
      option 'reload', 'Collect/reload environment'
      option 'update', 'Update this environment [Hash]'
      option 'delete', 'Delete this environment'
      option 'stop', 'Stop this environment'
    end
  end
end
