# Top namespace
module LabClient
  # Specifics
  class ProjectEnvironments < Common
    doc 'Delete' do
      desc 'Delete an environment from a project. [Project ID, Environment ID]'
      example 'client.projects.environments.delete(16, 1)'
    end

    doc 'Delete' do
      desc 'Via Project [Environment ID]'
      example <<~DOC
        project = client.projects.show(100)
        project.environment_delete(3)
      DOC
    end

    doc 'Delete' do
      desc 'Via ProjectEnvironment'
      example <<~DOC
        environment = client.projects.environments.show(100, 4)
        environment.delete
      DOC
    end

    def delete(project_id, environment_id)
      project_id = format_id(project_id)
      environment_id = format_id(environment_id)

      client.request(:delete, "projects/#{project_id}/environments/#{environment_id}")
    end
  end
end
