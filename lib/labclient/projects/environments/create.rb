# Top namespace
module LabClient
  # Specifics
  class ProjectEnvironments < Common
    doc 'Create' do
      desc 'Creates a new environment with the given name and external_url. [Project ID, Hash]'
      example <<~DOC
        client.projects.environments.create(16, name: 'rawr', external_url:'https://labclient')
      DOC

      result '=>  #<ProjectEnvironment id: 2, name: rawr, url: http://labclient.com>'
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute     | Type    | Required | Description                           |
        | ------------- | ------- | -------- | ------------------------------------- |
        | name          | string  | yes      | The name of the environment           |
        | external_url  | string  | no       | Place to link to for this environment |
      DOC
    end

    doc 'Create' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(100)
        environment = project.environment_create(name: 'rawr', external_url:'https://labclient')
      DOC
    end

    def create(project_id, query)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/environments", ProjectEnvironment, query)
    end
  end
end
