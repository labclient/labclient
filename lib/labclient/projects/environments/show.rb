# Top namespace
module LabClient
  # Specifics
  class ProjectEnvironments < Common
    doc 'Show' do
      desc 'Get a specific environment [Project ID, Environment ID]'
      example 'client.projects.environments.show(16, 1)'

      result '=>  #<ProjectEnvironment id: 1, name: first>'
    end

    doc 'Show' do
      desc 'Via Project [Environment ID]'
      example <<~DOC
        project = client.projects.show(16)
        environment = project.environment_show(1)
      DOC
    end

    def show(project_id, environment_id)
      project_id = format_id(project_id)
      environment_id = format_id(environment_id)

      client.request(:get, "projects/#{project_id}/environments/#{environment_id}", ProjectEnvironment)
    end
  end
end
