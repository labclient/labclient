# Top namespace
module LabClient
  # Specifics
  class ProjectEnvironments < Common
    doc 'Stop' do
      desc 'Stop an environment. [Project ID, Environment ID]'
      example <<~DOC
        client.projects.environments.stop(16, 2)
      DOC
    end

    doc 'Stop' do
      desc 'Via ProjectEnvironment'
      example <<~DOC
        environment = client.projects.environments.show(16, 4)
        environment.stop
      DOC
    end

    def stop(project_id, environment_id)
      project_id = format_id(project_id)
      environment_id = format_id(environment_id)

      client.request(:post, "projects/#{project_id}/environments/#{environment_id}/stop", nil)
    end
  end
end
