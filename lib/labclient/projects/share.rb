# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Update' do
      title 'Share'
      desc 'Allow to share project with group. [Project ID, {group_id, :groups_access}]'
      example 'client.projects.share(25, group_id: 78, group_access: :developer)'
      result '{:id=>3, :project_id=>25, :group_id=>78, :group_access=>30, :expires_at=>nil}'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | id | integer/string | yes | The ID or URL-encoded path of the project|
        | group_id | integer | yes | The ID of the group to share with |
        | group_access | integer | yes | The [permissions level](members.md) to grant the group |
        | expires_at | string | no | Share expiration date in ISO 8601 format: 2016-09-26 |
      DOC
    end

    doc 'Update' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(25)
        project.share(78)
      DOC
    end

    doc 'Update' do
      desc 'Access helper and Time Helper'
      example 'client.projects.share(25, group_id: 5, group_access: :developer, expires_at: 1.day.from_now)'
    end

    doc 'Update' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(299)
        project.upload("/tmp/text.txt")
      DOC
    end

    def share(project_id, query = {})
      project_id = format_id(project_id)

      # Convert Access if needed
      query_access_level(query, :group_access)

      # Convert Time if needed
      query_format_time(query, :expires_at)

      client.request(:post, "projects/#{project_id}/share", nil, query)
    end
  end
end
