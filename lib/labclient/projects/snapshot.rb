# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Snapshot' do
      desc 'Download a snapshot of the project (or wiki, if requested) Git repository [Project ID, Local File Path, Wiki]'
      example 'client.projects.snapshot(310, "output.tar")'
    end

    doc 'Snapshot' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(25)
        project.snapshot('output.tar')
      DOC
    end

    def snapshot(project_id, file_name, wiki = false)
      project_id = format_id(project_id)

      raw = client.request(:get, "projects/#{project_id}/snapshot"), { wiki: wiki }
      File.write(file_name, raw)
    end
  end
end
