# rubocop:disable Style/FormatStringToken
# Top namespace
module LabClient
  # Specifics
  class ProjectBadges < Common
    doc 'Create' do
      desc 'Adds a badge to a group. [Project ID, Hash]'
      example <<~DOC
        client.projects.badges.create(
          100,
          name: 'rawr',
          link_url:'https://example.gitlab.com/%{project_path}',
          image_url:'https://example.gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg'
        )
      DOC

      result '=>  #<ProjectBadge id: 2, url: http://labclient.com>'
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | name | string | no | Name for Badge |
        | link_url | string | yes | URL of the badge link |
        | image_url | string | yes | URL of the badge image |
      DOC
    end

    doc 'Create' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(100)
        badge = project.badge_create(
          name: 'rawr',
          link_url:'https://example.gitlab.com/%{project_path}',
          image_url:'https://example.gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg'
        )
      DOC
    end

    def create(project_id, query)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/badges", ProjectBadge, query)
    end
  end
end
# rubocop:enable Style/FormatStringToken
