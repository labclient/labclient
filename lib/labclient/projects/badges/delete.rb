# Top namespace
module LabClient
  # Specifics
  class ProjectBadges < Common
    doc 'Delete' do
      desc 'Removes a badge from a project. [Project ID, Badge ID]'
      example 'client.projects.badges.delete(16, 1)'
    end

    doc 'Delete' do
      desc 'Via Project [Badge ID]'
      example <<~DOC
        project = client.projects.show(100)
        project.badge_delete(3)
      DOC
    end

    doc 'Delete' do
      desc 'Via ProjectBadge'
      example <<~DOC
        badge = client.projects.badges.show(100, 4)
        badge.delete
      DOC
    end

    def delete(project_id, badge_id)
      project_id = format_id(project_id)
      badge_id = format_id(badge_id)

      client.request(:delete, "projects/#{project_id}/badges/#{badge_id}")
    end
  end
end
