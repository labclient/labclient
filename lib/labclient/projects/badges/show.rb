# Top namespace
module LabClient
  # Specifics
  class ProjectBadges < Common
    doc 'Show' do
      desc 'Gets a badge of a project. [Project ID, Badge ID]'
      example 'client.projects.badges.show(100, 1)'

      result '=>  #<ProjectBadge id: 1, name: first>'
    end

    doc 'Show' do
      desc 'Via Project [Badge ID]'
      example <<~DOC
        project = client.projects.show(16)
        badge = project.badge_show(1)
      DOC
    end

    def show(project_id, badge_id)
      project_id = format_id(project_id)
      badge_id = format_id(badge_id)

      client.request(:get, "projects/#{project_id}/badges/#{badge_id}", ProjectBadge)
    end
  end
end
