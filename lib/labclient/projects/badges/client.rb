# Top namespace
module LabClient
  # Inspect Helper
  class ProjectBadges < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def badges
      ProjectBadges.new(client)
    end
  end
end
