# Top namespace
module LabClient
  # Specifics
  class ProjectBadges < Common
    doc 'List' do
      desc 'Gets a list of a project badges. [Project ID]'
      example 'client.projects.badges.list(16)'

      result <<~DOC
        => [#<ProjectBadge id: 2, name: first >, #<ProjectBadge id: 3, name: rawr>]
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.badges
      DOC
    end

    def list(project_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/badges", ProjectBadge)
    end
  end
end
