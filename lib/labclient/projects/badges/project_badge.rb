# Top namespace
module LabClient
  # Inspect Helper
  class ProjectBadge < Klass
    include ClassHelpers
    def inspect
      "#<ProjectBadge id: #{id}, name: #{name}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show project_id
    end

    def update(query)
      project_id = collect_project_id

      update_self client.projects.badges.update(project_id, id, query)
    end

    def delete
      project_id = collect_project_id

      client.projects.badges.delete(project_id, id)
    end

    def preview
      project_id = collect_project_id

      client.projects.badges.preview(project_id, {
                                       link_url: link_url,
                                       image_url: image_url
                                     })
    end

    date_time_attrs %i[created_at]

    help do
      subtitle 'ProjectBadge'
      option 'project', 'Show Project'
      option 'update', 'Update this badge [Hash]'
      option 'delete', 'Delete this badge'
      option 'preview', 'Show render of interpolation'
    end
  end
end
