# Top namespace
module LabClient
  # Specifics
  class ProjectBadges < Common
    doc 'Update' do
      desc 'Updates a badge of a group. [Project ID, Badge ID, Params]'
      example 'client.projects.badges.update(100, 3, name: "planet bob")'

      result '=>  #<ProjectBadge id: 3, name: planet bob>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | name | string | no | Name for Badge |
        | link_url | string | no | URL of the badge link |
        | image_url | string | no | URL of the badge image |
      DOC
    end

    doc 'Update' do
      desc 'Via Project [Badge ID, Params]'
      example <<~DOC
        project = client.projects.show(16)
        badge = project.badge_update(4, name: "another")
      DOC
    end

    doc 'Update' do
      desc 'Via ProjectBadge [Params]'
      example <<~DOC
        badge = client.projects.badges.show(100, 4)
        badge.update(name: "switchy")
      DOC
    end

    def update(project_id, badge_id, query)
      project_id = format_id(project_id)
      badge_id = format_id(badge_id)

      client.request(:put, "projects/#{project_id}/badges/#{badge_id}", ProjectBadge, query)
    end
  end
end
