# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Update' do
      title 'Transfer'
      desc 'Transfer a project to a new namespace [Project ID ,Namespace Name/ID]'
      example 'client.projects.transfer(25, "root")'
      result '=> #<Project id: 25 badlands/chaurushunter>'
    end

    doc 'Update' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(25)
        project.transfer('root')
      DOC
    end

    def transfer(project_id, namespace_id)
      project_id = format_id(project_id)
      namespace_id = format_id(namespace_id)

      client.request(:put, "projects/#{project_id}/transfer", Project, { namespace: namespace_id })
    end
  end
end
