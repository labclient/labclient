# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Github Import' do
      desc 'Import your projects from GitHub to GitLab via the API. [Hash]'
      example 'client.projects.github_import(personal_access_token: "123123", repo_id: 2)'

      markdown <<~DOC
        | Attribute             | Type    | Required | Description                         |
        |---------------------- |---------|----------|------------------------------------ |
        | personal_access_token | string  | yes      | GitHub personal access token        |
        | repo_id               | integer | yes      | GitHub repository ID                |
        | new_name              | string  | no       | New repository name                 |
        | target_namespace      | string  | yes      | Namespace to import repository into |
      DOC
    end

    def github_import(query)
      client.request(:post, 'import/github', nil, query)
    end
  end
end
