# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Update' do
      title 'Housekeeping'
      desc 'Start the Housekeeping task for a Project [Project ID]'
      example 'client.projects.housekeeping(25)'
      result <<~DOC
        # Sidekiq Job ID
        => "961b99cf00910af6d2e6dbdc"
      DOC
    end

    doc 'Update' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(25)
        project.housekeeping
      DOC
    end

    def housekeeping(project_id)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/housekeeping", nil)
    end
  end
end
