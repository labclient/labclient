# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Update' do
      title 'Archive'
      example 'client.projects.archive(25)'
      result '#<Project id: 25 badlands/chaurushunter>'
    end

    doc 'Update' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(299)
        project.archive
      DOC
    end

    def archive(project_id)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/archive", Project)
    end
  end
end
