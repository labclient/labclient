# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Show' do
      desc 'Get a specific project..'
      example 'client.projects.show(16)'
      markdown <<~DOC

        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | id | integer/string | yes | The ID or URL-encoded path of the project |
        | statistics | boolean | no | Include project statistics |
        | license | boolean | no | Include project license data |
        | with_custom_attributes | boolean | no | Include custom attributes in response (admins only) |

      DOC

      result <<~DOC
        project = client.projects.show(16)

        => <Project id: 16 dimensionc137/cleanse>
        project.web_url
        => "https://labclient/dimensionc137/cleanse"
        project.keys
        => [:_links,
          :approvals_before_merge,
          :archived,
          :auto_cancel_pending_pipelines,
          :auto_devops_deploy_strategy,
          :auto_devops_enabled,
          :avatar_url,
          :build_coverage_regex,
          :external_authorization_classification_label,
          ...
        ]

      DOC
    end

    def show(project_id, query = {})
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}", Project, query)
    end
  end
end
