# Top namespace
module LabClient
  # Specifics
  class ProjectSnippets < Common
    doc 'Create' do
      desc 'Creates a new project snippet. [Project ID, Hash]'

      example <<~DOC
        client.projects.snippets.create(16,
          title: "Sweet Code",
          file_name: 'script.rb',
          content: 'puts "All your snippet are belong to us!"',
          description: "The one snippet",
          visibility: :private
        )
      DOC

      result <<~DOC
        => #<ProjectSnippet id: 1, title: Sweet Code>
      DOC
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute    | Type   | Required | Description                                        |
        |--------------|------- |--------- |--------------------------------------------------- |
        | title        | string | yes      | Title of a snippet.                                |
        | file_name    | string | yes      | Name of a snippet file.                            |
        | content      | string | yes      | Content of a snippet.                              |
        | description  | string | no       | Description of a snippet.                          |
        | visibility   | string | no       | Snippet's [visibility](#snippet-visibility-level). |
      DOC
    end

    def create(project_id, query = {})
      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/snippets", ProjectSnippet, query)
    end
  end
end
