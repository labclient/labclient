# Top namespace
module LabClient
  # Inspect Helper
  class ProjectSnippet < Klass
    include ClassHelpers

    def inspect
      "#<ProjectSnippet id: #{id}, title: #{title}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show project_id
    end

    def update(query = {})
      project_id = collect_project_id
      update_self client.projects.snippets.update(project_id, id, query)
    end

    def raw
      project_id = collect_project_id
      client.projects.snippets.raw(project_id, id)
    end

    alias content raw

    def agent_detail
      project_id = collect_project_id
      client.projects.snippets.agent_detail(project_id, id)
    end

    def delete
      project_id = collect_project_id
      client.projects.snippets.delete(project_id, id)
    end

    def notes
      client.notes.snippets.list(project_id, id)
    end

    def note_create(query)
      client.notes.snippets.create(project_id, id, query)
    end

    date_time_attrs %i[updated_at created_at]

    user_attrs %i[author]

    help do
      subtitle 'Project Snippet'
      option 'raw', 'Retrieves the raw snippet content.'
      option 'agent_detail', 'Hash of user agent details.'
      option 'delete', 'Delete this deploy key.'
      option 'update', 'Update this this deploy key.'

      # Notes
      option 'notes', 'List notes/comments. [Hash]'
      option 'note_create', 'Creates a new note. [Hash]'
    end
  end
end
