# Top namespace
module LabClient
  # Specifics
  class ProjectSnippets < Common
    doc 'Update' do
      desc 'Updates an existing project snippet [Project ID, Snippet ID, Hash]'

      example <<~DOC
        client.projects.snippets.update(16, 4, title: 'One Snippet', description: "One Snippet to rule them all")
      DOC

      result <<~DOC
        => #<ProjectSnippet id: 1, title: One Snippet>
      DOC
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute    | Type   | Required | Description                                        |
        |--------------|------- |--------- |--------------------------------------------------- |
        | title        | string | no       | Title of a snippet.                                |
        | file_name    | string | no       | Name of a snippet file.                            |
        | content      | string | no       | Content of a snippet.                              |
        | description  | string | no       | Description of a snippet.                          |
        | visibility   | string | no       | Snippet's [visibility](#snippet-visibility-level). |
      DOC
    end

    doc 'Update' do
      desc 'Via Project Snippet'
      example <<~DOC
        snippet.update(title: 'One Snippet to find them!')
      DOC
    end

    def update(project_id, snippet_id, query = {})
      snippet_id = format_id(snippet_id)
      project_id = format_id(project_id)
      client.request(:put, "projects/#{project_id}/snippets/#{snippet_id}", ProjectSnippet, query)
    end
  end
end
