# Top namespace
module LabClient
  # Specifics
  class ProjectSnippets < Common
    doc 'Show' do
      title 'Content'
      desc 'Returns the raw project snippet as plain text.  [Project ID, Snippet ID]'
      example 'client.projects.snippets.raw(16, 2)'
      result <<~DOC
        => "Here's the special content"
      DOC
    end

    doc 'Show' do
      desc 'via the Project Snippet'
      example <<~DOC
        snippet = client.snippets.raw(2)
        snippet.raw
      DOC
    end

    # Show Specific
    def raw(project_id, snippet_id)
      snippet_id = format_id(snippet_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/snippets/#{snippet_id}/raw")
    end

    alias content raw
  end
end
