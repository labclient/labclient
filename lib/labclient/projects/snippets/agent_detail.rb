# Top namespace
module LabClient
  # Specifics
  class ProjectSnippets < Common
    doc 'Show' do
      title 'Agent Details'
      desc 'Get user agent details [Project ID, Snippet ID]'
      example 'client.projects.snippets.agent_detail(16, 2)'
      result <<~DOC
        => {:user_agent=>"Mozilla/5.0 (X11; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0", :ip_address=>"10.0.0.14", :akismet_submitted=>false}
      DOC
    end

    doc 'Show' do
      desc 'via ProjectSnippet'
      example <<~DOC
        snippet = client.projects.snippets.show(16,2)
        snippet.agent_detail
      DOC
    end

    # Show Specific
    def agent_detail(project_id, snippet_id)
      snippet_id = format_id(snippet_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/snippets/#{snippet_id}/user_agent_detail")
    end
  end
end
