# Top namespace
module LabClient
  # Inspect Helper
  class ProjectSnippets < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def snippets
      ProjectSnippets.new(client)
    end
  end
end
