# Top namespace
module LabClient
  # Specifics
  class ProjectSnippets < Common
    doc 'List' do
      desc 'Get a list of project snippets. [Project ID]'
      example 'client.projects.snippets.list(16)'
      result <<~DOC
        => [#<ProjectSnippet id: 1, title: Sweet Code>, ...]
      DOC
    end

    # List
    def list(project_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/snippets", ProjectSnippet)
    end
  end
end
