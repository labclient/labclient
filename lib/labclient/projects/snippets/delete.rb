# Top namespace
module LabClient
  # Specifics
  class ProjectSnippets < Common
    doc 'Delete' do
      desc 'Deletes an existing project snippet. [Project ID, Snippet ID]'
      example <<~DOC
        client.projects.snippets.delete(16,4)
      DOC
    end

    doc 'Delete' do
      desc 'Delete through Snippet object'
      example <<~DOC
        snippet = client.projects.snippets.show(16,1)
        snippet.delete
      DOC
    end

    def delete(project_id, snippet_id)
      snippet_id = format_id(snippet_id)
      project_id = format_id(project_id)
      client.request(:delete, "projects/#{project_id}/snippets/#{snippet_id}")
    end
  end
end
