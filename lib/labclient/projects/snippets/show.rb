# Top namespace
module LabClient
  # Specifics
  class ProjectSnippets < Common
    doc 'Show' do
      desc 'Get a single project snippet. [Project ID, Snippet ID]'
      example 'client.projects.snippets.show(16, 1)'
      result <<~DOC
        => #<ProjectSnippet id: 1, title: Sweet Code>
      DOC
    end

    # Show Specific
    def show(project_id, snippet_id)
      snippet_id = format_id(snippet_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/snippets/#{snippet_id}", ProjectSnippet)
    end
  end
end
