# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'List' do
      desc 'Get a list of all visible projects across GitLab for the authenticated user. '
      example 'client.projects.list'
      result <<~DOC
        => [
         #<Project id: 318, "root/middleman">,
         #<Project id: 317, "skyrim/dwarvencenturion">,
         #<Project id: 303, "pawnshopplanet/pibbles">,
        ]
      DOC
    end

    doc 'List' do
      markdown <<~DOC
        | Attribute | Type | Description |
        | --------- | ---- | ----------- |
        | archived                    | boolean  | Limit by archived status |
        | visibility                  | string   | Limit by visibility public, internal, or private |
        | order_by                    | string   | Return projects ordered by id, name, path, created_at, updated_at, or last_activity_at fields. Default is created_at |
        | sort                        | string   | Return projects sorted in asc or desc order. Default is desc |
        | search                      | string   | Return list of projects matching the search criteria |
        | simple                      | boolean  | Return only limited fields for each project. This is a no-op without authentication as then _only_ simple fields are returned. |
        | owned                       | boolean  | Limit by projects explicitly owned by the current user |
        | membership                  | boolean  | Limit by projects that the current user is a member of |
        | starred                     | boolean  | Limit by projects starred by the current user |
        | statistics                  | boolean  | Include project statistics |
        | with_custom_attributes      | boolean  | Include [custom attributes](custom_attributes.md) in response (admins only) |
        | with_issues_enabled         | boolean  | Limit by enabled issues feature |
        | with_merge_requests_enabled | boolean  | Limit by enabled merge requests feature |
        | with_programming_language   | string   | Limit by projects which use the given programming language |
        | wiki_checksum_failed        | boolean  | **(PREMIUM)** Limit projects where the wiki checksum calculation has failed |
        | repository_checksum_failed  | boolean  | **(PREMIUM)** Limit projects where the repository checksum calculation has failed|
        | min_access_level            | integer  | Limit by current user minimal [access level](members.md) |
        | id_after                    | integer  | Limit results to projects with IDs greater than the specified ID |
        | id_before                   | integer  | Limit results to projects with IDs less than the specified ID |
      DOC
    end

    doc 'List' do
      title 'Examples'
      desc 'Visibility'
      example 'client.projects.list(visibility: :public)'
      result <<~DOC
        => [#<Project id: 318, "root/middleman">]
      DOC
    end

    doc 'List' do
      desc 'Search'
      example 'client.projects.list(search: "heal")'
      result <<~DOC
        => [#<Project id: 308, "badlands/heal">]
      DOC
    end

    doc 'List' do
      desc 'Access Level (by name)'
      example 'client.projects.list(min_access_level: :developer)'
      result <<~DOC
        => [
          #<Project id: 315 skyrim/bandit>,
          #<Project id: 305 pawnshopplanet/molecularmanipulation>,
          #<Project id: 302 bandlecity/thunderlordsdecree>
        ]
      DOC
    end

    doc 'List' do
      desc 'Access Level (by value)'
      example 'client.projects.list(min_access_level: 30)'
      result <<~DOC
        => [
          #<Project id: 315 skyrim/bandit>,
          #<Project id: 305 pawnshopplanet/molecularmanipulation>,
          #<Project id: 302 bandlecity/thunderlordsdecree>
        ]
      DOC
    end

    # List/Search users
    def list(query = {})
      query_access_level(query, :min_access_level)
      client.request(:get, 'projects', Project, query)
    end
  end
end
