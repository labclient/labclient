# Top namespace
module LabClient
  # Specifics
  class ProjectReleaseLinks < Common
    doc 'Delete' do
      desc 'Delete an asset as a link from a Release. [Project ID, Tag Name, Link ID]'
      example 'client.projects.release_links.delete(16, :release, 2)'
    end

    doc 'Delete' do
      desc 'Via Project [Tag Name, Link ID]'
      example <<~DOC
        project = client.projects.show(16)
        project.release_link_delete(:release_link)
      DOC
    end

    doc 'Delete' do
      desc 'Via ProjectReleaseLink'
      example <<~DOC
        release_link = client.projects.release_links.show(16, :release, 2)
        release_link.delete
      DOC
    end

    def delete(project_id, release_id, link_id)
      project_id = format_id(project_id)
      release_id = format_id(release_id)
      link_id = format_id(link_id)

      client.request(:delete, "projects/#{project_id}/releases/#{release_id}/assets/links/#{link_id}", ProjectReleaseLink)
    end
  end
end
