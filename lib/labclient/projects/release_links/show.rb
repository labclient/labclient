# Top namespace
module LabClient
  # Specifics
  class ProjectReleaseLinks < Common
    doc 'Show' do
      desc 'Get an asset as a link from a Release. [Project ID, Tag Name, Link ID]'
      example 'client.projects.release_links.show(16, :release, 1)'

      result '=> #<ProjectReleaseLink id: 1, name: Download>'
    end

    doc 'Show' do
      desc 'Via Project [Tag Name, Link ID]'
      example <<~DOC
        project = client.projects.show(16)
        release_link = project.release_link_show(:release, 1)
      DOC
    end

    def show(project_id, release_id, link_id)
      project_id = format_id(project_id)
      release_id = format_id(release_id)
      link_id = format_id(link_id)

      client.request(:get, "projects/#{project_id}/releases/#{release_id}/assets/links/#{link_id}", ProjectReleaseLink)
    end
  end
end
