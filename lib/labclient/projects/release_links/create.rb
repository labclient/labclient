# Top namespace
module LabClient
  # Specifics
  class ProjectReleaseLinks < Common
    doc 'Create' do
      desc 'Create an asset as a link from a Release. [Project ID, Tag Name Hash]'
      example <<~DOC
        client.projects.release_links.create(16, :release, name: 'Download', url: 'https://labclient/download/file.tgz')
      DOC

      result '=> #<ProjectReleaseLink tag_name: release_link, name: release_link>'
    end

    doc 'Create' do
      markdown <<~DOC

        | Attribute     | Type           | Required | Description                             |
        | ------------- | -------------- | -------- | --------------------------------------- |
        | name          | string         | yes      | The name of the link.                   |
        | url           | string         | yes      | The URL of the link.                    |

      DOC
    end

    doc 'Create' do
      desc 'Via Project [Hash]'
      example <<~DOC
        project = client.projects.show(16)
        release_link = project.release_link_create(:release, name: 'Another', url: 'https://labclient/download/thing.tgz')
      DOC
    end

    def create(project_id, release_id, query)
      project_id = format_id(project_id)
      release_id = format_id(release_id)

      client.request(:post, "projects/#{project_id}/releases/#{release_id}/assets/links", ProjectReleaseLink, query)
    end
  end
end
