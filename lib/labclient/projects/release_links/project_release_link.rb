# Top namespace
module LabClient
  # Inspect Helper
  class ProjectReleaseLink < Klass
    include ClassHelpers
    def inspect
      "#<ProjectReleaseLink id: #{id}, name: #{name}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show project_id
    end

    def update(query)
      project_id = collect_project_id
      release_id = collect_release_id

      update_self client.projects.release_links.update(project_id, release_id, id, query)
    end

    def delete
      project_id = collect_project_id
      release_id = collect_release_id

      client.projects.release_links.delete(project_id, release_id, id)
    end

    # User Fields
    user_attrs %i[author]

    date_time_attrs %i[created_at release_linkd_at]

    help do
      subtitle 'ProjectRelease'
      option 'project', 'Show Project'
      option 'update', 'Update this release link [Hash]'
      option 'delete', 'Delete this release link'
    end
  end
end
