# Top namespace
module LabClient
  # Specifics
  class ProjectReleaseLinks < Common
    doc 'List' do
      desc 'Get assets as links from a Release. [Project ID, Tag Name]'
      example 'client.projects.release_links.list(16, :release)'

      result <<~DOC
        => [#<ProjectReleaseLink id: 1, name: Download>, ...]
      DOC
    end

    doc 'List' do
      desc 'Via Project [Tag Name]'
      example <<~DOC
        project = client.projects.show(16)
        project.release_links(:release)
      DOC
    end

    doc 'List' do
      desc 'Via Release'
      example <<~DOC
        release = client.projects.releases.show(16, :release)
        release.links
      DOC
    end

    def list(project_id, release_id)
      project_id = format_id(project_id)
      release_id = format_id(release_id)

      client.request(:get, "projects/#{project_id}/releases/#{release_id}/assets/links", ProjectReleaseLink)
    end
  end
end
