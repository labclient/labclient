# Top namespace
module LabClient
  # Specifics
  class ProjectReleaseLinks < Common
    doc 'Update' do
      desc 'Update an asset as a link from a Release. [Project ID, Tag Name, Hash]'
      example 'client.projects.release_links.update(16, :release_link, name: "planet bob")'

      result '=> #<ProjectReleaseLink id: 1, name: switchy>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute     | Type           | Required | Description                             |
        | ------------- | -------------- | -------- | --------------------------------------- |
        | name          | string         | yes      | The name of the link.                   |
        | url           | string         | yes      | The URL of the link.                    |
      DOC
    end

    doc 'Update' do
      desc 'Via Project [Tag Name, Hash]'
      example <<~DOC
        project = client.projects.show(16)
        release_link = project.release_link_update(:release, 1, name: "another")
      DOC
    end

    doc 'Update' do
      desc 'Via ProjectReleaseLink [Params]'
      example <<~DOC
        release_link = client.projects.release_links.show(16, :release, 1)
        release_link.update(name: "switchy")
      DOC
    end

    def update(project_id, release_id, link_id, query)
      project_id = format_id(project_id)
      release_id = format_id(release_id)
      link_id = format_id(link_id)

      client.request(:put, "projects/#{project_id}/releases/#{release_id}/assets/links/#{link_id}", ProjectReleaseLink, query)
    end
  end
end
