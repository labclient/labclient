# Top namespace
module LabClient
  # Inspect Helper
  class ProjectReleases < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def releases
      ProjectReleases.new(client)
    end
  end
end
