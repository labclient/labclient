# Top namespace
module LabClient
  # Specifics
  class ProjectHooks < Common
    doc 'List' do
      desc 'Get a list of project hooks. [Project ID]'
      example 'client.projects.hooks.list(16)'

      result <<~DOC
        => [
          #<ProjectHook id: 1, url: https://labclient.url>,
          ...
        ]
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.hooks
      DOC
    end

    def list(project_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/hooks", ProjectHook)
    end
  end
end
