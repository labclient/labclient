# Top namespace
module LabClient
  # Specifics
  class ProjectHooks < Common
    doc 'Show' do
      desc 'Get a specific hook for a project. [Project ID, Hook ID]'
      example 'client.projects.hooks.show(16, 1)'

      result '=>  #<ProjectHook id: 1, url: https://labclient>'
    end

    doc 'Show' do
      desc 'Via Project [Hook ID]'
      example <<~DOC
        project = client.projects.show(16)
        hook = project.hook_show(1)
      DOC
    end

    def show(project_id, hook_id)
      project_id = format_id(project_id)
      hook_id = format_id(hook_id)

      client.request(:get, "projects/#{project_id}/hooks/#{hook_id}", ProjectHook)
    end
  end
end
