# Top namespace
module LabClient
  # Specifics
  class ProjectHooks < Common
    doc 'Create' do
      desc 'Adds a hook to a specified project. [Project ID, Hash]'
      example 'client.projects.hooks.create(16, url: "http://labclient.com")'

      result '=>  #<ProjectHook id: 2, url: http://labclient.com>'
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | url | string | yes | The hook URL |
        | push_events | boolean | no | Trigger hook on push events |
        | push_events_branch_filter | string | no | Trigger hook on push events for matching branches only |
        | issues_events | boolean | no | Trigger hook on issues events |
        | confidential_issues_events | boolean | no | Trigger hook on confidential issues events |
        | merge_requests_events | boolean | no | Trigger hook on merge requests events |
        | tag_push_events | boolean | no | Trigger hook on tag push events |
        | note_events | boolean | no | Trigger hook on note events |
        | job_events | boolean | no | Trigger hook on job events |
        | pipeline_events | boolean | no | Trigger hook on pipeline events |
        | wiki_page_events | boolean | no | Trigger hook on wiki events |
        | enable_ssl_verification | boolean | no | Do SSL verification when triggering the hook |
        | token | string | no | Secret token to validate received payloads; this will not be returned in the response |
      DOC
    end

    doc 'Create' do
      desc 'Via Project [Hook ID]'
      example <<~DOC
        project = client.projects.show(16)
        hook = project.hook_create(url: "https://dothething")
      DOC
    end

    def create(project_id, query)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/hooks", ProjectHook, query)
    end
  end
end
