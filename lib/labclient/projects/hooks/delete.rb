# Top namespace
module LabClient
  # Specifics
  class ProjectHooks < Common
    doc 'Delete' do
      desc 'Removes a hook from a project. [Project ID, Hook ID]'
      example 'client.projects.hooks.delete(16, 1)'
    end

    doc 'Delete' do
      desc 'Via Project [Hook ID]'
      example <<~DOC
        project = client.projects.show(16)
        project.hook_delete(1)
      DOC
    end

    doc 'Delete' do
      desc 'Via ProjectHook'
      example <<~DOC
        hook = client.projects.hooks.show(16, 1)
        hook.delete
      DOC
    end

    def delete(project_id, hook_id)
      project_id = format_id(project_id)
      hook_id = format_id(hook_id)

      client.request(:delete, "projects/#{project_id}/hooks/#{hook_id}")
    end
  end
end
