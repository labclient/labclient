# Top namespace
module LabClient
  # Specifics
  class ProjectHooks < Common
    doc 'Update' do
      desc 'Edits a hook for a specified project. [Project ID, Hook ID, Params]'
      example 'client.projects.hooks.update(16, 1, url: "http://labclient.com")'

      result '=>  #<ProjectHook id: 2, url: http://labclient.com>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | url | string | yes | The hook URL |
        | push_events | boolean | no | Trigger hook on push events |
        | push_events_branch_filter | string | no | Trigger hook on push events for matching branches only |
        | issues_events | boolean | no | Trigger hook on issues events |
        | confidential_issues_events | boolean | no | Trigger hook on confidential issues events |
        | merge_requests_events | boolean | no | Trigger hook on merge requests events |
        | tag_push_events | boolean | no | Trigger hook on tag push events |
        | note_events | boolean | no | Trigger hook on note events |
        | job_events | boolean | no | Trigger hook on job events |
        | pipeline_events | boolean | no | Trigger hook on pipeline events |
        | wiki_page_events | boolean | no | Trigger hook on wiki events |
        | enable_ssl_verification | boolean | no | Do SSL verification when triggering the hook |
        | token | string | no | Secret token to validate received payloads; this will not be returned in the response |
      DOC
    end

    doc 'Update' do
      desc 'Via Project [Hook ID, Params]'
      example <<~DOC
        project = client.projects.show(16)
        hook = project.hook_update(1, url: "https://dothething")
      DOC
    end

    doc 'Update' do
      desc 'Via ProjectHook [Params]'
      example <<~DOC
        hook = client.projects.hooks.show(16, 1)
        hook.update(url: "https://dothething")
      DOC
    end

    def update(project_id, hook_id, query)
      project_id = format_id(project_id)
      hook_id = format_id(hook_id)

      client.request(:put, "projects/#{project_id}/hooks/#{hook_id}", ProjectHook, query)
    end
  end
end
