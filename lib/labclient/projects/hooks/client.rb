# Top namespace
module LabClient
  # Inspect Helper
  class ProjectHooks < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def hooks
      ProjectHooks.new(client)
    end
  end
end
