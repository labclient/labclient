# Top namespace
module LabClient
  # Inspect Helper
  class ProjectHook < Klass
    include ClassHelpers
    def inspect
      "#<ProjectHook id: #{id}, url: #{url}>"
    end

    def project
      client.projects.show project_id
    end

    def update(query)
      update_self client.projects.hooks.update(project_id, id, query)
    end

    def delete
      client.projects.hooks.delete(project_id, id)
    end

    date_time_attrs %i[created_at]

    help do
      subtitle 'ProjectHook'
      option 'project', 'Show Project'
      option 'update', 'Update this hook [Hash]'
      option 'delete', 'Delete this hook'
    end
  end
end
