# Top namespace
module LabClient
  # Inspect Helper
  class ProjectClusters < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def clusters
      ProjectClusters.new(client)
    end
  end
end
