# Top namespace
module LabClient
  # Specifics
  class ProjectClusters < Common
    doc 'Delete' do
      desc 'Deletes an existing project cluster. [Project ID, Mirror ID, Params]'
      example 'client.projects.clusters.update(310, 3, enabled: true)'

      result '=>  #<ProjectCluster id: 3, name: planet bob>'
    end

    doc 'Delete' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | `id` | integer | yes | The ID of the project owned by the authenticated user |
        | `cluster_id` | integer | yes | The ID of the cluster |
      DOC
    end

    doc 'Delete' do
      desc 'Via ProjectCluster [Params]'
      example <<~DOC
        cluster = client.projects.clusters.list(310).first
        cluster.delete
      DOC
    end

    def delete(project_id, cluster_id)
      project_id = format_id(project_id)
      cluster_id = format_id(cluster_id)

      client.request(:delete, "projects/#{project_id}/clusters/#{cluster_id}", ProjectCluster)
    end
  end
end
