# Top namespace
module LabClient
  # Specifics
  class ProjectClusters < Common
    doc 'Update' do
      desc 'Updates an existing project cluster. [Project ID, Cluster ID, Hash]'
      example 'client.projects.clusters.update(310, 3, name: "Sweet")'

      result '=>  #<ProjectCluster id: 3, name: Sweet>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | name      | string | yes | The name of the cluster |
        | domain      | string | no | The base domain of the cluster |
        | management_project_id | integer | no | The ID of the management project for the cluster |
        | platform_kubernetes_attributes[api_url] | string | yes | The URL to access the Kubernetes API |
        | platform_kubernetes_attributes[token] | string | yes | The token to authenticate against Kubernetes |
        | platform_kubernetes_attributes[ca_cert] | string | no | TLS certificate. Required if API is using a self-signed TLS certificate. |
        | platform_kubernetes_attributes[namespace] | string | no | The unique namespace related to the project |
        | environment_scope | string | no | The associated environment to the cluster. Defaults to * **(PREMIUM)** |

        `name`, `api_url`, `ca_cert` and `token` can only be updated if the cluster was added through the
        “Add existing Kubernetes cluster” option or through the “Add existing cluster to project” endpoint.
      DOC
    end

    doc 'Update' do
      desc 'Via ProjectCluster [Params]'
      example <<~DOC
        cluster = client.projects.clusters.list(310).first
        cluster.update(name: 'bump')
      DOC
    end

    def update(project_id, cluster_id, query)
      project_id = format_id(project_id)
      cluster_id = format_id(cluster_id)

      client.request(:put, "projects/#{project_id}/clusters/#{cluster_id}", ProjectCluster, query)
    end
  end
end
