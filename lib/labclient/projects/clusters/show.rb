# Top namespace
module LabClient
  # Specifics
  class ProjectClusters < Common
    doc 'Show' do
      desc 'Gets a single project cluster. [Project ID, Cluster ID]'
      example 'client.projects.clusters.list(16)'

      result <<~DOC
        => #<ProjectCluster id: 1, name: cluster-5>
      DOC
    end

    doc 'Show' do
      desc 'Via Project [Cluster ID]'
      example <<~DOC
        project = client.projects.show(16)
        project.cluster(cluster_id)
      DOC
    end

    def show(project_id, cluster_id)
      cluster_id = format_id(cluster_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/clusters/#{cluster_id}", ProjectCluster)
    end
  end
end
