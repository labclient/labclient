# Top namespace
module LabClient
  # Specifics
  class ProjectClusters < Common
    doc 'List' do
      desc 'Returns a list of project clusters. [Project ID]'
      example 'client.projects.clusters.list(16)'

      result <<~DOC
        => #<ProjectCluster id: 1, name: cluster-5>]
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.clusters
      DOC
    end

    def list(project_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/clusters", ProjectCluster)
    end
  end
end
