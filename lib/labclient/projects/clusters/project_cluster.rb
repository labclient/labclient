# Top namespace
module LabClient
  # Inspect Helper
  class ProjectCluster < Klass
    include ClassHelpers
    def inspect
      "#<ProjectCluster id: #{id}, name: #{name}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show project_id
    end

    def update(query)
      project_id = collect_project_id

      update_self client.projects.clusters.update(project_id, id, query)
    end

    def delete
      project_id = collect_project_id

      client.projects.clusters.delete(project_id, id)
    end

    date_time_attrs %i[created_at]
    user_attrs %i[uster]

    help do
      subtitle 'ProjectCluster'
      option 'project', 'Show Project'
      option 'update', 'Update this Cluster [Hash]'
      option 'delete', 'Delete cluster'
    end
  end
end
