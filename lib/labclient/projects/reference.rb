# Top namespace
module LabClient
  # rubocop:disable Metrics/BlockLength
  # Help Do Block
  class Project < Klass
    help do
      subtitle 'Project'
      option 'archive', 'Archive this project'
      option 'delete', 'Schedule/Delete this project'
      option 'events', "List this project's events"
      option 'reload', 'Reload this project object (New API Call)'
      option 'wait_for_import', 'Looping, wait for project import [Timeout, Interval]'

      option 'fork_existing', 'Create Fork Relationship [Target/Source Project]'
      option 'fork_remove', 'Remove Fork Relationshgip'
      option 'fork', 'Fork this project, accepts hash, [namespace, path, name]'
      option 'forks', 'List forks of this project'

      # Issues
      option 'issues', 'List project issues [Hash]'
      option 'issue_create', 'Create a new issue in this project [Hash]'

      option 'housekeeping', 'Start the Housekeeping'
      option 'languages', 'Get percentage value of languages used.'

      # Mirror
      option 'mirror_start', 'Start mirroring process'
      option 'mirrors', 'List remote mirrors'
      option 'mirror_create', 'Create new remote mirror [Hash]'

      option 'push_rule_create', 'Create project push rule [Hash]'
      option 'push_rule_delete', 'Delete project push rule'
      option 'push_rules_update', 'Update existing project push rule [Hash]'
      option 'push_rules', 'List the project push rules'
      option 'restore', 'Restores project marked for deletion'
      option 'search', 'Search by scope within project [scope, search_string]'
      option 'share', 'Share project with group [{group_id, group_access }]'
      option 'star', 'Star this project'
      option 'starrers', 'List users who have starred this project'
      option 'transfer', 'Transfer to namespace [Namespace Name/ID]'
      option 'unarchive', 'Unarchive this project'
      option 'unshare', 'Remove project group share [ group_id ]'
      option 'unstar', 'Unstar this project'
      option 'update', 'Update this project'
      option 'upload', 'Upload file to project [ full_file_path ]'
      option 'snapshot', 'Download snapshot [ file_path, wiki/boolean ]'
      option 'users', "List this project's users"

      # Registry
      option 'registry_repositories', 'Get a list of registry repositories [Hash]'

      # Hooks
      option 'hooks', "List this project's hooks"
      option 'hook_create', 'Create project hook [Hash]'
      option 'hook_delete', 'Delete project hook [Hook ID]'
      option 'hook_show', 'Show specific hook from project [Hook ID]'
      option 'hook_update', 'Update project hook [Hook ID, Hash]'

      # Badges
      option 'badge_create', 'Create project badge [Hash]'
      option 'badge_delete', 'Delete project badge [Badge ID]'
      option 'badge_show', 'Show specific badge from project [Badge ID]'
      option 'badge_update', 'Update project badge [Badge ID, Hash]'
      option 'badges', "List this project's badges"

      # Environments
      option 'environments', "List this project's environments"
      option 'environment_create', 'Create project environment [Hash]'
      option 'environment_delete', 'Delete project environment [Environment ID]'
      option 'environment_show', 'Show specific environment from project [Environment ID]'
      option 'environment_update', 'Update project environment [Environment ID, Hash]'
      option 'environment_stop', 'Stop project environment [Environment ID]'

      # Protected Environments
      option 'protected_environments', 'List protected environments [String]'
      option 'protected_environment', 'Show single protected environment [String]'
      option 'protect_environment', 'Protect a single environment [Hash]'
      option 'unprotect_environment', 'Unprotect environment [Environment]'

      # Deployments
      option 'deployments', "List this project's deployments"
      option 'deployment_create', 'Create project deployment [Hash]'
      option 'deployment_merge_requests', 'Show related merge requests [Deployment ID, Hash]'
      option 'deployment_show', 'Show specific deployment from project [Deployment ID]'
      option 'deployment_update', 'Update project deployment [Deployment ID, String Status]'

      # Triggers
      option 'trigger_create', 'Create project trigger [String]'
      option 'trigger_delete', 'Delete project trigger [Trigger ID]'
      option 'trigger_show', 'Show specific trigger from project [Trigger ID]'
      option 'triggers', "List this project's triggers"

      # Variables
      option 'variable_create', 'Create project variable [Hash]'
      option 'variable_delete', 'Delete project variable [Variable Key]'
      option 'variable_show', 'Show specific variable from project [Variable Key]'
      option 'variable_update', 'Update project variable [Variable Key, Hash]'
      option 'variables', "List this project's variables"

      # Labels
      option 'labels', "List this project's labels"
      option 'label', 'Show specific label from project [Label ID]'
      option 'label_create', 'Create project label [Hash]'
      option 'label_delete', 'Delete project label [Label ID]'
      option 'label_update', 'Update project label [Label ID, Hash]'
      option 'label_promote', 'Promote label to group [Label ID]'
      option 'label_subscribe', 'Subscribe to label notifications [Label ID]'
      option 'label_unsubscribe', 'Unsubscribe to label notifications [Label ID]'

      # Releases
      option 'release_create', 'Create project release [Hash]'
      option 'release_delete', 'Delete project release [Tag Name]'
      option 'release_show', 'Show specific release from project [Tag Name]'
      option 'release_update', 'Update project release [Tag Name, Hash]'
      option 'release_evidence', 'Create Evidence for existing Release [Tag Name, Hash]'
      option 'releases', "List this project's releases"

      # Release Links
      option 'release_link_create', 'Create project release link [Tag Name, Hash]'
      option 'release_link_delete', 'Delete project release link [Tag Name, Link ID]'
      option 'release_link_show', 'Show specific release_link from project [Tag Name, Link ID]'
      option 'release_link_update', 'Update project release link [Tag Name, Link ID, Hash]'
      option 'release_link_evidence', 'Create Evidence for existing Release [Tag Name, Link ID, Hash]'
      option 'release_links', "List this project's release links [Tag Name]"

      # Services
      option 'service_delete', 'Delete project service [Service Slug]'
      option 'service_show', 'Show specific service from project [Service Slug]'
      option 'service_update', 'Update/Create project service [Service Slug, Hash]'
      option 'services', "List this project's services"

      # Deploy Keys
      option 'deploy_keys', 'List Project DeployKeys'
      option 'deploy_key_add', 'Add Project DeployKeys [Hash]'
      option 'deploy_key_show', 'Find specific Project Deploy Key [Key ID]'
      option 'deploy_key_update', 'Update specific Project Deploy Key [Key ID, Hash]'
      option 'deploy_key_delete', 'Remove specific Project Deploy Key [Key ID]'
      option 'deploy_key_enable', 'Enable specific Project Deploy Key [Key ID]'

      # Commits
      option 'commits', 'List Commits [Hash]'
      option 'commit', 'Show Commit [String/Commit Sha]'
      option 'commit_create', 'Create new commit [Hash]'
      option 'commit_refs', 'Get all references (from branches or tags) a commit is pushed to. [Commit ID, Scope]'
      option 'commit_cherry_pick', 'Cherry picks a commit to a target branch. [Commit ID, Branch]'
      option 'commit_comments', 'Get the comments of a commit [Commit ID]'
      option 'commit_comment_create', 'Post a new Comment on Commit [Commit ID, Hash]'
      option 'commit_merge_requests', 'Get a list of Merge Requests related to the specified commit. [Commit ID]'

      option 'commit_status', 'List the statuses of a commit. [Commit ID, Hash]'
      option 'commit_status_update', 'Adds or updates a build status of a commit. [Commit ID, Hash]'

      option 'wikis', 'Get all wiki pages. [Boolean, include content]'
      option 'wiki', 'Get specific wiki page. [String/Slug]'
      option 'wiki_create', 'Create a new wiki page. [Hash]'
      option 'wiki_update', 'Update existing wiki page. [Slug, Hash]'
      option 'wiki_delete', 'Delete wiki page. [Slug]'
      option 'wiki_upload', 'Upload file to wiki. [File Path]'

      # Runners
      option 'runners', 'List project runners. [Hash]'
      option 'runner_enable', 'Enable runner for project. [Runner ID]'
      option 'runner_disable', 'Disable runner for project. [Runner ID]'

      # Pipelines
      option 'pipelines', 'List pipelines for Project. [Hash]'
      option 'pipeline', 'Show pipeline for Project. [Pipeline ID]'
      option 'pipeline_variables', 'Show pipeline_variables for Project. [Pipeline ID]'
      option 'pipeline_create', 'Create new Pipeline. [Hash]'
      option 'pipeline_retry', 'Retry Jobs in Pipeline [Pipeline ID]'
      option 'pipeline_cancel', 'Cancel Jobs in Pipeline [Pipeline ID]'
      option 'pipeline_delete', 'Delete Pipeline [Pipeline ID]'

      # Jobs
      option 'jobs', 'List project jobs [Scope]'
      option 'job', 'Show specific job [Job ID]'
      option 'job_artifacts', 'Download Job Artifacts [Job ID, File Path]'
      option 'job_artifacts_latest', 'Download latest artifacts from given ref Artifacts [Branch Name, File Path]'
      option 'job_artifacts_path', 'Download artifacts path [Job ID, File Path, Output Path]'
      option 'job_trace', 'Get job output[Job ID]'
      option 'job_cancel', 'Cancel single project job [Job ID]'
      option 'job_retry', 'Retry single project job [Job ID]'
      option 'job_erase', 'Erase single project job [Job ID]'
      option 'job_keep', 'Prevents artifacts from being deleted when expiration is set [Job ID]'
      option 'job_delete', 'Delete artifacts of a job [Job ID]'
      option 'job_play', 'Triggers a manual action to start a job [Job ID]'

      # Members
      option 'members', 'List project members [Hash]'
      option 'members_all', 'List project members, including inherited [Hash]'
      option 'member', 'Show specific member [User ID]'
      option 'member_all', 'Show specific member, including inherited [User ID]'
      option 'member_add', 'Add user to project [User ID, Hash]'
      option 'member_update', 'Update user on project [User ID, Hash]'
      option 'member_delete', 'Remove user from Project [User ID]'

      # Repository
      option 'tree', 'Get a list of repository files and directories [Hash]'
      option 'blob', 'Allows you to receive information about blob [Sha, Raw/Boolean]'
      option 'download_archive', 'Get an archive of the repository [File Path, Format]'
      option 'compare', 'Compare branches, tags or commits [Hash]'
      option 'contributors', 'Get repository contributors list [Hash]'
      option 'merge_base', 'Get the common ancestor for 2 or more refs [Array(Refs)]'

      # Files
      option 'file', 'Retrieve file information [File Path, Ref, Kind]'
      option 'file_create', 'Create a single file [File Path, Hash]'
      option 'file_update', 'Update a single file [File Path, Hash]'
      option 'file_delete', 'Remove a single file [File Path, Hash]'

      # Branches
      option 'branches', 'List repository branches [String]'
      option 'branch', 'Get a single repository branches [Branch Name]'
      option 'branch_create', 'Create a repository branches [Hash]'
      option 'branch_delete', 'Delete a single repository branches [Branch Name]'
      option 'branch_delete_merged', 'Will delete all branches that are merged into the project’s default branch'

      # Protected Branches
      option 'protected_branches', 'List protected branches [String]'
      option 'protected_branch', 'Show single protected branch [String]'
      option 'protect_branch', 'Protect a single or wildcard branch [Hash]'
      option 'unprotect_branch', 'Unprotect branch [Branch]'
      option 'branch_code_owner_approval', 'Update the “code owner approval required” [Branch, Boolean]'

      # Tags
      option 'tags', 'List repository tags  [Hash]'
      option 'tag', 'Get a specific repository tag by name [String]'
      option 'tag_create', 'Creates a new tag in the repository [Hash]'
      option 'tag_delete', 'Deletes a tag of a repository with given name [String]'
      option 'tag_release', 'Add release notes to the existing Git tag [String, String]'
      option 'tag_update', 'Updates the release notes of a given release. [String, String]'

      # Protected Tags
      option 'protected_tags', 'List protected tags [String]'
      option 'protected_tag', 'Show single protected tag [String]'
      option 'protect_tag', 'Protect a single or wildcard tag [Hash]'
      option 'unprotect_tag', 'Unprotect tag [Branch]'

      # Approvals
      option 'approvals', 'Show project’s approval configuration.'
      option 'approvals_update', 'Show approval configuration. [Hash]'
      option 'approvals_rules', 'List approval rules. [Hash]'
      option 'approvals_rule_create', 'Create new approval rule. [Hash]'
      option 'approvals_rule_update', 'Update approval rule. [ID, Hash]'
      option 'approvals_rule_delete', 'Delete approval rule. [ID]'

      # Submodule
      option 'submodule', 'Update existing submodule. [Submodule, Hash]'

      # Merge Requests
      option 'merge_requests', 'List project merge requests, [Hash]'
      option 'merge_request_create', 'Create new merge request [Hash]'

      # Access Requests
      option 'access_requests', 'List access requests for project'
      option 'request_access', 'Request access to project'

      # Milestones
      option 'milestones', 'List milestones for project [Hash]'

      # Pipeline Schedules
      option 'pipeline_schedules', 'List pipeline schedules for project'

      # Clusters
      option 'clusters', 'Returns a list of project clusters.'

      # Clusters
      option 'templates', 'List Templates of Type [String/Symbol]'
      option 'template_show', 'Get one template of a particular type [Type: String/Symbol, Name: String/Symbol, Params: Hash]'

      option 'events', 'List project events [Hash]'
      option 'parent', 'Show (API Call) Project Parent, returns either Group or User'
    end
  end
  # rubocop:enable Metrics/BlockLength
end
