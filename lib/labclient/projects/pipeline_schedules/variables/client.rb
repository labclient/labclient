# Top namespace
module LabClient
  # Inspect Helper
  class PipelineVariables < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class PipelineSchedules < Common
    def variables
      PipelineVariables.new(client)
    end
  end
end
