# Top namespace
module LabClient
  # Specifics
  class PipelineVariables < Common
    @group_name = 'Pipeline Schedules'
    doc 'Variables' do
      title 'Update'
      desc 'Update a new variable of a pipeline schedule. [Project ID, Pipeline Schedule ID, Variable Key, Hash]'
      example 'client.projects.pipeline_schedules.variables.update(16, 1, :key_name, value: :better)'
      result <<~DOC
        => {:variable_type=>"env_var", :key=>"key_name", :value=>"better"}
      DOC
    end

    doc 'Variables' do
      markdown <<~DOC
        | Attribute              | Type           | required | Description              |
        |------------------------|----------------|----------|--------------------------|
        | pipeline_schedule_id   | integer        | yes      | The pipeline schedule ID |
        | key                    | string         | yes      | The key of a variable; must have no more than 255 characters; only A-Z, a-z, 0-9, and _ are allowed |
        | value                  | string         | yes      | The value of a variable |
        | variable_type          | string         | no       | The type of a variable. Available types are: env_var (default) and file |
      DOC
    end

    doc 'Variables' do
      desc 'via PipelineSchedule'
      example <<~DOC
        pipeline_schedule = client.projects.pipeline_schedules.show(16,1)
        pipeline_schedule.update_variable(:key_name, value: :var_value)
      DOC
    end

    def update(project_id, pipeline_schedule_id, variable_key, query)
      pipeline_schedule_id = format_id(pipeline_schedule_id)
      project_id = format_id(project_id)

      client.request(:put, "projects/#{project_id}/pipeline_schedules/#{pipeline_schedule_id}/variables/#{variable_key}", nil, query)
    end
  end
end
