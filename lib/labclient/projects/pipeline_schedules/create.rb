# Top namespace
module LabClient
  # Specifics
  class PipelineSchedules < Common
    doc 'Create' do
      desc 'Creates a new project pipeline_schedule. [Project ID, Hash]'

      example <<~DOC
        client.projects.pipeline_schedules.create(16, description: 'Regular Run', cron: '0 1 * * *', ref: :master)
      DOC

      result <<~DOC
        => #<PipelineSchedule id: 1, ref: master>
      DOC
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute     | Type    | required | Description              |
        |---------------|---------|----------|--------------------------|
        | description   | string  | yes      | The description of pipeline schedule         |
        | ref           | string  | yes      | The branch/tag name will be triggered         |
        | cron          | string  | yes      | The cron (e.g. 0 1 * * *) ([Cron syntax](https://en.wikipedia.org/wiki/Cron))       |
        | cron_timezone | string  | no       | The timezone supported by ActiveSupport::TimeZone (e.g. Pacific Time (US & Canada)) (default: 'UTC')     |
        | active        | boolean | no       | The activation of pipeline schedule. If false is set, the pipeline schedule will deactivated initially (default: true) |
      DOC
    end

    def create(project_id, query = {})
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/pipeline_schedules", PipelineSchedule, query)
    end
  end
end
