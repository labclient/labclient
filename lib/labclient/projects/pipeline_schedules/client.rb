# Top namespace
module LabClient
  # Inspect Helper
  class PipelineSchedules < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def pipeline_schedules
      PipelineSchedules.new(client)
    end
  end
end
