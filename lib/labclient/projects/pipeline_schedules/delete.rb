# Top namespace
module LabClient
  # Specifics
  class PipelineSchedules < Common
    doc 'Delete' do
      desc 'Delete the pipeline schedule of a project. [Project ID, Pipeline Schedule ID]'
      example <<~DOC
        client.projects.pipeline_schedules.delete(16, 2)
      DOC
    end

    doc 'Delete' do
      desc 'via Pipeline Schedule'
      example <<~DOC
        pipeline_schedule = client.projects.pipeline_schedules.show(16, 1)
        pipeline_schedule.delete
      DOC
    end

    def delete(project_id, pipeline_schedule_id)
      pipeline_schedule_id = format_id(pipeline_schedule_id)
      project_id = format_id(project_id)
      client.request(:delete, "projects/#{project_id}/pipeline_schedules/#{pipeline_schedule_id}")
    end
  end
end
