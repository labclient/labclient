# Top namespace
module LabClient
  # Specifics
  class PipelineSchedules < Common
    doc 'Update' do
      title 'Take Ownership'
      desc 'Update the owner of the pipeline schedule of a project.  [Project ID, Pipeline Schedule ID]'
      example 'client.projects.pipeline_schedules.take_ownership(16, 2)'
      result <<~DOC
        => #<PipelineSchedule id: 2, ref: master>
      DOC
    end

    doc 'Update' do
      desc 'via the Project Pipeline Schedule'
      example <<~DOC
        pipeline_schedule = client.pipeline_schedules.burndown(2)
        pipeline_schedule.take_ownership
      DOC
    end

    def take_ownership(project_id, pipeline_schedule_id)
      pipeline_schedule_id = format_id(pipeline_schedule_id)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/pipeline_schedules/#{pipeline_schedule_id}/take_ownership", PipelineSchedule)
    end
  end
end
