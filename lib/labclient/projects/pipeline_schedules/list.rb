# Top namespace
module LabClient
  # Specifics
  class PipelineSchedules < Common
    doc 'List' do
      desc 'Get a list of the pipeline schedules of a project. [Project ID]'
      example 'client.projects.pipeline_schedules.list(16)'
      result <<~DOC
        => [#<PipelineSchedule id: 1, ref: master>, ...]
      DOC
    end

    doc 'List' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.pipeline_schedules
      DOC
    end

    # List
    def list(project_id, query = {})
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/pipeline_schedules", PipelineSchedule, query)
    end
  end
end
