# Top namespace
module LabClient
  # Specifics
  class PipelineSchedules < Common
    doc 'Show' do
      desc 'Get the pipeline schedule of a project. [Project ID, Pipeline Schedule ID]'
      example 'client.projects.pipeline_schedules.show(16, 1)'
      result <<~DOC
        => #<PipelineSchedule id: 1, ref: master>
      DOC
    end

    # Show Specific
    def show(project_id, pipeline_schedule_id)
      pipeline_schedule_id = format_id(pipeline_schedule_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/pipeline_schedules/#{pipeline_schedule_id}", PipelineSchedule)
    end
  end
end
