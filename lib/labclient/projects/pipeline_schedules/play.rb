# Top namespace
module LabClient
  # Specifics
  class PipelineSchedules < Common
    doc 'Update' do
      title 'Play'
      desc 'Trigger a new scheduled pipeline, which runs immediately. The next scheduled run of this pipeline is not affected. [Project ID, Pipeline Schedule ID]'
      example 'client.projects.pipeline_schedules.play(16, 1)'
      result <<~DOC
        => { "message": "201 Created" }
      DOC
    end

    doc 'Update' do
      desc 'via PipelineSchedule'
      example <<~DOC
        pipeline_schedule = client.projects.pipeline_schedules.show(16,1)
        pipeline_schedule.play
      DOC
    end

    def play(project_id, pipeline_schedule_id)
      pipeline_schedule_id = format_id(pipeline_schedule_id)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/pipeline_schedules/#{pipeline_schedule_id}/play")
    end
  end
end
