# Top namespace
module LabClient
  # Inspect Helper
  class PipelineSchedule < Klass
    include ClassHelpers

    def inspect
      "#<PipelineSchedule id: #{id}, ref: #{ref}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show project_id
    end

    def update(query = {})
      project_id = collect_project_id
      update_self client.projects.pipeline_schedules.update(project_id, id, query)
    end

    def play
      project_id = collect_project_id
      client.projects.pipeline_schedules.play(project_id, id)
    end

    def take_ownership
      project_id = collect_project_id
      client.projects.pipeline_schedules.take_ownership(project_id, id)
    end

    def delete
      project_id = collect_project_id
      client.projects.pipeline_schedules.delete(project_id, id)
    end

    # Vars
    def create_variable(query = {})
      project_id = collect_project_id
      client.projects.pipeline_schedules.variables.create(project_id, id, query)
    end

    def update_variable(variable_key, query = {})
      project_id = collect_project_id
      client.projects.pipeline_schedules.variables.update(project_id, id, variable_key, query)
    end

    def delete_variable(variable_key)
      project_id = collect_project_id
      client.projects.pipeline_schedules.variables.delete(project_id, id, variable_key)
    end

    date_time_attrs %i[created_at updated_at next_run_at]
    user_attrs %i[owner]

    help do
      subtitle 'Project Pipeline Schedule'
      option 'project', 'Show project for pipeline_schedule.'
      option 'play', 'Trigger a new scheduled pipeline.'
      option 'take_ownership', 'Update the owner of the pipeline schedule of a project.'
      option 'delete', 'Delete this pipeline_schedule.'
      option 'update', 'Update this this pipeline_schedule. [Hash]'

      # Variables
      option 'create_variable', 'Create a new variable. [Hash]'
      option 'update_variable', 'Update existing variable. [Variable Key, Hash]'
      option 'delete_variable', 'Delete existing variable. [Variable Key,  Hash]'
    end
  end
end
