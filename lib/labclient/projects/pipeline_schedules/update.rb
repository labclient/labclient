# Top namespace
module LabClient
  # Specifics
  class PipelineSchedules < Common
    doc 'Update' do
      desc 'Updates the pipeline schedule of a project. Once the update is done, it will be rescheduled automatically. [Project ID, Pipeline Schedule ID, Hash]'

      example <<~DOC
        client.projects.pipeline_schedules.update(16, 1, description: 'Better Detail')
      DOC

      result <<~DOC
        => #<PipelineSchedule id: 1, ref: master>
      DOC
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute     | Type    | required | Description              |
        |---------------|---------|----------|--------------------------|
        | description   | string  | no       | The description of pipeline schedule         |
        | ref           | string  | no       | The branch/tag name will be triggered         |
        | cron          | string  | no       | The cron (e.g. 0 1 * * *) ([Cron syntax](https://en.wikipedia.org/wiki/Cron))       |
        | cron_timezone | string  | no       | The timezone supported by ActiveSupport::TimeZone (e.g. Pacific Time (US & Canada)) (default: 'UTC')     |
        | active        | boolean | no       | The activation of pipeline schedule. If false is set, the pipeline schedule will deactivated initially (default: true) |
      DOC
    end

    doc 'Update' do
      desc 'Via Pipeline Schedule'
      example <<~DOC
        pipeline_schedule.update(title: 'One Pipeline Schedule to find them!')
      DOC
    end

    def update(project_id, pipeline_schedule_id, query = {})
      pipeline_schedule_id = format_id(pipeline_schedule_id)
      project_id = format_id(project_id)

      client.request(:put, "projects/#{project_id}/pipeline_schedules/#{pipeline_schedule_id}", PipelineSchedule, query)
    end
  end
end
