# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Show' do
      title 'Users'
      desc 'Get the users list of a project.'
      example 'client.projects.users(10)'
      result <<~DOC
        => [
          #<User id: 2, username: veigar>,
          #<User id: 3, username: janna>,
          #<User id: 5, username: denethor>, ...
        ]
      DOC

      markdown <<~DOC
        | Attribute    | Type          | Required | Description |
        | ------------ | ------------- | -------- | ----------- |
        | search     | string        | no       | Search for specific users |
        | skip_users | integer array | no       | Filter out users with the specified IDs |
      DOC
    end

    doc 'Show' do
      desc 'Via project object'
      example <<~DOC
        project = client.projects.show(10)
        project.users
      DOC
    end

    def users(project_id, query = {})
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/users", User, query)
    end
  end
end
