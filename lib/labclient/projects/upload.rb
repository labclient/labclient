# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Update' do
      title 'Upload'
      desc 'Uploads a file to the specified project to be used in an issue or merge request description, or a comment. [ Project ID, File Path'
      example 'client.projects.upload(25, "/tmp/text.txt")'
      result '{:alt=>"text.txt", :url=>"/uploads/123123/text.txt", :markdown=>"[text.txt](/uploads/123123/text.txt)"}'
    end

    doc 'Update' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(299)
        project.upload("/tmp/text.txt")
      DOC
    end

    def upload(project_id, path)
      project_id = format_id(project_id)

      file = File.open(path, 'r')

      client.request(:post, "projects/#{project_id}/uploads", nil, { file: file }, false)
    end
  end
end
