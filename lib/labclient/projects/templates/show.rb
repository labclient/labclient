# Top namespace
module LabClient
  # Specifics
  class ProjectTemplates < Common
    doc 'Show' do
      desc 'Get one template of a particular type. [Project ID, Type: String/Symbol, Name: String/Symbol, Hash]'
      example 'client.projects.templates.show(15,:issues,:elasticsearch)'
      result '[#<Runner id: 1, status: online>, ... ]'

      markdown <<~DOC

        | Attribute | Type | Required | Description |
        | ---------------------------- | ----------------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | |
        | type | string | yes | The type of the template. One of: `dockerfiles`, `gitignores`, `gitlab_ci_ymls`, `licenses`, `issues`, or `merge_requests`. |
        | name | string | yes | The key of the template, as obtained from the collection endpoint |
        | source_template_project_id | integer | no | The project ID where a given template is being stored. This is useful when multiple templates from different projects have the same name. If multiple templates have the same name, the match from `closest ancestor` is returned if `source_template_project_id` is not specified |
        | project | string | no | The project name to use when expanding placeholders in the template. Only affects licenses |
        | fullname | string | no | The full name of the copyright holder to use when expanding placeholders in the template. Only affects licenses |
      DOC
    end

    doc 'List' do
      desc 'Via Project [String/Symbol]'
      example <<~DOC
        project = client.projects.show(16)
        project.template_show(:issues, 'Elasticsearch')
      DOC
    end

    def show(project_id, template_type, template_name, query = {})
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/templates/#{template_type}/#{template_name}", nil, query)
    end
  end
end
