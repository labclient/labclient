# Top namespace
module LabClient
  # Specifics
  class ProjectTemplates < Common
    doc 'List' do
      desc 'Get all templates of a particular type. [Project ID, String/Symbol]'
      example 'client.projects.templates.list(15, :issues)'
      result '[{:key=>"Code Contributions", :name=>"Code Contributions"}, ...]'

      markdown <<~DOC
        | Attribute | Type   | Required | Description                                                                                                                          |
        | --------- | ------ | -------- | ------------------------------------------------------------------------------------------------------------------------------------ |
        | type      | string | yes      | The type of the template. Accepted values are: `dockerfiles`, `gitignores`, `gitlab_ci_ymls`, `licenses`, `issues`, `merge_requests` |
      DOC
    end

    doc 'List' do
      desc 'Via Project [String/Symbol]'
      example <<~DOC
        project = client.projects.show(16)
        project.templates(:issues)
      DOC
    end

    def list(project_id, template_type)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/templates/#{template_type}")
    end
  end
end
