# Top namespace
module LabClient
  # Inspect Helper
  class ProjectTemplates < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def templates
      ProjectTemplates.new(client)
    end
  end
end
