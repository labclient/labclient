# Top namespace
module LabClient
  # Project Definition
  class Project < Klass
    include ProjectDeployKey
    include ProjectCommits
    include ClassHelpers
    include ProjectMethods
    def inspect
      "#<Project id: #{id} #{path_with_namespace}>"
    end

    date_time_attrs %i[last_activity_at created_at]

    user_attrs %i[owner]
  end
end
