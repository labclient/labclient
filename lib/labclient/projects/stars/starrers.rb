# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Starrers' do
      desc 'List the users who starred the specified project. [user id]'
      example 'client.projects.starrers(5)'
      result <<~DOC
        => [#<User id: 1, username: root>, ...]

        # Additional `starred_since` on User
        users = client.projects.starrers(5)
        users.first.starred_since
        => 2020-02-26 14:31:02 -0700
      DOC
    end

    doc 'Starrers' do
      markdown <<~DOC
        Note: starrers `User` response will include `starred_since`

        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | search | string | no | Search for specific users. |

      DOC
    end

    def starrers(project_id, search = '')
      project_id = format_id(project_id)

      results = if search.empty?
                  client.request(:get, "projects/#{project_id}/starrers", nil)
                else
                  client.request(:get, "projects/#{project_id}/starrers", nil, search: search)
                end

      results.map do |star_data|
        star_data.user[:starred_since] = Time.parse star_data.starred_since
        User.new star_data.user
      end
    end
  end
end
