# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Star' do
      desc 'Stars a given project'
      example 'client.projects.star(10)'
      result <<~DOC
        => #<Project id: 30 valenwood/frosttroll>
      DOC
    end

    doc 'Star' do
      desc 'Via project'
      example <<~DOC
        project = client.projects.show(10)
        project.star
      DOC
    end

    doc 'Star' do
      subtitle 'Unstars'
      desc 'Unstars a given project'
      example 'client.projects.star(10)'
    end

    doc 'Star' do
      desc 'Via project'
      example <<~DOC
        project = client.projects.show(10)
        project.unstar
      DOC
    end

    def star(project_id)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/star", Project)
    end

    def unstar(project_id)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/unstar", Project)
    end
  end
end
