# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'List' do
      desc 'Get a list of projects starred by the given user. [user id]'
      example 'client.projects.starred(5)'
      result <<~DOC
        => [
         #<Project id: 318, "root/middleman">,
         #<Project id: 317, "skyrim/dwarvencenturion">,
         #<Project id: 303, "pawnshopplanet/pibbles">,
        ]
      DOC
    end

    doc 'List' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | archived | boolean | no | Limit by archived status |
        | visibility | string | no | Limit by visibility public, internal, or private |
        | order_by | string | no | Return projects ordered by id, name, path, created_at, updated_at, or last_activity_at fields. Default is created_at |
        | sort | string | no | Return projects sorted in asc or desc order. Default is desc |
        | search | string | no | Return list of projects matching the search criteria |
        | simple | boolean | no | Return only limited fields for each project. This is a no-op without authentication as then _only_ simple fields are returned. |
        | owned | boolean | no | Limit by projects explicitly owned by the current user |
        | membership | boolean | no | Limit by projects that the current user is a member of |
        | starred | boolean | no | Limit by projects starred by the current user |
        | statistics | boolean | no | Include project statistics |
        | with_custom_attributes | boolean | no | Include [custom attributes](custom_attributes.md) in response (admins only) |
        | with_issues_enabled | boolean | no | Limit by enabled issues feature |
        | with_merge_requests_enabled | boolean | no | Limit by enabled merge requests feature |
        | with_programming_language | string | no | Limit by projects which use the given programming language |
        | min_access_level | integer | no | Limit by current user minimal [access level](members.md) |
        | id_after                    | integer | no | Limit results to projects with IDs greater than the specified ID |
        | id_before                   | integer | no | Limit results to projects with IDs less than the specified ID |

      DOC
    end

    # List/Search users
    def starred(user_id, query = {})
      query_access_level(query, :min_access_level)

      client.request(:get, "users/#{user_id}/starred_projects", Project, query)
    end
  end
end
