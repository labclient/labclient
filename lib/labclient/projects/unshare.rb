# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Update' do
      subtitle 'Remove Share'
      desc 'Unshare the project from the group. [Project ID, Group ID]'
      example 'client.projects.unshare(25, 78)'
      result '=> nil'
    end

    doc 'Update' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(25)
        project.unshare(78)
      DOC
    end

    def unshare(project_id, group_id)
      project_id = format_id(project_id)
      group_id = format_id(group_id)

      client.request(:delete, "projects/#{project_id}/share/#{group_id}", nil)
    end
  end
end
