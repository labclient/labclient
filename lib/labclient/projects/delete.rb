# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Delete' do
      desc 'Removes a project including all associated resources.'
      example 'client.projects.delete(25)'
      result '{:message=>"202 Accepted"}'
    end

    doc 'Delete' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(299)
        project.delete
      DOC
    end

    def delete(project_id)
      project_id = format_id(project_id)

      client.request(:delete, "projects/#{project_id}")
    end
  end
end
