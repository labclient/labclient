# Top namespace
module LabClient
  # Specifics
  class ProjectDeployments < Common
    doc 'Show' do
      desc 'Get a specific deployment [Project ID, Deployment ID]'
      example 'client.projects.deployments.show(16, 1)'

      result '=> #<ProjectDeployment id: 1, status: running>'
    end

    doc 'Show' do
      desc 'Via Project [Deployment ID]'
      example <<~DOC
        project = client.projects.show(16)
        deployment = project.deployment_show(1)
      DOC
    end

    def show(project_id, deployment_id)
      project_id = format_id(project_id)
      deployment_id = format_id(deployment_id)

      client.request(:get, "projects/#{project_id}/deployments/#{deployment_id}", ProjectDeployment)
    end
  end
end
