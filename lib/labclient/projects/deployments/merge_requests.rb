# Top namespace
module LabClient
  # Specifics
  class ProjectDeployments < Common
    doc 'Merge Requests' do
      desc 'Retrieves the list of merge requests shipped with a given deployment. [Project ID, Environment ID, Params]'
      example 'client.projects.deployments.merge_requests(16, 1)'
    end

    doc 'Merge Requests' do
      desc 'Via Project [Environment ID]'
      example <<~DOC
        project = client.projects.show(100)
        project.deployment_merge_requests(3)
      DOC
    end

    doc 'Merge Requests' do
      desc 'Via ProjectDeployment'
      example <<~DOC
        deployment = client.projects.deployments.show(100, 4)
        deployment.merge_requests
      DOC
    end

    def merge_requests(project_id, deployment_id, query = {})
      project_id = format_id(project_id)
      deployment_id = format_id(deployment_id)

      client.request(:get, "projects/#{project_id}/deployments/#{deployment_id}/merge_requests", MergeRequest, query)
    end
  end
end
