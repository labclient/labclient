# Top namespace
module LabClient
  # Inspect Helper
  class ProjectDeployment < Klass
    include ClassHelpers
    def inspect
      "#<ProjectDeployment id: #{id}, status: #{status}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show project_id
    end

    def update(query)
      project_id = collect_project_id

      update_self client.projects.deployments.update(project_id, id, query)
    end

    def merge_requests
      project_id = collect_project_id

      client.projects.deployments.merge_requests(project_id, id)
    end

    date_time_attrs %i[created_at updated_at]
    user_attrs %i[user]

    help do
      subtitle 'Project Deployment'
      option 'project', 'Show Project'
      option 'update', 'Update this deployment [Hash]'
      option 'stop', 'Stop this deployment'
    end
  end
end
