# Top namespace
module LabClient
  # Specifics
  class ProjectDeployments < Common
    doc 'List' do
      desc 'Get a list of deployments in a project. [Project ID, Filter Params]'
      example 'client.projects.deployments.list(16)'

      result <<~DOC
        => [#<ProjectDeployment id: 1, status: running>, #<ProjectDeployment id: 2, status: running>, #<ProjectDeployment id: 3, status: running>]
      DOC
    end

    doc 'List' do
      markdown <<~DOC
        | Attribute | Type    | Required | Description         |
        |-----------|---------|----------|---------------------|
        | order_by| string  | no       | Return deployments ordered by id or iid or created_at or updated_at or ref fields. Default is id |
        | sort    | string  | no       | Return deployments sorted in asc or desc order. Default is asc |
        | updated_after | datetime | no | Return deployments updated after the specified date |
        | updated_before | datetime | no | Return deployments updated before the specified date |
        | environment | string | no | The name of the environment to filter deployments by |
        | status | string | no | The status to filter deployments by |

        The status attribute can be one of the following values:

          - created
          - running
          - success
          - failed
          - canceled

      DOC
    end

    doc 'List' do
      desc 'Filter by Status'
      example 'client.projects.deployments.list(status: :created)'
    end
    doc 'List' do
      desc 'Search by Environment'
      example 'client.projects.deployments.list(environment: :title)'
    end

    doc 'List' do
      desc 'Filter by time'
      example 'client.projects.deployments.list(updated_after: 1.year.ago)'
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.deployments
      DOC
    end

    def list(project_id, query = {})
      project_id = format_id(project_id)

      query[:updated_after] = query[:updated_after].to_time.iso8601 if format_time?(query[:updated_after])
      query[:updated_before] = query[:updated_before].to_time.iso8601 if format_time?(query[:updated_before])

      client.request(:get, "projects/#{project_id}/deployments", ProjectDeployment, query)
    end
  end
end
