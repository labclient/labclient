# Top namespace
module LabClient
  # Specifics
  class ProjectDeployments < Common
    doc 'Create' do
      desc 'Create a deployment. [Project ID, Hash]'
      example <<~DOC
        params = {
          environment: :rawr,
          sha: '0bc128f3',
          ref: :master,
          tag: false,
          status: 'running'
        }

        client.projects.deployments.create(16, params)
      DOC

      result '=>  #<ProjectDeployment id: 2, name: rawr, url: http://labclient.com>'
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute        | Type           | Required | Description         |
        |------------------|----------------|----------|---------------------|
        | environment      | string         | yes      | The name of the environment to create the deployment for |
        | sha              | string         | yes      | The SHA of the commit that is deployed |
        | ref              | string         | yes      | The name of the branch or tag that is deployed |
        | tag              | boolean        | yes      | A boolean that indicates if the deployed ref is a tag (true) or not (false) |
        | status           | string         | yes      | The status of the deployment |

        The status can be one of the following values:

        - created
        - running
        - success
        - failed
        - canceled
      DOC
    end

    doc 'Create' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(100)
        deployment = project.deployment_create(name: 'rawr', external_url:'https://labclient')
      DOC
    end

    def create(project_id, query)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/deployments", ProjectDeployment, query)
    end
  end
end
