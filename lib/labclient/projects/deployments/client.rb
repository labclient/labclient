# Top namespace
module LabClient
  # Inspect Helper
  class ProjectDeployments < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def deployments
      ProjectDeployments.new(client)
    end
  end
end
