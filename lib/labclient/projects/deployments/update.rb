# Top namespace
module LabClient
  # Specifics
  class ProjectDeployments < Common
    doc 'Update' do
      desc 'Updating a deployment status [Project ID, Environment ID, Status]'
      example 'client.projects.deployments.update(16, 3, :success)'

      result '=>  #<ProjectDeployment id: 3, status: success>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute        | Type           | Required | Description         |
        |------------------|----------------|----------|---------------------|
        | status           | string         | yes      | The status of the deployment |

        The status can be one of the following values:

        - created
        - running
        - success
        - failed
        - canceled
      DOC
    end

    doc 'Update' do
      desc 'Via Project [Environment ID, Status]'
      example <<~DOC
        project = client.projects.show(16)
        deployment = project.deployment_update(4, :running)
      DOC
    end

    doc 'Update' do
      desc 'Via ProjectDeployment [Status]'
      example <<~DOC
        deployment = client.projects.deployments.show(16, 4)
        deployment.update(:failed)
      DOC
    end

    def update(project_id, deployment_id, status)
      project_id = format_id(project_id)
      deployment_id = format_id(deployment_id)
      query = { status: status }

      client.request(:put, "projects/#{project_id}/deployments/#{deployment_id}", ProjectDeployment, query)
    end
  end
end
