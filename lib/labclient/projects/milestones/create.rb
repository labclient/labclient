# Top namespace
module LabClient
  # Specifics
  class ProjectMilestones < Common
    doc 'Create' do
      desc 'Creates a new project milestone. [Project ID, Hash]'

      example <<~DOC
        client.projects.milestones.create(16, title: "Sweet Code")
      DOC

      result <<~DOC
        => #<ProjectMilestone id: 2, title: Sweet Code>
      DOC
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute    | Type   | Required | Description                       |
        |--------------|------- |--------- |---------------------------------- |
        | title        | string | yes      | Title of a milestone.             |
        | description  | string | no       | The description of the milestone  |
        | due_date     | string | no       | The due date of the milestone     |
        | start_date   | string | no       | The start date of the milestone   |
      DOC
    end

    def create(project_id, query = {})
      project_id = format_id(project_id)

      query[:due_date] = query[:due_date].to_time.iso8601 if format_time?(query[:due_date])
      query[:start_date] = query[:start_date].to_time.iso8601 if format_time?(query[:start_date])

      client.request(:post, "projects/#{project_id}/milestones", ProjectMilestone, query)
    end
  end
end
