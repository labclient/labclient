# Top namespace
module LabClient
  # Specifics
  class ProjectMilestones < Common
    doc 'List' do
      desc 'Get a list of project milestones. [Project ID, Hash]'
      example 'client.projects.milestones.list(16)'
      result <<~DOC
        => [#<ProjectMilestone id: 1, title: Its a mile long stone>, ...]
      DOC
    end

    doc 'List' do
      markdown <<~DOC

        | Attribute | Type   | Required | Description |
        | --------- | ------ | -------- | ----------- |
        | iids[]  | integer array | optional | Return only the milestones having the given iid |
        | state   | string | optional | Return only active or closed milestones |
        | title   | string | optional | Return only the milestones having the given title |
        | search  | string | optional | Return only milestones with a title or description matching the provided string |

      DOC
    end

    doc 'List' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.milestones
      DOC
    end

    # List
    def list(project_id, query = {})
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/milestones", ProjectMilestone, query)
    end
  end
end
