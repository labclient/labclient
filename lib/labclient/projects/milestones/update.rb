# Top namespace
module LabClient
  # Specifics
  class ProjectMilestones < Common
    doc 'Update' do
      desc 'Updates an existing project milestone [Project ID, Project Milestone ID, Hash]'

      example <<~DOC
        client.projects.milestones.update(16, 2, title: 'Better Title')
      DOC

      result <<~DOC
        => #<ProjectMilestone id: 2, title: Better Title>
      DOC
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute    | Type   | Required | Description                       |
        |--------------|------- |--------- |---------------------------------- |
        | title        | string | yes      | Title of a milestone.             |
        | description  | string | no       | The description of the milestone  |
        | due_date     | string | no       | The due date of the milestone     |
        | start_date   | string | no       | The start date of the milestone   |
      DOC
    end

    doc 'Update' do
      desc 'Via Project Milestone'
      example <<~DOC
        milestone.update(title: 'One Project Milestone to find them!')
      DOC
    end

    def update(project_id, milestone_id, query = {})
      milestone_id = format_id(milestone_id)
      project_id = format_id(project_id)

      query[:due_date] = query[:due_date].to_time.iso8601 if format_time?(query[:due_date])
      query[:start_date] = query[:start_date].to_time.iso8601 if format_time?(query[:start_date])

      client.request(:put, "projects/#{project_id}/milestones/#{milestone_id}", ProjectMilestone, query)
    end
  end
end
