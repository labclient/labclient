# Top namespace
module LabClient
  # Inspect Helper
  class ProjectMilestones < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def milestones
      ProjectMilestones.new(client)
    end
  end
end
