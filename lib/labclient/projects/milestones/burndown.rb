# Top namespace
module LabClient
  # Specifics
  class ProjectMilestones < Common
    doc 'Show' do
      title 'Burndown'
      desc 'Gets all burndown chart events for a single milestone.  [Project ID, Project Milestone ID]'
      example 'client.projects.milestones.burndown(16, 2)'
      result <<~DOC
        [
          {
            :created_at => "2020-04-17T14:39:18.827-06:00",
                :weight => nil,
                :action => "created"
          }
        ]
      DOC
    end

    doc 'Show' do
      desc 'via the Project Project Milestone'
      example <<~DOC
        milestone = client.milestones.burndown(2)
        milestone.burndown
      DOC
    end

    # Show Specific
    def burndown(project_id, milestone_id)
      milestone_id = format_id(milestone_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/milestones/#{milestone_id}/burndown_events")
    end
  end
end
