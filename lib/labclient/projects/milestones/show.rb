# Top namespace
module LabClient
  # Specifics
  class ProjectMilestones < Common
    doc 'Show' do
      desc 'Get a single project milestone. [Project ID, Project Milestone ID]'
      example 'client.projects.milestones.show(16, 1)'
      result <<~DOC
        => #<ProjectMilestone id: 1, title: Its a mile long stone>
      DOC
    end

    # Show Specific
    def show(project_id, milestone_id)
      milestone_id = format_id(milestone_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/milestones/#{milestone_id}", ProjectMilestone)
    end
  end
end
