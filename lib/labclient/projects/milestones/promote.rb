# Top namespace
module LabClient
  # Specifics
  class ProjectMilestones < Common
    doc 'Update' do
      title 'Promote'
      desc 'Promote project milestone to a group milestone [Project ID, Project Milestone ID]'
      example <<~DOC
        client.projects.milestones.promote(16,2)
      DOC
    end

    doc 'Update' do
      desc 'via Project Milestone'
      example <<~DOC
        milestone = client.projects.milestones.show(16,1)
        milestone.promote
      DOC
    end

    def promote(project_id, milestone_id)
      milestone_id = format_id(milestone_id)
      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/milestones/#{milestone_id}/promote")
    end
  end
end
