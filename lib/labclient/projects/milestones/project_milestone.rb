# Top namespace
module LabClient
  # Inspect Helper
  class ProjectMilestone < Klass
    include ClassHelpers

    def inspect
      "#<ProjectMilestone id: #{id}, title: #{title}>"
    end

    def project
      client.projects.show project_id
    end

    def update(query = {})
      project_id = collect_project_id
      update_self client.projects.milestones.update(project_id, id, query)
    end

    def promote
      project_id = collect_project_id
      client.projects.milestones.promote(project_id, id)
    end

    def issues
      project_id = collect_project_id
      client.projects.milestones.issues(project_id, id)
    end

    def merge_requests
      project_id = collect_project_id
      client.projects.milestones.merge_requests(project_id, id)
    end

    def burndown
      project_id = collect_project_id
      client.projects.milestones.burndown(project_id, id)
    end

    def delete
      project_id = collect_project_id
      client.projects.milestones.delete(project_id, id)
    end

    date_time_attrs %i[updated_at created_at due_date start_date]

    help do
      subtitle 'Project Project Milestone'
      option 'project', 'Show project for milestone.'
      option 'issues', 'Show issues assigned to milestone.'
      option 'merge_requests', 'Show merge requests assigned to milestone.'
      option 'promote', 'Promote to group milestone.'
      option 'delete', 'Delete this milestone.'
      option 'burndown', 'Show burndown events.'
      option 'update', 'Update this this milestone. [Hash]'
    end
  end
end
