# Top namespace
module LabClient
  # Specifics
  class ProjectMilestones < Common
    doc 'Show' do
      title 'Merge Requests'
      desc 'Gets all merge requests assigned to a single project milestone. [Project ID, Project Milestone ID]'
      example 'client.projects.milestones.merge_requests(16, 1)'
      result <<~DOC
        => [#<MergeRequest id: 7, title: Update README.md, state: opened>]
      DOC
    end

    doc 'Show' do
      desc 'via ProjectMilestone'
      example <<~DOC
        milestone = client.projects.milestones.show(16,1)
        milestone.merge_requests
      DOC
    end

    def merge_requests(project_id, milestone_id)
      milestone_id = format_id(milestone_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/milestones/#{milestone_id}/merge_requests", MergeRequest)
    end
  end
end
