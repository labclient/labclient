# Top namespace
module LabClient
  # Specifics
  class ProjectMilestones < Common
    doc 'Delete' do
      desc 'Deletes an existing project milestone. [Project ID, Project Milestone ID]'
      example <<~DOC
        client.projects.milestones.delete(16,2)
      DOC
    end

    doc 'Delete' do
      desc 'via Project Milestone'
      example <<~DOC
        milestone = client.projects.milestones.show(16,1)
        milestone.delete
      DOC
    end

    def delete(project_id, milestone_id)
      milestone_id = format_id(milestone_id)
      project_id = format_id(project_id)
      client.request(:delete, "projects/#{project_id}/milestones/#{milestone_id}")
    end
  end
end
