# Top namespace
module LabClient
  # Specifics
  class ProjectMilestones < Common
    doc 'Show' do
      title 'Issues'
      desc 'Gets all issues assigned to a single project milestone. [Project ID, Project Milestone ID]'
      example 'client.projects.milestones.issues(16, 1)'
      result <<~DOC
        => [#<Issue id: 36, title: Implemented high-level process improvement, state: opened>, #<Issue id: 35, title: Proactive didactic implementation, state: opened>]
      DOC
    end

    doc 'Show' do
      desc 'via ProjectMilestone'
      example <<~DOC
        milestone = client.projects.milestones.show(16,1)
        milestone.issues
      DOC
    end

    def issues(project_id, milestone_id)
      milestone_id = format_id(milestone_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/milestones/#{milestone_id}/issues", Issue)
    end
  end
end
