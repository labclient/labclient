# Top namespace
module LabClient
  # Inspect Helper
  class ProjectVariables < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def variables
      ProjectVariables.new(client)
    end
  end
end
