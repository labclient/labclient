# Top namespace
module LabClient
  # Inspect Helper
  class ProjectVariable < Klass
    include ClassHelpers
    def inspect
      "#<ProjectVariable key: #{key}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show project_id
    end

    def update(query)
      project_id = collect_project_id

      update_self client.projects.variables.update(project_id, key, query)
    end

    def delete
      project_id = collect_project_id

      client.projects.variables.delete(project_id, key)
    end

    def show
      project_id = collect_project_id

      client.projects.variables.show(project_id, key)
    end

    help do
      subtitle 'ProjectVariable'
      option 'project', 'Show Project'
      option 'update', 'Update this variable [Hash]'
      option 'delete', 'Delete this variable'
      option 'show', 'Get the details'
    end
  end
end
