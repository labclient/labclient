# Top namespace
module LabClient
  # Specifics
  class ProjectVariables < Common
    doc 'Create' do
      desc 'Adds a variable to a group. [Project ID, Hash]'
      example <<~DOC
        client.projects.variables.create(
          16,
          key: :rawr,
          value: :text
        )
      DOC

      result '=>  #<ProjectVariable key: rawr>'
    end

    doc 'Create' do
      markdown <<~DOC

        | Attribute         | Type    | required | Description           |
        |-------------------|---------|----------|-----------------------|
        | key               | string  | yes      | The `key` of a variable; must have no more than 255 characters; only `A-Z`, `a-z`, `0-9`, and `_` are allowed |
        | value             | string  | yes      | The value of a variable |
        | variable_type     | string  | no       | The type of a variable. Available types are: env_var (default) and file |
        | protected         | boolean | no       | Whether the variable is protected |
        | masked            | boolean | no       | Whether the variable is masked |
        | environment_scope | string  | no       | The environment_scope of the variable |
      DOC
    end

    doc 'Create' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(100)
        variable = project.variable_create(
          key: :rawr,
          value: :text
        )
      DOC
    end

    def create(project_id, query)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/variables", ProjectVariable, query)
    end
  end
end
