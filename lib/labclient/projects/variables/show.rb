# Top namespace
module LabClient
  # Specifics
  class ProjectVariables < Common
    doc 'Show' do
      desc 'Gets a variable of a project. [Project ID, Variable Key]'
      example 'client.projects.variables.show(16, :rawr)'

      result '=>  #<ProjectVariable key: 3, name: rawr>'
    end

    doc 'Show' do
      desc 'Via Project [Variable ID]'
      example <<~DOC
        project = client.projects.show(16)
        variable = project.variable_show(1)
      DOC
    end

    def show(project_id, variable_id)
      project_id = format_id(project_id)
      variable_id = format_id(variable_id)

      client.request(:get, "projects/#{project_id}/variables/#{variable_id}", ProjectVariable)
    end
  end
end
