# Top namespace
module LabClient
  # Specifics
  class ProjectVariables < Common
    doc 'Update' do
      desc 'Updates a variable of a group. [Project ID, Variable Key, Params]'
      example 'client.projects.variables.update(16, :rawr, value: "planet bob")'

      result '=> #<ProjectVariable id: rawr>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute         | Type    | required | Description             |
        |-------------------|---------|----------|-------------------------|
        | value             | string  | yes      | The value of a variable |
        | variable_type     | string  | no       | The type of a variable. Available types are: env_var (default) and file |
        | protected         | boolean | no       | Whether the variable is protected |
        | masked            | boolean | no       | Whether the variable is masked |
        | environment_scope | string  | no       | The environment_scope of the variable |
      DOC
    end

    doc 'Update' do
      desc 'Via Project [Variable ID, Params]'
      example <<~DOC
        project = client.projects.show(16)
        variable = project.variable_update(4, value: "switchy")
      DOC
    end

    doc 'Update' do
      desc 'Via ProjectVariable [Params]'
      example <<~DOC
        variable = client.projects.variables.show(100, 4)
        variable.update(value: "switchy")
      DOC
    end

    def update(project_id, variable_id, query)
      project_id = format_id(project_id)
      variable_id = format_id(variable_id)

      client.request(:put, "projects/#{project_id}/variables/#{variable_id}", ProjectVariable, query)
    end
  end
end
