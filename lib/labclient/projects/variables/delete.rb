# Top namespace
module LabClient
  # Specifics
  class ProjectVariables < Common
    doc 'Delete' do
      desc 'Removes a variable from a project. [Project ID, Variable Key]'
      example 'client.projects.variables.delete(16, :rawr)'
    end

    doc 'Delete' do
      desc 'Via Project [Variable ID]'
      example <<~DOC
        project = client.projects.show(100)
        project.variable_delete(3)
      DOC
    end

    doc 'Delete' do
      desc 'Via ProjectVariable'
      example <<~DOC
        variable = client.projects.variables.show(100, 4)
        variable.delete
      DOC
    end

    def delete(project_id, variable_id)
      project_id = format_id(project_id)
      variable_id = format_id(variable_id)

      client.request(:delete, "projects/#{project_id}/variables/#{variable_id}")
    end
  end
end
