# Top namespace
module LabClient
  # Specifics
  class ProjectVariables < Common
    doc 'List' do
      desc 'Gets a list of a project variables. [Project ID]'
      example 'client.projects.variables.list(16)'

      result <<~DOC
        => [#<ProjectVariable key: 2, name: first >, #<ProjectVariable key: 3, name: rawr>]
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.variables
      DOC
    end

    def list(project_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/variables", ProjectVariable)
    end
  end
end
