# Top namespace
# Uses the '/search?scope=projects' endpoint
module LabClient
  # Projects Specifics
  class Projects < Common
    doc 'Search' do
      desc 'Search through all projects, or within a specific project'

      markdown <<~DOC
        Search within project scopes

        - issues
        - merge_requests
        - milestones
        - notes
        - wiki_blobs
        - commits
        - blobs
        - users
      DOC
    end

    doc 'Search' do
      subtitle 'Examples'
      desc 'Search for projects'
      example "client.projects.search('project')"
      result '=> [#<Project id: 311 root/project-search-api>, ... ]'
    end

    doc 'Search' do
      desc 'Search within a project with :issues scope'
      example "client.projects.search_within(311, :issues, 'rawr')"
      result '=> [#<Issue id: 1, title: rawr, state: opened>]'
    end

    doc 'Search' do
      desc 'Search within a project with :notes scope'
      example "client.projects.search_within(311, :notes, 'Sweet String')"
      result '=> [#<Note id: 3, type: Issue>]'
    end

    doc 'Search' do
      desc 'Search via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.search(:notes, 'sweet search')
      DOC
      result '=> [#<Note id: 3, type: Issue>]'
    end

    # Search All Projects
    def search_within(project_id, scope, search_string = '')
      project_id = format_id(project_id)

      scope = scope.to_sym # Case Simplicity
      query = { scope: scope, search: search_string }

      klass = klass_type(scope)

      client.request(:get, "projects/#{project_id}/search", klass, query)
    end

    # Search All Projects
    def search(search_string = '')
      query = {
        scope: :projects,
        search: search_string
      }

      client.request(:get, 'search', Project, query)
    end

    private

    # TODO: - Finish Classes
    def klass_type(scope)
      case scope
      when :issues then Issue
      when :merge_requests then MergeRequest
      when :notes then Note
      when :wiki_blobs, :milestones, :blobs then nil
      when :commits then Commit
      when :users then User
      end
    end
  end
end
