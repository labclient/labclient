# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Update' do
      desc 'Updates an existing project.'
      example 'client.projects.update(299, name: "Space")'
      result '#<Project id: 299 root/space>'

      markdown <<~DOC
        [See official docs for all possible params](https://docs.gitlab.com/ee/api/projects.html#edit-project)

        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | name | string | yes if path is not provided | The name of the new project. Equals path if not provided. |
        | path | string | yes if name is not provided | Repository name for new project. Generated based on name if not provided (generated lowercased with dashes). |
        | description | string | no | Short project description |
        | default_branch | string | no | master by default |

      DOC
    end

    doc 'Update' do
      desc 'Via project'
      example <<~DOC
        project = client.projects.show(299)
        project.update(description: "Dodge this")
      DOC
    end

    def update(project_id, query = {})
      project_id = format_id(project_id)

      client.request(:put, "projects/#{project_id}", Project, query)
    end
  end
end
