# Top namespace
module LabClient
  # Specifics
  class ProjectReleases < Common
    doc 'List' do
      desc 'List project releases. [Project ID]'
      example 'client.projects.releases.list(16)'

      result <<~DOC
        => [#<ProjectRelease tag_name: release, name: release>, ...]
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.releases
      DOC
    end

    def list(project_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/releases", ProjectRelease)
    end
  end
end
