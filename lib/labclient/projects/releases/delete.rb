# Top namespace
module LabClient
  # Specifics
  class ProjectReleases < Common
    doc 'Delete' do
      desc 'Delete a Release. Deleting a Release will not delete the associated tag. [Project ID, Tag Name]'
      example 'client.projects.releases.delete(16, :release)'
    end

    doc 'Delete' do
      desc 'Via Project [Tag Name]'
      example <<~DOC
        project = client.projects.show(16)
        project.release_delete(:release)
      DOC
    end

    doc 'Delete' do
      desc 'Via ProjectRelease'
      example <<~DOC
        release = client.projects.releases.show(16, :release)
        release.delete
      DOC
    end

    def delete(project_id, release_id)
      project_id = format_id(project_id)
      release_id = format_id(release_id)

      client.request(:delete, "projects/#{project_id}/releases/#{release_id}", ProjectRelease)
    end
  end
end
