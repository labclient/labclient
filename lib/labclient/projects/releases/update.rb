# Top namespace
module LabClient
  # Specifics
  class ProjectReleases < Common
    doc 'Update' do
      desc 'Update a Release. [Project ID, Tag Name, Hash]'
      example 'client.projects.releases.update(16, :release, name: "planet bob")'

      result '=> #<ProjectRelease tag_name: release, name: planet bob>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute     | Type            | Required | Description                                                                                               |
        | ------------- | --------------- | -------- | --------------------------------------------------------------------------------------------------------- |
        | tag_name      | string          | yes      | The tag where the release will be created from.                                                           |
        | name          | string          | no       | The release name.                                                                                         |
        | description   | string          | no       | The description of the release.                                                                           |
        | milestones    | array of string | no       | The title of each milestone to associate with the release ([] to remove all milestones from the release). |
        | released_at   | datetime        | no       | The date when the release will be/was ready. Expected in ISO 8601 format (2019-03-15T08:00:00Z).          |
      DOC
    end

    doc 'Update' do
      desc 'Via Project [Tag Name, Hash]'
      example <<~DOC
        project = client.projects.show(16)
        release = project.release_update(:release, name: "another")
      DOC
    end

    doc 'Update' do
      desc 'Via ProjectRelease [Params]'
      example <<~DOC
        release = client.projects.releases.show(16, :release)
        release.update(name: "switchy")
      DOC
    end

    def update(project_id, release_id, query)
      project_id = format_id(project_id)
      release_id = format_id(release_id)

      client.request(:put, "projects/#{project_id}/releases/#{release_id}", ProjectRelease, query)
    end
  end
end
