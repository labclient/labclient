# Top namespace
module LabClient
  # Inspect Helper
  class ProjectRelease < Klass
    include ClassHelpers
    def inspect
      "#<ProjectRelease tag_name: #{tag_name}, name: #{name}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show project_id
    end

    def update(query)
      project_id = collect_project_id

      update_self client.projects.releases.update(project_id, tag_name, query)
    end

    def delete
      project_id = collect_project_id

      client.projects.releases.delete(project_id, tag_name)
    end

    def evidence
      project_id = collect_project_id

      client.projects.releases.evidence(project_id, tag_name)
    end

    def commit
      Commit.new(@table[:commit], response, client)
    end

    def links
      project_id = collect_project_id

      client.projects.release_links.list(project_id, tag_name)
    end

    # User Fields
    user_attrs %i[author]

    date_time_attrs %i[created_at released_at]

    help do
      subtitle 'ProjectRelease'
      option 'project', 'Show Project'
      option 'update', 'Update this release [Hash]'
      option 'delete', 'Delete this release'
      option 'evidence', 'Create evidence this release'
      option 'links', 'Show release links for this release'
    end
  end
end
