# Top namespace
module LabClient
  # Inspect Helper
  class ProjectReleaseLinks < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def release_links
      ProjectReleaseLinks.new(client)
    end
  end
end
