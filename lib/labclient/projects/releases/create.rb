# Top namespace
module LabClient
  # Specifics
  class ProjectReleases < Common
    doc 'Create' do
      desc 'Create a Release. You need push access to the repository to create a Release. [Project ID, Hash]'
      example <<~DOC
        client.projects.releases.create(16, tag_name: :release)
      DOC

      result '=> #<ProjectRelease tag_name: release, name: release>'
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute          | Type          | Required                       | Description                                                                                                                      |
        | -------------------| ------------- | --------                       | -------------------------------------------------------------------------------------------------------------------------------- |
        | tag_name         | string          | yes                            | The tag where the release will be created from.                                                                                  |
        | ref              | string          | yes, if tag_name doesn't exist | If tag_name doesn't exist, the release will be created from ref. It can be a commit SHA, another tag name, or a branch name.     |
        | name             | string          | no                             | The release name.                                                                                                                |
        | description      | string          | no                             | The description of the release.                                                                                                  |
        | milestones       | array of string | no                             | The title of each milestone the release is associated with.                                                                      |
        | assets:links     | array of hash   | no                             | An array of assets links.                                                                                                        |
        | assets:links:name| string          | required by: assets:links      | The name of the link.                                                                                                            |
        | assets:links:url | string          | required by: assets:links      | The URL of the link.                                                                                                             |
        | assets:links:filepath | string     | no                             | Optional path for a Direct Asset link.                                                                                           |
        | released_at      | datetime        | no                             | The date when the release will be/was ready. Defaults to the current time. Expected in ISO 8601 format (2019-03-15T08:00:00Z).   |
      DOC
    end

    doc 'Create' do
      desc 'Via Project [Hash]'
      example <<~DOC
        project = client.projects.show(100)
        release = project.release_create(tag_name: :release)
      DOC
    end

    def create(project_id, query)
      project_id = format_id(project_id)

      query[:released_at] = query[:released_at].to_time.iso8601 if format_time?(query[:released_at])

      client.request(:post, "projects/#{project_id}/releases", ProjectRelease, query)
    end
  end
end
