# Top namespace
module LabClient
  # Specifics
  class ProjectReleases < Common
    doc 'Show' do
      desc 'Get a Release for the given tag. [Project ID, Tag Name]'
      example 'client.projects.releases.show(16, :release)'

      result '=>  #<ProjectRelease tag_name: release, name: release>'
    end

    doc 'Show' do
      desc 'Via Project [Tag Name]'
      example <<~DOC
        project = client.projects.show(16)
        release = project.release_show(:release)
      DOC
    end

    def show(project_id, release_id)
      project_id = format_id(project_id)
      release_id = format_id(release_id)

      client.request(:get, "projects/#{project_id}/releases/#{release_id}", ProjectRelease)
    end
  end
end
