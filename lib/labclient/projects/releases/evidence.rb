# Top namespace
module LabClient
  # Specifics
  class ProjectReleases < Common
    doc 'Update' do
      title 'Evidence'
      desc 'Create Evidence for an existing Release. [Project ID, Tag Name]'
      example 'client.projects.releases.evidence(16, :release)'
    end

    doc 'Update' do
      desc 'Via Project [Tag Name, Hash]'
      example <<~DOC
        project = client.projects.show(16)
        release = project.release_evidence(:release)
      DOC
    end

    doc 'Update' do
      desc 'Via ProjectRelease [Params]'
      example <<~DOC
        release = client.projects.releases.show(16, :release)
        release.evidence
      DOC
    end

    def evidence(project_id, release_id)
      project_id = format_id(project_id)
      release_id = format_id(release_id)

      client.request(:post, "projects/#{project_id}/releases/#{release_id}/evidence", ProjectRelease)
    end
  end
end
