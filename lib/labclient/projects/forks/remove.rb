# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Remove' do
      desc 'Delete an existing forked from relationship [Project ID]'
      example 'client.projects.fork_remove(309)'
      result '#<Project id: 299 root/space>'
    end

    doc 'Remove' do
      desc 'Via project'
      example <<~DOC
        project = client.projects.show(200)
        project.fork_remove
      DOC
    end

    def fork_remove(project_id)
      project_id = format_id(project_id)

      client.request(:delete, "projects/#{project_id}/fork")
    end
  end
end
