# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Associate' do
      desc 'Create a forked from/to relation between existing projects [Target ID, Source ID]'
      example 'client.projects.fork_existing(309, 10)'
      result '#<Project id: 299 root/space>'
    end

    doc 'Associate' do
      desc 'Via project'
      example <<~DOC
        project = client.projects.show(200)
        project.fork_existing(15)
      DOC
    end

    def fork_existing(project_id, source_id)
      project_id = format_id(project_id)
      source_id = format_id(source_id)

      client.request(:post, "projects/#{project_id}/fork/#{source_id}", Project)
    end
  end
end
