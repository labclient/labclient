# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'List' do
      desc 'List the projects accessible to the calling user that have an established, forked relationship with the specified project'
      example 'client.projects.forks(299)'
      result <<~DOC
        => [
          #<Project id: 302 root/nonconflict>,
          ...
        ]
      DOC
    end

    doc 'List' do
      markdown <<~DOC
        [See Docs for all available params](https://docs.gitlab.com/ee/api/projects.html#list-forks-of-a-project)
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(299)
        project.forks
      DOC
    end

    def forks(project_id, query = {})
      project_id = format_id(project_id)

      query_access_level(query, :min_access_level)

      client.request(:get, "projects/#{project_id}/forks", Project, query)
    end
  end
end
