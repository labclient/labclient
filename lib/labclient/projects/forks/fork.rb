# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Create' do
      desc 'Forks a project into the user namespace of the authenticated user or the one provided.'
      example 'client.projects.fork(299)'
      result '#<Project id: 299 root/space>'

      markdown <<~DOC

        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | namespace | integer/string | yes | The ID or path of the namespace that the project will be forked to |
        | path | string | no | The path that will be assigned to the resultant project after forking |
        | name | string | no | The name that will be assigned to the resultant project after forking |

      DOC
    end

    doc 'Create' do
      desc 'Into specific namespace'
      example <<~DOC
        project = client.projects.fork(299, namespace: 'root', name: 'nonconflict', path: 'nonconflict')
      DOC
    end

    doc 'Create' do
      desc 'Via project'
      example <<~DOC
        project = client.projects.show(200)
        new_project = project.fork
      DOC
    end

    def fork(project_id, query = {})
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/fork", Project, query)
    end
  end
end
