# Top namespace
module LabClient
  # Specifics
  class ProjectRunners < Common
    doc 'List' do
      desc 'List all runners (specific and shared) available in the project. Shared runners are listed if at least one shared runner is defined. [Project ID, Hash]'
      example 'client.projects.runners.list(15)'
      result '[#<Runner id: 1, status: online>, ... ]'

      markdown <<~DOC

        | Attribute  | Type           | Required | Description         |
        |------------|----------------|----------|---------------------|
        | scope    | string         | no       | Deprecated: Use type or status instead. The scope of specific runners to show, one of: active, paused, online, offline; showing all runners if none provided |
        | type     | string         | no       | The type of runners to show, one of: instance_type, group_type, project_type |
        | status   | string         | no       | The status of runners to show, one of: active, paused, online, offline |
        | tag_list | string array   | no       | List of of the runner's tags |
      DOC
    end

    doc 'List' do
      desc 'Via Project [Hash]'
      example <<~DOC
        project = client.projects.show(16)
        project.runners
      DOC
    end

    def list(project_id, query = {})
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/runners", Runner, query)
    end
  end
end
