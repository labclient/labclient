# Top namespace
module LabClient
  # Specifics
  class ProjectRunners < Common
    doc 'Enable' do
      desc 'Enable an available specific runner in the project. [Project ID, Runner ID]'
      example 'client.projects.runners.enable(16, 3)'

      result '=>  #<Runner id: 1, status: online>'
    end

    doc 'Enable' do
      desc 'Via Project [Runner ID]'
      example <<~DOC
        project = client.projects.show(16)
        project.runner_enable(1)
      DOC
    end

    def enable(project_id, runner_id)
      project_id = format_id(project_id)
      runner_id = format_id(runner_id)

      client.request(:post, "projects/#{project_id}/runners", Runner, { runner_id: runner_id })
    end
  end
end
