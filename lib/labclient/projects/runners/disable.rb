# Top namespace
module LabClient
  # Specifics
  class ProjectRunners < Common
    doc 'Disable' do
      desc 'Disable a specific runner from the project. It works only if the project isn’t the only project associated with the specified runner. [Project ID, Runner ID]'
      example 'client.projects.runners.disable(16, 3)'
    end

    doc 'Disable' do
      desc 'Via Project [Runner ID]'
      example <<~DOC
        project = client.projects.show(16)
        project.runner_enable(1)
      DOC
    end

    def disable(project_id, runner_id)
      project_id = format_id(project_id)
      runner_id = format_id(runner_id)

      client.request(:delete, "projects/#{project_id}/runners/#{runner_id}")
    end
  end
end
