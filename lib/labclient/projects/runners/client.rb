# Top namespace
module LabClient
  # Inspect Helper
  class ProjectRunners < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def runners
      ProjectRunners.new(client)
    end
  end
end
