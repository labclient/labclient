# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Update' do
      title 'Start Mirror'
      desc 'Start the pull mirroring process for a Project [Project ID]'
      example 'client.projects.mirror_start(296)'
      result '# => 200'
    end

    doc 'Update' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(25)
        project.mirror_start
      DOC
    end

    def mirror_start(project_id)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/mirror/pull", nil)
    end
  end
end
