# Top namespace
module LabClient
  # Inspect Helper
  class ProjectTrigger < Klass
    include ClassHelpers
    def inspect
      "#<ProjectTrigger id: #{id}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show project_id
    end

    def delete
      project_id = collect_project_id

      client.projects.triggers.delete(project_id, id)
    end

    date_time_attrs %i[created_at updated_at last_used]
    user_attrs %i[owner]

    help do
      subtitle 'ProjectTrigger'
      option 'project', 'Show Project'
      option 'update', 'Update this variable [Hash]'
      option 'delete', 'Delete this variable'
      option 'show', 'Get the details'
    end
  end
end
