# Top namespace
module LabClient
  # Specifics
  class ProjectTriggers < Common
    doc 'Show' do
      desc 'Gets a trigger of a project. [Project ID, Trigger ID]'
      example 'client.projects.triggers.show(16, 2)'

      result '=>  #<ProjectTrigger id: 2>'
    end

    doc 'Show' do
      desc 'Via Project [Trigger ID]'
      example <<~DOC
        project = client.projects.show(16)
        trigger = project.trigger_show(1)
      DOC
    end

    def show(project_id, trigger_id)
      project_id = format_id(project_id)
      trigger_id = format_id(trigger_id)

      client.request(:get, "projects/#{project_id}/triggers/#{trigger_id}", ProjectTrigger)
    end
  end
end
