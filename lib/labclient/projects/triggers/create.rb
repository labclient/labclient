# Top namespace
module LabClient
  # Specifics
  class ProjectTriggers < Common
    doc 'Create' do
      desc 'Adds a trigger to a group. [Project ID, String]'
      example <<~DOC
        client.projects.triggers.create(16, 'sweet description')
      DOC

      result '=>  #<ProjectTrigger id: 1>'
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute   | Type    | required | Description              |
        |-------------|---------|----------|--------------------------|
        | description | string  | yes      | The trigger name         |
      DOC
    end

    doc 'Create' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        trigger = project.trigger_create('sweet description')
      DOC
    end

    def create(project_id, description)
      project_id = format_id(project_id)
      query = { description: description }

      client.request(:post, "projects/#{project_id}/triggers", ProjectTrigger, query)
    end
  end
end
