# Top namespace
module LabClient
  # Inspect Helper
  class ProjectTriggers < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Users Specifics
  class Projects < Common
    def triggers
      ProjectTriggers.new(client)
    end
  end
end
