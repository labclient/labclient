# Top namespace
module LabClient
  # Specifics
  class ProjectTriggers < Common
    doc 'List' do
      desc 'Gets a list of a project triggers. [Project ID]'
      example 'client.projects.triggers.list(16)'

      result <<~DOC
        =>  [#<ProjectTrigger id: 2>]
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.triggers
      DOC
    end

    def list(project_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/triggers", ProjectTrigger)
    end
  end
end
