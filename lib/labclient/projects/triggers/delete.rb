# Top namespace
module LabClient
  # Specifics
  class ProjectTriggers < Common
    doc 'Delete' do
      desc 'Removes a trigger from a project. [Project ID, Trigger ID]'
      example 'client.projects.triggers.delete(16, 1)'
    end

    doc 'Delete' do
      desc 'Via Project [Trigger ID]'
      example <<~DOC
        project = client.projects.show(100)
        project.trigger_delete(3)
      DOC
    end

    doc 'Delete' do
      desc 'Via ProjectTrigger'
      example <<~DOC
        trigger = client.projects.triggers.show(100, 4)
        trigger.delete
      DOC
    end

    def delete(project_id, trigger_id)
      project_id = format_id(project_id)
      trigger_id = format_id(trigger_id)

      client.request(:delete, "projects/#{project_id}/triggers/#{trigger_id}")
    end
  end
end
