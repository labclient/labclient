# Top namespace
module LabClient
  # Specifics
  class ProjectAccessRequests < Common
    doc 'List' do
      desc 'Gets a list of access requests viewable by the authenticated user. [Project ID]'
      example 'client.projects.access_requests.list(16)'
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.access_requests
      DOC
    end

    def list(project_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/access_requests", ProjectAccessRequest)
    end
  end
end
