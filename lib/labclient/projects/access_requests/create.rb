# Top namespace
module LabClient
  # Specifics
  class ProjectAccessRequests < Common
    doc 'Create' do
      desc 'Requests access for the authenticated user to a or project.'
      example <<~DOC
        client.projects.access_requests.create(310)
      DOC
    end

    doc 'Create' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(100)
        project.request_access
      DOC
    end

    def create(project_id)
      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/access_requests", ProjectAccessRequest)
    end
  end
end
