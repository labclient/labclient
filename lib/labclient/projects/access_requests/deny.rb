# Top namespace
module LabClient
  # Specifics
  class ProjectAccessRequests < Common
    doc 'Deny' do
      desc 'Deny an access request for the given user. [Project ID, User ID]'
      example 'client.projects.access_requests.deny(310, 3)'
    end

    doc 'Deny' do
      desc 'Via ProjectAccessRequest [Access Level]'
      example <<~DOC
        request = client.projects.access_requests(16).first
        request.deny
      DOC
    end

    def deny(project_id, user_id)
      project_id = format_id(project_id)
      user_id = format_id(user_id)

      client.request(:delete, "projects/#{project_id}/access_requests/#{user_id}/deny")
    end
  end
end
