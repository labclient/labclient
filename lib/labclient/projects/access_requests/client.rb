# Top namespace
module LabClient
  # Inspect Helper
  class ProjectAccessRequests < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def access_requests
      ProjectAccessRequests.new(client)
    end
  end
end
