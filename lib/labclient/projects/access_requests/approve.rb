# Top namespace
module LabClient
  # Specifics
  class ProjectAccessRequests < Common
    doc 'Approve' do
      desc 'Approves an access request for the given user. [Project ID, User ID, Access Level]'
      example 'client.projects.access_requests.approve(310, 3)'

      result '=>  #<ProjectAccessRequest id: 3, name: planet bob>'
    end

    doc 'Approve' do
      markdown <<~DOC
        | Attribute    | Type    | Description                                                 |
        | ----------             | ---------                                                   |
        | access_level | integer | A valid access level (defaults: 30, developer access level) |
      DOC
    end

    doc 'Approve' do
      desc 'Via ProjectAccessRequest [Access Level]'
      example <<~DOC
        request = client.projects.access_requests(16).first
        request.approve(:developer)
      DOC
    end

    def approve(project_id, user_id, access_level = :developer)
      project_id = format_id(project_id)
      user_id = format_id(user_id)

      query = { access_level: access_level }
      query_access_level(query, :access_level)

      client.request(:put, "projects/#{project_id}/access_requests/#{user_id}/approve", ProjectAccessRequest, query)
    end
  end
end
