# Top namespace
module LabClient
  # Inspect Helper
  class ProjectAccessRequest < Klass
    include ClassHelpers
    def inspect
      "#<ProjectAccessRequest id: #{id}, name: #{name}, state: #{state}>"
    end

    def user
      client.users.show(id)
    end

    def approve(access_level = :developer)
      project_id = collect_project_id
      client.projects.access_requests.approve(project_id, id, access_level)
    end

    def deny
      project_id = collect_project_id
      client.projects.access_requests.deny(project_id, id)
    end

    date_time_attrs %i[created_at]

    help do
      subtitle 'ProjectAccessRequest'
      option 'user', 'Show user for access request'
      option 'approve', 'Accept access request [Access Level]'
      option 'deny', 'Deny request'
    end
  end
end
