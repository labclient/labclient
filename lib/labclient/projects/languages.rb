# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Show' do
      title 'Languages'
      desc 'Get languages used in a project with percentage value.'
      example 'client.projects.languages(299)'
      result <<~DOC
        => {:Ruby=>73.41, :CSS=>15.59, :HTML=>10.72, :JavaScript=>0.28}
      DOC
    end

    doc 'Show' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(299)
        project.languages
      DOC
    end

    def languages(project_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/languages")
    end
  end
end
