# Top namespace
module LabClient
  # Inspect Helper
  class ProjectMirror < Klass
    include ClassHelpers
    def inspect
      "#<ProjectMirror id: #{id}, status: #{update_status}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show project_id
    end

    def update(query)
      project_id = collect_project_id

      update_self client.projects.mirrors.update(project_id, id, query)
    end

    date_time_attrs %i[last_update_started_at last_successful_update_at]

    help do
      subtitle 'ProjectMirror'
      option 'project', 'Show Project'
      option 'update', 'Update this Mirror [Hash]'
    end
  end
end
