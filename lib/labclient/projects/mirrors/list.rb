# Top namespace
module LabClient
  # Specifics
  class ProjectMirrors < Common
    doc '' do
      markdown '**Note:** __This is for push mirroring__'
    end

    doc 'List' do
      desc 'List remote mirrors and their statuses. [Project ID]'
      example 'client.projects.mirrors.list(16)'

      result <<~DOC
        => [#<ProjectMirror id: 1, status: finished>]
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.mirrors
      DOC
    end

    def list(project_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/remote_mirrors", ProjectMirror)
    end
  end
end
