# Top namespace
module LabClient
  # Specifics
  class ProjectMirrors < Common
    doc 'Create' do
      desc 'Create a remote mirror for a project. The mirror will be disabled by default. [Project ID, Hash]'
      example <<~DOC
        client.projects.mirrors.create(
          310,
          url: 'https://labclient/test'
        )
      DOC

      result '=>  #<ProjectMirror id: 2, status: none>'
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute              | Type    | Required   | Description                                         |
        | ----------             | -----   | ---------  | ------------                                        |
        | url                    | String  | yes        | The URL of the remote repository to be mirrored.    |
        | enabled                | Boolean | no         | Determines if the mirror is enabled.                |
        | only_protected_branches| Boolean | no         | Determines if only protected branches are mirrored. |
      DOC
    end

    doc 'Create' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(100)
        project.mirror_create(310,  url: 'https://labclient/test')
      DOC
    end

    def create(project_id, query)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/remote_mirrors", ProjectMirror, query)
    end
  end
end
