# Top namespace
module LabClient
  # Specifics
  class ProjectMirrors < Common
    doc 'Update' do
      desc 'Toggle a remote mirror on or off, or change which types of branches are mirrored: [Project ID, Mirror ID, Params]'
      example 'client.projects.mirrors.update(310, 3, enabled: true)'

      result '=>  #<ProjectMirror id: 3, name: planet bob>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute              | Type    | Required   | Description                                         |
        | ----------             | -----   | ---------  | ------------                                        |
        | url                    | String  | yes        | The URL of the remote repository to be mirrored.    |
        | enabled                | Boolean | no         | Determines if the mirror is enabled.                |
        | only_protected_branches| Boolean | no         | Determines if only protected branches are mirrored. |
      DOC
    end

    doc 'Update' do
      desc 'Via Project [Mirror ID, Params]'
      example <<~DOC
        project = client.projects.show(16)
        mirror = project.mirror_update(2, enabled: false)
      DOC
    end

    doc 'Update' do
      desc 'Via ProjectMirror [Params]'
      example <<~DOC
        mirror = client.projects.mirrors.list(310).first
        mirror.update(enabled: false)
      DOC
    end

    def update(project_id, mirror_id, query)
      project_id = format_id(project_id)
      mirror_id = format_id(mirror_id)

      client.request(:put, "projects/#{project_id}/remote_mirrors/#{mirror_id}", ProjectMirror, query)
    end
  end
end
