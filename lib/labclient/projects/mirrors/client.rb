# Top namespace
module LabClient
  # Inspect Helper
  class ProjectMirrors < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def mirrors
      ProjectMirrors.new(client)
    end
  end
end
