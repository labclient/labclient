# Top namespace
module LabClient
  # Inspect Helper
  class ProjectServices < Common
    include ClassHelpers

    doc '' do
      subtitle 'Usage'

      markdown <<~DOC
        The same methods are used for configuring all of the services. The payloads and the 'slug' will be different, but each of these methods will be used universally.

        See the [services](https://docs.gitlab.com/ee/api/services.html) documentation for individual options and service/slug availability.

        The *Update* method is used for creating and updating service settings
      DOC
    end
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def services
      ProjectServices.new(client)
    end
  end
end
