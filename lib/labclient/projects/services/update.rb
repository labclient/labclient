# Top namespace
module LabClient
  # Specifics
  class ProjectServices < Common
    doc 'Create/Update' do
      desc 'Set service settings for a project. [Project ID, Service Slug, Params]'
      example 'client.projects.services.update(16, :asana, api_key: "123123")'

      result '=>  #<ProjectService slug: asana, title: Asana, active: true>'
    end

    doc 'Update' do
      markdown <<~DOC
        Required Parameters will be unique to the service you are configuring.
      DOC
    end

    doc 'Update' do
      desc 'Via Project [Service Slug, Params]'
      example <<~DOC
        project = client.projects.show(16)
        service = project.service_update(:asana, api_key: "another")
      DOC
    end

    doc 'Update' do
      desc 'Via ProjectService [Params]'
      example <<~DOC
        service = client.projects.services.show(16, :asana)
        service.update(api_key: "switchy")
      DOC
    end

    def update(project_id, service_slug, query)
      project_id = format_id(project_id)
      service_slug = format_id(service_slug)

      client.request(:put, "projects/#{project_id}/services/#{service_slug}", ProjectService, query)
    end
  end
end
