# Top namespace
module LabClient
  # Specifics
  class ProjectServices < Common
    doc 'Delete' do
      desc 'Delete service for a project. [Project ID, Service Slug]'
      example 'client.projects.services.delete(16, :asana)'
    end

    doc 'Delete' do
      desc 'Via Project [Service Slug]'
      example <<~DOC
        project = client.projects.show(16)
        project.service_delete(:asana)
      DOC
    end

    doc 'Delete' do
      desc 'Via ProjectService'
      example <<~DOC
        service = client.projects.services.show(100, :asana)
        service.delete
      DOC
    end

    def delete(project_id, service_slug)
      project_id = format_id(project_id)
      service_slug = format_id(service_slug)

      client.request(:delete, "projects/#{project_id}/services/#{service_slug}")
    end
  end
end
