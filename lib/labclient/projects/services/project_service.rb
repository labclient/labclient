# Top namespace
module LabClient
  # Inspect Helper
  class ProjectService < Klass
    include ClassHelpers
    def inspect
      "#<ProjectService slug: #{slug}, title: #{title}, active: #{active}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show(project_id)
    end

    def update(query)
      project_id = collect_project_id

      update_self(client.projects.services.update(project_id, slug, query))
    end

    def delete
      project_id = collect_project_id

      client.projects.services.delete(project_id, slug)
    end

    date_time_attrs %i[created_at updated_at]

    help do
      subtitle 'ProjectService'
      option 'project', 'Show Project'
      option 'update', 'Update this service [Hash]'
      option 'delete', 'Delete this service'
    end
  end
end
