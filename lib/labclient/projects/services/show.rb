# Top namespace
module LabClient
  # Specifics
  class ProjectServices < Common
    doc 'Show' do
      desc 'Get service settings for a project. [Project ID, Service Slug]'
      example 'client.projects.services.show(100, :asana)'

      result '=>  #<ProjectService slug: asana, title: Asana, active: false>'
    end

    doc 'Show' do
      desc 'Via Project [Service Slug]'
      example <<~DOC
        project = client.projects.show(16)
        service = project.service_show(:asana)
      DOC
    end

    def show(project_id, service_slug)
      project_id = format_id(project_id)
      service_slug = format_id(service_slug)

      client.request(:get, "projects/#{project_id}/services/#{service_slug}", ProjectService)
    end
  end
end
