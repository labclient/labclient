# Top namespace
module LabClient
  # Specifics
  class ProjectServices < Common
    doc 'List' do
      desc 'Get a list of all active project services. [Project ID]'
      example 'client.projects.services.list(16)'

      result <<~DOC
        => [#<ProjectService id: 2, name: first >, #<ProjectService id: 3, name: rawr>]
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.services
      DOC
    end

    def list(project_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/services", ProjectService)
    end
  end
end
