# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Show' do
      title 'Events'
      desc 'See Events for additional info.'
      example 'client.events.project(1)'
    end

    doc 'Show' do
      desc 'Via project object'
      example <<~DOC
        project = client.projects.show(10)
        project.events
      DOC
    end
  end
end
