# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Update' do
      title 'Unarchive'
      example 'client.projects.unarchive(25)'
      result '#<Project id: 25 badlands/chaurushunter>'
    end

    doc 'Update' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(299)
        project.unarchive
      DOC
    end

    def unarchive(project_id)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/unarchive", Project)
    end
  end
end
