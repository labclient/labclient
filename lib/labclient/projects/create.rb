# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Create' do
      desc 'Creates a new project owned by the authenticated user.'
      example 'client.projects.create(name: "Galaxy", description: "On orions belt")'
      result '#<Project id: 299 root/galaxy>'

      markdown <<~DOC
        [See official docs for all possible params](https://docs.gitlab.com/ee/api/projects.html#create-project)

        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | name | string | yes if path is not provided | The name of the new project. Equals path if not provided. |
        | path | string | yes if name is not provided | Repository name for new project. Generated based on name if not provided (generated lowercased with dashes). |
        | description | string | no | Short project description |
        | namespace_id | integer | no | Namespace for the new project (defaults to the current user's namespace) |
        | default_branch | string | no | master by default |

      DOC
    end

    doc 'Create' do
      subtitle 'Examples'
      desc 'Specific Namespace'
      example 'client.projects.create(name: "Winter", description: "Is coming", namespace_id: 80 )'
    end

    doc 'Create' do
      desc 'Visibility'
      example 'client.projects.create(name: "Verbal", description: "hes gone", visibility:  :private)'
    end

    doc 'Create' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.show(51)
        group.project_create(name: "Hobby", description: "We do have time for your darned hobbies!")
      DOC

      example 'client.projects.create(name: "Verbal", description: "hes gone", visibility:  :private)'
    end

    doc 'Create' do
      title 'For User'
      desc 'Creates a new project owned by the specified user [user id, params]'
      example 'client.projects.create_for_user(1, name: "Rule of battle", description: "WHOO-HOO! IM RIGHT HERE")'
    end

    def create_for_user(user_id, query = {})
      client.request(:post, "projects/user/#{user_id}", Project, query)
    end

    # TODO: Test Project/Group Creation
    # TODO: Create helper for group
    def create(query = {})
      format_query_id(:namespace_id, query)
      client.request(:post, 'projects', Project, query)
    end
  end
end
