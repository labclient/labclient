# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Resources' do
      desc 'There are a lot of resources for projects'

      markdown <<~DOC
        | Name | Description |
        | ----  | ----------- |
        | <a target="_" href="/#Project_Hooks">Hooks</a> | Project specific Hooks and Webhooks |
        | <a target="_" href="/#Stars">Stars</a> | Stars, List all stars, and project starrers |
        | <a target="_" href="/#Forks">Forks</a> | Forks |
        | <a target="_" href="/#Push_Rules">Push Rules</a> | Push Rules |
        | <a target="_" href="/#Deploy_Keys">Deploy Keys</a> | Deploy Keys |
        | <a target="_" href="/#Project_Runners">Runners</a> | Project Specific Runners |
        | <a target="_" href="/#Project_Mirrors">Mirrors</a> | Push Mirrors |
        | <a target="_" href="/#Project_Hooks">Hooks</a> | Hooks |
        | <a target="_" href="/#Project_Badges">Badges</a> | Badges |
        | <a target="_" href="/#Project_Access_Requests">Access Requests</a> | Access Requests |
      DOC
    end
  end
end
