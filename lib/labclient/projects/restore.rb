# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Delete' do
      title 'Restore'
      desc 'Restores project marked for deletion.'
      example 'client.projects.restore(25)'
      result '#<Project id: 25 badlands/chaurushunter>'
    end

    doc 'Delete' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(299)
        project.restore
      DOC
    end

    def restore(project_id)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/restore", Project)
    end
  end
end
