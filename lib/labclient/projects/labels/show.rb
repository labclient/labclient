# Top namespace
module LabClient
  # Specifics
  class ProjectLabels < Common
    doc 'Show' do
      desc 'Get a single label for a given project. [Project ID, Label ID]'
      example 'client.projects.labels.show(16, 7)'

      result '=>  #<ProjectLabel id: 2, name: feature>'

      markdown <<~DOC
        | Attribute               | Type    | Description                                                                  |
        | ---------               | ------- | ---------------------                                                        |
        | with_counts             | boolean | Whether or not to include issue and merge request counts. Defaults to false. |
        | include_ancestor_groups | boolean | Include ancestor groups. Defaults to true.                                   |
      DOC
    end

    doc 'Show' do
      desc 'Via Project [Label ID]'
      example <<~DOC
        project = client.projects.show(16)
        label = project.label(7)
      DOC
    end

    def show(project_id, label_id, query = {})
      project_id = format_id(project_id)
      label_id = format_id(label_id)

      client.request(:get, "projects/#{project_id}/labels/#{label_id}", ProjectLabel, query)
    end
  end
end
