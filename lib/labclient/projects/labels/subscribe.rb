# Top namespace
module LabClient
  # Specifics
  class ProjectLabels < Common
    doc 'Subscribe' do
      desc 'Subscribes the authenticated user to a label to receive notifications [Project ID, Label ID]'
      example 'client.projects.labels.subscribe(16, 7)'

      result '=> #<ProjectLabel id: 7, name: switchy>'
    end

    doc 'Subscribe' do
      desc 'Via Project [Label ID]'
      example <<~DOC
        project = client.projects.show(16)
        label = project.label_subscribe(7)
      DOC
    end

    doc 'Subscribe' do
      desc 'Via ProjectLabel'
      example <<~DOC
        label = client.projects.labels.show(16,7)
        label.subscribe
      DOC
    end

    def subscribe(project_id, label_id)
      project_id = format_id(project_id)
      label_id = format_id(label_id)

      client.request(:post, "projects/#{project_id}/labels/#{label_id}/subscribe", ProjectLabel)
    end
  end
end
