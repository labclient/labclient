# Top namespace
module LabClient
  # Specifics
  class ProjectLabels < Common
    doc 'Create' do
      desc 'Creates a new label for the given repository with the given name and color. [Project ID, Hash]'
      example <<~DOC
        client.projects.labels.create(16, name: :feature, color: :green)
      DOC

      result '=> #<ProjectLabel id: 3, name: feature>'
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute     | Type    | Required | Description                  |
        | ------------- | ------- | -------- | ---------------------------- |
        | name          | string  | yes      | The name of the label        |
        | color         | string  | yes      | The color of the label given in 6-digit hex notation or CSS Color Names|
        | description   | string  | no       | The description of the label |
        | priority      | integer | no       | The priority of the label. Must be greater or equal than zero or null to remove the priority. |
      DOC
    end

    doc 'Create' do
      desc 'Via Project [Hash]'
      example <<~DOC
        project = client.projects.show(16)
        label = project.label_create(name: :bug, color: :red)
      DOC
    end

    def create(project_id, query)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/labels", ProjectLabel, query)
    end
  end
end
