# Top namespace
module LabClient
  # Specifics
  class ProjectLabels < Common
    doc 'Delete' do
      desc 'Deletes a label with a given name. [Project ID, Label ID]'
      example 'client.projects.labels.delete(16, 7)'
    end

    doc 'Delete' do
      desc 'Via Project [Label ID]'
      example <<~DOC
        project = client.projects.show(16)
        project.label_delete(7)
      DOC
    end

    doc 'Delete' do
      desc 'Via ProjectLabel'
      example <<~DOC
        label = client.projects.labels.show(16, 7)
        label.delete
      DOC
    end

    def delete(project_id, label_id)
      project_id = format_id(project_id)
      label_id = format_id(label_id)

      client.request(:delete, "projects/#{project_id}/labels/#{label_id}", ProjectLabel)
    end
  end
end
