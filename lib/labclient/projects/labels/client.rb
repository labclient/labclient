# Top namespace
module LabClient
  # Inspect Helper
  class ProjectLabels < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def labels
      ProjectLabels.new(client)
    end
  end
end
