# Top namespace
module LabClient
  # Specifics
  class ProjectLabels < Common
    doc 'Unsubscribe' do
      desc 'Unsubscribes the authenticated user from a label to not receive notifications from it [Project ID, Label ID]'
      example 'client.projects.labels.unsubscribe(16, 7)'

      result '=> #<ProjectLabel id: 7, name: switchy>'
    end

    doc 'Unsubscribe' do
      desc 'Via Project [Label ID]'
      example <<~DOC
        project = client.projects.show(16)
        project.label_subscribe(7)
      DOC
    end

    doc 'Unsubscribe' do
      desc 'Via ProjectLabel'
      example <<~DOC
        label = client.projects.labels.show(16,7)
        label.unsubscribe
      DOC
    end

    def unsubscribe(project_id, label_id)
      project_id = format_id(project_id)
      label_id = format_id(label_id)

      client.request(:post, "projects/#{project_id}/labels/#{label_id}/unsubscribe", ProjectLabel)
    end
  end
end
