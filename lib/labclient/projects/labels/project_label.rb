# Top namespace
module LabClient
  # Inspect Helper
  class ProjectLabel < Klass
    include ClassHelpers
    def inspect
      "#<ProjectLabel id: #{id}, name: #{name}>"
    end

    def project
      project_id = collect_project_id
      client.projects.show project_id
    end

    def update(query)
      project_id = collect_project_id

      update_self client.projects.labels.update(project_id, id, query)
    end

    def delete
      project_id = collect_project_id

      client.projects.labels.delete(project_id, id)
    end

    def promote
      project_id = collect_project_id

      client.projects.labels.promote(project_id, id)
    end

    def subscribe
      project_id = collect_project_id

      client.projects.labels.subscribe(project_id, id)
    end

    def unsubscribe
      project_id = collect_project_id

      client.projects.labels.unsubscribe(project_id, id)
    end

    help do
      subtitle 'ProjectLabel'
      option 'project', 'Show Project'
      option 'update', 'Update this label [Hash]'
      option 'delete', 'Delete this label'
      option 'promote', 'Promote this label'
      option 'subscribe', 'Subscribe to label notifications'
      option 'unsubscribe', 'Unsubscribe to label notifications'
    end
  end
end
