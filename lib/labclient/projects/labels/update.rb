# Top namespace
module LabClient
  # Specifics
  class ProjectLabels < Common
    doc 'Update' do
      desc 'Updates an existing label with new name or new color. At least one parameter is required, to update the label. [Project ID, Label ID, Hash]'
      example 'client.projects.labels.update(16, 7, new_name: "planet bob")'

      result '=> #<ProjectLabel id: 7, name: planet bob>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute     | Type    | Required | Description                  |
        | ------------- | ------- | -------- | ---------------------------- |
        | new_name      | string  | yes      | The name of the label        |
        | color         | string  | yes      | The color of the label given in 6-digit hex notation or CSS Color Names|
        | description   | string  | no       | The description of the label |
        | priority      | integer | no       | The priority of the label. Must be greater or equal than zero or null to remove the priority. |
      DOC
    end

    doc 'Update' do
      desc 'Via Project [Label ID, Hash]'
      example <<~DOC
        project = client.projects.show(16)
        label = project.label_update(7, new_name: "another")
      DOC
    end

    doc 'Update' do
      desc 'Via ProjectLabel [Hash]'
      example <<~DOC
        label = client.projects.labels.show(16, 7)
        label.update(new_name: "switchy")
      DOC
    end

    def update(project_id, label_id, query)
      project_id = format_id(project_id)
      label_id = format_id(label_id)

      client.request(:put, "projects/#{project_id}/labels/#{label_id}", ProjectLabel, query)
    end
  end
end
