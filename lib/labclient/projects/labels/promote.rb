# Top namespace
module LabClient
  # Specifics
  class ProjectLabels < Common
    doc 'Promote' do
      desc 'Promotes a project label to a group label. [Project ID, Label ID]'
      example 'client.projects.labels.promote(16, 7)'

      result '=> #<ProjectLabel id: 7, name: switchy>'
    end

    doc 'Promote' do
      desc 'Via Project [Label ID]'
      example <<~DOC
        project = client.projects.show(16)
        label = project.label_promote(7)
      DOC
    end

    doc 'Promote' do
      desc 'Via ProjectLabel'
      example <<~DOC
        label = client.projects.labels.show(16, 7)
        label.promote
      DOC
    end

    def promote(project_id, label_id)
      project_id = format_id(project_id)
      label_id = format_id(label_id)

      client.request(:put, "projects/#{project_id}/labels/#{label_id}/promote", ProjectLabel)
    end
  end
end
