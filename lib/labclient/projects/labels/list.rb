# Top namespace
module LabClient
  # Specifics
  class ProjectLabels < Common
    doc 'List' do
      desc 'Get all labels for a given project. [Project ID]'
      example 'client.projects.labels.list(16)'

      result <<~DOC
        => [#<ProjectLabel id: 2, name: feature>, ...]
      DOC

      markdown <<~DOC
        | Attribute               | Type           | Description                                                                  |
        | ---------               | -------        | ---------------------                                                        |
        | with_counts             | boolean        | Whether or not to include issue and merge request counts. Defaults to false. |
        | include_ancestor_groups | boolean        | Include ancestor groups. Defaults to true.                                   |
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.labels
      DOC
    end

    def list(project_id, query = {})
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/labels", ProjectLabel, query)
    end
  end
end
