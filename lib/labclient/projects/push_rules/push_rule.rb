# Top namespace
module LabClient
  # Inspect Helper
  class PushRule < Klass
    include ClassHelpers
    def inspect
      "#<PushRule id: #{id}>"
    end

    def project
      client.projects.show project_id
    end

    def update(query)
      update_self client.projects.push_rules.update(project_id, query)
    end

    def delete
      client.projects.push_rules.delete(project_id)
    end

    date_time_attrs %i[created_at]

    help do
      subtitle 'Push Rules'
      option 'project', 'Show Project'
      option 'update', 'Update this hook [Hash]'
      option 'delete', 'Delete this hook'
    end
  end
end
