# Top namespace
module LabClient
  # Inspect Helper
  class PushRules < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    def push_rules
      PushRules.new(client)
    end
  end
end
