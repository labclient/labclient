# Top namespace
module LabClient
  # Specifics
  class PushRules < Common
    doc 'Delete' do
      desc 'Removes a push rule from a project. [Project ID]'
      example 'client.projects.push_rules.delete(16)'
    end

    doc 'Delete' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        hook = project.push_rule_delete
      DOC
    end

    doc 'Delete' do
      desc 'Via PushRule'
      example <<~DOC
        rule = client.projects.push_rules.show(16, 1)
        rule.delete
      DOC
    end

    def delete(project_id)
      project_id = format_id(project_id)

      client.request(:delete, "projects/#{project_id}/push_rule")
    end
  end
end
