# Top namespace
module LabClient
  # Specifics
  class PushRules < Common
    doc 'Show' do
      desc 'Get the push rules of a project. [Project ID]'
      example 'client.projects.push_rules.show(16)'

      result <<~DOC
        project = client.projects.show(16)
        => [
          #<PushRule id: 5 >, ...
        ]
      DOC
    end

    doc 'Show' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.push_rules
      DOC
    end

    def show(project_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/push_rule", PushRule)
    end
  end
end
