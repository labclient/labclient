# Top namespace
module LabClient
  # Specifics
  class PushRules < Common
    doc 'Update' do
      desc 'Edits a push rule for a specified project. [Project ID, Params]'
      example 'client.projects.push_rules.update(16, max_file_size: 100)'
      result '=>  #<PushRule id: 2>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute                      | Type           | Required | Description |
        | -------------------------------| -------------- | -------- | ----------- |
        | deny_delete_tag                | boolean        | no       | Deny deleting a tag |
        | member_check                   | boolean        | no       | Restrict commits by author (email) to existing GitLab users |
        | prevent_secrets                | boolean        | no       | GitLab will reject any files that are likely to contain secrets |
        | commit_message_regex           | string         | no       | All commit messages must match this, e.g. Fixed \d+\..* |
        | commit_message_negative_regex  | string         | no       | No commit message is allowed to match this, e.g. ssh\:\/\/ |
        | branch_name_regex              | string         | no       | All branch names must match this, e.g. (feature|hotfix)\/* |
        | author_email_regex             | string         | no       | All commit author emails must match this, e.g. @my-company.com$ |
        | file_name_regex                | string         | no       | All committed filenames must **not** match this, e.g. (jar|exe)$ |
        | max_file_size                  | integer        | no       | Maximum file size (MB) |
        | commit_committer_check         | boolean        | no       | Users can only push commits to this repository that were committed with one of their own verified emails. |
        | reject_unsigned_commits        | boolean        | no       | Reject commit when it is not signed through GPG. |
      DOC
    end

    doc 'Update' do
      desc 'Via Project [Params]'
      example <<~DOC
        project = client.projects.show(16)
        project.push_rules_update(deny_delete_tag: true)
      DOC
    end

    doc 'Update' do
      desc 'Via PushRule [Params]'
      example <<~DOC
        rule = client.projects.push_rules.show(16)
        rule.update(deny_delete_tag: true)
      DOC
    end

    def update(project_id, query)
      project_id = format_id(project_id)

      client.request(:put, "projects/#{project_id}/push_rule", PushRule, query)
    end
  end
end
