# Top namespace
module LabClient
  # Specifics
  class PushRules < Common
    doc 'Create' do
      desc 'Adds a push rule to a specified project. [Project ID]'
      example <<~DOC
        client.projects.push_rules.create(16, deny_delete_tag: true, branch_name_regex: '\d+\..*' )
      DOC
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute                      | Type           | Required | Description |
        | -------------------------------| -------------- | -------- | ----------- |
        | deny_delete_tag                | boolean        | no       | Deny deleting a tag |
        | member_check                   | boolean        | no       | Restrict commits by author (email) to existing GitLab users |
        | prevent_secrets                | boolean        | no       | GitLab will reject any files that are likely to contain secrets |
        | commit_message_regex           | string         | no       | All commit messages must match this, e.g. Fixed \d+\..* |
        | commit_message_negative_regex  | string         | no       | No commit message is allowed to match this, e.g. ssh\:\/\/ |
        | branch_name_regex              | string         | no       | All branch names must match this, e.g. (feature|hotfix)\/* |
        | author_email_regex             | string         | no       | All commit author emails must match this, e.g. @my-company.com$ |
        | file_name_regex                | string         | no       | All committed filenames must **not** match this, e.g. (jar|exe)$ |
        | max_file_size                  | integer        | no       | Maximum file size (MB) |
        | commit_committer_check         | boolean        | no       | Users can only push commits to this repository that were committed with one of their own verified emails. |
        | reject_unsigned_commits        | boolean        | no       | Reject commit when it is not signed through GPG. |
      DOC
    end

    doc 'Create' do
      desc 'Via Project [Hash]'
      example <<~DOC
        project = client.projects.show(16)
        project.push_rules_create(deny_delete_tag: false)
      DOC
    end

    def create(project_id, query)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/push_rule", PushRule, query)
    end
  end
end
