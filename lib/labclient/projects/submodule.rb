# Top namespace
module LabClient
  # Specifics
  class Projects < Common
    doc 'Update' do
      title 'Submodule'
      desc 'Update existing submodule reference in repository [Project ID, Submodule, Hash]'
      example 'client.projects.submodule(16, "module/path", branch: "master", commit_sha: "123123")'

      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | `submodule` | string | yes | URL encoded full path to the submodule. For example, `lib%2Fclass%2Erb` |
        | `branch` | string | yes | Name of the branch to commit into |
        | `commit_sha` | string | yes | Full commit SHA to update the submodule to |
        | `commit_message` | string | no | Commit message. If no message is provided, a default one will be set |
      DOC
    end

    doc 'Update' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(25)
        project.housekeeping
      DOC
    end

    def submodule(project_id, submodule_id, query)
      project_id = format_id(project_id)

      client.request(:put, "projects/#{project_id}/repository/submodules/#{submodule_id}", Commit, query)
    end
  end
end
