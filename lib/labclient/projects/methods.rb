# Top namespace
module LabClient
  # Location for Helper Methods
  # rubocop:disable Metrics/ModuleLength
  module ProjectMethods
    def users
      client.projects.users(id)
    end

    def events(query = {})
      client.events.project(id, query)
    end

    def delete
      client.projects.delete(id)
    end

    def restore
      client.projects.restore(id)
    end

    def archive
      client.projects.archive(id)
    end

    def unarchive
      client.projects.unarchive(id)
    end

    def languages
      client.projects.languages(id)
    end

    def star
      client.projects.star(id)
    end

    def unstar
      client.projects.unstar(id)
    end

    def starrers(search = '')
      client.projects.starrers(id, search)
    end

    def update(query = {})
      update_self(client.projects.update(id, query))
    end

    def upload(path)
      client.projects.upload(id, path)
    end

    def share(query)
      client.projects.share(id, query)
    end

    def unshare(group_id)
      client.projects.unshare(id, group_id)
    end

    def fork(query = {})
      client.projects.fork(id, query)
    end

    def forks(query = {})
      client.projects.forks(id, query)
    end

    def fork_remove
      client.projects.fork_remove(id)
    end

    def fork_existing(target_id)
      update_self(client.projects.fork_existing(id, target_id))
    end

    # Issues
    def issues(query = {})
      client.issues.project_issues(id, query)
    end

    def issue_create(query)
      client.issues.create(id, query)
    end

    # Hooks
    def hooks
      client.projects.hooks.list(id)
    end

    def hook_show(hook_id)
      client.projects.hooks.show(id, hook_id)
    end

    def hook_create(query)
      client.projects.hooks.create(id, query)
    end

    def hook_update(hook_id, query)
      client.projects.hooks.update(id, hook_id, query)
    end

    def hook_delete(hook_id)
      client.projects.hooks.delete(id, hook_id)
    end

    def search(scope, search_string = '')
      client.projects.search_within(id, scope, search_string)
    end

    def push_rules
      client.projects.push_rules.show(id)
    end

    def push_rules_delete
      client.projects.push_rules.delete(id)
    end

    def push_rules_create(query = {})
      client.projects.push_rules.create(id, query)
    end

    def push_rules_update(query = {})
      client.projects.push_rules.update(id, query)
    end

    def housekeeping
      client.projects.housekeeping(id)
    end

    def transfer(target_id)
      client.projects.transfer(id, target_id)
    end

    # Mirrors
    def mirror_start
      client.projects.mirror_start(id)
    end

    def mirrors
      client.projects.mirrors.list(id)
    end

    def mirror_create(query)
      client.projects.mirrors.create(id, query)
    end

    def mirror_update(mirror_id, query)
      client.projects.mirrors.update(id, mirror_id, query)
    end

    def snapshot(file_name, wiki = false)
      client.projects.snapshot(id, file_name, wiki)
    end

    def wikis(with_content = false)
      client.wikis.list(id, with_content)
    end

    def wiki(slug)
      client.wikis.show(id, slug)
    end

    def wiki_create(query)
      client.wikis.create(id, query)
    end

    def wiki_update(slug, query)
      client.wikis.update(id, slug, query)
    end

    def wiki_delete(slug)
      client.wikis.delete(id, slug)
    end

    def wiki_upload(path)
      client.wikis.upload(id, path)
    end

    def runners(query = {})
      client.projects.runners.list(id, query)
    end

    def runner_enable(runner_id)
      client.projects.runners.enable(id, runner_id)
    end

    def runner_disable(runner_id)
      client.projects.runners.disable(id, runner_id)
    end

    def pipelines(query = {})
      client.pipelines.list(id, query)
    end

    def pipeline(pipeline_id)
      client.pipelines.show(id, pipeline_id)
    end

    def pipeline_variables(pipeline_id)
      client.pipelines.variables(id, pipeline_id)
    end

    def pipeline_create(query)
      client.pipelines.create(id, query)
    end

    def pipeline_retry(pipeline_id)
      client.pipelines.retry(id, pipeline_id)
    end

    def pipeline_cancel(pipeline_id)
      client.pipelines.cancel(id, pipeline_id)
    end

    def pipeline_delete(pipeline_id)
      client.pipelines.delete(id, pipeline_id)
    end

    def jobs(scope = nil)
      client.jobs.project(id, scope)
    end

    def job(job_id)
      client.jobs.show(id, job_id)
    end

    def job_download_artifacts(job_id, file_path = nil, job_token = nil)
      client.jobs.download_artifacts(id, job_id, file_path, job_token)
    end

    def job_artifacts(job_id, job_token = nil)
      client.jobs.artifacts(id, job_id,  job_token)
    end

    def job_artifacts_latest(branch_name, job_name, file_path = nil, job_token = nil)
      client.jobs.artifacts_latest(id, branch_name, job_name, file_path, job_token)
    end

    def job_artifacts_path(job_id, artifacts_path, file_path = nil)
      client.jobs.artifacts_path(id, job_id, artifacts_path, file_path)
    end

    def job_trace(job_id)
      client.jobs.trace(id, job_id)
    end

    def job_cancel(job_id)
      client.jobs.cancel(id, job_id)
    end

    def job_retry(job_id)
      client.jobs.retry(id, job_id)
    end

    def job_erase(job_id)
      client.jobs.erase(id, job_id)
    end

    def job_keep(job_id)
      client.jobs.keep(id, job_id)
    end

    def job_delete(job_id)
      client.jobs.delete(id, job_id)
    end

    def job_play(job_id)
      client.jobs.play(id, job_id)
    end

    # Members
    def members(query = nil)
      client.members.projects.list(id, query)
    end

    def members_all(query = nil)
      client.members.projects.list_all(id, query)
    end

    def member(user_id)
      client.members.projects.show(id, user_id)
    end

    def member_all(user_id)
      client.members.projects.show_all(id, user_id)
    end

    def member_add(user_id, query)
      client.members.projects.add(id, user_id, query)
    end

    def member_update(user_id, query)
      client.members.projects.update(id, user_id, query)
    end

    def member_delete(user_id)
      client.members.projects.delete(id, user_id)
    end

    # Repository
    def tree(query = {})
      client.repo.tree(id, query)
    end

    def blob(sha, raw = nil)
      client.repo.blob(id, sha, raw)
    end

    def download_archive(file_path = nil, format = '.tar.gz')
      client.repo.archive(id, file_path, format)
    end

    def compare(query)
      client.repo.compare(id, query)
    end

    def contributors(query = {})
      client.repo.contributors(id, query)
    end

    def merge_base(refs)
      client.repo.merge_base(id, refs)
    end

    # Files
    def file(file_path, ref = :main, kind = nil)
      client.files.show(id, file_path, ref, kind)
    end

    def file_create(file_path, query)
      client.files.create(id, file_path, query)
    end

    def file_update(file_path, query)
      client.files.update(id, file_path, query)
    end

    def file_delete(file_path, query)
      client.files.delete(id, file_path, query)
    end

    # Branches
    def branches(search = nil)
      client.branches.list(id, search)
    end

    def branch(branch_id)
      client.branches.show(id, branch_id)
    end

    def branch_create(query)
      client.branches.create(id, query)
    end

    def branch_delete(branch_id)
      client.branches.delete(id, branch_id)
    end

    def branch_delete_merged
      client.branches.delete_merged(id)
    end

    # Protected Branches
    def protected_branches(search = nil)
      client.protected_branches.list(id, search)
    end

    def protected_branch(branch_id)
      client.protected_branches.show(id, branch_id)
    end

    def protect_branch(query)
      client.protected_branches.protect(id, query)
    end

    def unprotect_branch(branch_id)
      client.protected_branches.unprotect(id, branch_id)
    end

    def branch_code_owner_approval(branch_id, approv_id = true)
      client.protected_branches.code_owner_approval(id, branch_id, approv_id)
    end

    # Tags
    def tags(query = {})
      client.tags.list(id, query)
    end

    def tag(tag_id)
      client.tags.show(id, tag_id)
    end

    def tag_create(query)
      client.tags.create(id, query)
    end

    def tag_delete(tag_id)
      client.tags.delete(id, tag_id)
    end

    def tag_release(tag_id, desc_details)
      client.tags.release(id, tag_id, desc_details)
    end

    def tag_update(tag_id, desc_details)
      client.tags.update(id, tag_id, desc_details)
    end

    # Protected Tags
    def protected_tags
      client.protected_tags.list(id)
    end

    def protected_tag(tag_id)
      client.protected_tags.show(id, tag_id)
    end

    def protect_tag(query)
      client.protected_tags.protect(id, query)
    end

    def unprotect_tag(tag_id)
      client.protected_tags.unprotect(id, tag_id)
    end

    # Approvals
    def approvals
      client.approvals.project.show(id)
    end

    def approvals_update(query = {})
      client.approvals.project.update(id, query)
    end

    def approvals_rules
      client.approvals.project.rules(id)
    end

    def approvals_rule_create(query)
      client.approvals.project.create_rule(id, query)
    end

    def approvals_rule_update(approval_rule_id, query)
      client.approvals.project.update_rule(id, approval_rule_id, query)
    end

    def approvals_rule_delete(approval_rule_id)
      client.approvals.project.delete_rule(id, approval_rule_id)
    end

    # Project Badges
    def badges
      client.projects.badges.list(id)
    end

    def badge_show(badge_id)
      client.projects.badges.show(id, badge_id)
    end

    def badge_create(query)
      client.projects.badges.create(id, query)
    end

    def badge_update(badge_id, query)
      client.projects.badges.update(id, badge_id, query)
    end

    def badge_delete(badge_id)
      client.projects.badges.delete(id, badge_id)
    end

    # Project Environments
    def environments
      client.projects.environments.list(id)
    end

    def environment_show(environment_id)
      client.projects.environments.show(id, environment_id)
    end

    alias environment environment_show

    def environment_create(query)
      client.projects.environments.create(id, query)
    end

    def environment_update(environment_id, query)
      client.projects.environments.update(id, environment_id, query)
    end

    def environment_delete(environment_id)
      client.projects.environments.delete(id, environment_id)
    end

    def environment_stop(environment_id)
      client.projects.environments.stop(id, environment_id)
    end

    # Protected Environments
    def protected_environments
      client.protected_environments.list(id)
    end

    def protected_environment(environment_id)
      client.protected_environments.show(id, environment_id)
    end

    def protect_environment(query)
      client.protected_environments.protect(id, query)
    end

    def unprotect_environment(environment_id)
      client.protected_environments.unprotect(id, environment_id)
    end

    # Project Triggers
    def triggers
      client.projects.triggers.list(id)
    end

    def trigger_show(trigger_id)
      client.projects.triggers.show(id, trigger_id)
    end

    def trigger_create(query)
      client.projects.triggers.create(id, query)
    end

    def trigger_delete(trigger_id)
      client.projects.triggers.delete(id, trigger_id)
    end

    # Project Deployments
    def deployments
      client.projects.deployments.list(id)
    end

    def deployment_show(deployment_id)
      client.projects.deployments.show(id, deployment_id)
    end

    def deployment_create(query)
      client.projects.deployments.create(id, query)
    end

    def deployment_update(deployment_id, query)
      client.projects.deployments.update(id, deployment_id, query)
    end

    def deployment_merge_requests(deployment_id)
      client.projects.deployments.merge_requests(id, deployment_id)
    end

    # Project Variables
    def variables
      client.projects.variables.list(id)
    end

    def variable_show(variable_id)
      client.projects.variables.show(id, variable_id)
    end

    def variable_create(query)
      client.projects.variables.create(id, query)
    end

    def variable_update(variable_id, query)
      client.projects.variables.update(id, variable_id, query)
    end

    def variable_delete(variable_id)
      client.projects.variables.delete(id, variable_id)
    end

    # Project Releases
    def releases
      client.projects.releases.list(id)
    end

    def release_show(release_id)
      client.projects.releases.show(id, release_id)
    end

    def release_create(query)
      client.projects.releases.create(id, query)
    end

    def release_update(release_id, query)
      client.projects.releases.update(id, release_id, query)
    end

    def release_delete(release_id)
      client.projects.releases.delete(id, release_id)
    end

    def release_evidence(release_id)
      client.projects.releases.evidence(id, release_id)
    end

    # Project Release Links
    def release_links(release_id)
      client.projects.release_links.list(id, release_id)
    end

    def release_link_show(release_id, link_id)
      client.projects.release_links.show(id, release_id, link_id)
    end

    def release_link_create(release_id, query)
      client.projects.release_links.create(id, release_id, query)
    end

    def release_link_update(release_id, link_id, query)
      client.projects.release_links.update(id, release_id, link_id, query)
    end

    def release_link_delete(release_id, link_id)
      client.projects.release_links.delete(id, release_id, link_id)
    end

    # Project Services
    def services
      client.projects.services.list(id)
    end

    def service_show(service_slug)
      client.projects.services.show(id, service_slug)
    end

    def service_update(service_slug, query)
      client.projects.services.update(id, service_slug, query)
    end

    def service_delete(service_slug)
      client.projects.services.delete(id, service_slug)
    end

    # Submodule
    def submodule(submodule_path, query)
      client.projects.submodule(id, submodule_path, query)
    end

    # Merge Requests
    def merge_requests(query = {})
      client.merge_requests.list_project(id, query)
    end

    def merge_request_create(query)
      client.merge_requests.create(id, query)
    end

    def merge_request_delete(merge_request_iid)
      client.merge_requests.delete(id, merge_request_iid)
    end

    # Access Requests
    def request_access
      client.projects.access_requests.create(id)
    end

    def access_requests
      client.projects.access_requests.list(id)
    end

    # Labels
    def labels(query = {})
      client.projects.labels.list(id, query)
    end

    def label(label_id, query = {})
      client.projects.labels.show(id, label_id, query)
    end

    def label_create(query)
      client.projects.labels.create(id, query)
    end

    def label_delete(label_id)
      client.projects.labels.delete(id, label_id)
    end

    def label_update(label_id, query)
      client.projects.labels.update(id, label_id, query)
    end

    def label_promote(label_id)
      client.projects.labels.promote(id, label_id)
    end

    def label_subscribe(label_id)
      client.projects.labels.subscribe(id, label_id)
    end

    def label_unsubscribe(label_id)
      client.projects.labels.unsubscribe(id, label_id)
    end

    # Registry
    def registry_repositories(query = {})
      client.registry.list(id, query)
    end

    # Milestones
    def milestones(query = {})
      client.projects.milestones.list(id, query)
    end

    # Pipeline Schedules
    def pipeline_schedules(query = {})
      client.projects.pipeline_schedules.list(id, query)
    end

    # Clusters
    def clusters
      client.projects.clusters.list(id)
    end

    def cluster(cluster_id)
      client.projects.clusters.show(id, cluster_id)
    end

    # Templates
    def templates(template_type)
      client.projects.templates.list(id, template_type)
    end

    def template_show(template_type, template_name, query = {})
      client.projects.templates.show(id, template_type, template_name, query)
    end

    # Reload
    def reload
      update_self client.projects.show(id)
    end

    # Parent Helper
    def parent
      case namespace.kind
      when 'user'
        client.users.show(namespace.id)
      when 'group'
        client.groups.show(namespace.id)
      end
    end

    # Snippets
    def snippets
      client.projects.snippets.list(id)
    end

    def snippet_create(query)
      client.projects.snippets.create(id, query)
    end

    # Wait for Import / Set a Hard Limit
    def wait_for_import(total_time = 300, sleep_time = 15)
      # none
      # scheduled
      # failed
      # started
      # finished

      Timeout.timeout(total_time) do
        loop do
          reload
          logger.info('Waiting for Import Status', status: import_status) unless quiet?
          break if %w[none finished].include? import_status
          raise "Import Failed: #{import_error}" if import_status == 'failed'

          sleep sleep_time
        end
      end
    end
  end
  # rubocop:enable Metrics/ModuleLength
end
