# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'List' do
      desc 'Get all issues the authenticated user has access to. By default it returns only issues created by the current user. To get all issues, use parameter scope=all.'
      example 'client.issues.list'
      result <<~DOC
        => #<Issue id: 1, title: Front-line global approach, state: opened>
      DOC
    end

    doc 'List' do
      markdown <<~DOC
        See [API docs here](https://docs.gitlab.com/ee/api/issues.html#list-issues) for full list of attributes

        | Attribute           | Type           | Description                                                                                                                                         |
        | ------------------- | -------------- | --------------------------------------------------------------------------------------------------------------------------------------------------- |
        | labels            | string           | Comma-separated list of label names, issues must have all labels to be returned. None lists all issues with no labels. Any lists all issues with at least one label.|
        | milestone         | string           | The milestone title. None lists all issues with no milestone. Any lists all issues that have an assigned milestone. |
        | order_by          | string           | Return issues ordered by created_at, updated_at, priority, due_date, relative_position, label_priority, milestone_due, popularity, weight fields. Default is created_at |
        | sort              | string           | Return issues sorted in asc or desc order. Default is desc |
        | search            | string           | Search issues against their title and description |
        | created_after     | datetime         | Return issues created on or after the given time. Accepts DateTime |
        | created_before    | datetime         | Return issues created on or before the given time. Accepts DateTime |
        | updated_after     | datetime         | Return issues updated on or after the given time. Accepts DateTime |
        | updated_before    | datetime         | Return issues updated on or before the given time. Accepts DateTime |
      DOC
    end

    doc 'List' do
      desc 'Filter by State'
      example 'client.issues.list(state: :opened)'
    end

    doc 'List' do
      desc 'Filter by Labels'
      example 'client.issues.list(labels: ["11sadf"])'
    end

    doc 'List' do
      desc 'Search in Title'
      example 'client.issues.list(search: "Title Text", in: :title)'
    end

    doc 'List' do
      desc 'Filter by time'
      example 'client.issues.list(created_before: 1.year.ago)'
    end

    def list(query = {})
      query[:created_after] = query[:created_after].to_time.iso8601 if format_time?(query[:created_after])
      query[:created_before] = query[:created_before].to_time.iso8601 if format_time?(query[:created_before])

      client.request(:get, 'issues', Issue, query)
    end
  end
end
