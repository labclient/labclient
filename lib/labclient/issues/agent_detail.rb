# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Show' do
      title 'Agent Detail'
      desc 'Get user agent details [Project ID, Issue IID]'
      example 'client.issues.agent_detail(30, 1)'
      result <<~DOC
        => {:user_agent=>"Mozilla/5.0 (X11; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0", :ip_address=>"10.0.0.14", :akismet_submitted=>false}
      DOC
    end

    doc 'Show' do
      desc 'From Issue Object'
      example 'issue.agent_detail'
      result <<~DOC
        issue = client.issues.show(30,1)
        => #<Issue id: 1, title: Robust incremental superstructure, state: opened>
        issue.agent_detail
        => {:user_agent=>"Mozilla/5.0 (X11; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0", :ip_address=>"10.0.0.14", :akismet_submitted=>false}
      DOC
    end

    # Show Specific
    def agent_detail(project_id, issue_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/issues/#{issue_id}/user_agent_detail")
    end
  end
end
