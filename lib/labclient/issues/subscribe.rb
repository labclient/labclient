# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Update' do
      title 'Subscribe'
      desc 'Subscribes the authenticated user to an issue to receive notifications. [Project ID, Issue IID]'

      example 'client.issues.subscribe(343, 7)'

      result <<~DOC
        => #<Issue id: 3, title: Add new file>
      DOC
    end

    doc 'Update' do
      desc 'Subscribe through Issue'
      example <<~DOC
        issue = client.issues.show(30,2)
        issue.subscribe
      DOC
    end

    def subscribe(project_id, issue_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      client.request(:post, "projects/#{project_id}/issues/#{issue_id}/subscribe", Issue)
    end
  end
end
