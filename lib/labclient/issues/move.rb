# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Move' do
      desc 'Moves an issue to a different project. [Project ID, issue iid, target project id]'
      example 'client.issues.move(5, 1, 6)'
      result <<~DOC
        => #<Issue id: 1, title: Moar Fancy!, state: opened>
      DOC
    end

    doc 'Move' do
      desc 'Through Issue Object [target project id]'
      example <<~DOC
        issue = client.issues.show(5,1)
        issue.move(6)
      DOC
    end

    def move(project_id, issue_id, target_id)
      project_id = format_id(project_id)
      target_id = format_id(target_id)

      client.request(:post, "projects/#{project_id}/issues/#{issue_id}/move", Issue, to_project_id: target_id)
    end
  end
end
