# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'List' do
      title 'Group'
      desc 'Get a list of a group’s issues.'
      example 'client.issues.group_issues(1)'
      result <<~DOC
        => [#<Issue id: 1, title: Front-line global approach, state: opened>, ..]
      DOC
    end

    doc 'List' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.show(1)
        group.issues
      DOC
    end

    def group_issues(group_id, query = {})
      query[:created_after] = query[:created_after].to_time.iso8601 if format_time?(query[:created_after])
      query[:created_before] = query[:created_before].to_time.iso8601 if format_time?(query[:created_before])

      client.request(:get, "groups/#{group_id}/issues", Issue, query)
    end
  end
end
