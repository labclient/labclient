# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Update' do
      title 'Todo'
      desc 'Manually creates a todo for the current user on an issue. [Project ID, merge request iid]'

      example 'client.issues.todo(30, 1)'

      result <<~DOC
        => #<Todo id: 7513, state: pending>
      DOC
    end

    doc 'Update' do
      desc 'Create Todo through Issue'
      example <<~DOC
        issue = client.issues.show(30,2)
        issue.todo
      DOC
    end

    def todo(project_id, issue_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      client.request(:post, "projects/#{project_id}/issues/#{issue_id}/todo", Todo)
    end
  end
end
