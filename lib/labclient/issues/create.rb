# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Create' do
      desc 'Creates a new project issue. [Project ID]'
      example 'client.issues.create(5, title: "Fancy!")'
      result <<~DOC
        => #<Issue id: 1, title: Fancy!, state: opened>
      DOC
    end

    doc 'Create' do
      markdown <<~DOC
        See [API docs here](https://docs.gitlab.com/ee/api/issues.html#new-issue) for full list of attributes

        | Attribute   | Type           | Required | Description  |
        |-------------|----------------|----------|--------------|
        | title       | string         | yes      | The title of an issue |
        | description | string         | no       | The description of an issue. Limited to 1,048,576 characters. |

        &nbsp;
      DOC
    end

    doc 'Create' do
      desc 'Via Project [Hash]'
      example <<~DOC
        project = client.projects.show(16)
        project.issue_create(title: "Fancy!")
      DOC
    end

    def create(project_id, query = {})
      project_id = format_id(project_id)
      format_query_id(:assignee_id, query)

      client.request(:post, "projects/#{project_id}/issues", Issue, query)
    end
  end
end
