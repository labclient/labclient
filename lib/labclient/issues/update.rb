# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Update' do
      desc 'Updates an existing project issue. This call is also used to mark an issue as closed. [Project ID, Issue IID]'
      example 'client.issues.update(5, 1, title: "Moar Fancy!")'
      result <<~DOC
        => #<Issue id: 1, title: Moar Fancy!, state: opened>
      DOC
    end

    doc 'Update' do
      markdown <<~DOC
        See [API docs here](https://docs.gitlab.com/ee/api/issues.html#edit-issue) for full list of attributes

        | Attribute                                 | Type           | Required | Description  |
        |-------------------------------------------|----------------|----------|--------------|
        | title                                   | string         | no      | The title of an issue |
        | description                             | string         | no       | The description of an issue. Limited to 1,048,576 characters. |


      DOC
    end

    doc 'Update' do
      title 'Close / Reopen'
      markdown <<~DOC
        Closing/Reopening of issues is handled via state_events. Either :close, or :reopen. The issue objects themselves also have helpers `reopen` `close`.
      DOC

      example 'client.issues.update(5, 1, state_event: :close)'
    end

    doc 'Update' do
      example 'client.issues.update(5, 1, state_event: :reopen)'
    end

    doc 'Update' do
      desc 'Through Issue Object'
      example <<~DOC
        issue = client.issues.show(5,1)
        issue.close
      DOC
    end

    doc 'Update' do
      desc 'Through Issue Object'
      example <<~DOC
        issue = client.issues.show(5,1)
        issue.reopen
      DOC
    end

    def update(project_id, issue_id, query = {})
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      client.request(:put, "projects/#{project_id}/issues/#{issue_id}", Issue, query)
    end
  end
end
