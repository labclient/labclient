# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Show' do
      desc 'Get a single project issue. [project, Issue IID]'
      example 'client.issues.show(343, 1)'
      result <<~DOC
        => #<Issue id: 1, title: Front-line global approach, state: opened>
      DOC
    end

    # Show
    def show(project_id, issue_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      client.request(:get, "projects/#{project_id}/issues/#{issue_id}", Issue)
    end
  end
end
