# Top namespace
module LabClient
  # Inspect Helper
  class Issue < Klass
    include ClassHelpers

    def inspect
      "#<Issue id: #{iid}, title: #{title}, state: #{state}>"
    end

    # DateTime Fields
    date_time_attrs %i[closed_at created_at updated_at]

    # User Fields
    user_attrs %i[author assignee]

    # Via State Events
    def close
      client.issues.update(project_id, iid, state_event: :close)
    end

    # Via State Events
    def reopen
      client.issues.update(project_id, iid, state_event: :reopen)
    end

    def update(query)
      client.issues.update(project_id, iid, query)
    end

    def assignees
      @table[:assignees].map { |x| User.new(x, response, client) } if has? :assignees
    end

    def delete
      client.issues.delete(project_id, iid)
    end

    def move(target_id)
      client.issues.move(project_id, iid, target_id)
    end

    def subscribe
      client.issues.subscribe(project_id, iid)
    end

    def unsubscribe
      client.issues.unsubscribe(project_id, iid)
    end

    def todo
      client.issues.todo(project_id, iid)
    end

    def add_spent_time(duration)
      client.issues.add_spent_time(project_id, iid, duration)
    end

    def time_estimate(duration)
      client.issues.time_estimate(project_id, iid, duration)
    end

    def reset_time_estimate
      client.issues.reset_time_estimate(project_id, iid)
    end

    def reset_spent_time
      client.issues.reset_spent_time(project_id, iid)
    end

    def participants
      client.issues.participants(project_id, iid)
    end

    def notes
      client.notes.issues.list(project_id, iid)
    end

    def note_create(query)
      client.notes.issues.create(project_id, iid, query)
    end

    def agent_detail
      client.issues.agent_detail(project_id, iid)
    end

    def related_merge_requests
      client.issues.related_merge_requests(project_id, iid)
    end

    def closed_by
      client.issues.closed_by(project_id, iid)
    end

    # Resource Labels
    def resource_labels
      client.resource_labels.issues.list(project_id, iid)
    end

    def resource_label(resource_event_id)
      client.resource_labels.issues.show(project_id, iid, resource_event_id)
    end

    # Reload Helper
    def reload
      update_self client.issues.show(project_id, iid)
    end

    def project
      # If from List Project ID isn't stored
      project_id = collect_project_id if project_id.nil?

      client.projects.show(project_id)
    end

    help do
      subtitle 'Issue'
      option 'update', 'Update issue (accepts hash).'
      option 'move', 'Move this issue to a different project [target project id]'
      option 'todo', 'Create todo'
      option 'subscribe', 'Subscribe to notifications'
      option 'unsubscribe', 'Unsubscribe to notifications'
      option 'participants', 'List participants in this issue.'
      option 'notes', 'List notes/comments. [Hash]'
      option 'note_create', 'Creates a new note for issue. [Hash]'
      option 'agent_detail', 'Get user agent details.'
      option 'related_merge_requests', 'Get all related Merge Requests.'
      option 'closed_by', 'Get all Merge Requests that will close this issue.'
      option 'time_stats', 'Get time tracking stats.'
      option 'add_time_spent', 'Adds spent time. Accepts Duration or Human Format. (1.hour or "1h")'
      option 'time_estimate', 'Sets an estimated time. Accepts Duration or Human Format. (1.hour or "1h")'
      option 'reset_time_estimate', 'Resets the estimated time'
      option 'reset_spent_time', 'Resets the spent time'
      option 'delete', 'Delete this issue.'

      # Resource Labels
      option 'resource_labels', 'List of all label events'
      option 'resource_label', 'Show single label event [Resource Event ID]'

      # Reload Helper
      option 'reload', 'Reload this object (New API Call)'
    end
  end
end
