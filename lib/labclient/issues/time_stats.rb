# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Time Stats' do
      title 'Show'
      desc 'Get time tracking stats. [Project ID, Issue IID]'
      example 'client.issues.time_stats(30, 1)'
      result <<~DOC
        => {:time_estimate=>0, :total_time_spent=>0, :human_time_estimate=>nil, :human_total_time_spent=>nil}
      DOC
    end

    doc 'Time Stats' do
      desc 'Show through Issue'
      example <<~DOC
        issue = client.issues.show(30, 1)'
        issue.time_stats
      DOC
    end

    def time_stats(project_id, issue_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      client.request(:get, "projects/#{project_id}/issues/#{issue_id}/time_stats")
    end

    doc 'Time Stats' do
      title 'Add'
      desc 'Adds spent time for this merge request. [Project ID, issue iid, duration]'
      markdown 'Duration accepts Human Format, or ActiveSupport::Duration (e.g. 1.hour)'
    end

    doc 'Time Stats' do
      desc 'Duration Example'
      example 'client.issues.add_spent_time(30, 1, 1.hour)'
    end

    doc 'Time Stats' do
      desc 'Human Format Example'
      example 'client.issues.add_spent_time(30, 1, "1h")'
    end

    doc 'Time Stats' do
      desc 'Add through Issue'
      example <<~DOC
        issue = client.issues.show(30, 1)
        issue.add_spent_time(1.hour)
      DOC

      result <<~DOC
        issue.add_spent_time(1.hour)
        => {:time_estimate=>0, :total_time_spent=>12978000, :human_time_estimate=>nil, :human_total_time_spent=>"22mo 2w 5h"}
        issue.add_spent_time('1h')
        => {:time_estimate=>0, :total_time_spent=>12978001, :human_time_estimate=>nil, :human_total_time_spent=>"22mo 2w 5h 1s"}
      DOC
    end

    def add_spent_time(project_id, mr_id, duration)
      project_id = format_id(project_id)
      mr_id = format_id(mr_id)

      duration = ChronicDuration.output(ChronicDuration.parse(duration.to_s), format: :short)
      client.request(:post, "projects/#{project_id}/issues/#{mr_id}/add_spent_time", nil, duration: duration)
    end

    doc 'Time Stats' do
      title 'Estimate'
      desc 'Sets an estimated time of work for this merge request. [Project ID, issue iid, duration]'
      markdown 'Duration accepts Human Format, or ActiveSupport::Duration (e.g. 1.hour)'
    end

    doc 'Time Stats' do
      desc 'Duration Example'
      example 'client.issues.time_estimate(30, 1, 1.hour)'
    end

    doc 'Time Stats' do
      desc 'Human Format Example'
      example 'client.issues.time_estimate(30, 1, "1h")'
    end

    doc 'Time Stats' do
      desc 'Add through Issue'
      example <<~DOC
        issue = client.issues.show(30, 1)
        issue.add_spent_time(1.hour)
      DOC

      result <<~DOC
        issue.add_spent_time(1.hour)
        => {:time_estimate=>0, :total_time_spent=>12978000, :human_time_estimate=>nil, :human_total_time_spent=>"22mo 2w 5h"}
        issue.add_spent_time('1h')
        => {:time_estimate=>0, :total_time_spent=>12978001, :human_time_estimate=>nil, :human_total_time_spent=>"22mo 2w 5h 1s"}
      DOC
    end

    def time_estimate(project_id, mr_id, duration)
      project_id = format_id(project_id)
      mr_id = format_id(mr_id)

      duration = ChronicDuration.output(ChronicDuration.parse(duration.to_s), format: :short)
      client.request(:post, "projects/#{project_id}/issues/#{mr_id}/time_estimate", nil, duration: duration)
    end

    # Resets
    doc 'Time Stats' do
      title 'Reset'
      subtitle 'Estimate'
      desc 'Resets the estimated time for this merge request to 0 seconds. [Project ID, Issue IID]'
      example 'client.issues.reset_time_estimate(30, 1)'
      result <<~DOC
        => {:time_estimate=>0, :total_time_spent=>0, :human_time_estimate=>nil, :human_total_time_spent=>nil}
      DOC
    end

    doc 'Time Stats' do
      desc 'Show through Issue'
      example <<~DOC
        issue = client.issues.show(30, 1)
        issue.reset_time_estimate
      DOC
    end

    def reset_time_estimate(project_id, issue_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      client.request(:post, "projects/#{project_id}/issues/#{issue_id}/reset_time_estimate")
    end

    doc 'Time Stats' do
      subtitle 'Time Spent'
      desc 'Resets the estimated time for this merge request to 0 seconds. [Project ID, Issue IID]'
      example 'client.issues.reset_spent_time(30, 1)'
      result <<~DOC
        => {:time_estimate=>0, :total_time_spent=>0, :human_time_estimate=>nil, :human_total_time_spent=>nil}
      DOC
    end

    doc 'Time Stats' do
      desc 'Show through Issue'
      example <<~DOC
        issue = client.issues.show(30, 1)
        issue.reset_spent_time
      DOC
    end

    def reset_spent_time(project_id, issue_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      client.request(:post, "projects/#{project_id}/issues/#{issue_id}/reset_spent_time")
    end
  end
end
