# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'List' do
      title 'Project'
      desc 'Get a list of a project’s issues.'
      example 'client.issues.project_issues(1)'
      result <<~DOC
        => [#<Issue id: 1, title: Front-line global approach, state: opened>, ..]
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(1)
        project.issues
      DOC
    end

    def project_issues(project_id, query = {})
      project_id = format_id(project_id)

      query[:created_after] = query[:created_after].to_time.iso8601 if format_time?(query[:created_after])
      query[:created_before] = query[:created_before].to_time.iso8601 if format_time?(query[:created_before])

      client.request(:get, "projects/#{project_id}/issues", Issue, query)
    end
  end
end
