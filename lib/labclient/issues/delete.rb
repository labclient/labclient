# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Delete' do
      desc 'Only for administrators and project owners. Deletes an issue. [Project ID, Issue IID]'
      example 'client.issues.delete(5, 1)'
      result <<~DOC
        => #<TyphoeusResponse code: 204>
      DOC
    end

    doc 'Delete' do
      desc 'Through Issue Object'
      example <<~DOC
        issue = client.issues.show(5,1)
        issue.delete
      DOC
    end

    def delete(project_id, issue_id)
      project_id = format_id(project_id)
      client.request(:delete, "projects/#{project_id}/issues/#{issue_id}")
    end
  end
end
