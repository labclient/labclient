# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Show' do
      title 'Notes'
      desc 'Notes are handled via the Notes resource. Accessible via the object or notes'
      example <<~DOC
        issue = client.issues.show(30,2)
        issue.notes
      DOC

      result <<~DOC
        => [#<Note id: 21, type: Issue>, #<Note id: 20, type: Issue>, .. ]
      DOC
    end
  end
end
