# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Update' do
      title 'Unsubscribe'
      desc 'Unsubscribes the authenticated user from the issue to not receive notifications from it. [Project ID, Issue IID]'

      example 'client.issues.unsubscribe(343, 7)'

      result <<~DOC
        => #<Issue id: 10, title: Compatible holistic solution, state: opened>
      DOC
    end

    doc 'Update' do
      desc 'Unsubscribe through Issue'
      example <<~DOC
        issue = client.issues.show(343,2)
        issue.unsubscribe
      DOC
    end

    def unsubscribe(project_id, issue_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      client.request(:post, "projects/#{project_id}/issues/#{issue_id}/unsubscribe", Issue)
    end
  end
end
