# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Show' do
      title 'Merge Requests'
      subtitle 'Related'
      desc 'Get all the merge requests that are related to the issue. [Project ID, Issue IID]'
      example 'client.issues.related_merge_requests(5, 1)'
      result <<~DOC
        => [#<MergeRequest id: 5, title: Update README.md, state: opened>]
      DOC
    end

    doc 'Show' do
      desc 'Through Issue Object'
      example <<~DOC
        issue = client.issues.show(5,1)
        issue.related_merge_requests
      DOC
    end

    def related_merge_requests(project_id, issue_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      client.request(:get, "projects/#{project_id}/issues/#{issue_id}/related_merge_requests", MergeRequest)
    end
  end
end
