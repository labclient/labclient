# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Show' do
      subtitle 'Closed By'
      desc 'Get all the merge requests that will close issue when merged. [Project ID, Issue IID]'
      example 'client.issues.closed_by(5, 1)'
      result <<~DOC
        => [#<MergeRequest id: 5, title: Update README.md, state: opened>]
      DOC
    end

    doc 'Show' do
      desc 'Through Issue Object'
      example <<~DOC
        issue = client.issues.show(5,1)
        issue.related_merge_requests
      DOC
    end

    def closed_by(project_id, issue_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/issues/#{issue_id}/closed_by", MergeRequest)
    end
  end
end
