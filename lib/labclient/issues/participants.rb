# Top namespace
module LabClient
  # Specifics
  class Issues < Common
    doc 'Show' do
      title 'Participants'
      desc 'Get a list of issue participants. [Project ID, Issue IID]'
      example 'client.issues.participants(30,1)'
      result <<~DOC
        => [#<User id: 1, username: braum>, #<User id: 52, username: anivia>]
      DOC
    end

    doc 'Show' do
      desc 'List through MergeRequest'
      example <<~DOC
        mr = client.issues.show(30,1)
        mr.participants
      DOC
      result <<~DOC
        issue = client.issues.show(30,1)
        => #<Issue id: 1, title: Robust incremental superstructure, state: opened>
        issue.participants
        => [#<User id: 1, username: root>]
      DOC
    end

    # Show Specific
    def participants(project_id, issue_id = nil)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/issues/#{issue_id}/participants", User)
    end
  end
end
