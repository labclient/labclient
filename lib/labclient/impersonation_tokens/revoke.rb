# Top namespace
module LabClient
  # Specifics
  class ImpersonationTokens < Common
    doc 'Revoke' do
      desc 'Revoke an impersonation token. [User ID, Token ID]'
      example 'client.impersonation_tokens.revoke(5,7)'
    end

    doc 'Revoke' do
      desc 'Via User [Token ID]'
      example <<~DOC
        user = client.users.show(5)
        user.impersonation_tokens_revoke(2)
      DOC
    end

    doc 'Revoke' do
      desc 'Via ImpersonationToken [Token ID]'
      example <<~DOC
        token = client.impersonation_tokens.show(3)
        token.revoke
      DOC
    end

    def revoke(user_id, token_id)
      token_id = format_id(token_id)
      user_id = format_id(user_id)
      client.request(:delete, "users/#{user_id}/impersonation_tokens/#{token_id}")
    end
  end
end
