# Top namespace
module LabClient
  # Inspect Helper
  class ImpersonationToken < Klass
    include ClassHelpers

    def inspect
      "#<ImpersonationToken id: #{id}, name: #{name}, active: #{active}>"
    end

    def revoke
      user_id = collect_user_id
      client.impersonation_tokens.revoke(user_id, id)
    end

    date_time_attrs %i[created_at expires_at]
  end
end
