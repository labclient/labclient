# Top namespace
module LabClient
  # Specifics
  class ImpersonationTokens < Common
    doc 'Show' do
      desc 'Show a user impersonation token. [User ID, Token ID]'
      example 'client.impersonation_tokens.show(5,7)'
      result '#<ImpersonationToken id: 7, name: User!, active: false>'
    end

    def show(user_id, token_id)
      token_id = format_id(token_id)
      user_id = format_id(user_id)

      client.request(:get, "users/#{user_id}/impersonation_tokens/#{token_id}", ImpersonationToken)
    end
  end
end
