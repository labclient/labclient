# Top namespace
module LabClient
  # Specifics
  class ImpersonationTokens < Common
    doc 'Create' do
      desc 'It creates a new impersonation token.  You are only able to create impersonation tokens to impersonate the user and perform both API calls and Git reads and writes. [User ID, Hash]'
      example 'client.impersonation_tokens.create(5, name: "Special", scopes: [:api])'
      result '#<ImpersonationToken id: 8, name: Special, active: true>'

      markdown <<~DOC


        | Attribute    | Type    | Required | Description |
        | ------------ | ------- | -------- | ----------- |
        | name       | string  | yes      | The name of the impersonation token |
        | expires_at | date    | no       | The expiration date of the impersonation token in ISO format (YYYY-MM-DD)|
        | scopes     | array   | yes      | The array of scopes of the impersonation token (api, read_user) |

      DOC
    end

    doc 'Create' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(5)
        user.impersonation_tokens_create(name: 'User!', scopes: [:read_user], expires_at: 1.month.from_now)
      DOC
    end

    def create(user_id, query)
      user_id = format_id(user_id)
      query_format_time(query, :expires_at)

      client.request(:post, "users/#{user_id}/impersonation_tokens", ImpersonationToken, query)
    end
  end
end
