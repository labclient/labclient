# Top namespace
module LabClient
  # Specifics
  class ImpersonationTokens < Common
    @group_name = 'Impersonation'
    doc 'List' do
      desc 'It retrieves every impersonation token of the user. [User ID, Filter State]'
      example 'client.impersonation_tokens.list(5)'
      result '[#<ImpersonationToken id: 8, name: Special, active: true>, ...]'

      markdown <<~DOC
        Filter tokens based on state: all, active, or inactive
      DOC
    end

    doc 'List' do
      desc 'Via User'
      example <<~DOC
        user = client.users.show(5, :active)
        user.impersonation_tokens
      DOC
    end

    def list(user_id, filter = :all)
      user_id = format_id(user_id)
      client.request(:get, "users/#{user_id}/impersonation_tokens", ImpersonationToken, { filter: filter })
    end
  end
end
