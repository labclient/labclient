# Overall version
module LabClient
  VERSION = '0.7.0'.freeze

  # Make it easy to print version
  def self.version
    VERSION
  end
end
