# Top namespace
module LabClient
  # Specifics
  class GroupAccessRequests < Common
    doc 'List' do
      desc 'Gets a list of access requests viewable by the authenticated user. [Group ID]'
      example 'client.groups.access_requests.list(16)'
    end

    doc 'List' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.show(16)
        group.access_requests
      DOC
    end

    def list(group_id)
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}/access_requests", GroupAccessRequest)
    end
  end
end
