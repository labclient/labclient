# Top namespace
module LabClient
  # Specifics
  class GroupAccessRequests < Common
    doc 'Create' do
      desc 'Requests access for the authenticated user to a group.'
      example <<~DOC
        client.groups.access_requests.create(310)
      DOC
    end

    doc 'Create' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.show(100)
        group.request_access
      DOC
    end

    def create(group_id)
      group_id = format_id(group_id)
      client.request(:post, "groups/#{group_id}/access_requests", GroupAccessRequest)
    end
  end
end
