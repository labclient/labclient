# Top namespace
module LabClient
  # Inspect Helper
  class GroupAccessRequest < Klass
    include ClassHelpers
    def inspect
      "#<GroupAccessRequest id: #{id}, name: #{name}, state: #{state}>"
    end

    def user
      client.users.show(id)
    end

    def approve(access_level = :developer)
      group_id = collect_group_id
      client.groups.access_requests.approve(group_id, id, access_level)
    end

    def deny
      group_id = collect_group_id
      client.groups.access_requests.deny(group_id, id)
    end

    date_time_attrs %i[created_at]

    help do
      @group_name = 'Groups'
      subtitle 'GroupAccessRequest'
      option 'user', 'Show user for access request'
      option 'approve', 'Accept access request [Access Level]'
      option 'deny', 'Deny request'
    end
  end
end
