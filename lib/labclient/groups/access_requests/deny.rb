# Top namespace
module LabClient
  # Specifics
  class GroupAccessRequests < Common
    doc 'Deny' do
      desc 'Deny an access request for the given user. [Group ID, User ID]'
      example 'client.groups.access_requests.deny(310, 3)'
    end

    doc 'Deny' do
      desc 'Via GroupAccessRequest [Access Level]'
      example <<~DOC
        request = client.groups.access_requests(16).first
        request.deny
      DOC
    end

    def deny(group_id, user_id)
      group_id = format_id(group_id)
      user_id = format_id(user_id)

      client.request(:delete, "groups/#{group_id}/access_requests/#{user_id}/deny")
    end
  end
end
