# Top namespace
module LabClient
  # Inspect Helper
  class GroupAccessRequests < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    def access_requests
      GroupAccessRequests.new(client)
    end
  end
end
