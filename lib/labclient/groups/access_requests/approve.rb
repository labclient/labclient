# Top namespace
module LabClient
  # Specifics
  class GroupAccessRequests < Common
    doc 'Approve' do
      desc 'Approves an access request for the given user. [Group ID, User ID, Access Level]'
      example 'client.groups.access_requests.approve(310, 3)'

      result '=>  #<GroupAccessRequest id: 3, name: planet bob>'
    end

    doc 'Approve' do
      markdown <<~DOC
        | Attribute    | Type    | Description                                                 |
        | ----------             | ---------                                                   |
        | access_level | integer | A valid access level (defaults: 30, developer access level) |
      DOC
    end

    doc 'Approve' do
      desc 'Via GroupAccessRequest [Access Level]'
      example <<~DOC
        request = client.groups.access_requests(16).first
        request.approve(:developer)
      DOC
    end

    def approve(group_id, user_id, access_level = :developer)
      group_id = format_id(group_id)
      user_id = format_id(user_id)

      query = { access_level: access_level }
      query_access_level(query, :access_level)

      client.request(:put, "groups/#{group_id}/access_requests/#{user_id}/approve", GroupAccessRequest, query)
    end
  end
end
