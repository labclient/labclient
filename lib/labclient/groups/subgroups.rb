# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    doc 'List' do
      title 'Subgroups'
      desc 'Get a list of visible direct subgroups in this group'

      example 'client.groups.subgroups(68)'

      result '=> [#<Group id: 110, fensofserech/subgroup>]'

      markdown <<~DOC
        | Attribute                | Type              | Required | Description |
        | ------------------------ | ----------------- | -------- | ---------- |
        | skip_groups            | array of integers | no       | Skip the group IDs passed |
        | all_available          | boolean           | no       | Show all the groups you have access to (defaults to false for authenticated users, true for admin); Attributes owned and min_access_level have precedence |
        | search                 | string            | no       | Return the list of authorized groups matching the search criteria |
        | order_by               | string            | no       | Order groups by name, path or id. Default is name |
        | sort                   | string            | no       | Order groups in asc or desc order. Default is asc |
        | statistics             | boolean           | no       | Include group statistics (admins only) |
        | with_custom_attributes | boolean           | no       | Include custom attributes in response (admins only) |
        | owned                  | boolean           | no       | Limit to groups explicitly owned by the current user |
        | min_access_level       | integer           | no       | Limit to groups where current user has at least this access level |
      DOC
    end

    doc 'List' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.list.last
        group.subgroups
      DOC
    end

    def subgroups(group_id, query = {})
      group_id = format_id(group_id)

      # Map Skip Groups
      format_query_ids(:skip_groups, query)

      query_access_level(query, :min_access_level)

      client.request(:get, "groups/#{group_id}/subgroups", Group, query)
    end
  end
end
