# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    doc 'Create' do
      desc 'Creates a new project group [Hash]'

      example 'client.groups.create(path: "groot", name: "Groot")'

      result '=> #<Group id: 112, groot>'

      markdown <<~DOC
        See [Docs](https://docs.gitlab.com/ee/api/groups.html#new-group) for available params

        | Attribute  | Description |
        | ---------- | ----------- |
        | name       | The name of the group. |
        | path       | The path of the group. |
      DOC
    end

    doc 'Create' do
      desc 'With visibility'
      example 'client.groups.create(path: "iam", name: "I am", description: "Groot", visibility:  :private)'
    end

    def create(query = {})
      client.request(:post, 'groups', Group, query)
    end
  end
end
