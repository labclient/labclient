# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    doc 'Show' do
      subtitle 'Details'
      desc 'Get all details of a group [Hash]'

      example 'client.groups.show(68)'

      result '=> [#<Project id: 77 fensofserech/flameatronach>, #<Project id: 76 fensofserech/sizechanging>, ... ]'

      markdown <<~DOC
        See [Docs](https://docs.gitlab.com/ee/api/groups.html#details-of-a-group) on extra attributes

        | Attribute                | Type           | Required | Description |
        | ------------------------ | -------------- | -------- | ----------- |
        | with_custom_attributes | boolean        | no       | Include custom attributes in response (admins only). |
        | with_projects          | boolean        | no       | Include details from projects that belong to the specified group (defaults to true). |
      DOC
    end

    doc 'Show' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.list.last
        group.details
      DOC
    end

    def show(group_id, query = {})
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}", Group, query)
    end
  end
end
