# Top namespace
module LabClient
  # Inspect Helper
  class GroupHooks < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    def hooks
      GroupHooks.new(client)
    end
  end
end
