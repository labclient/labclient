# Top namespace
module LabClient
  # Inspect Helper
  class GroupHook < Klass
    include ClassHelpers
    def inspect
      "#<GroupHook id: #{id}, url: #{url}>"
    end

    def group
      group_id = collect_group_id
      client.groups.show group_id
    end

    def update(query)
      group_id = collect_group_id

      update_self client.groups.hooks.update(group_id, id, query)
    end

    def delete
      group_id = collect_group_id

      client.groups.hooks.delete(group_id, id)
    end

    date_time_attrs %i[created_at]

    help do
      @group_name = 'Groups'
      subtitle 'GroupHook'
      option 'group', 'Show Group'
      option 'update', 'Update this hook [Hash]'
      option 'delete', 'Delete this hook'
    end
  end
end
