# Top namespace
module LabClient
  # Specifics
  class GroupHooks < Common
    doc 'Delete' do
      desc 'Removes a hook from a group. [Group ID, Hook ID]'
      example 'client.groups.hooks.delete(16, 1)'
    end

    doc 'Delete' do
      desc 'Via Group [Hook ID]'
      example <<~DOC
        group = client.groups.show(16)
        group.hook_delete(1)
      DOC
    end

    doc 'Delete' do
      desc 'Via GroupHook'
      example <<~DOC
        hook = client.groups.hooks.show(16, 1)
        hook.delete
      DOC
    end

    def delete(group_id, hook_id)
      group_id = format_id(group_id)
      hook_id = format_id(hook_id)

      client.request(:delete, "groups/#{group_id}/hooks/#{hook_id}")
    end
  end
end
