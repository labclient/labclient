# Top namespace
module LabClient
  # Specifics
  class GroupHooks < Common
    doc 'Show' do
      desc 'Get a specific hook for a group. [Group ID, Hook ID]'
      example 'client.groups.hooks.show(16, 1)'

      result '=>  #<GroupHook id: 1, url: https://labclient>'
    end

    doc 'Show' do
      desc 'Via Group [Hook ID]'
      example <<~DOC
        group = client.groups.show(16)
        hook = group.hook_show(1)
      DOC
    end

    def show(group_id, hook_id)
      group_id = format_id(group_id)
      hook_id = format_id(hook_id)

      client.request(:get, "groups/#{group_id}/hooks/#{hook_id}", GroupHook)
    end
  end
end
