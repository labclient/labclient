# Top namespace
module LabClient
  # Specifics
  class GroupHooks < Common
    doc 'Update' do
      desc 'Edits a hook for a specified group. [Group ID, Hook ID, Params]'
      example 'client.groups.hooks.update(16, 1, url: "http://labclient.com")'

      result '=>  #<GroupHook id: 2, url: http://labclient.com>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | url | string | yes | The hook URL |
        | push_events | boolean | no | Trigger hook on push events |
        | push_events_branch_filter | string | no | Trigger hook on push events for matching branches only |
        | issues_events | boolean | no | Trigger hook on issues events |
        | confidential_issues_events | boolean | no | Trigger hook on confidential issues events |
        | merge_requests_events | boolean | no | Trigger hook on merge requests events |
        | tag_push_events | boolean | no | Trigger hook on tag push events |
        | note_events | boolean | no | Trigger hook on note events |
        | job_events | boolean | no | Trigger hook on job events |
        | pipeline_events | boolean | no | Trigger hook on pipeline events |
        | wiki_page_events | boolean | no | Trigger hook on wiki events |
        | enable_ssl_verification | boolean | no | Do SSL verification when triggering the hook |
        | token | string | no | Secret token to validate received payloads; this will not be returned in the response |
      DOC
    end

    doc 'Update' do
      desc 'Via Group [Hook ID, Params]'
      example <<~DOC
        group = client.groups.show(16)
        hook = group.hook_update(1, url: "https://dothething")
      DOC
    end

    doc 'Update' do
      desc 'Via GroupHook [Params]'
      example <<~DOC
        hook = client.groups.hooks.show(16, 1)
        hook.update(url: "https://dothething")
      DOC
    end

    def update(group_id, hook_id, query)
      group_id = format_id(group_id)
      hook_id = format_id(hook_id)

      client.request(:put, "groups/#{group_id}/hooks/#{hook_id}", GroupHook, query)
    end
  end
end
