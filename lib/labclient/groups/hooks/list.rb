# Top namespace
module LabClient
  # Specifics
  class GroupHooks < Common
    doc 'List' do
      desc 'Get a list of group hooks. [Group ID]'
      example 'client.groups.hooks.list(16)'

      result <<~DOC
        => [
          #<GroupHook id: 1, url: https://labclient.url>,
          ...
        ]
      DOC
    end

    doc 'List' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.show(16)
        group.hooks
      DOC
    end

    def list(group_id)
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}/hooks", GroupHook)
    end
  end
end
