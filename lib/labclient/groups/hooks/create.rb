# Top namespace
module LabClient
  # Specifics
  class GroupHooks < Common
    doc 'Create' do
      desc 'Adds a hook to a specified group. [Group ID, Hash]'
      example 'client.groups.hooks.create(16, url: "http://labclient.com")'

      result '=>  #<GroupHook id: 2, url: http://labclient.com>'
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | url | string | yes | The hook URL |
        | push_events | boolean | no | Trigger hook on push events |
        | push_events_branch_filter | string | no | Trigger hook on push events for matching branches only |
        | issues_events | boolean | no | Trigger hook on issues events |
        | confidential_issues_events | boolean | no | Trigger hook on confidential issues events |
        | merge_requests_events | boolean | no | Trigger hook on merge requests events |
        | tag_push_events | boolean | no | Trigger hook on tag push events |
        | note_events | boolean | no | Trigger hook on note events |
        | job_events | boolean | no | Trigger hook on job events |
        | pipeline_events | boolean | no | Trigger hook on pipeline events |
        | wiki_page_events | boolean | no | Trigger hook on wiki events |
        | enable_ssl_verification | boolean | no | Do SSL verification when triggering the hook |
        | token | string | no | Secret token to validate received payloads; this will not be returned in the response |
      DOC
    end

    doc 'Create' do
      desc 'Via Group [Hook ID]'
      example <<~DOC
        group = client.groups.show(16)
        hook = group.hook_create(url: "https://dothething")
      DOC
    end

    def create(group_id, query)
      group_id = format_id(group_id)

      client.request(:post, "groups/#{group_id}/hooks", GroupHook, query)
    end
  end
end
