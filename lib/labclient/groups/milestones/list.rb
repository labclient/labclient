# Top namespace
module LabClient
  # Specifics
  class GroupMilestones < Common
    doc 'List' do
      desc 'Get a list of group milestones. [Group ID, Hash]'
      example 'client.groups.milestones.list(16)'
      result <<~DOC
        => [#<GroupMilestone id: 1, title: Its a mile long stone>, ...]
      DOC
    end

    doc 'List' do
      markdown <<~DOC

        | Attribute | Type   | Required | Description |
        | --------- | ------ | -------- | ----------- |
        | iids[]  | integer array | optional | Return only the milestones having the given iid |
        | state   | string | optional | Return only active or closed milestones |
        | title   | string | optional | Return only the milestones having the given title |
        | search  | string | optional | Return only milestones with a title or description matching the provided string |

      DOC
    end

    doc 'List' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(264)
        group.milestones
      DOC
    end

    # List
    def list(group_id, query = {})
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}/milestones", GroupMilestone, query)
    end
  end
end
