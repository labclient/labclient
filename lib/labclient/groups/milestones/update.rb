# Top namespace
module LabClient
  # Specifics
  class GroupMilestones < Common
    doc 'Update' do
      desc 'Updates an existing group milestone [Group ID, Group Milestone ID, Hash]'

      example <<~DOC
        client.groups.milestones.update(16, 2, title: 'Better Title')
      DOC

      result <<~DOC
        => #<GroupMilestone id: 2, title: Better Title>
      DOC
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute    | Type   | Required | Description                       |
        |--------------|------- |--------- |---------------------------------- |
        | title        | string | yes      | Title of a milestone.             |
        | description  | string | no       | The description of the milestone  |
        | due_date     | string | no       | The due date of the milestone     |
        | start_date   | string | no       | The start date of the milestone   |
      DOC
    end

    doc 'Update' do
      desc 'Via Group Milestone'
      example <<~DOC
        milestone.update(title: 'One Group Milestone to find them!')
      DOC
    end

    def update(group_id, milestone_id, query = {})
      milestone_id = format_id(milestone_id)
      group_id = format_id(group_id)

      query[:due_date] = query[:due_date].to_time.iso8601 if format_time?(query[:due_date])
      query[:start_date] = query[:start_date].to_time.iso8601 if format_time?(query[:start_date])

      client.request(:put, "groups/#{group_id}/milestones/#{milestone_id}", GroupMilestone, query)
    end
  end
end
