# Top namespace
module LabClient
  # Specifics
  class GroupMilestones < Common
    doc 'Create' do
      desc 'Creates a new group milestone. [Group ID, Hash]'

      example <<~DOC
        client.groups.milestones.create(16, title: "Sweet Code")
      DOC

      result <<~DOC
        => #<GroupMilestone id: 2, title: Sweet Code>
      DOC
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute    | Type   | Required | Description                       |
        |--------------|------- |--------- |---------------------------------- |
        | title        | string | yes      | Title of a milestone.             |
        | description  | string | no       | The description of the milestone  |
        | due_date     | string | no       | The due date of the milestone     |
        | start_date   | string | no       | The start date of the milestone   |
      DOC
    end

    def create(group_id, query = {})
      group_id = format_id(group_id)

      query[:due_date] = query[:due_date].to_time.iso8601 if format_time?(query[:due_date])
      query[:start_date] = query[:start_date].to_time.iso8601 if format_time?(query[:start_date])

      client.request(:post, "groups/#{group_id}/milestones", GroupMilestone, query)
    end
  end
end
