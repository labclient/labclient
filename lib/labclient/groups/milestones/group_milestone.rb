# Top namespace
module LabClient
  # Inspect Helper
  class GroupMilestone < Klass
    include ClassHelpers

    def inspect
      "#<GroupMilestone id: #{id}, title: #{title}>"
    end

    def group
      client.groups.show group_id
    end

    def update(query = {})
      group_id = collect_group_id
      update_self client.groups.milestones.update(group_id, id, query)
    end

    def issues
      group_id = collect_group_id
      client.groups.milestones.issues(group_id, id)
    end

    def merge_requests
      group_id = collect_group_id
      client.groups.milestones.merge_requests(group_id, id)
    end

    def burndown
      group_id = collect_group_id
      client.groups.milestones.burndown(group_id, id)
    end

    def delete
      group_id = collect_group_id
      client.groups.milestones.delete(group_id, id)
    end

    date_time_attrs %i[updated_at created_at due_date start_date]

    help do
      subtitle 'Group Milestone'
      option 'group', 'Show group for milestone.'
      option 'issues', 'Show issues assigned to milestone.'
      option 'merge_requests', 'Show merge requests assigned to milestone.'
      option 'delete', 'Delete this milestone.'
      option 'burndown', 'Show burndown events.'
      option 'update', 'Update this this milestone. [Hash]'
    end
  end
end
