# Top namespace
module LabClient
  # Specifics
  class GroupMilestones < Common
    doc 'Show' do
      desc 'Get a single group milestone. [Group ID, Group Milestone ID]'
      example 'client.groups.milestones.show(16, 1)'
      result <<~DOC
        => #<GroupMilestone id: 1, title: Its a mile long stone>
      DOC
    end

    # Show Specific
    def show(group_id, milestone_id)
      milestone_id = format_id(milestone_id)
      group_id = format_id(group_id)
      client.request(:get, "groups/#{group_id}/milestones/#{milestone_id}", GroupMilestone)
    end
  end
end
