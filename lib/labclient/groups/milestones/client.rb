# Top namespace
module LabClient
  # Inspect Helper
  class GroupMilestones < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    def milestones
      GroupMilestones.new(client)
    end
  end
end
