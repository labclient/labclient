# Top namespace
module LabClient
  # Specifics
  class GroupMilestones < Common
    doc 'Show' do
      title 'Merge Requests'
      desc 'Gets all merge requests assigned to a single group milestone. [Group ID, Group Milestone ID]'
      example 'client.groups.milestones.merge_requests(16, 1)'
      result <<~DOC
        => [#<MergeRequest id: 7, title: Update README.md, state: opened>]
      DOC
    end

    doc 'Show' do
      desc 'via GroupMilestone'
      example <<~DOC
        milestone = client.groups.milestones.show(16,1)
        milestone.merge_requests
      DOC
    end

    def merge_requests(group_id, milestone_id)
      milestone_id = format_id(milestone_id)
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}/milestones/#{milestone_id}/merge_requests", MergeRequest)
    end
  end
end
