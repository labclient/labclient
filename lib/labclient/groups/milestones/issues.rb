# Top namespace
module LabClient
  # Specifics
  class GroupMilestones < Common
    doc 'Show' do
      title 'Issues'
      desc 'Gets all issues assigned to a single group milestone. [Group ID, Group Milestone ID]'
      example 'client.groups.milestones.issues(16, 1)'
      result <<~DOC
        => [#<Issue id: 36, title: Implemented high-level process improvement, state: opened>, #<Issue id: 35, title: Proactive didactic implementation, state: opened>]
      DOC
    end

    doc 'Show' do
      desc 'via GroupMilestone'
      example <<~DOC
        milestone = client.groups.milestones.show(16,1)
        milestone.issues
      DOC
    end

    def issues(group_id, milestone_id)
      milestone_id = format_id(milestone_id)
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}/milestones/#{milestone_id}/issues", Issue)
    end
  end
end
