# Top namespace
module LabClient
  # Specifics
  class GroupMilestones < Common
    doc 'Show' do
      title 'Burndown'
      desc 'Gets all burndown chart events for a single milestone.  [Group ID, Group Milestone ID]'
      example 'client.groups.milestones.burndown(16, 2)'
      result <<~DOC
        [
          {
            :created_at => "2020-04-17T14:39:18.827-06:00",
                :weight => nil,
                :action => "created"
          }
        ]
      DOC
    end

    doc 'Show' do
      desc 'via the Group Group Milestone'
      example <<~DOC
        milestone = client.milestones.burndown(2)
        milestone.burndown
      DOC
    end

    # Show Specific
    def burndown(group_id, milestone_id)
      milestone_id = format_id(milestone_id)
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}/milestones/#{milestone_id}/burndown_events")
    end
  end
end
