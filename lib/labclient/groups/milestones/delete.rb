# Top namespace
module LabClient
  # Specifics
  class GroupMilestones < Common
    doc 'Delete' do
      desc 'Deletes an existing group milestone. [Group ID, Group Milestone ID]'
      example <<~DOC
        client.groups.milestones.delete(16,2)
      DOC
    end

    doc 'Delete' do
      desc 'via Group Milestone'
      example <<~DOC
        milestone = client.groups.milestones.show(16,1)
        milestone.delete
      DOC
    end

    def delete(group_id, milestone_id)
      milestone_id = format_id(milestone_id)
      group_id = format_id(group_id)
      client.request(:delete, "groups/#{group_id}/milestones/#{milestone_id}")
    end
  end
end
