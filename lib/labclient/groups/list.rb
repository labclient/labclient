# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    doc 'List' do
      desc 'Get a list of visible groups for the authenticated user [Hash]'

      markdown <<~DOC

        | Attribute                | Type              | Required | Description |
        | ------------------------ | ----------------- | -------- | ---------- |
        | skip_groups            | array of integers | no       | Skip the group IDs passed |
        | all_available          | boolean           | no       | Show all the groups you have access to (defaults to false for authenticated users, true for admin); Attributes owned and min_access_level have precedence |
        | search                 | string            | no       | Return the list of authorized groups matching the search criteria |
        | order_by               | string            | no       | Order groups by name, path or id. Default is name |
        | sort                   | string            | no       | Order groups in asc or desc order. Default is asc |
        | statistics             | boolean           | no       | Include group statistics (admins only) |
        | with_custom_attributes | boolean           | no       | Include custom attributes in response (admins only) |
        | owned                  | boolean           | no       | Limit to groups explicitly owned by the current user |
        | min_access_level       | integer           | no       | Limit to groups where current user has at least this access level |

      DOC
    end

    doc 'List' do
      title 'Examples'
      example 'client.groups.list'
      result <<~DOC
        => [
          #<Group id: 81 alphabetrium>,
          #<Group id: 85 alphaquadrant>,
          #<Group id: 86 argonath>,
          #<Group id: 106 badlands>
        ]
      DOC
    end

    doc 'List' do
      desc 'Owned'
      example 'client.groups.list(owned: true)'
      result <<~DOC
        => [#<User id: 56, username: "rakan">]
      DOC
    end

    doc 'List' do
      desc 'Access Level (by name)'
      example 'client.groups.list(min_access_level: :developer)'
      result <<~DOC
        => [
          #<Group id: 61 cyrodiil>,
          #<Group id: 102 demacia>,
          #<Group id: 64 dimensionc137
        ]
      DOC
    end

    doc 'List' do
      desc 'Access Level (by value)'
      example 'client.groups.list(min_access_level: 30)'
      result <<~DOC
        => [
          #<Group id: 61 cyrodiil>,
          #<Group id: 102 demacia>,
          #<Group id: 64 dimensionc137
        ]
      DOC
    end

    def list(query = {})
      query_access_level(query, :min_access_level)

      client.request(:get, 'groups', Group, query)
    end
  end
end
