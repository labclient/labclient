# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    doc 'Update' do
      title 'Transfer'
      desc 'Transfer a project to the Group namespace [Group ID , Project ID]'
      example 'client.groups.transfer(3,25)'
      result '=> #<Project id: 25 badlands/chaurushunter>'
    end

    doc 'Update' do
      desc 'via Group'
      example <<~DOC
        group = client.group.show(3)
        group.transfer(25)
      DOC
    end

    def transfer(group_id, project_id)
      group_id = format_id(group_id)
      project_id = format_id(project_id)

      client.request(:post, "groups/#{group_id}/projects/#{project_id}", Project)
    end
  end
end
