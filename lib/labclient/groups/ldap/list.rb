# Top namespace
module LabClient
  # Specifics
  class GroupLdap < Common
    @group_name = 'Group LDAP'

    doc 'List' do
      desc 'Lists LDAP group links. [Group ID]'
      example 'client.groups.ldap.list(3)'
      result '=> [#<GroupLink provider: ldapmain, level: maintainer>, ...'
    end

    doc 'List' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(96)
        group.ldap_list
      DOC
    end

    def list(group_id)
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}/ldap_group_links", GroupLink)
    end
  end
end
