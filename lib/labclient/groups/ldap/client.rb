# Top namespace
module LabClient
  # Inspect Helper
  class GroupLdap < Common
    @group_name = 'Group LDAP'
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    def ldap
      GroupLdap.new(client)
    end
  end
end
