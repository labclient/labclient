# Top namespace
module LabClient
  # Inspect Helper
  class GroupLink < Klass
    include AccessLevel
    include ClassHelpers
    @group_name = 'Group LDAP'

    def inspect
      "#<GroupLink provider: #{provider}, level: #{access_level}>"
    end

    def access_level
      human_access_level group_access
    end
  end
end
