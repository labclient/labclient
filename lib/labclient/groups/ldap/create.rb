# Top namespace
module LabClient
  # Specifics
  class GroupLdap < Common
    @group_name = 'Group LDAP'

    doc 'Create' do
      desc 'Adds an LDAP group link. [Group ID, Hash]'
      example 'client.groups.ldap.create(5, cn: "admins", group_access: :developer, provider: :ldapmain)'
      result '=> #<GroupLink provider: ldapmain, level: developer'

      markdown <<~DOC
        | Param        | Description  |
        |--------------|----------------|
        | cn           | The CN of a LDAP group |
        | group_access | Minimum access level for members of the LDAP group, number or name |
        | provider     | LDAP provider for the LDAP group |
      DOC
    end

    doc 'Create' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(96)
        group.ldap_create(cn: "admins", group_access: :developer, provider: :ldapmain)
      DOC
    end

    def create(group_id, query)
      group_id = format_id(group_id)
      query_access_level(query, :group_access)
      client.request(:post, "groups/#{group_id}/ldap_group_links", GroupLink, query)
    end
  end
end
