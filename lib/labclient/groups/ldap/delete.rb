# Top namespace
module LabClient
  # Specifics
  class GroupLdap < Common
    @group_name = 'Group LDAP'

    doc 'Delete' do
      desc 'Deletes an LDAP group link. [Group ID, CN, Provider(optional)]'
      example 'client.groups.ldap.delete(5, "admin")'

      markdown <<~DOC
        | Param        | Description  |
        |--------------|----------------|
        | cn           | The CN of a LDAP group |
        | group_access | Minimum access level for members of the LDAP group, number or name |
        | provider     | LDAP provider for the LDAP group |
      DOC
    end

    doc 'Delete' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(96)
        group.ldap_delete("admins", :ldapmain)
      DOC
    end

    def delete(group_id, name, provider = nil)
      group_id = format_id(group_id)
      if provider
        client.request(:delete, "groups/#{group_id}/ldap_group_links/#{provider}/#{name}")
      else
        client.request(:delete, "groups/#{group_id}/ldap_group_links/#{name}")
      end
    end
  end
end
