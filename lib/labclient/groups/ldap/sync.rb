# Top namespace
module LabClient
  # Specifics
  class GroupLdap < Common
    @group_name = 'Group LDAP'

    doc 'Sync' do
      desc 'Syncs the group with its linked LDAP group. [Group ID]'
      example 'client.groups.ldap.sync(5)'
    end

    doc 'Sync' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(3)
        group.ldap_sync
      DOC
    end

    def sync(group_id)
      group_id = format_id(group_id)

      client.request(:post, "groups/#{group_id}/ldap_sync")
    end
  end
end
