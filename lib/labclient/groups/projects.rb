# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    doc 'List' do
      title 'Projects'
      desc 'Get a list of projects in this group [Hash]'

      example 'client.groups.projects(68)'

      result '=> [#<Project id: 77 fensofserech/flameatronach>, #<Project id: 76 fensofserech/sizechanging>, ... ]'

      markdown <<~DOC

        | Attribute                     | Type           | Required | Description |
        | ----------------------------- | -------------- | -------- | ----------- | owned by the authenticated user |
        | archived                    | boolean        | no       | Limit by archived status |
        | visibility                  | string         | no       | Limit by visibility public, internal, or private |
        | order_by                    | string         | no       | Return projects ordered by id, name, path, created_at, updated_at, or last_activity_at fields. Default is created_at |
        | sort                        | string         | no       | Return projects sorted in asc or desc order. Default is desc |
        | search                      | string         | no       | Return list of authorized projects matching the search criteria |
        | simple                      | boolean        | no       | Return only the ID, URL, name, and path of each project |
        | owned                       | boolean        | no       | Limit by projects owned by the current user |
        | starred                     | boolean        | no       | Limit by projects starred by the current user |
        | with_issues_enabled         | boolean        | no       | Limit by projects with issues feature enabled. Default is false |
        | with_merge_requests_enabled | boolean        | no       | Limit by projects with merge requests feature enabled. Default is false |
        | with_shared                 | boolean        | no       | Include projects shared to this group. Default is true |
        | include_subgroups           | boolean        | no       | Include projects in subgroups of this group. Default is false   |
        | min_access_level            | integer        | no       | Limit to projects where current user has at least this [access level](members.md) |
        | with_custom_attributes      | boolean        | no       | Include [custom attributes](custom_attributes.md) in response (admins only) |
        | with_security_reports       | boolean        | no       | Return only projects that have security reports artifacts present in any of their builds. This means "projects with security reports enabled". Default is false |
      DOC
    end

    doc 'List' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.list.last
        group.projects
      DOC
    end

    def projects(group_id, query = {})
      group_id = format_id(group_id)

      # Map Skip Groups
      format_query_ids(:skip_groups, query)

      query_access_level(query, :min_access_level)

      client.request(:get, "groups/#{group_id}/projects", Project, query)
    end
  end
end
