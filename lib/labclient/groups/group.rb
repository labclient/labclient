# Top namespace
module LabClient
  # Inspect Helper
  class Group < Klass
    include ClassHelpers
    def inspect
      "#<Group id: #{id}, #{full_path}>"
    end

    def subgroups(query = {})
      client.groups.subgroups(id, query)
    end

    def update(query = {})
      update_self client.groups.update(id, query)
    end

    def delete
      client.groups.delete(id)
    end

    def search(scope, search_string = '')
      client.groups.search_within(id, scope, search_string)
    end

    def restore
      client.groups.restore(id)
    end

    def transfer(project_id)
      client.groups.transfer(id, project_id)
    end

    def audit_events
      client.audit_events.group_list(id)
    end

    def audit_event(audit_event_id)
      client.audit_events.group_show(id, audit_event_id)
    end

    def projects(query = {})
      # Details Query Includes Projects
      if query.empty? && !@table[:projects].blank?
        @table[:projects].map { |project| LabClient::Project.new(project, response, client) }
      else
        client.groups.projects(id, query)
      end
    end

    def project_create(query)
      query[:namespace_id] = id
      client.projects.create(query)
    end

    def ldap_list
      client.groups.ldap.list(id)
    end

    alias ldap_links ldap_list

    def ldap_sync
      client.groups.ldap.sync(id)
    end

    def ldap_create(query)
      client.groups.ldap.create(id, query)
    end

    def ldap_delete(name, provider = nil)
      client.groups.ldap.delete(id, name, provider)
    end

    def details(query = {})
      update_self client.groups.show(id, query)
    end

    def runners(query = {})
      client.groups.runners(id, query)
    end

    # Members
    def members(query = nil)
      client.members.groups.list(id, query)
    end

    def members_all(query = nil)
      client.members.groups.list_all(id, query)
    end

    def member(user_id)
      client.members.groups.show(id, user_id)
    end

    def member_all(user_id)
      client.members.groups.show_all(id, user_id)
    end

    def member_add(user_id, query)
      client.members.groups.add(id, user_id, query)
    end

    def member_update(user_id, query)
      client.members.groups.update(id, user_id, query)
    end

    def member_delete(user_id)
      client.members.groups.delete(id, user_id)
    end

    # Group Hooks
    def hooks
      client.groups.hooks.list(id)
    end

    def hook_show(hook_id)
      client.groups.hooks.show(id, hook_id)
    end

    def hook_create(query)
      client.groups.hooks.create(id, query)
    end

    def hook_update(hook_id, query)
      client.groups.hooks.update(id, hook_id, query)
    end

    def hook_delete(hook_id)
      client.groups.hooks.delete(id, hook_id)
    end

    # Group Badges
    def badges
      client.groups.badges.list(id)
    end

    def badge_show(badge_id)
      client.groups.badges.show(id, badge_id)
    end

    def badge_create(query)
      client.groups.badges.create(id, query)
    end

    def badge_update(badge_id, query)
      client.groups.badges.update(id, badge_id, query)
    end

    def badge_delete(badge_id)
      client.groups.badges.delete(id, badge_id)
    end

    # Group Clusters
    def clusters
      client.groups.clusters.list(id)
    end

    def cluster(cluster_id)
      client.groups.clusters.show(id, cluster_id)
    end

    # Group Epics
    def epics
      client.epics.list(id)
    end

    def epic_show(epic_id)
      client.epics.show(id, epic_id)
    end

    def epic_create(query)
      client.epics.create(id, query)
    end

    def epic_update(epic_id, query)
      client.epics.update(id, epic_id, query)
    end

    def epic_delete(epic_id)
      client.epics.delete(id, epic_id)
    end

    def epic_issues(epic_id)
      client.epics.issues(id, epic_id)
    end

    def epic_issue_add(epic_iid, issue_id)
      client.epics.issue_add(id, epic_iid, issue_id)
    end

    def epic_issue_remove(epic_iid, issue_id)
      client.epics.issue_remove(id, epic_iid, issue_id)
    end

    def epic_issue_update(epic_iid, issue_id, query)
      client.epics.issue_update(id, epic_iid, issue_id, query)
    end

    # Issues
    def issues(query = {})
      client.issues.group_issues(id, query)
    end

    # Merge Requests
    def merge_requests(query = {})
      client.merge_requests.list_group(id, query)
    end

    # Access Requests
    def request_access
      client.groups.access_requests.create(id)
    end

    def access_requests
      client.groups.access_requests.list(id)
    end

    # Labels
    def labels(query = {})
      client.groups.labels.list(id, query)
    end

    def label(label_id, query = {})
      client.groups.labels.show(id, label_id, query)
    end

    def label_create(query)
      client.groups.labels.create(id, query)
    end

    def label_delete(label_id)
      client.groups.labels.delete(id, label_id)
    end

    def label_update(label_id, query)
      client.groups.labels.update(id, label_id, query)
    end

    def label_subscribe(label_id)
      client.groups.labels.subscribe(id, label_id)
    end

    def label_unsubscribe(label_id)
      client.groups.labels.unsubscribe(id, label_id)
    end

    # Registry
    def registry_repositories(query = {})
      client.registry.group(id, query)
    end

    # Milestones
    def milestones(query = {})
      client.groups.milestones.list(id, query)
    end

    # Reload Helper
    def reload
      update_self client.groups.show(id)
    end

    # rubocop:disable Metrics/BlockLength
    help do
      subtitle 'Group'
      option 'issues', 'List group Issues [Hash]'
      option 'subgroups', 'List direct subgroups [Hash]'
      option 'audit_events', "List this group's AuditEvents"
      option 'audit_event', 'Show a specific AuditEvents [Audit Event ID]'
      option 'projects', 'List direct Projects [Hash]'
      option 'project_create', 'Create a project in this Group [Hash]'
      option 'details', 'Get all details [Hash]'
      option 'search', 'Search by scope within group [scope, search_string]'
      option 'delete', 'Delete this Group'
      option 'restore', 'Restore if marked for deletion'
      option 'transfer', 'Transfer project to this group [Project ID]'
      option 'ldap_list', 'Lists LDAP group links'
      option 'ldap_links', 'Alias for ldap_list'
      option 'ldap_sync', 'Syncs the group with its linked LDAP group'
      option 'ldap_create', 'Adds an LDAP group link. [Hash]'
      option 'ldap_delete', 'Deletes an LDAP group link. [CN, Provider(optional)]'
      option 'runners', 'List Group Runners. [Hash]'

      # Members
      option 'members', 'List group members [Hash]'
      option 'members_all', 'List group members, including inherited [Hash]'
      option 'member', 'Show specific member [User ID]'
      option 'member_all', 'Show specific member, including inherited [User ID]'
      option 'member_add', 'Add user to group [User ID, Hash]'
      option 'member_update', 'Update user on group [User ID, Hash]'
      option 'member_delete', 'Remove user from group [User ID]'

      # Hooks
      option 'hooks', "List this group's hooks"
      option 'hook_create', 'Create group hook [Hash]'
      option 'hook_delete', 'Delete group hook [Hook ID]'
      option 'hook_show', 'Show specific hook from group [Hook ID]'
      option 'hook_update', 'Update group hook [Hook ID, Hash]'

      # Badges
      option 'badges', "List this group's badges"
      option 'badge_create', 'Create group badge [Hash]'
      option 'badge_delete', 'Delete group badge [Badge ID]'
      option 'badge_show', 'Show specific badge from group [Badge ID]'
      option 'badge_update', 'Update group badge [Badge ID, Hash]'

      # Clusters
      option 'clusters', "List this group's clusters"
      option 'cluster', 'Show specific cluster from group [Cluster ID]'

      # Epics
      option 'epics', "List this group's epics"
      option 'epic_create', 'Create group epic [Hash]'
      option 'epic_delete', 'Delete group epic [Epic IID]'
      option 'epic_show', 'Show specific epic from group [Epic IID]'
      option 'epic_update', 'Update group epic [Epic IID, Hash]'
      option 'epic_issues', 'List epic Issues [Epic IID]'
      option 'epic_issue_add', 'Add issue to group epic [Epic IID, Issue ID]'
      option 'epic_issue_remove', 'Remove issue from group epic [Epic IID, Issue ID]'
      option 'epic_issue_update', 'Update issue group epic [Epic IID, Issue ID, Hash]'

      # Merge Requests
      option 'merge_requests', 'List group merge requests [Hash]'

      # Access Requests
      option 'access_requests', 'List access requests for group'
      option 'request_access', 'Request access to group'

      # Labels
      option 'labels', "List this group's labels"
      option 'label', 'Show specific label from group [Label ID]'
      option 'label_create', 'Create group label [Hash]'
      option 'label_delete', 'Delete group label [Label ID]'
      option 'label_update', 'Update group label [Label ID, Hash]'
      option 'label_subscribe', 'Subscribe to label notifications [Label ID]'
      option 'label_unsubscribe', 'Unsubscribe to label notifications [Label ID]'

      # Registry
      option 'registry_repositories', 'Get a list of registry repositories [Hash]'

      # Reload Helper
      option 'reload', 'Reload this object (New API Call)'
    end
    # rubocop:enable Metrics/BlockLength
  end
end
