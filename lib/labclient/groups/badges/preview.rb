# rubocop:disable Style/FormatStringToken
# Top namespace
module LabClient
  # Specifics
  class GroupBadges < Common
    doc 'Preview' do
      desc 'Returns how the link_url and image_url final URLs would be after resolving the placeholder interpolation. [Group ID, Badge ID]'
      example <<~DOC
        client.groups.badges.preview(
          100,
          link_url:'https://example.gitlab.com/%{project_path}',
          image_url:'https://example.gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg'
        )
      DOC
    end

    doc 'Preview' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | name | string | no | Name for Badge |
        | link_url | string | yes | URL of the badge link |
        | image_url | string | yes | URL of the badge image |
      DOC
    end

    doc 'Preview' do
      desc 'Via GroupBadge'
      example <<~DOC
        badge = client.groups.badges.show(100, 4)
        badge.preview
      DOC
    end

    def preview(group_id, query)
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}/badges/render", nil, query)
    end
  end
end
# rubocop:enable Style/FormatStringToken
