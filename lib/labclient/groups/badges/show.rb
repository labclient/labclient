# Top namespace
module LabClient
  # Specifics
  class GroupBadges < Common
    doc 'Show' do
      desc 'Gets a badge of a group. [Group ID, Badge ID]'
      example 'client.groups.badges.show(100, 1)'

      result '=>  #<GroupBadge id: 1, name: first>'
    end

    doc 'Show' do
      desc 'Via Group [Badge ID]'
      example <<~DOC
        group = client.groups.show(16)
        badge = group.badge_show(1)
      DOC
    end

    def show(group_id, badge_id)
      group_id = format_id(group_id)
      badge_id = format_id(badge_id)

      client.request(:get, "groups/#{group_id}/badges/#{badge_id}", GroupBadge)
    end
  end
end
