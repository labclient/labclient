# Top namespace
module LabClient
  # Specifics
  class GroupBadges < Common
    doc 'Update' do
      desc 'Updates a badge of a group. [Group ID, Badge ID, Params]'
      example 'client.groups.badges.update(100, 3, name: "planet bob")'

      result '=>  #<GroupBadge id: 3, name: planet bob>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | name | string | no | Name for Badge |
        | link_url | string | no | URL of the badge link |
        | image_url | string | no | URL of the badge image |
      DOC
    end

    doc 'Update' do
      desc 'Via Group [Badge ID, Params]'
      example <<~DOC
        group = client.groups.show(16)
        badge = group.badge_update(4, name: "another")
      DOC
    end

    doc 'Update' do
      desc 'Via GroupBadge [Params]'
      example <<~DOC
        badge = client.groups.badges.show(100, 4)
        badge.update(name: "switchy")
      DOC
    end

    def update(group_id, badge_id, query)
      group_id = format_id(group_id)
      badge_id = format_id(badge_id)

      client.request(:put, "groups/#{group_id}/badges/#{badge_id}", GroupBadge, query)
    end
  end
end
