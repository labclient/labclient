# Top namespace
module LabClient
  # Specifics
  class GroupBadges < Common
    doc 'Delete' do
      desc 'Removes a badge from a group. [Group ID, Badge ID]'
      example 'client.groups.badges.delete(16, 1)'
    end

    doc 'Delete' do
      desc 'Via Group [Badge ID]'
      example <<~DOC
        group = client.groups.show(100)
        group.badge_delete(3)
      DOC
    end

    doc 'Delete' do
      desc 'Via GroupBadge'
      example <<~DOC
        badge = client.groups.badges.show(100, 4)
        badge.delete
      DOC
    end

    def delete(group_id, badge_id)
      group_id = format_id(group_id)
      badge_id = format_id(badge_id)

      client.request(:delete, "groups/#{group_id}/badges/#{badge_id}")
    end
  end
end
