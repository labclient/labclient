# Top namespace
module LabClient
  # Inspect Helper
  class GroupBadge < Klass
    include ClassHelpers
    def inspect
      "#<GroupBadge id: #{id}, name: #{name}>"
    end

    def group
      group_id = collect_group_id
      client.groups.show group_id
    end

    def update(query)
      group_id = collect_group_id

      update_self client.groups.badges.update(group_id, id, query)
    end

    def delete
      group_id = collect_group_id

      client.groups.badges.delete(group_id, id)
    end

    def preview
      group_id = collect_group_id

      client.groups.badges.preview(group_id, {
                                     link_url: link_url,
                                     image_url: image_url
                                   })
    end

    date_time_attrs %i[created_at]

    help do
      @group_name = 'Groups'
      subtitle 'GroupBadge'
      option 'group', 'Show Group'
      option 'update', 'Update this badge [Hash]'
      option 'delete', 'Delete this badge'
      option 'preview', 'Show render of interpolation'
    end
  end
end
