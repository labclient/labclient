# rubocop:disable Style/FormatStringToken
# Top namespace
module LabClient
  # Specifics
  class GroupBadges < Common
    doc 'Create' do
      desc 'Adds a badge to a group. [Group ID, Hash]'
      example <<~DOC
        client.groups.badges.create(
          100,
          name: 'rawr',
          link_url:'https://example.gitlab.com/%{project_path}',
          image_url:'https://example.gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg'
        )
      DOC

      result '=>  #<GroupBadge id: 2, url: http://labclient.com>'
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | name | string | no | Name for Badge |
        | link_url | string | yes | URL of the badge link |
        | image_url | string | yes | URL of the badge image |
      DOC
    end

    doc 'Create' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.show(100)
        badge = group.badge_create(
          name: 'rawr',
          link_url:'https://example.gitlab.com/%{project_path}',
          image_url:'https://example.gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg'
        )
      DOC
    end

    def create(group_id, query)
      group_id = format_id(group_id)

      client.request(:post, "groups/#{group_id}/badges", GroupBadge, query)
    end
  end
end
# rubocop:enable Style/FormatStringToken
