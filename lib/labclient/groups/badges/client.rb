# Top namespace
module LabClient
  # Inspect Helper
  class GroupBadges < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    def badges
      GroupBadges.new(client)
    end
  end
end
