# Top namespace
module LabClient
  # Specifics
  class GroupBadges < Common
    doc 'List' do
      desc 'Gets a list of a group’s badges. [Group ID]'
      example 'client.groups.badges.list(16)'

      result <<~DOC
        => [#<GroupBadge id: 2, name: first >, #<GroupBadge id: 3, name: rawr>]
      DOC
    end

    doc 'List' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.show(16)
        group.hooks
      DOC
    end

    def list(group_id)
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}/badges", GroupBadge)
    end
  end
end
