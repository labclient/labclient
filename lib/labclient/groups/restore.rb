# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    doc 'Delete' do
      title 'Restore'
      desc 'Restores a group marked for deletion. [Group ID]'
      example 'client.groups.restore(3)'
      result '=> #<Group id: 68, deleted>'
    end

    doc 'Delete' do
      desc 'via Group'
      example <<~DOC
        group = client.group.show(3)
        group.restore
      DOC
    end

    def restore(group_id)
      group_id = format_id(group_id)

      client.request(:post, "groups/#{group_id}/restore", Group)
    end
  end
end
