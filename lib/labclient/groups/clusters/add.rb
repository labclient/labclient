# Top namespace
module LabClient
  # Specifics
  class GroupClusters < Common
    doc 'Add' do
      desc 'Adds an existing Kubernetes cluster to the group. [Group ID, Hash]'
      example <<~DOC
        params = {
          name: 'cluster-5',
          platform_kubernetes_attributes: {
            api_url: 'https://35.111.51.20',
            token: '12345',
            ca_cert: "-----BEGIN .....-----END CERTIFICATE-----"
          }
        }

          client.groups.clusters.add(310, params)
      DOC

      result '=>  #<GroupCluster id: 2, name: cluster-5>'
    end

    doc 'Add' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | name      | string | yes | The name of the cluster |
        | domain      | string | no | The base domain of the cluster |
        | management_project_id | integer | no | The ID of the management project for the cluster |
        | enabled      | boolean | no | Determines if cluster is active or not, defaults to true |
        | managed      | boolean | no | Determines if GitLab will manage namespaces and service accounts for this cluster, defaults to true |
        | platform_kubernetes_attributes[api_url] | string | yes | The URL to access the Kubernetes API |
        | platform_kubernetes_attributes[token] | string | yes | The token to authenticate against Kubernetes |
        | platform_kubernetes_attributes[ca_cert] | string | no | TLS certificate. Required if API is using a self-signed TLS certificate. |
        | platform_kubernetes_attributes[authorization_type] | string | no | The cluster authorization type: rbac, abac or unknown_authorization. Defaults to rbac. |
        | environment_scope | string | no | The associated environment to the cluster. Defaults to * **(PREMIUM)** |
      DOC
    end

    def add(group_id, query)
      group_id = format_id(group_id)

      client.request(:post, "groups/#{group_id}/clusters/user", GroupCluster, query)
    end
  end
end
