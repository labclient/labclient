# Top namespace
module LabClient
  # Inspect Helper
  class GroupCluster < Klass
    include ClassHelpers
    def inspect
      "#<GroupCluster id: #{id}, name: #{name}>"
    end

    def group
      group_id = collect_group_id
      client.groups.show group_id
    end

    def update(query)
      group_id = collect_group_id

      update_self client.groups.clusters.update(group_id, id, query)
    end

    def delete
      group_id = collect_group_id

      client.groups.clusters.delete(group_id, id)
    end

    date_time_attrs %i[created_at]
    user_attrs %i[uster]

    help do
      subtitle 'GroupCluster'
      option 'group', 'Show Group'
      option 'update', 'Update this Cluster [Hash]'
      option 'delete', 'Delete cluster'
    end
  end
end
