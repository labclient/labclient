# Top namespace
module LabClient
  # Inspect Helper
  class GroupClusters < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    def clusters
      GroupClusters.new(client)
    end
  end
end
