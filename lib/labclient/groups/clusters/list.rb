# Top namespace
module LabClient
  # Specifics
  class GroupClusters < Common
    doc 'List' do
      desc 'Returns a list of group clusters. [Group ID]'
      example 'client.groups.clusters.list(16)'

      result <<~DOC
        => #<GroupCluster id: 1, name: cluster-5>]
      DOC
    end

    doc 'List' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.show(16)
        group.clusters
      DOC
    end

    def list(group_id)
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}/clusters", GroupCluster)
    end
  end
end
