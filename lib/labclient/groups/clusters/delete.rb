# Top namespace
module LabClient
  # Specifics
  class GroupClusters < Common
    doc 'Delete' do
      desc 'Deletes an existing group cluster. [Project ID, Mirror ID, Params]'
      example 'client.group.clusters.update(310, 3, enabled: true)'

      result '=>  #<GroupCluster id: 3, name: planet bob>'
    end

    doc 'Delete' do
      markdown <<~DOC
        | Attribute              | Type    | Required   | Description                                         |
        | ----------             | -----   | ---------  | ------------                                        |
        | `id` | integer/string | yes | The ID or URL-encoded path of the group |
        | `cluster_id` | integer | yes | The ID of the cluster |
      DOC
    end

    doc 'Delete' do
      desc 'Via GroupCluster [Params]'
      example <<~DOC
        cluster = client.groups.clusters.list(310).first
        cluster.delete
      DOC
    end

    def delete(group_id, cluster_id)
      group_id = format_id(group_id)
      cluster_id = format_id(cluster_id)

      client.request(:delete, "groups/#{group_id}/clusters/#{cluster_id}", GroupCluster)
    end
  end
end
