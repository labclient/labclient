# Top namespace
module LabClient
  # Specifics
  class GroupClusters < Common
    doc 'Show' do
      desc 'Gets a single group cluster. [Group ID, Cluster ID]'
      example 'client.groups.clusters.list(16)'

      result <<~DOC
        => #<GroupCluster id: 1, name: cluster-5>
      DOC
    end

    doc 'Show' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.show(16)
        group.cluster(cluster_id)
      DOC
    end

    def show(group_id, cluster_id)
      cluster_id = format_id(cluster_id)
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}/clusters/#{cluster_id}", GroupCluster)
    end
  end
end
