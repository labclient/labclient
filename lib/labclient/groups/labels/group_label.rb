# Top namespace
module LabClient
  # Inspect Helper
  class GroupLabel < Klass
    include ClassHelpers
    def inspect
      "#<GroupLabel id: #{id}, name: #{name}>"
    end

    def group
      group_id = collect_group_id
      client.groups.show group_id
    end

    def update(query)
      group_id = collect_group_id

      update_self client.groups.labels.update(group_id, id, query)
    end

    def delete
      group_id = collect_group_id

      client.groups.labels.delete(group_id, id)
    end

    def subscribe
      group_id = collect_group_id

      client.groups.labels.subscribe(group_id, id)
    end

    def unsubscribe
      group_id = collect_group_id

      client.groups.labels.unsubscribe(group_id, id)
    end

    help do
      @group_name = 'Groups'
      subtitle 'GroupLabel'
      option 'group', 'Show Group'
      option 'update', 'Update this label [Hash]'
      option 'delete', 'Delete this label'
      option 'subscribe', 'Subscribe to label notifications'
      option 'unsubscribe', 'Unsubscribe to label notifications'
    end
  end
end
