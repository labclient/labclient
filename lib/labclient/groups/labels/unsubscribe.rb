# Top namespace
module LabClient
  # Specifics
  class GroupLabels < Common
    doc 'Unsubscribe' do
      desc 'Unsubscribes the authenticated user from a label to not receive notifications from it [Group ID, Label ID]'
      example 'client.groups.labels.unsubscribe(16, 7)'

      result '=> #<GroupLabel id: 7, name: switchy>'
    end

    doc 'Unsubscribe' do
      desc 'Via Group [Label ID]'
      example <<~DOC
        group = client.groups.show(16)
        group.label_subscribe(7)
      DOC
    end

    doc 'Unsubscribe' do
      desc 'Via GroupLabel'
      example <<~DOC
        label = client.groups.labels.show(16,7)
        label.unsubscribe
      DOC
    end

    def unsubscribe(group_id, label_id)
      group_id = format_id(group_id)
      label_id = format_id(label_id)

      client.request(:post, "groups/#{group_id}/labels/#{label_id}/unsubscribe", GroupLabel)
    end
  end
end
