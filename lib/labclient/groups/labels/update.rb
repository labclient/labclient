# Top namespace
module LabClient
  # Specifics
  class GroupLabels < Common
    doc 'Update' do
      desc 'Updates an existing label with new name or new color. At least one parameter is required, to update the label. [Group ID, Label ID, Hash]'
      example 'client.groups.labels.update(16, 7, new_name: "planet bob")'

      result '=> #<GroupLabel id: 7, name: planet bob>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute     | Type    | Required | Description                  |
        | ------------- | ------- | -------- | ---------------------------- |
        | new_name      | string  | yes      | The name of the label        |
        | color         | string  | yes      | The color of the label given in 6-digit hex notation or CSS Color Names|
        | description   | string  | no       | The description of the label |
        | priority      | integer | no       | The priority of the label. Must be greater or equal than zero or null to remove the priority. |
      DOC
    end

    doc 'Update' do
      desc 'Via Group [Label ID, Hash]'
      example <<~DOC
        group = client.groups.show(16)
        label = group.label_update(7, new_name: "another")
      DOC
    end

    doc 'Update' do
      desc 'Via GroupLabel [Hash]'
      example <<~DOC
        label = client.groups.labels.show(16, 7)
        label.update(new_name: "switchy")
      DOC
    end

    def update(group_id, label_id, query)
      group_id = format_id(group_id)
      label_id = format_id(label_id)

      client.request(:put, "groups/#{group_id}/labels/#{label_id}", GroupLabel, query)
    end
  end
end
