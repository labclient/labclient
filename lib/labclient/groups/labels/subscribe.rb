# Top namespace
module LabClient
  # Specifics
  class GroupLabels < Common
    doc 'Subscribe' do
      desc 'Subscribes the authenticated user to a label to receive notifications [Group ID, Label ID]'
      example 'client.groups.labels.subscribe(16, 7)'

      result '=> #<GroupLabel id: 7, name: switchy>'
    end

    doc 'Subscribe' do
      desc 'Via Group [Label ID]'
      example <<~DOC
        group = client.groups.show(16)
        label = group.label_subscribe(7)
      DOC
    end

    doc 'Subscribe' do
      desc 'Via GroupLabel'
      example <<~DOC
        label = client.groups.labels.show(16,7)
        label.subscribe
      DOC
    end

    def subscribe(group_id, label_id)
      group_id = format_id(group_id)
      label_id = format_id(label_id)

      client.request(:post, "groups/#{group_id}/labels/#{label_id}/subscribe", GroupLabel)
    end
  end
end
