# Top namespace
module LabClient
  # Inspect Helper
  class GroupLabels < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    def labels
      GroupLabels.new(client)
    end
  end
end
