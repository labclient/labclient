# Top namespace
module LabClient
  # Specifics
  class GroupLabels < Common
    doc 'Delete' do
      desc 'Deletes a label with a given name. [Group ID, Label ID]'
      example 'client.groups.labels.delete(16, 7)'
    end

    doc 'Delete' do
      desc 'Via Group [Label ID]'
      example <<~DOC
        group = client.groups.show(16)
        group.label_delete(7)
      DOC
    end

    doc 'Delete' do
      desc 'Via GroupLabel'
      example <<~DOC
        label = client.groups.labels.show(16, 7)
        label.delete
      DOC
    end

    def delete(group_id, label_id)
      group_id = format_id(group_id)
      label_id = format_id(label_id)

      client.request(:delete, "groups/#{group_id}/labels/#{label_id}", GroupLabel)
    end
  end
end
