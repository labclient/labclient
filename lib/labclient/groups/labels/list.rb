# Top namespace
module LabClient
  # Specifics
  class GroupLabels < Common
    doc 'List' do
      desc 'Get all labels for a given group. [Group ID]'
      example 'client.groups.labels.list(16)'

      result <<~DOC
        => [#<GroupLabel id: 2, name: feature>, ...]
      DOC

      markdown <<~DOC
        | Attribute               | Type           | Description                                                                  |
        | ---------               | -------        | ---------------------                                                        |
        | with_counts             | boolean        | Whether or not to include issue and merge request counts. Defaults to false. |
        | include_ancestor_groups | boolean        | Include ancestor groups. Defaults to true.                                   |
      DOC
    end

    doc 'List' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.show(16)
        group.labels
      DOC
    end

    def list(group_id, query = {})
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}/labels", GroupLabel, query)
    end
  end
end
