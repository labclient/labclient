# Top namespace
module LabClient
  # Specifics
  class GroupLabels < Common
    doc 'Show' do
      desc 'Get a single label for a given group. [Group ID, Label ID]'
      example 'client.groups.labels.show(16, 7)'

      result '=>  #<GroupLabel id: 2, name: feature>'

      markdown <<~DOC
        | Attribute               | Type    | Description                                                                  |
        | ---------               | ------- | ---------------------                                                        |
        | with_counts             | boolean | Whether or not to include issue and merge request counts. Defaults to false. |
        | include_ancestor_groups | boolean | Include ancestor groups. Defaults to true.                                   |
      DOC
    end

    doc 'Show' do
      desc 'Via Group [Label ID]'
      example <<~DOC
        group = client.groups.show(16)
        label = group.label(7)
      DOC
    end

    def show(group_id, label_id, query = {})
      group_id = format_id(group_id)
      label_id = format_id(label_id)

      client.request(:get, "groups/#{group_id}/labels/#{label_id}", GroupLabel, query)
    end
  end
end
