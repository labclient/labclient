# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    doc 'Runners' do
      desc 'List all runners (specific and shared) available in the group as well it’s ancestor groups. Shared runners are listed if at least one shared runner is defined. [Group ID, Hash]'
      example 'client.groups.runners(15)'
      result '[#<Runner id: 1, status: online>, ... ]'

      markdown <<~DOC

        | Attribute  | Type           | Required | Description         |
        |------------|----------------|----------|---------------------|
        | type     | string         | no       | The type of runners to show, one of: instance_type, group_type, project_type |
        | status   | string         | no       | The status of runners to show, one of: active, paused, online, offline |
        | tag_list | string array   | no       | List of of the runner's tags |
      DOC
    end

    doc 'Runners' do
      desc 'Via Group [Hash]'
      example <<~DOC
        group = client.groups.show(16)
        group.runners
      DOC
    end

    def runners(group_id, query = {})
      group_id = format_id(group_id)
      client.request(:get, "groups/#{group_id}/runners", Runner, query)
    end
  end
end
