# Top namespace
# Uses the '/search?scope=projects' endpoint
module LabClient
  # Projects Specifics
  class Groups < Common
    doc 'Search' do
      desc 'Search for Group, or within the Group with scopes'

      markdown <<~DOC
        Search within Group scopes

        - projects
        - issues
        - merge_requests
        - milestones
        - wiki_blobs
        - commits
        - blobs
        - users
      DOC
    end

    doc 'Search' do
      subtitle 'Examples'
      desc 'Search for Groups'
      example "client.groups.search 'alpha'"
      result '=> [#<Group id: 75, alphabetrium>, #<Group id: 97, alphaquadrant>]'
    end

    doc 'Search' do
      desc 'Search within a group with :issues scope'
      example "client.projects.search_within(311, :issues, 'rawr')"
      result '=> [#<Issue id: 1, title: rawr, state: opened>]'
    end

    doc 'Search' do
      desc 'Search within a Group with :users scope'
      example "client.projects.search_within(311, :users, 'username')"
      result '=>  [#<User id: 1, username: root>]'
    end

    doc 'Search' do
      desc 'Search via Group'
      example <<~DOC
        group = client.groups.show(16)
        group.search(:issues, 'sweet search')
      DOC
    end

    # Search All Projects
    def search_within(group_id, scope, search_string = '')
      group_id = format_id(group_id)

      scope = scope.to_sym # Case Simplicity
      query = { scope: scope, search: search_string }

      klass = klass_type(scope)

      client.request(:get, "groups/#{group_id}/search", klass, query)
    end

    # Search Groups
    def search(search_string = '')
      query = {
        search: search_string
      }

      client.request(:get, 'groups', Group, query)
    end

    private

    # TODO: - Finish Classes
    def klass_type(scope)
      case scope
      when :projects then Project
      when :issues then Issue
      when :merge_requests then MergeRequest
      when :milestones, :wiki_blobs, :blobs then nil
      when :commits then Commit
      when :users then User
      end
    end
  end
end
