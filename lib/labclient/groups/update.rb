# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    doc 'Update' do
      example 'client.groups.update(3, name: "Sweet Name")'
      result '=> #<Group id: 68, fensofserech>'

      markdown <<~DOC
        Updates the project group [Group ID , Hash]. See [Docs](https://docs.gitlab.com/ee/api/groups.html#update-group) for available options.

        | Attribute                            | Type    | Required | Description |
        | ------------------------------------ | ------- | -------- | ----------- |
        | name                               | string  | no       | The name of the group. |
        | path                               | string  | no       | The path of the group. |
        | description                        | string  | no       | The description of the group. |
      DOC
    end

    doc 'Update' do
      desc 'via Group'
      example <<~DOC
        group = client.group.show(3)
        group.update(name: 'rawr')
      DOC
    end

    def update(group_id, query = {})
      group_id = format_id(group_id)

      client.request(:put, "groups/#{group_id}", Group, query)
    end
  end
end
