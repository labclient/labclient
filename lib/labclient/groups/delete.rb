# Top namespace
module LabClient
  # Specifics
  class Groups < Common
    doc 'Delete' do
      desc 'Removes group, and queues a background job to delete all projects in the group as well. [Group ID]'
      example 'client.groups.delete(3)'
      result '=> {:message=>"202 Accepted"}'
    end

    doc 'Delete' do
      desc 'via Group'
      example <<~DOC
        group = client.group.show(3)
        group.delete
      DOC
    end

    def delete(group_id)
      group_id = format_id(group_id)

      client.request(:delete, "groups/#{group_id}", nil)
    end
  end
end
