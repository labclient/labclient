# The glorious gitlab labclient of labs
module LabClient
  # Helper for Docs
  class Overview
    extend Docs

    doc 'Overview' do
      title 'About'
      markdown <<~DOC
        LabClient is a Gitlab API Gem. Focusing on ease of use, documentation, helpers, and snippets.

        - Objects for resources (User, Project)
        - Method Chaining ( project.branch(:master).pipelines )

        Lots of Helpers

        - Pagination
        - IRB/Pry
        - Setup: credentials file, profiles, and prompt
        - Permission Levels
        - Curl generator

        Here is a quick whirlwind example of some of the features:
      DOC

      demo '350689'
    end

    doc 'Overview' do
      title 'Setup'
      desc 'Install from RubyGems'
      example 'gem install labclient'
    end

    doc 'Overview' do
      title 'Initialize Client'
      example <<~DOC
        client = LabClient::Client.new(
          url: 'https://gitlab.labclient',
          token: 'gitlab api token',
          ssl_verify: false
        )
      DOC
    end

    doc 'Authentication' do
      title 'Credentials File'
      desc 'The client will use ~/.gitlab-labclient for credentials. Configuring this will automatically setup client'
      example <<~DOC
        {
          "url": "https://labclient",
          "token": "super_secure",
          "ssl_verify": false,
          "paginate": false
        }
      DOC
    end

    doc 'Authentication' do
      title 'Named Profiles'
      markdown 'Within the `~/.gitlab-labclient` you can also specify profile names'
      example <<~DOC
        {
          "prod": {
            "url": "https://labclient-prod",
            "token": "super_secure",
            "paginate": false
          },
          "dev": {
            "url": "https://labclient-dev",
            "token": "super_secure",
            "ssl_verify": false,
            "paginate": true
          }
        }
      DOC
    end

    doc 'Authentication' do
      desc 'Manually select a named profile'
      example <<~DOC
        client = LabClient::Client.new(profile: 'prod')
      DOC
    end

    doc 'Authentication' do
      desc 'Automatically select via ENV Variables'
      example <<~DOC
        ENV['LABCLIENT_PROFILE'] = 'dev'
        client = LabClient::Client.new
      DOC
    end

    doc 'Authentication' do
      title 'Environment Variables'
      desc 'If not provided, and credentials file does not exist will attempt to use ENV variables'
      example <<~DOC
        LABCLIENT_URL='http://gitlab.com'
        LABCLIENT_TOKEN='superAwesomeToken'
        client = LabClient::Client.new
      DOC
    end

    doc 'Authentication' do
      title 'OAuth Authorization'
      desc 'Use OAuth access tokens to make requests to the API on behalf of a user. '
      example <<~DOC
        client = LabClient::Client.new(
          url: 'https://gitlab.labclient',
          token: 'gitlab oauth token',
          token_type: 'Authorization'
        )
      DOC
    end
    doc 'Pagination' do
      desc 'Pagination is enabled by default if not specified. Can be explicitly enabled or disabled'
      example <<~DOC
        client = LabClient::Client.new(
          url: 'https://gitlab.labclient',
          token: 'gitlab api token',
          paginate: false
        )
      DOC
    end

    doc 'Pagination' do
      title 'auto_paginate'
      desc 'Automatically collect and return all results for a query. Accepts block'
      example 'client.users.list.auto_paginate'
      result <<~DOC
        [
          #<User id: 56, username: leonardmccoy>,
          #<User id: 55, username: harrykim>,
          #<User id: 54, username: princehumperdinck>,
          #<User id: 53, username: yorick>,
          #<User id: 52, username: anivia>,
          ...
        ]
      DOC
    end

    doc 'Pagination' do
      markdown '`auto_paginate` with a block'
      example <<~DOC
        client.users.list.auto_paginate do |page|
          puts page.length
        end
      DOC

      result <<~DOC
        20
        20
        16
      DOC
    end

    doc 'Pagination' do
      title 'each_page'
      markdown 'Similar with `auto_paginate`, you can call `each_page` directly'
      example <<~DOC
        client.users.list.each_page do |page|
          puts page.length
        end
      DOC

      result <<~DOC
        20
        20
        16
      DOC
    end

    doc 'Pagination' do
      title 'paginate_with_limit'
      desc 'Iterate through pages, but end when a certain limit is reached'
      example <<~DOC
        client.users.list.paginate_with_limit(25)
      DOC

      result <<~DOC
        # 5 API Calls, 5 Results per page
        users = client.users.list(per_page: 5).paginate_with_limit(25)
        users.length
        => 25
      DOC
    end

    doc 'Pagination' do
      desc 'Iterate through pages with a block'
      example <<~DOC
        client.users.list.paginate_with_limit(25) do |page|
          puts page.length
        end
      DOC

      result <<~DOC
        # 5 API Calls, 5 Results per page
        users = client.users.list(per_page: 5).paginate_with_limit(25)
        users.length
        => 25
      DOC
    end

    doc 'Pagination' do
      title 'Other Pagination Helpers'
    end

    doc 'Pagination' do
      desc 'Total Results'
      example <<~DOC
        client.users.list.total
      DOC
    end

    doc 'Pagination' do
      desc 'Total Pages'
      example <<~DOC
        client.users.list.total_pages
      DOC
    end

    doc 'Pagination' do
      desc 'Current Page'
      example <<~DOC
        client.users.list.page
      DOC
    end

    doc 'Response' do
      title 'Status'
      desc 'Each object/call will return a "response" object or the instantiated class object. On each of these you can use "success?" to verify the response of a call.'

      result <<~DOC
        project = client.projects.show 123123
        # => #<TyphoeusResponse code: 404>
        project.success?
        # => false

        user = client.users.show 1
        => #<User id: 1, username: root>
        user.success?
        => true
      DOC

      result <<~DOC
        curl -k -H "Private-Token: super_secret_token" -H "Accept: application/json" "https://labclient/api/v4/users?"
      DOC
    end

    doc 'Other' do
      title 'Common Helpers'
      markdown <<~DOC
        The main api namespaces have a `help` and an `api_methods` methods. These will print out available methods and examples.

        These each accept a filter argument
      DOC

      result <<~DOC
        # General Help / Available Methods
        client.help
        client.api_methods

        # Show Project specific Methods
        client.projects.api_methods
        # => [:show, :list, :create]

        # Show Users specific help
        client.users.help
        Users
          Available Methods
            - list show

        List
          - client.users.list
          - client.users.list(username: "rakan")
          - client.users.list(search: "Sid")
          - client.users.list(active: true)

        User
          - client.users.show(5)
          - client.users.show(5)

        # General Help Filter
        client.help :protect
        Available Methods
        - protected_branches protected_environments protected_tags

        # Projects Help Filter
        client.projects.help :cluster
        Projects
          Available Methods
              - clusters

        # Project Object Help
        project = client.projects.show(16)
        project.help

        # Project Object Help Filter
        project.help :member
      DOC
    end

    doc 'Other' do
      title 'Object Helpers'
      markdown <<~DOC
        Each created object will have additional helper methods
      DOC

      result <<~DOC
        project = client.projects.show(5)

        # Awesome Print all details
        project.verbose

        # Awesome Print all details
        project.valid_group_project_levels
        # => [:guest, :reporter, :developer, :maintainer, :owner]

        # Select Data from objects with `slice`
        project.slice(:readme_url, :owner)
      DOC
    end

    doc 'Other' do
      desc 'Object Help'

      result <<~DOC
        project = client.projects.show(5)

        # List Possible Methods
        project.help

        # Array result of methods
        project.api_methods
      DOC
    end

    doc 'Other' do
      title 'Object IDs'

      markdown <<~DOC
        LabClient will automatically escape Project ID parameters that are namespaces. [More info](https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding)

        This lets you pass Project ID parameters in several ways.
      DOC

      example <<~DOC
        client.projects.show(16)
        client.projects.show('16')
        client.projects.show('group/project')
      DOC
    end

    doc 'Other' do
      desc 'Or pass LabClient Objects into API Calls'

      example <<~DOC
        project = client.projects.show(16)
        client.commits.list project
        # => Project Commits
      DOC
    end

    doc 'Other' do
      title 'Retry'
      markdown <<~DOC
        Gitlab.com and other instances may have protected paths or rate limiting enabled. By default the LabClient will:

        1. Check if a `Retry Later` request was received (Return Code 429)
        2. Wait for combined `retry-after` and `delay_factor` value
        3. Retry until retries `max` is reached
      DOC

      example <<~DOC
        client = LabClient::Client.new(
          url: 'https://gitlab.labclient',
          token: 'gitlab api token',
          retry: {
            max: 3, delay_factor: 2
          }
        )

        # Or after the init
        @client.settings[:retry] = { max: 3, delay_factor: 2 }
      DOC
    end

    doc 'Other' do
      title 'Quiet'
      desc 'Error messages by default are printed to STDOUT. This can be supressed via the :quiet setting'

      example <<~DOC
        client = LabClient::Client.new(
          url: 'https://gitlab.labclient',
          token: 'gitlab api token',
          quiet: true
        )

        # Or after the init
        client.settings[:quiet] = true
      DOC
    end

    doc 'Other' do
      title 'Debug'
      desc 'Print Request Information'

      example <<~DOC
        client = LabClient::Client.new(
          url: 'https://gitlab.labclient',
          token: 'gitlab api token',
          debug: true
        )

        # Or after the init
        client.settings[:debug] = true
      DOC
    end

    doc 'Other' do
      title 'Curl'
      desc 'Sometimes you just wana have a curl example to test or send someone'
      markdown <<~DOC
        This will attempt to make a curl request from a generated request/response. **Note** this will include the private token!
      DOC

      example <<~DOC
        list = client.users.list # Get output
        list.curl # Show Curl
      DOC

      result <<~DOC
        curl -k -H "Private-Token: super_secret_token" -H "Accept: application/json" "https://labclient/api/v4/users?"
      DOC
    end

    doc 'Other' do
      title 'Useful Links'
      markdown <<~DOC
        - [Gitlab API Docs](https://docs.gitlab.com/ee/api/)
        - [API Authentication](https://docs.gitlab.com/ee/api/README.html#authentication)
      DOC
    end

    doc 'Docker' do
      desc 'A docker image is created from master and can be used to execute snippets or load a quick pry session'

      example 'docker run -it registry.gitlab.com/labclient/labclient'

      result <<~DOC
        # Run Container
        docker run -it registry.gitlab.com/labclient/labclient
        # Without settings configured, initial will prompt for credentials

        client.users.list
      DOC
    end

    doc 'Docker' do
      desc 'The docker container configured and execute code snippets'

      example 'docker run -it registry.gitlab.com/labclient/labclient'

      result <<~DOC
        docker run -it -e LABCLIENT_URL="https://labclient" -e LABCLIENT_TOKEN="secure_token" labclient 'puts client.users.list.count'
      DOC
    end
  end
end
