# Top namespace
module LabClient
  # Specifics
  class SystemHooks < Common
    doc 'Test' do
      desc 'Test System Hook [System Hook ID]'
      example 'client.system_hooks.test(1)'
      result <<~DOC
         {
           "project_id" : 1,
           "owner_email" : "labclient",
           "owner_name" : "Someone",
           "name" : "Ruby",
           "path" : "ruby",
           "event_name" : "project_create"
        }
      DOC
    end

    def test(system_hook_id)
      system_hook_id = format_id(system_hook_id)
      client.request(:get, "hooks/#{system_hook_id}")
    end
  end
end
