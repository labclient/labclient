# Top namespace
module LabClient
  # Inspect Helper
  class SystemHook < Klass
    include ClassHelpers
    def inspect
      "#<SystemHook id: #{id}, url: #{url}>"
    end

    def delete
      client.system_hooks.delete(id)
    end

    help do
      subtitle 'System Hook'
      option 'delete', 'Delete this hook'
    end
  end
end
