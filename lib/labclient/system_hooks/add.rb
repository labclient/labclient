# Top namespace
module LabClient
  # Specifics
  class SystemHooks < Common
    doc 'Add' do
      desc 'Add a new system hook. [Hash]'
      example 'client.system_hooks.add(url: "http://labclient")'
      result <<~DOC
        => #<SystemHook id: 6, url: http://labclient>
      DOC
    end

    doc 'Add' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | url | string | yes | The hook URL |
        | token | string | no | Secret token to validate received payloads; this will not be returned in the response |
        | push_events | boolean |  no | When true, the hook will fire on push events |
        | tag_push_events | boolean | no | When true, the hook will fire on new tags being pushed |
        | merge_requests_events | boolean | no | Trigger hook on merge requests events |
        | repository_update_events | boolean | no | Trigger hook on repository update events |
        | enable_ssl_verification | boolean | no | Do SSL verification when triggering the hook |

      DOC
    end

    # Add
    def add(query = {})
      client.request(:post, 'hooks', SystemHook, query)
    end
  end
end
