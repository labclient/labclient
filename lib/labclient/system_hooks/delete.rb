# Top namespace
module LabClient
  # Specifics
  class SystemHooks < Common
    doc 'Delete' do
      desc 'Deletes a system hook. [System Hook ID]'
      example 'client.system_hooks.delete(1)'
    end

    doc 'Delete' do
      desc 'via System Hook'
      example <<~DOC
        hook = client.system_hooks.list.first
        hook.delete
      DOC
    end

    def delete(system_hook_id)
      system_hook_id = format_id(system_hook_id)
      client.request(:delete, "hooks/#{system_hook_id}")
    end
  end
end
