# Top namespace
module LabClient
  # Specifics
  class SystemHooks < Common
    doc 'List' do
      desc 'Get a list of all system hooks.'
      example 'client.system_hooks.list'
      result <<~DOC
        => [#<SystemHook id: 5, url: http://labclient>, ...]
      DOC
    end

    def list
      client.request(:get, 'hooks', SystemHook)
    end
  end
end
