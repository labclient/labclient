# Generate a curl example
module CurlHelper
  # rubocop:disable Metrics/AbcSize
  def curl
    output = 'curl '
    output += '-k ' unless client.settings[:ssl_verify]

    method = response.request.options[:method]
    output += "-X #{method.upcase} "

    # output = "-H "
    response.request.options[:headers].each do |key, value|
      # Hide JSON if GET
      next if key == 'User-Agent' # Don't bother with agent
      next if key == 'Expect' # Typheous Specific
      next if key == 'Content-Type' && method == :get

      output += "-H \"#{key}: #{value}\" "
    end

    output += "-d '#{response.request.options[:body]}' " if method != :get

    output += "\"#{response.request.url}\""
    puts output
  end
  # rubocop:enable Metrics/AbcSize
end
