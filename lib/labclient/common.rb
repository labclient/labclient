# Top Namesapce
module LabClient
  # Shared Methods
  class Common
    extend Docs
    include AccessLevel

    attr_reader :client

    def initialize(client)
      @client = client
    end

    def api_methods
      public_methods(false).sort
    end

    def help(help_filter = nil)
      api_methods_help(help_filter)

      LabClient::Docs.docs[group_name]&.each do |key, group|
        next if help_filter && !key.downcase.include?(help_filter.to_s)

        puts "\n=====[ #{key} ]====="
        group.select { |x| x[:example] }.each { |x| puts "#{x[:example]}\n" }
      end

      nil
    end

    def api_methods_help(help_filter = nil)
      puts group_name
      puts '  Available Methods'

      shown_subclasses = if help_filter
                           api_methods.grep(/#{help_filter}/)
                         else
                           api_methods
                         end

      puts "      #{shown_subclasses.join(' ')}\n"
    end

    # Helper to get docs
    def klass
      self.class.name.split('::', 2).last.split(/(?=[A-Z])/).join(' ')
    end

    # Category and Primary Key for docs
    # If/Else for Instance Variable Warnings
    def group_name
      if self.class.instance_variable_defined? '@group_name'
        self.class.instance_variable_get('@group_name')
      else
        klass
      end
    end

    def inspect
      api_methods_help
      "#<LabClient::Client url: \"#{client.settings[:url]}\">"
    end

    # =======================
    # Query Helpers
    # =======================
    # URL Encoding Object ID Transformation
    # "1" == 1, 1 => 1, "1" => 1
    # nil => nil
    # 'group/name' => group%F2name
    # LabClient::Class => id

    def format_id(obj_id)
      # Return if Empty
      return nil if obj_id.nil?

      # Already a Integer
      return obj_id if obj_id.instance_of?(Integer)

      # If LabClient Object, send ID
      return obj_id.id if obj_id.class.module_parent_name == 'LabClient'

      # Valid Integer in String
      return obj_id if /\A\d+\z/.match(obj_id)

      ERB::Util.url_encode obj_id.to_s
    end

    def format_query_id(key, query)
      query[key] = format_id(query[key]) if query.key? key
      query[key.to_s] = format_id(query[key.to_s]) if query.key? key.to_s
    end

    def format_query_ids(key, query)
      query[key] = query[key].map { |x| format_id(x) } if query.key? key

      query[key.to_s] = query[key.to_s].map { |x| format_id(x) } if query.key? key.to_s
    end

    # Formatting Time Helper
    def format_time?(time)
      time.respond_to?(:to_time)
    end

    def query_format_time(query, key)
      return false unless query.key?(key) && format_time?(query[key])

      query[key] = query[key].to_time.utc.iso8601
    end

    # Convert Symbol to Integer Numbers
    # :developer => 30
    # :owner => 50
    def query_access_level(query, key = :group_access)
      query[key] = machine_access_level query[key] if query.key?(key) && query[key].instance_of?(Symbol)
    end

    # TODO: See if these are even needed
    # Protected Branches
    # Convert Symbol to Integer Numbers
    # 0  => No access
    # 30 => Developer access
    # 40 => Maintainer access
    # 60 => Admin access
    def protected_query_access_level(query, key = :push_access_level)
      query[key] = machine_protected_access_level query[key] if query.key?(key) && query[key].instance_of?(Symbol)
    end
  end
end
