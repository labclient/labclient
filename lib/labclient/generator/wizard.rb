module LabClient
  # Generator Namespace
  module Generator
    # Helper to Generate Data / Populate GitLab
    class Wizard
      include Generator::Names # Name Generator
      include LabClient::Logger
      attr_reader :client
      attr_accessor :count, :random, :password, :templates, :domain, :skip_confirmation

      def inspect
        "#<Wizard count=#{count}, random=#{random}, domain=#{domain}> templates=#{templates}"
      end

      def templates_default
        %i[pages pipeline_trigger environments]
      end

      def templates_all
        LabClient::Generator::TemplateHelper.descendants
      end

      def initialize(client)
        require 'faker' # Ensure Faker is Available

        @client = client
        self.random = true # Populate Random or use only Templates
        self.count = default_count
        self.password = SecureRandom.uuid
        logger.info('Wizard Default Password', password: password) unless client.quiet?

        self.skip_confirmation = true
        self.domain = URI.parse(client.settings[:url]).hostname

        # Default Templates
        self.templates ||= templates_default
      end

      def template(name, opts = {})
        template_klass = templates_all.find { |x| x.template_name == name.to_sym }

        raise "Invalid Template! Available Templates: #{templates_all.map(&:template_name).join(', ')}" unless template_klass

        template_klass.new(client, opts)
      end

      # Random Counters
      def default_count
        {
          users: 20,
          projects: 5,
          groups: 5,
          issues: 2
        }
      end

      def generate_users
        logger.info 'Generating Users' unless client.quiet?
        @users = @user_names.map do |name|
          username = name.downcase.gsub(/[^0-9A-Za-z]/, '')
          email = "#{username}@#{domain}"
          logger.info('User', name: name, username: username, email: email) unless client.quiet?

          client.users.create(
            name: name,
            email: email,
            password: password,
            username: username,
            skip_confirmation: skip_confirmation
          )
        end
      end

      def generate_groups
        logger.info 'Generating Groups' unless client.quiet?
        @groups = @group_names.map do |name|
          path = name.downcase.gsub(/[^0-9A-Za-z]/, '')
          logger.info "Group -- #{name}/#{path}" unless client.quiet?
          client.groups.create(name: name, path: path)
        end
      end

      def generate_group_membership
        logger.info 'Adding Group members' unless client.quiet?
        ## Group Access Level
        @groups.each do |group|
          @users.sample(rand(1..@users.count)).each do |user|
            level = group.valid_group_project_levels.sample
            logger.info('Group Add', name: group.name, user: user.name, level: level) unless client.quiet?
            group.member_add(user, access_level: level)
            # :nocov:
          rescue StandardError => e
            logger.fatal e.message unless client.quiet?
            next
            # :nocov:
          end
        end
      end

      def generate_projects(group)
        logger.info 'Generating Projects' unless client.quiet?
        # Collect Group Members
        members = group.members

        # Loop through project names, create project add issues
        @project_names.uniq.map do |project_name|
          logger.info "Project: #{project_name}" unless client.quiet?
          project = group.project_create(name: project_name, description: gen_description)

          rand(count[:issues]).times do
            project.issue_create(generate_issue_data(members.sample))
          end
        end
      end

      def generate_issue_data(member)
        {
          assignee_id: member.id,
          description: Faker::Hacker.say_something_smart,
          title: Faker::Company.catch_phrase
        }
      end

      # Execute Generation
      def run!
        # Collect Names
        generate_names
        generate_users
        generate_groups
        generate_group_membership

        @groups.map do |group|
          generate_projects(group)
        end

        # Run Templates
        templates.each { |template_klass| template(template_klass).run! }

        nil
      end
    end
  end
end
