# The glorious gitlab labclient of labs
module LabClient
  # Generator Namespace
  module Generator
    # Docs for the wizard
    class GeneratorDocs
      @group_name = 'Wizard'
      extend Docs

      doc 'Wizard' do
        markdown <<~DOC
          <h6 style="color:red"><b>Experimental</b></h6>

          This is experimental and will be likely changing frequently and dramatically. This will create (*potentially many*) and manipulate objects within GitLab. **This is not recommended for use in production environments**
        DOC
      end

      doc 'Wizard' do
        title 'About'
        markdown <<~DOC
          The Wizard is designed to help setup test instances, and provide easy mechanisms for populating and testing GitLab features.

          Requires 'faker' to function, which will need to be manually/added installed, as it is not a depdenency of the gem.
        DOC

        example 'gem install faker'
      end

      doc 'Wizard' do
        title 'Quickstart'
        desc 'To run, create the client/wizard and run!'
        example <<~DOC
          require 'labclient'
          client = LabClient::Client.new(url: 'https://labclient', token: 'super_secret')
          client.wizard.run!
        DOC
      end

      doc 'Wizard' do
        title 'Client'
        desc <<-DOC
          The wizard client will self-populate with defaults and parameters from the main client object. But each can be manually configured.
        DOC

        example <<~DOC
          wizard = client.wizard
          => #<Wizard count={:users=>20, :projects=>5, :groups=>5, :issues=>2}, random=true, domain=labclient>

          # Disable Random
          wizard.random = false
        DOC
      end

      doc 'Wizard' do
        markdown <<~DOC
          | Setting           | Type    | Description                                            |
          | ----------------- | ------- | ------------------------------------------------------ |
          | random            | Boolean | Run Populate. Setup random Users, Projects, and Groups |
          | count             | Hash    | Settings for random. User, Project, Group count        |
          | password          | String  | Default password for generated Users                   |
          | domain            | String  | Defaults to client url. Default email domain for users |
          | skip_confirmation | Boolean | Enable/Disable skip_confirmation on new users          |
        DOC
      end

      doc 'Wizard' do
        title 'Run one templates'
        desc <<-DOC
          Outside of the wizard at large, you can also select and manually run specific templates.
        DOC

        example <<~DOC
          pages = client.wizard.template(:pages)
          => #<LabClient::Template Pages>
          pages.run! # Run pages template
        DOC
      end
    end
  end
end
