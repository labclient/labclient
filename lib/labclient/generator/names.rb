# Name Generator
module LabClient
  # Helper to Generate Data / Populate GitLab
  module Generator
    # Name Helper
    module Names
      def create_file(content, branch = :master, message = nil)
        {
          branch: branch,
          commit_message: message || gen_description,
          content: content
        }
      end

      def gen_groups
        [
          Faker::Games::LeagueOfLegends.location,
          Faker::Movies::LordOfTheRings.location,
          Faker::TvShows::RickAndMorty.location,
          Faker::TvShows::StarTrek.location,
          Faker::Games::ElderScrolls.region,
          Faker::Games::ElderScrolls.city,
          Faker::Games::Zelda.location,
          Faker::Games::SuperSmashBros.stage
        ].map { |x| x.gsub(/[^0-9A-Za-z]/, '') }
      end

      def gen_projects
        [
          Faker::Games::ElderScrolls.creature,
          Faker::Movies::LordOfTheRings.location,
          Faker::Games::LeagueOfLegends.summoner_spell,
          Faker::Games::Pokemon.move,
          Faker::TvShows::RickAndMorty.location,
          Faker::Games::LeagueOfLegends.masteries,
          Faker::Superhero.power
        ].map { |x| x.gsub(/[^0-9A-Za-z]/, '') }
      end

      def gen_people
        [
          Faker::Movies::LordOfTheRings.character,
          Faker::Games::WorldOfWarcraft.hero,
          Faker::TvShows::StarTrek.character,
          Faker::Games::LeagueOfLegends.champion,
          Faker::Movies::PrincessBride.character
        ]
      end

      def gen_description
        [
          Faker::Hacker.say_something_smart,
          Faker::Games::LeagueOfLegends.quote,
          Faker::Company.bs,
          Faker::Movies::PrincessBride.quote
        ].sample
      end

      # rubocop:disable Metrics/AbcSize
      def generate_names
        @user_names = Array.new(count[:users]) { gen_people }.flatten.uniq.sample(count[:users])
        @group_names = Array.new(count[:groups]) { gen_groups }.flatten.uniq.sample(count[:groups])
        @project_names = Array.new(count[:projects]) { gen_projects }.flatten.uniq.sample(count[:projects])
      end
      # rubocop:enable Metrics/AbcSize
    end
  end
end
