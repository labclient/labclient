module LabClient
  module Generator
    # Public Method Helpers
    module TemplateMethods
      extend ActiveSupport::Concern

      @group_prefix = 'Environment'

      class_methods do
        def template_name
          name.demodulize.underscore.to_sym
        end
      end
    end

    # Common Helper Class
    class TemplateHelper
      include TemplateMethods
      include LabClient::Logger

      attr_reader :client
      attr_accessor :opts

      def inspect
        "#<LabClient::Template #{self.class.to_s.demodulize}>"
      end

      def initialize(client, opts = {})
        @client = client
        self.opts = opts
        setup
      end

      def setup
        # Blank Place Holder
      end
    end

    # ====================================================
    # Helper for  Common Group/Project Templates
    # - Create Group, @group
    # - Execute any `setup_` methods
    # - Print all @projects, and @group
    class GroupTemplateHelper < TemplateHelper
      @group_prefix = 'Environment'
      include Names
      attr_accessor :group_name, :group_path, :group_suffix, :project_name

      def run!
        logger.info "Running: #{group_suffix}"
        generate_group

        # Run `setup_` prefixed classes
        self.class.instance_methods.grep(/setup_/).each { |x| send(x) }

        # Print Created Groups/Project
        logger.info 'Group', name: @group.name, web_url: @group.web_url
        @projects.each do |project|
          logger.info 'Project', name: project.name, web_url: project.web_url
        end

        {
          group: @group,
          projects: @projects
        }
      end

      def setup
        self.group_suffix = self.class.name.demodulize
        self.group_name = opts[:group_name] || "#{gen_groups.sample} #{group_suffix}"
        self.group_path = opts[:group_path] || group_name.downcase.gsub(' ', '-')

        @projects = []
      end

      def generate_group
        @group = client.groups.create(name: group_name, path: group_path)
        raise 'Unable to Create Group' unless @group.success?
      end
    end
  end
end
