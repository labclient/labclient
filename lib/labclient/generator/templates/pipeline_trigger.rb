module LabClient
  module Generator
    # Docs for the wizard
    class GeneratorDocs
      doc 'Templates' do
        title 'Pipeline Triggers'
        markdown <<~DOC
          Create GitLab Pipeline Triggers group and test projects.

          - Child Pipeline: Parent pipeline with artifact, that is then executed as a child pipeline

          | Setting    | Default   | Type    | Description                            |
          | ---------- | --------- | ------- | -------------------------------------- |
          | group_name | Generated | String  | Parent Group name for pages projects   |
          | group_path | Generated | String  | Parent Group path                      |
        DOC
      end
    end

    # Child and other Trigger Examples
    # https://docs.gitlab.com/ee/ci/yaml/#trigger
    class PipelineTrigger < GroupTemplateHelper
      def setup_projects
        create_child_pipeline
      end

      def trigger_child_pipeline_yaml
        <<~YAML
          image: busybox:latest

          build_child:
            stage: build
            script:
              - cp child_pipeline.yml artifact.yml
            artifacts:
              paths:
                - artifact.yml

          trigger_child:
            stage: deploy
            trigger:
              include:
                - artifact: artifact.yml
                  job: build_child

        YAML
      end

      def child_pipeline_yaml
        <<~YAML
          image: busybox:latest

          child:
            script:
              - echo "Do your build here"
        YAML
      end

      def create_child_pipeline
        project = @group.project_create(
          name: 'Child Pipeline',
          description: 'Child Pipeline',
          auto_devops_enabled: false
        )

        # Create Child
        project.file_create(
          'child_pipeline.yml',
          create_file(child_pipeline_yaml)
        )

        # Create Parent
        project.file_create(
          '.gitlab-ci.yml',
          create_file(trigger_child_pipeline_yaml)
        )

        @projects.push project
      end
    end
  end
end
