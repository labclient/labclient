module LabClient
  module Generator
    # Docs for the wizard
    class GeneratorDocs
      doc 'Templates' do
        title 'Pages'
        markdown <<~DOC
          Create GitLab Pages group and test projects. Projects imported from Gitlab's [demo pages projects](https://gitlab.com/pages)

          | Setting    | Default   | Type    | Description                            |
          | ---------- | --------- | ------- | -------------------------------------- |
          | group_name | Generated | String  | Parent Group name for pages projects   |
          | group_path | Generated | String  | Parent Group path                      |
          | count      | 5         | Integer | Number of projects to import (max: 28) |
        DOC
      end
    end

    # Page Import Creation
    class Pages < GroupTemplateHelper
      attr_accessor :count

      # All Available Pages Projects
      def list
        %w[
          brunch doxygen emacs-reveal frozen-flask gatsby
          gitbook hakyll harp hexo hugo hyde ikiwiki
          jekyll jigsaw lektor metalsmith
          middleman mkdocs nanoc nikola nuxt octopress
          org-mode pelican plain-html sphinx vuepress zim
        ]
      end

      def setup
        super
        self.count = opts[:count] || 5
      end

      def generate_project(name)
        @group.project_create(
          name: name,
          description: "#{name} Pages",
          import_url: "https://gitlab.com/pages/#{name}.git"
        )
      end

      def generate_pipelines
        @projects.each do |project|
          project.pipeline_create(ref: :master)
        end
      end

      def wait_for_import
        @projects.each(&:wait_for_import)
      end

      def setup_projects
        @projects = list.sample(count).map do |name|
          generate_project(name)
        end

        wait_for_import
        generate_pipelines
      end
    end
  end
end
