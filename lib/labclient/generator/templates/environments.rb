module LabClient
  module Generator
    # Docs for the wizard
    class GeneratorDocs
      doc 'Templates' do
        title 'Environments'
        markdown <<~DOC
          - On Stop: Create environment on new branch. Stop environment on merge

          | Setting    | Default   | Type    | Description                            |
          | ---------- | --------- | ------- | -------------------------------------- |
          | group_name | Generated | String  | Parent Group name for pages projects   |
          | group_path | Generated | String  | Parent Group path                      |
        DOC
      end
    end

    # Child and other Trigger Examples
    # https://docs.gitlab.com/ee/ci/yaml/#trigger
    class Environments < GroupTemplateHelper
      def on_stop_yaml
        <<~YAML
          image: busybox:latest

          build:
            stage: build
            script:
              - echo "Build on both master and branches"

          branch-review:
            stage: deploy
            environment:
              name: ${CI_COMMIT_REF_NAME}
              on_stop: review-teardown
            script:
              - echo "branch-review"
            only:
              - branches
            except:
              - master

          review-teardown:
            stage: deploy
            when: manual
            variables:
              GIT_STRATEGY: none
            environment:
              name: ${CI_COMMIT_REF_NAME}
              action: stop
            script:
              - echo "review-teardown"
            only:
              - branches
            except:
              - master
        YAML
      end

      # rubocop:disable Metrics/MethodLength
      def setup_master_branch_environment_on_stop
        project = @group.project_create(
          name: 'On Stop',
          description: 'Environments created on non-master branches, stopped on merge',
          auto_devops_enabled: false,
          only_allow_merge_if_pipeline_succeeds: true
        )

        # Create Parent
        project.file_create('README.md', create_file("# #{project.name}"))

        # # Create Branch
        project.branch_create(branch: :branch, ref: :master)

        # Create Branch Files
        project.file_create('.gitlab-ci.yml', create_file(on_stop_yaml, :branch))

        # Create Merge Request
        merge_request = project.merge_request_create(
          title: 'Merge Test Branch!',
          source_branch: :branch,
          target_branch: :master
        )

        # Wait for Merge
        merge_request.wait_for_merge_status

        # Merge
        merge_request.accept(
          should_remove_source_branch: true,
          merge_when_pipeline_succeeds: true
        )

        @projects.push project
      end
      # rubocop:enable Metrics/MethodLength
    end
  end
end
