# Top namespace
module LabClient
  # Inspect Helper
  class Todo < Klass
    include ClassHelpers

    def inspect
      "#<Todo id: #{id}, state: #{state}>"
    end

    date_time_attrs %i[created_at]

    def mark_as_done
      update_self client.todos.mark_as_done(id)
    end

    help do
      subtitle 'Todo'
      option 'mark_as_done', 'Mark this todo as done.'
    end
  end
end
