# Top namespace
module LabClient
  # Specifics
  class Todos < Common
    doc 'List' do
      desc 'Returns a list of todos. When no filter is applied, it returns all pending todos for the current user. Different filters allow the user to precise the request..'
      example 'client.todos.list'
      result <<~DOC
        => [#<Todo id: 7, state: pending>]
      DOC
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | action    | string | no | The action to be filtered. Can be assigned, mentioned, build_failed, marked, approval_required, unmergeable or directly_addressed. |
        | author_id | integer| no | The ID of an author |
        | project_id| integer| no | The ID of a project |
        | group_id  | integer| no | The ID of a group |
        | state     | string | no | The state of the todo. Can be either pending or done |
        | type      | string | no | The type of a todo. Can be either Issue or MergeRequest |
      DOC
    end

    doc 'List' do
      desc 'Filter by todos state'
      example 'client.todos.list(state: :done)'
      result <<~DOC
        => [#<Todo id: 8, state: done>, #<Todo id: 6, state: done>, ...]
      DOC
    end

    # List
    def list(query = {})
      client.request(:get, 'todos', Todo, query)
    end
  end
end
