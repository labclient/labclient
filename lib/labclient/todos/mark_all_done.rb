# Top namespace
module LabClient
  # Specifics
  class Todos < Common
    doc 'Mark all todos as done' do
      desc 'Marks all pending todos for the current user as done. It returns the HTTP status code 204 with an empty response.'
      example 'client.todos.mark_all_done'
      result <<~DOC
        =>  nil
      DOC
    end

    # Mark all as Done
    def mark_all_done
      client.request(:post, 'todos/mark_as_done')
    end
  end
end
