# Top namespace
module LabClient
  # Specifics
  class Todos < Common
    doc 'Mark a todo as done' do
      desc 'Marks a single pending todo given by its ID for the current user as done. The todo marked as done is returned in the response.'
      example 'client.todos.mark_as_done(10)'
      result <<~DOC
        => #<Todo id: 10, state: done>
      DOC
    end

    doc 'Mark a todo as done' do
      desc 'Update through todo object'
      example <<~DOC
        todo = client.todos.list.first
        todo.mark_as_done
      DOC
      result <<~DOC
        => #<Todo id: 17, state: done>
      DOC
    end

    # Mark as Done
    def mark_as_done(todo_id)
      client.request(:post, "todos/#{todo_id}/mark_as_done", Todo)
    end
  end
end
