# Top Namesapce
module LabClient
  # Shared Methods
  module AccessLevel
    HUMAN_ACCESS_LEVELS = {
      0 => :none,
      10 => :guest,
      20 => :reporter,
      30 => :developer,
      40 => :maintainer,
      50 => :owner,
      60 => :admin
    }.freeze

    MACHINE_ACCESS_LEVELS = {
      none: 0,
      guest: 10,
      reporter: 20,
      developer: 30,
      maintainer: 40,
      owner: 50,
      admin: 60
    }.freeze

    def human_access_level(level = 10)
      HUMAN_ACCESS_LEVELS[level]
    end

    def machine_access_level(level = :developer)
      MACHINE_ACCESS_LEVELS[level]
    end

    def machine_protected_access_level(level = :developer)
      MACHINE_ACCESS_LEVELS[level]
    end

    def human_protected_access_level(level = 10)
      HUMAN_ACCESS_LEVELS[level]
    end
  end
end
