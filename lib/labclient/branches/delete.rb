# Top namespace
module LabClient
  # Specifics
  class Branches < Common
    doc 'Delete' do
      desc 'Delete a single project repository branch. [Project ID, Branch]'
      example 'client.branches.delete(264, :feature)'
    end

    doc 'Delete' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.branch_delete(:feature)
      DOC
    end

    def delete(project_id, branch_id)
      project_id = format_id(project_id)

      client.request(:delete, "projects/#{project_id}/repository/branches/#{branch_id}")
    end
  end
end
