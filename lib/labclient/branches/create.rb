# Top namespace
module LabClient
  # Specifics
  class Branches < Common
    doc 'Create' do
      desc 'Get a list of repository branches from a project, sorted by name alphabetically. [Project ID, String]'
      example 'client.branches.create(264, branch: :branch, ref: :master)'
      result '[#<Branch name: feature>, #<Branch name: master>]'

      markdown <<~DOC
        | Attribute | Type    | Required | Description                                                                                                  |
        |----------|--------|---------|-------------------------------------------------------------------------------------------------------------|
        | id      | integer | yes      | ID or [URL-encoded path of the project](README.md#namespaced-path-encoding) owned by the authenticated user. |
        | branch  | string  | yes      | Name of the branch.                                                                                          |
        | ref     | string  | yes      | Branch name or commit SHA to create branch from.                                                             |
      DOC
    end

    doc 'Create' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.branch_create(branch: :branch, ref: :master)
      DOC
    end

    def create(project_id, query)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/repository/branches", Branch, query)
    end
  end
end
