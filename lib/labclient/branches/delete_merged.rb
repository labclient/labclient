# Top namespace
module LabClient
  # Specifics
  class Branches < Common
    doc 'Delete' do
      title 'Merged'
      desc 'Will delete all branches that are merged into the project’s default branch. [Project ID]'
      example 'client.branches.branch_delete_merged(264, :feature)'
      result '=> {:message=>"202 Accepted"}'
    end

    doc 'Delete' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.branch_delete_merged(:feature)
      DOC
    end

    def delete_merged(project_id)
      project_id = format_id(project_id)
      client.request(:delete, "projects/#{project_id}/repository/merged_branches")
    end
  end
end
