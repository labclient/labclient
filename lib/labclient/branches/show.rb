# Top namespace
module LabClient
  # Specifics
  class Branches < Common
    doc 'Show' do
      desc 'Get a single project repository branch. [Project ID, Branch]'
      example 'client.branches.show(264, :master)'
      result '#<Branch name: master>'
    end

    doc 'Show' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.branch(:master)
      DOC
    end

    def show(project_id, branch_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/repository/branches/#{branch_id}", Branch)
    end
  end
end
