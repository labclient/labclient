# Top namespace
module LabClient
  # Inspect Helper
  class Branch < Klass
    include ClassHelpers

    def inspect
      "#<Branch name: #{name}>"
    end

    def commit
      Commit.new(@table[:commit], response, client)
    end

    def pipelines
      project_id = collect_project_id
      client.pipelines.list(project_id, ref: name)
    end

    # Wait for Import / Set a Hard Limit
    def wait_for_pipelines(total_time = 300, sleep_time = 15)
      Timeout.timeout(total_time) do
        loop do
          reload
          logger.info 'Waiting for Pipelines' unless quiet?
          break unless pipelines.empty?

          sleep sleep_time
        end
      end
    end

    help do
      subtitle 'Branch'
      option 'commit', 'Show Commit information for branch'
      option 'pipelines', 'Show Pipelines for this Branch/Ref'
      option 'wait_for_pipelines', 'Looping, on newly created branches, wait for pipelines to populate (not finish) [Timeout, Interval]'
    end
  end
end
