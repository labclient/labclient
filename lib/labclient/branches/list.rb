# Top namespace
module LabClient
  # Specifics
  class Branches < Common
    doc 'List' do
      desc 'Get a list of repository branches from a project, sorted by name alphabetically. [Project ID, String]'
      example 'client.branches.list(264)'
      result '[#<Branch name: feature>, #<Branch name: master>]'

      markdown <<~DOC
        Search: You can use ^term and term$ to find branches that begin and end with term respectively.
      DOC
    end

    doc 'List' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.branches
      DOC
    end

    def list(project_id, search = nil)
      query = if search.nil?
                nil
              else
                { search: search }
              end

      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/repository/branches", Branch, query)
    end
  end
end
