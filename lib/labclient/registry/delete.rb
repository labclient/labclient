# Top namespace
module LabClient
  # Specifics
  class Registry < Common
    doc 'Delete' do
      desc 'Delete a repository in registry. [Project ID, Registry Repository ID]'
      example 'client.registry.delete(16, 1)'
    end

    doc 'Delete' do
      desc 'via Registry Repository'
      example <<~DOC
        repo = client.registry.list(16).first
        repo.delete
      DOC
    end

    def delete(project_id, repository_id)
      repository_id = format_id(repository_id)
      project_id = format_id(project_id)

      client.request(:delete, "projects/#{project_id}/registry/repositories/#{repository_id}")
    end
  end
end
