# Top namespace
module LabClient
  # Specifics
  class Registry < Common
    doc 'List' do
      title 'Group'
      desc 'Get a list of registry repositories in a group. [Group ID]'
      example 'client.registry.group(59)'
      result '[#<RegistryRepository id: 1>, #<RegistryRepository id: 2>]'

      markdown <<~DOC
        | Attribute | Type    | Required | Description |
        | --------- | ----    | -------- | ----------- |
        | tags      | boolean | no | If the parameter is included as true, each repository will include an array of "tags" in the response. |
        | name      | string  | no | Returns a list of repositories with a name that matches the value. |
      DOC
    end

    doc 'List' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(59)
        group.registry_repositories
      DOC
    end

    def group(group_id, query = {})
      group_id = format_id(group_id)

      client.request(:get, "groups/#{group_id}/registry/repositories", RegistryRepository, query)
    end
  end
end
