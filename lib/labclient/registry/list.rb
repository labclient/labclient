# Top namespace
module LabClient
  # Specifics
  class Registry < Common
    doc 'List' do
      title 'Project'
      desc 'Get a list of registry repositories in a project. [Project ID, Hash]'
      example 'client.registry.list_repository(16)'
      result '[#<RegistryRepository id: 1>, #<RegistryRepository id: 2>]'

      markdown <<~DOC
        | Attribute | Type    | Required | Description |
        | --------- | ----    | -------- | ----------- |
        | tags      | boolean | no | If the parameter is included as true, each repository will include an array of "tags" in the response. |
        | name      | string  | no | Returns a list of repositories with a name that matches the value. |
      DOC
    end

    doc 'List' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.registry_repositories
      DOC
    end

    def list(project_id, query = {})
      client.request(:get, "projects/#{project_id}/registry/repositories", RegistryRepository, query)
    end
  end
end
