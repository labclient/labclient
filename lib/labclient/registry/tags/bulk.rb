# Top namespace
module LabClient
  # Specifics
  class Registry < Common
    doc 'Delete' do
      title 'Bulk'
      desc 'Delete registry repository tags in bulk based on given criteria. [Project ID, Registry Repository ID, Params]'
      example 'client.registry.bulk(16, 7, name_regex_delete: "*" )'
    end

    doc 'Delete' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | name_regex_delete | string | yes | The [re2](https://github.com/google/re2/wiki/Syntax) regex of the name to delete. To delete all tags specify .*.|
        | name_regex_keep | string | no | The [re2](https://github.com/google/re2/wiki/Syntax) regex of the name to keep. This value will override any matches from name_regex_delete. Note: setting to .* will result in a no-op. |
        | keep_n | integer | no | The amount of latest tags of given name to keep. |
        | older_than | string | no | Tags to delete that are older than the given time, written in human readable form 1h, 1d, 1month. |

      DOC
    end

    def bulk(project_id, repository_id, query)
      repository_id = format_id(repository_id)
      project_id = format_id(project_id)

      client.request(:delete, "projects/#{project_id}/registry/repositories/#{repository_id}/tags", nil, query)
    end
  end
end
