# Top namespace
module LabClient
  # Specifics
  class Registry < Common
    doc 'List' do
      title 'Tags'
      desc 'Get details of a registry repository tag. [Project ID, Registry Repository ID]'
      example 'client.registry.tags(16, 1)'
      result '[#<RegistryTag name: name>, #<RegistryTag name: name>]'
    end

    doc 'List' do
      desc 'via Registry Repository'
      example <<~DOC
        repo = client.registry.list(16).first
        repo.tags
      DOC
    end

    def tags(project_id, repository_id)
      repository_id = format_id(repository_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/registry/repositories/#{repository_id}/tags", RegistryTag)
    end
  end
end
