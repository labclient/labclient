# Top namespace
module LabClient
  # Specifics
  class Registry < Common
    doc 'Delete' do
      title 'Tag'
      desc 'Delete a registry repository tag. [Project ID, Registry Repository ID, Tag Name]'
      example 'client.registry.delete_tag(16, 7, :name)'
    end

    doc 'Delete' do
      desc 'via Registry Tag'
      example <<~DOC
        tag = client.registry.details(264, 7, :name)
        tag.delete
      DOC
    end

    def delete_tag(project_id, repository_id, tag_name)
      project_id = format_id(project_id)
      repository_id = format_id(repository_id)
      tag_name = format_id(tag_name)

      client.request(:delete, "projects/#{project_id}/registry/repositories/#{repository_id}/tags/#{tag_name}")
    end
  end
end
