# Top namespace
module LabClient
  # Specifics
  class Registry < Common
    doc 'Show' do
      title 'Tag'
      desc 'Get details of a registry repository tag. [Project ID, Registry Repository ID, Tag Name]'
      example 'client.registry.details(16, 1, :name)'
      result '#=> #<RegistryTag name: name>'
    end

    def details(project_id, repository_id, tag_name)
      project_id = format_id(project_id)
      repository_id = format_id(repository_id)
      tag_name = format_id(tag_name)

      client.request(:get, "projects/#{project_id}/registry/repositories/#{repository_id}/tags/#{tag_name}", RegistryTag)
    end
  end
end
