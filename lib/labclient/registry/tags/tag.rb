# Top namespace
module LabClient
  # Inspect Helper
  class RegistryTag < Klass
    include ClassHelpers

    def inspect
      "#<RegistryTag name: #{name}>"
    end

    def delete
      project_id = collect_project_id
      repository_id = collect_repository_id
      client.registry.delete_tag(project_id, repository_id, name)
    end

    help do
      @group_name = 'Registry'
      subtitle 'Registry Tag'
      option 'delete', 'Delete tag'
    end
  end
end
