# Top namespace
module LabClient
  # Inspect Helper
  class RegistryRepository < Klass
    include ClassHelpers

    def inspect
      "#<RegistryRepository id: #{id}>"
    end

    def project
      client.projects.show(project_id)
    end

    def tags
      project_id = collect_project_id
      client.registry.tags(project_id, id)
    end

    def delete
      project_id = collect_project_id
      client.registry.delete(project_id, id)
    end

    help do
      @group_name = 'Registry'
      subtitle 'Registry Repository'
      option 'tags', 'List Tags'
    end
  end
end
