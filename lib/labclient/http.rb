# Typheous Wrapper
module LabClient
  # Request Helper
  class HTTP
    attr_accessor :settings

    def initialize(settings)
      self.settings = settings
    end

    # Pagination Helper, Raw/Full HTTP Requests
    def url(path)
      if path.include? 'http'
        path
      else
        "#{settings[:url]}/api/v4/#{path}"
      end
    end

    def request(method, path, body = {}, dump_json = true)
      options = { method: method, headers: headers(dump_json) }

      if body && !body.empty?
        case method
        when :get
          options[:params] = body
        else
          # File Upload shouldn't be jsonfied
          options[:body] = dump_json ? Oj.dump(body, mode: :compat) : body
        end
      end

      options.merge!(disable_ssl) unless settings[:ssl_verify]

      Typhoeus::Request.new(url(path), options).run
    end

    def disable_ssl
      { ssl_verifyhost: 0, ssl_verifypeer: false }
    end

    def headers(dump_json)
      default_headers = {

        'Accept' => 'application/json',
        'User-Agent' => "LabClient #{LabClient::VERSION}"
      }

      token_type = settings[:token_type]
      default_headers[token_type] = if token_type == 'Authorization'
                                      "Bearer #{settings[:token]}"
                                    else
                                      settings[:token]
                                    end

      default_headers['Content-Type'] = 'application/json' if dump_json

      default_headers
    end
  end
end

# Typheous Shim
module Typhoeus
  # Add Data :  OJ Parsing
  class Response
    include CurlHelper
    def inspect
      "#<TyphoeusResponse code: #{code}>"
    end

    attr_reader :path, :client

    def data
      @data ||= process_body

      @data
    end

    # Shim for CurlHelper
    def response
      self
    end

    # Check if response body can be parsed
    def json_body?
      content_type = headers['content-type']

      return false unless content_type
      return true if content_type.include? 'application/json'

      false
    end

    def process_body
      return nil if body.empty?

      if json_body?
        result = Oj.load(body, mode: :compat, symbol_keys: true, object_class: LabClient::LabStruct)
        result.instance_variable_set(:@response, self) if result.instance_of?(LabClient::LabStruct)
        result
      else
        body
      end
    end

    # Retry Helper Accessor
    def retry?
      code == 429
    end

    # Print Error information
    # 1. Use Typheous `return_message` if there isn't any return body
    # For network/uri/dns related issues
    # 2. Use body for parsed responses
    # For Bad Request, invalid params
    # 3. Return raw data
    # For non body responses
    def find_friendly_error
      case data
      when nil
        return_message
      when LabClient::LabStruct
        data[:message] || data[:error]
      else # Handle String as well
        data
      end
    end

    def friendly_error
      "#{code} - #{find_friendly_error}"
    end
  end
end
