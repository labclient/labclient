# Top namespace
module LabClient
  # Specifics
  class Notifications < Common
    doc '' do
      subtitle 'Levels & Custom'

      markdown <<~DOC
        For each global/group/project a specific level, or custom settings can be specified.

        See [docs](https://docs.gitlab.com/ee/api/notification_settings.html#valid-notification-levels) on valid levels and custom settings.
      DOC
    end

    doc 'List' do
      desc 'Get current notification settings and email address.'
      example 'client.notifications.list_global'
      result <<~DOC
        => {:level=>"watch", :notification_email=>"admin@labclient.com"}
      DOC
    end

    doc 'List' do
      desc 'List Alias'
      example 'client.notifications.list'
    end

    def list_global
      client.request(:get, 'notification_settings')
    end

    alias list list_global

    doc 'List' do
      desc 'Get group notification settings. [Group ID]'
      example 'client.notifications.list_group(5)'
    end

    def list_group(group_id)
      group_id = format_id(group_id)
      client.request(:get, "groups/#{group_id}/notification_settings")
    end

    doc 'List' do
      desc 'Get project notification settings. [Project ID]'
      example 'client.notifications.list_project(16)'
    end

    def list_project(project_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/notification_settings")
    end
  end
end
