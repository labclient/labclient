# Top namespace
module LabClient
  # Specifics
  class Notifications < Common
    doc 'Update' do
      desc 'Get a namespace by ID. [Hash]'
      example 'client.notifications.update_global(level: :watch)'
      result <<~DOC
        => {:level=>"watch", :notification_email=>"admin@labclient.com"}
      DOC
    end

    def update_global(query)
      client.request(:put, 'notification_settings', nil, query)
    end

    alias update update_global

    doc 'Update' do
      desc 'Update Group [Group ID, Hash]'
      example 'client.notifications.update_group(5, level: :mention)'
    end

    def update_group(group_id, query)
      group_id = format_id(group_id)
      client.request(:put, "groups/#{group_id}/notification_settings", nil, query)
    end

    doc 'Update' do
      desc 'Update Project [Project ID, Hash'
      example 'client.notifications.update_project(16, level: :custom)'

      result <<~DOC
           LabStruct {
             :level => "custom",
            :events => LabStruct {
                           :new_release => nil,
                              :new_note => nil,
                             :new_issue => nil,
                          :reopen_issue => nil,
                           :close_issue => nil,
                        :reassign_issue => nil,
                             :issue_due => nil,
                     :new_merge_request => nil,
                 :push_to_merge_request => nil,
                  :reopen_merge_request => nil,
                   :close_merge_request => nil,
                :reassign_merge_request => nil,
                   :merge_merge_request => nil,
                       :failed_pipeline => true,
                        :fixed_pipeline => true,
                      :success_pipeline => nil,
                              :new_epic => nil
            }
        }
      DOC
    end

    def update_project(project_id, query)
      project_id = format_id(project_id)
      client.request(:put, "projects/#{project_id}/notification_settings", nil, query)
    end
  end
end
