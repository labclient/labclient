# Error helper that includes the HTTParty Response
module LabClient
  # Class Shim
  class Error < StandardError
    attr_reader :resp

    def initialize(resp)
      super
      @resp = resp
    end

    # Helper for Raising Exceptions
    def error_details
      { code: resp.code, message: resp.find_friendly_error }
    end
  end

  # Class Shim
  class Retry < StandardError
  end
end
