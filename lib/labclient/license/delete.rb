module LabClient
  # Specifics
  class Licenses < Common
    doc 'Delete' do
      title 'Delete a license'
      example 'client.license.delete(:id)'
      result <<~DOC
        =>  nil
      DOC
    end

    # Remove current license
    def delete(license_id)
      client.request(:delete, "license/#{license_id}")
    end
  end
end
