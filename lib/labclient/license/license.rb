module LabClient
  # Inspect Helper
  class License < Klass
    include ClassHelpers

    date_time_attrs %i[expires_at created_at starts_at]

    def inspect
      "#<License - id: #{id}, plan: #{plan}, created at: #{created_at}, expires at: #{expires_at}>"
    end
  end
end
