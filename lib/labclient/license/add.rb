module LabClient
  # Specifics
  class Licenses < Common
    doc 'Add' do
      title 'Add a new license'
      example 'client.license.add("eyJkYXRhIjoiMHM5Q...S01Udz09XG4ifQ==")'
      result <<~DOC
        =>  #<License - id: 6, plan: premium, expires at: 2200-11-04>
      DOC
    end

    doc 'Add' do
      desc 'A 400 and error message will be returned if the license could not be added'
    end

    # Get current license
    def add(obj)
      client.request(:post, 'license', License, license: obj)
    end
  end
end
