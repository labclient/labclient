module LabClient
  # Specifics
  class Licenses < Common
    doc 'List' do
      title 'Current license'
      desc 'Retrieve information about the current license.'
      example 'client.license.current'
      result <<~DOC
        =>  #<License - id: 5, plan: premium, expires at: 2200-11-04>
      DOC
    end

    doc 'List' do
      title 'All Licenses'
      desc 'Retrieve information about all licenses'
      example 'client.license.list'
      result <<~DOC
        => [#<License - id: 5, plan: premium, expires at: 2200-11-04>, #<License - id: 1, plan: ultimate, expires at: 2050-11-14>]
      DOC
    end

    doc 'List' do
      title 'View current license detail'
      desc 'Retrieve detailed information of current license'
      example 'client.license.current.verbose'
      result <<~DOC
        LabClient::License {
                           :id => 1,
                           :plan => "ultimate",
                           :created_at => "2019-11-18T11:27:24.597-06:00",
                           :starts_at => "2019-11-14",
                           :expires_at => "2050-11-14",
                           :historical_max => 0,
                           :maximum_user_count => 48,
                           :licensee => LabStruct {
                           :Name => "GitLab Henley",
                           :Email => "test_user@gitlab.com",
                           :Company => "GitLab, Inc."
               },
                           :add_ons => LabStruct {},
                           :expired => false,
                           :overage => 0,
                           :user_limit => 100,
                           :active_users => 52
               }
        => nil
      DOC
    end

    doc 'List' do
      title 'View all licenses detail'
      desc 'Retrieve detailed information of all licenses'
      example 'client.license.list.each(&:verbose)'
    end

    doc 'List' do
      markdown <<~DOC
        Please see [here for a explanation of license detail values](https://docs.gitlab.com/ee/api/license.html#retrieve-information-about-all-licenses)
      DOC
    end

    # Get current license
    def current
      client.request(:get, 'license', License)
    end

    def list
      client.request(:get, 'licenses', License)
    end
  end
end
