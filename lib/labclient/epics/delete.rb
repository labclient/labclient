# Top namespace
module LabClient
  # Specifics
  class Epics < Common
    doc 'Delete' do
      desc 'Deletes an epic [Group ID, Epic IID]'
      example 'client.epics.delete(59, 1)'
    end

    doc 'Delete' do
      desc 'Via Group [Epic ID]'
      example <<~DOC
        group = client.groups.show(16)
        group.epic_delete(1)
      DOC
    end

    doc 'Delete' do
      desc 'Via Epic'
      example <<~DOC
        epic = client.epics.show(16, 1)
        epic.delete
      DOC
    end

    def delete(group_id, epic_id)
      # DELETE /groups/:id/epics/:epic_iid

      group_id = format_id(group_id)
      epic_id = format_id(epic_id)

      client.request(:delete, "groups/#{group_id}/epics/#{epic_id}")
    end
  end
end
