# Top namespace
module LabClient
  # Inspect Helper
  class Epic < Klass
    include ClassHelpers
    def inspect
      "#<Epic iid: #{iid}, title: #{title}>"
    end

    # User Fields
    user_attrs %i[author]

    def group
      client.groups.show group_id
    end

    def update(query)
      update_self client.epics.update(group_id, iid, query)
    end

    def delete
      client.epics.delete(group_id, iid)
    end

    def todo
      client.epics.todo(group_id, iid)
    end

    def issues
      client.epics.issues(group_id, iid)
    end

    def issue_add(issue_id)
      client.epics.issue_add(group_id, iid, issue_id)
    end

    def issue_remove(issue_id)
      client.epics.issue_remove(group_id, iid, issue_id)
    end

    def issue_update(issue_id, _query)
      client.epics.issue_update(group_id, iid, issue_id, query)
    end

    # Resource Labels
    def resource_labels
      client.resource_labels.epics.list(group_id, id)
    end

    def resource_label(resource_event_id)
      client.resource_labels.epics.show(group_id, id, resource_event_id)
    end

    # Notes
    def notes
      client.notes.epics.list(group_id, iid)
    end

    def note_create(query)
      client.notes.epics.create(group_id, iid, query)
    end

    date_time_attrs %i[created_at closed_at updated_at]

    help do
      subtitle 'Epic'
      option 'group', 'Show Group'
      option 'update', 'Update this epic [Hash]'
      option 'delete', 'Delete this epic'
      option 'todo', 'Add todo this epic'
      option 'issues', 'List issues in epic'
      option 'issue_add', 'Add issue to Epic [Issue ID]'
      option 'issue_remove', 'Remove issue from Epic [Epic Issue ID]'
      option 'issue_update', 'Update issue association [Epic Issue ID, Hash]'

      # Notes
      option 'notes', 'List notes/comments. [Hash]'
      option 'note_create', 'Creates a new note. [Hash]'

      # Resource Labels
      option 'resource_labels', 'List of all label events'
      option 'resource_label', 'Show single label event [Resource Event ID]'
    end
  end
end
