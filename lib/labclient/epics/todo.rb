# Top namespace
module LabClient
  # Specifics
  class Epics < Common
    doc 'Create' do
      desc 'Manually creates a todo for the current user on an epic [Group ID]'
      example 'client.epics.create(5, url: "http://labclient.com")'

      result '=>  #<Epic id: 2, url: http://labclient.com>'
    end

    doc 'Create' do
      desc 'Via Epic'
      example <<~DOC
        epic = client.epics.show(5, 1)
        epic.todo
      DOC
    end

    def todo(group_id, epic_id)
      group_id = format_id(group_id)
      epic_id = format_id(epic_id)

      client.request(:post, "groups/#{group_id}/epics/#{epic_id}/todo", Todo)
    end
  end
end
