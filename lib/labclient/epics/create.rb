# Top namespace
module LabClient
  # Specifics
  class Epics < Common
    doc 'Create' do
      desc 'Creates a new epic. [Group ID, Hash]'
      example 'client.epics.create(59, title: "Initial Release")'

      result '=>  #<Epic iid: 4, title: Initial Release>'
    end

    doc 'Create' do
      markdown <<~DOC
        | Attribute           | Type             | Required   | Description                                                                            |
        | ------------------- | ---------------- | ---------- | ---------------------------------------------------------------------------------------|
        | title             | string           | yes        | The title of the epic |
        | labels            | string           | no         | The comma separated list of labels |
        | description       | string           | no         | The description of the epic. Limited to 1,048,576 characters.  |
        | confidential      | boolean          | no         | Whether the epic should be confidential. Will be ignored if confidential_epics feature flag is disabled. |
        | start_date_is_fixed | boolean        | no         | Whether start date should be sourced from start_date_fixed or from milestones (since 11.3) |
        | start_date_fixed  | string           | no         | The fixed start date of an epic (since 11.3) |
        | due_date_is_fixed | boolean          | no         | Whether due date should be sourced from due_date_fixed or from milestones (since 11.3) |
        | due_date_fixed    | string           | no         | The fixed due date of an epic (since 11.3) |
        | parent_id         | integer/string   | no         | The id of a parent epic (since 11.11) |
      DOC
    end

    doc 'Create' do
      desc 'Via Group [Hash]'
      example <<~DOC
        group = client.groups.show(16)
        epic = group.epic_create(url: "https://dothething")
      DOC
    end

    def create(group_id, query)
      group_id = format_id(group_id)

      client.request(:post, "groups/#{group_id}/epics", Epic, query)
    end
  end
end
