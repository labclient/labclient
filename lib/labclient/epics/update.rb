# Top namespace
module LabClient
  # Specifics
  class Epics < Common
    doc 'Update' do
      desc 'Updates an epic. [Group ID, Epic IID, Params]'
      example 'client.epics.update(59, 1, title: "Full Release")'

      result '=> #<Epic id: 4, title: Full Release>'
    end

    doc 'Update' do
      markdown <<~DOC
        | Attribute           | Type             | Required   | Description                                                                            |
        | ------------------- | ---------------- | ---------- | ---------------------------------------------------------------------------------------|
        | epic_iid          | integer/string   | yes        | The internal ID of the epic  |
        | title             | string           | no         | The title of an epic |
        | description       | string           | no         | The description of an epic. Limited to 1,048,576 characters.  |
        | confidential      | boolean          | no         | Whether the epic should be confidential. Will be ignored if confidential_epics feature flag is disabled. |
        | labels            | string           | no         | The comma separated list of labels |
        | start_date_is_fixed | boolean        | no         | Whether start date should be sourced from start_date_fixed or from milestones (since 11.3) |
        | start_date_fixed  | string           | no         | The fixed start date of an epic (since 11.3) |
        | due_date_is_fixed | boolean          | no         | Whether due date should be sourced from due_date_fixed or from milestones (since 11.3) |
        | due_date_fixed    | string           | no         | The fixed due date of an epic (since 11.3) |
        | state_event       | string           | no         | State event for an epic. Set close to close the epic and reopen to reopen it (since 11.4) |
      DOC
    end

    doc 'Update' do
      desc 'Via Group [Epic IID, Params]'
      example <<~DOC
        group = client.groups.show(16)
        epic = group.epic_update(1, title: "To Isengard!")
      DOC
    end

    doc 'Update' do
      desc 'Via Epic [Params]'
      example <<~DOC
        epic = client.epics.show(16, 1)
        epic.update(title: "Do all things")
      DOC
    end

    def update(group_id, epic_id, query)
      group_id = format_id(group_id)
      epic_id = format_id(epic_id)

      client.request(:put, "groups/#{group_id}/epics/#{epic_id}", Epic, query)
    end
  end
end
