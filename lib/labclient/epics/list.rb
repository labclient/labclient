# Top namespace
module LabClient
  # Specifics
  class Epics < Common
    doc 'List' do
      desc 'Gets all epics of the requested group and its subgroups. [Group ID]'
      example 'client.epics.list(59)'

      result <<~DOC
        => [#<Epic iid: 3, title: Initial Release>, ...]
      DOC

      markdown <<~DOC
        | Attribute           | Type             | Required   | Description                                                                                                                 |
        | ------------------- | ---------------- | ---------- | --------------------------------------------------------------------------------------------------------------------------- |
        | author_id         | integer          | no         | Return epics created by the given user id                                                                                 |
        | labels            | string           | no         | Return epics matching a comma separated list of labels names. Label names from the epic group or a parent group can be used |
        | with_labels_details | boolean        | no         | If true, response will return more details for each label in labels field: :name, :color, :description, :description_html, :text_color. Default is false. |
        | order_by          | string           | no         | Return epics ordered by created_at or updated_at fields. Default is created_at                                        |
        | sort              | string           | no         | Return epics sorted in asc or desc order. Default is desc                                                             |
        | search            | string           | no         | Search epics against their title and description                                                                        |
        | state             | string           | no         | Search epics against their state, possible filters: opened, closed and all, default: all                          |
        | created_after     | datetime         | no         | Return epics created on or after the given time                                                                             |
        | created_before    | datetime         | no         | Return epics created on or before the given time                                                                            |
        | updated_after     | datetime         | no         | Return epics updated on or after the given time                                                                             |
        | updated_before    | datetime         | no         | Return epics updated on or before the given time                                                                            |
        | include_ancestor_groups | boolean    | no         | Include epics from the requested group's ancestors. Default is false                                                      |
        | include_descendant_groups | boolean  | no         | Include epics from the requested group's descendants. Default is true                                                     |
      DOC
    end

    doc 'List' do
      desc 'Via Group'
      example <<~DOC
        group = client.groups.show(16)
        group.epics
      DOC
    end

    # rubocop:disable Metrics/AbcSize
    def list(group_id, query = {})
      group_id = format_id(group_id)

      query[:created_after] = query[:created_after].to_time.iso8601 if format_time?(query[:created_after])
      query[:created_before] = query[:created_before].to_time.iso8601 if format_time?(query[:created_before])

      query[:updated_after] = query[:updated_after].to_time.iso8601 if format_time?(query[:updated_after])
      query[:updated_before] = query[:updated_before].to_time.iso8601 if format_time?(query[:updated_before])

      client.request(:get, "groups/#{group_id}/epics", Epic, query)
    end
    # rubocop:enable Metrics/AbcSize
  end
end
