# Top namespace
module LabClient
  # Specifics
  class Epics < Common
    doc 'Issues' do
      title 'UPdate'
      desc 'Update Epic issue association. [Group ID, Epic IID, Epic Issue ID, Hash]'
      example 'client.epics.issue_update(59, 3, 4, move_before: 3)'

      markdown <<~DOC
              | Attribute           | Type             | Required   | Description                                                                           |
        | ------------------- | ---------------- | ---------- | --------------------------------------------------------------------------------------------|
        | move_before_id    | integer/string   | no         | The ID of the issue - epic association that should be placed before the link in the question. |
        | move_after_id     | integer/string   | no         | The ID of the issue - epic association that should be placed after the link in the question.  |
      DOC
    end

    doc 'Issues' do
      desc 'Via Group'
      example <<~DOC
        group = client.group.show(59)
        group.epic_issue_update(3,4, move_before: 3)
      DOC
    end

    doc 'Issues' do
      desc 'Via Epic'
      example <<~DOC
        epic = client.epics.show(3)
        epic.issue_update(4, move_before: 3)
      DOC
    end

    def issue_update(group_id, epic_id, epic_issue_id, query)
      group_id = format_id(group_id)
      epic_id = format_id(epic_id)

      # This is horrible, but has to be the epic_issue_id, not the issue's id or iid
      epic_issue_id = epic_issue_id.epic_issue_id if epic_issue_id.instance_of?(Issue)

      client.request(:put, "groups/#{group_id}/epics/#{epic_id}/issues/#{epic_issue_id}", nil, query)
    end
  end
end
