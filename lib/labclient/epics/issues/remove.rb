# Top namespace
module LabClient
  # Specifics
  class Epics < Common
    doc 'Issues' do
      title 'Remove'
      desc 'Remove issue from Epic. [Group ID, Epic IID, Epic Issue ID]'
      example 'client.epics.issue_remove(59, 3, 4)'
    end

    doc 'Issues' do
      desc 'Via Group'
      example <<~DOC
        group = client.group.show(59)
        group.epic_issue_remove(3,4)
      DOC
    end

    doc 'Issues' do
      desc 'Via Epic'
      example <<~DOC
        epic = client.epics.show(3)
        epic.issue_remove(4)
      DOC
    end

    def issue_remove(group_id, epic_id, epic_issue_id)
      group_id = format_id(group_id)
      epic_id = format_id(epic_id)

      # This is horrible, but has to be the epic_issue_id, not the issue's id or iid
      epic_issue_id = epic_issue_id.epic_issue_id if epic_issue_id.instance_of?(Issue)

      client.request(:delete, "groups/#{group_id}/epics/#{epic_id}/issues/#{epic_issue_id}")
    end
  end
end
