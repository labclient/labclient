# Top namespace
module LabClient
  # Specifics
  class Epics < Common
    doc 'Issues' do
      title 'List'
      desc 'Gets all issues that are assigned to an epic. [Group ID, Epic IID]'
      example 'client.epics.issues(59, 3)'

      result <<~DOC
        => [#<Issue id: 32, title: De-engineered even-keeled application, state: opened>, ...]
      DOC
    end

    doc 'Issues' do
      desc 'Via Group'
      example <<~DOC
        group = client.group.show(59)
        group.epic_issues(3)
      DOC
    end

    doc 'Issues' do
      desc 'Via Epic'
      example <<~DOC
        epic = client.epics.show(3)
        epic.issues
      DOC
    end

    def issues(group_id, epic_id)
      group_id = format_id(group_id)
      epic_id = format_id(epic_id)

      client.request(:get, "groups/#{group_id}/epics/#{epic_id}/issues", Issue)
    end
  end
end
