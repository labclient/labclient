# Top namespace
module LabClient
  # Specifics
  class Epics < Common
    doc 'Issues' do
      title 'Add'
      desc 'Creates an epic - issue association [Group ID, Epic IID, Issue ID]'
      example 'client.epics.issue_add(59, 3, 6459)'
    end

    doc 'Issues' do
      desc 'Via Group'
      example <<~DOC
        group = client.group.show(59)
        group.epic_issue_add(3,6459)
      DOC
    end

    doc 'Issues' do
      desc 'Via Epic'
      example <<~DOC
        epic = client.epics.show(4)
        epic.issue_add(6459)
      DOC
    end

    def issue_add(group_id, epic_id, issue_id)
      group_id = format_id(group_id)
      epic_id = format_id(epic_id)
      issue_id = format_id(issue_id)

      client.request(:post, "groups/#{group_id}/epics/#{epic_id}/issues/#{issue_id}")
    end
  end
end
