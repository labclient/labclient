# Top namespace
module LabClient
  # Specifics
  class Epics < Common
    doc 'Show' do
      desc 'Gets a single epic. [Group ID, Epic IID]'
      example 'client.epics.show(59, 1)'

      result '=>  #<Epic iid: 4, title: Initial Release>'
    end

    doc 'Show' do
      desc 'Via Group [Epic IID]'
      example <<~DOC
        group = client.groups.show(16)
        epic = group.epic_show(1)
      DOC
    end

    def show(group_id, epic_id)
      # GET /groups/:id/epics/:epic_iid

      group_id = format_id(group_id)
      epic_id = format_id(epic_id)

      client.request(:get, "groups/#{group_id}/epics/#{epic_id}", Epic)
    end
  end
end
