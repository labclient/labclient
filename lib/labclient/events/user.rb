# Top namespace
module LabClient
  # Specifics
  class Events < Common
    doc 'User' do
      desc 'Get the contribution events for the specified user, sorted from newest to oldest [ user id ]'
      example 'client.events.user(1)'
      result <<~DOC
        => [#<Event type: imported>, #<Event type: created>, #<Event type: pushed to>, ...]
      DOC
    end

    doc 'User' do
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | action | string | no | Include only events of a particular action type |
        | target_type | string | no | Include only events of a particular target type] |
        | before | date | no |  Include only events created before a particular date. |
        | after | date | no |  Include only events created after a particular date. |
        | scope | string | no | Include all events across a user's projects. |
        | sort | string | no | Sort events in asc or desc order by created_at. Default is desc |



        **Available parameters for the `target_type` and `action`**:

        | Type        | Parameters |
        | ----------- | ---------- |
        | action      | issue, merge_request, milestone, note, project, snippet, user |
        | target_type | closed, commented, created, destroyed, expired, joined, left, merged, pushed, reopened, updated |


      DOC
    end

    doc 'User' do
      desc 'Filter by Action'
      example 'client.events.user(1, action: :created)'
    end

    doc 'User' do
      desc 'Filter by Target Type'
      example 'client.events.user(1, target_type: :issue)'
    end

    doc 'User' do
      markdown <<~DOC
        ##### DateTime Helpers

        Both :after and :before can be used with Date or Time objects. (responds_to: to_time)
      DOC

      example <<~DOC
        client.events.user(1, after: 2.day.ago)
      DOC
    end

    def user(user_id, query = {})
      query[:before] = query[:before].to_time.iso8601 if format_time?(query[:before])
      query[:after] = query[:after].to_time.iso8601 if format_time?(query[:after])

      client.request(:get, "users/#{user_id}/events", Event, query)
    end
  end
end
