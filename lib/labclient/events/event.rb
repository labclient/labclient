# Top namespace
module LabClient
  # Inspect Helper
  class Event < Klass
    include ClassHelpers
    def inspect
      "#<Event type: #{action_name}>"
    end

    date_time_attrs %i[created_at]
    user_attrs %i[author]
  end
end
