# Top namespace
module LabClient
  # Specifics
  class ProtectedTags < Common
    doc 'Unprotect' do
      desc 'Unprotects the given protected tag or wildcard protected tag [Project ID, Tag]'
      example 'client.protected_tags.unprotect(16, :release)'
      result '#<Tag name: master>'
    end

    doc 'Unprotect' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.unprotect_tag(:other)
      DOC
    end

    def unprotect(project_id, tag_id)
      project_id = format_id(project_id)
      client.request(:delete, "projects/#{project_id}/protected_tags/#{tag_id}", Tag)
    end
  end
end
