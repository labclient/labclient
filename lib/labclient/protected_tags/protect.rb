# Top namespace
module LabClient
  # Specifics
  class ProtectedTags < Common
    doc 'Protect' do
      desc 'Protects a single repository tag or several project repository tags using a wildcard protected tag. [Project ID, Hash]'
      example 'client.protected_tags.protect(16, name: :release)'
      result '#<Tag name: master>'

      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | name | string | yes | The name of the tag or wildcard |
        | create_access_level | string | no | Access levels allowed to create (defaults: 40, maintainer access level) |

          *Valid access levels*
          0  => No access, :none
          30 => Developer access, :developer
          40 => Maintainer access, :maintainer
          60 => Admin access, :admin

      DOC
    end

    doc 'Protect' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.protect_tag(name: :other, push_access_level: :admin)
      DOC
    end

    def protect(project_id, query)
      protected_query_access_level(query, :push_access_level)
      protected_query_access_level(query, :merge_access_level)
      protected_query_access_level(query, :create_access_level)
      # POST /projects/:id/protected_tags

      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/protected_tags", Tag, query)
    end
  end
end
