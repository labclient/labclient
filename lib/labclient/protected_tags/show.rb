# Top namespace
module LabClient
  # Specifics
  class ProtectedTags < Common
    doc 'Show' do
      desc 'Gets a single protected tag or wildcard protected tag. [Project ID, Tag]'
      example 'client.protected_tags.show(16, :release)'
      result '#<Tag name: release>'
    end

    doc 'Show' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.protected_tag(:master)
      DOC
    end

    def show(project_id, tag_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/protected_tags/#{tag_id}", Tag)
    end
  end
end
