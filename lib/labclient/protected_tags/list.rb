# Top namespace
module LabClient
  # Specifics
  class ProtectedTags < Common
    doc 'List' do
      desc 'Gets a list of protected tags from a project. [Project ID]'
      example 'client.protected_tags.list(16)'
      result '[#<Tag name: feature>, #<Tag name: master>]'

      markdown <<~DOC
        Search: You can use ^term and term$ to find tags that begin and end with term respectively.
      DOC
    end

    doc 'List' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.protected_tags
      DOC
    end

    def list(project_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/protected_tags", Tag)
    end
  end
end
