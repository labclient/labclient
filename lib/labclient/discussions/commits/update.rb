# Top namespace
module LabClient
  # Specifics
  class CommitDiscussions < Common
    doc 'Commits' do
      title 'Update'
      desc 'Get a single project commit. [Project ID, Commit Sha, Discussion ID]'
      example 'client.discussions.commits.update(16, 1, "2dc2a8d", "Updaaate")'
      result <<~DOC
        => #<Discussions id: 2dc2a8d>
      DOC
    end

    doc 'Commits' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
      DOC
    end

    # Show
    def update(project_id, commit_id, discussion_id, body)
      project_id = format_id(project_id)
      commit_id = format_id(commit_id)
      discussion_id = format_id(discussion_id)
      query = { body: body }

      client.request(:put, "projects/#{project_id}/commits/#{commit_id}/discussions/#{discussion_id}", Discussion, query)
    end
  end
end
