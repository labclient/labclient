# Top namespace
module LabClient
  # Specifics
  class CommitDiscussions < Common
    doc 'Commits' do
      title 'Delete'
      desc 'Get a single project commit. [Project ID, Commit Sha, Discussion ID]'
      example 'client.discussions.commits.delete(16, 2, "7ae79")'
    end

    # Delete
    def delete(project_id, commit_id, discussion_id)
      project_id = format_id(project_id)
      commit_id = format_id(commit_id)
      discussion_id = format_id(discussion_id)

      client.request(:delete, "projects/#{project_id}/commits/#{commit_id}/discussions/#{discussion_id}")
    end
  end
end
