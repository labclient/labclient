# Top namespace
module LabClient
  # Specifics
  class CommitDiscussions < Common
    doc 'Commits' do
      title 'Show'
      desc 'Returns a single discussion for a given merge request. [Project ID, Commit Sha, Discussion ID]'
      example 'client.discussions.commits.show(16, 1, "7ae79")'
      result <<~DOC
        => #<Discussions id: 7ae79>
      DOC
    end

    def show(project_id, commit_id, discussion_id)
      project_id = format_id(project_id)
      commit_id = format_id(commit_id)
      discussion_id = format_id(discussion_id)

      client.request(:get, "projects/#{project_id}/commits/#{commit_id}/discussions/#{discussion_id}", Discussion)
    end
  end
end
