# Top namespace
module LabClient
  # Inspect Helper
  class CommitDiscussions < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Discussions < Common
    def commits
      CommitDiscussions.new(client)
    end
  end
end
