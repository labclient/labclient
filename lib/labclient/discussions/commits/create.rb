# Top namespace
module LabClient
  # Specifics
  class CommitDiscussions < Common
    doc 'Commits' do
      title 'Create'
      desc 'Creates a new discussion for a single merge request [Project, Commit Sha]'
      example 'client.discussions.commits.create(16, 2, body: "Hello!")'
      result <<~DOC
        => #<Discussions id: 2dc2a8d>
      DOC
    end

    doc 'Commits' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
        | created_at | string | no | Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z |
      DOC
    end

    def create(project_id, commit_id, query = {})
      project_id = format_id(project_id)
      commit_id = format_id(commit_id)

      query[:created_at] = query[:created_at].to_time.iso8601 if format_time?(query[:created_at])

      client.request(:post, "projects/#{project_id}/commits/#{commit_id}/discussions", Discussion, query)
    end
  end
end
