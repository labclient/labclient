# Top namespace
module LabClient
  # Specifics
  class CommitDiscussions < Common
    @group_name = 'Discussions'
    doc 'Commits' do
      title 'List'
      desc 'Gets a list of all discussion items for a single commit. [Project ID, Commit Sha]'
      example 'client.discussions.commits.list(16, 1)'
      result <<~DOC
        => [#<Discussions id: 2dc2a8d>, .. ]
      DOC
    end

    def list(project_id, commit_id)
      project_id = format_id(project_id)
      commit_id = format_id(commit_id)

      client.request(:get, "projects/#{project_id}/commits/#{commit_id}/discussions", Discussion)
    end
  end
end
