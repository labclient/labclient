# Top namespace
module LabClient
  # Inspect Helper
  class Discussion < Klass
    include ClassHelpers

    def inspect
      "#<Discussions id: #{id}>"
    end

    # DateTime Fields
    date_time_attrs %i[created_at updated_at]

    # User Fields
    user_attrs %i[resolved_by author]
  end
end
