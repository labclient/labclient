# Top namespace
module LabClient
  # Specifics
  class MergeRequestDiscussions < Common
    doc 'MergeRequests' do
      title 'Update'
      desc 'Get a single project merge_request. [Project ID, MergeRequest IID, Discussion ID]'
      example 'client.discussions.merge_requests.update(16, 1, "2dc2a8d", "Updaaate")'
      result <<~DOC
        => #<Discussions id: 2dc2a8d>
      DOC
    end

    doc 'MergeRequests' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
      DOC
    end

    # Show
    def update(project_id, merge_request_id, discussion_id, body)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)
      discussion_id = format_id(discussion_id)
      query = { body: body }

      client.request(:put, "projects/#{project_id}/merge_requests/#{merge_request_id}/discussions/#{discussion_id}", Discussion, query)
    end
  end
end
