# Top namespace
module LabClient
  # Specifics
  class MergeRequestDiscussions < Common
    @group_name = 'Discussions'
    doc 'MergeRequests' do
      title 'List'
      desc 'Gets a list of all discussion items for a single merge_request. [Project ID, MergeRequest IID]'
      example 'client.discussions.merge_requests.list(16, 1)'
      result <<~DOC
        => [#<Discussions id: 2dc2a8d>, .. ]
      DOC
    end

    def list(project_id, merge_request_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}/discussions", Discussion)
    end
  end
end
