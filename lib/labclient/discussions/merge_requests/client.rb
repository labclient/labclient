# Top namespace
module LabClient
  # Inspect Helper
  class MergeRequestDiscussions < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Discussions < Common
    def merge_requests
      MergeRequestDiscussions.new(client)
    end
  end
end
