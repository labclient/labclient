# Top namespace
module LabClient
  # Specifics
  class MergeRequestDiscussions < Common
    doc 'MergeRequests' do
      title 'Delete'
      desc 'Get a single project merge_request. [Project ID, MergeRequest IID, Discussion ID]'
      example 'client.discussions.merge_requests.delete(16, 2, "7ae79")'
    end

    # Delete
    def delete(project_id, merge_request_id, discussion_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)
      discussion_id = format_id(discussion_id)

      client.request(:delete, "projects/#{project_id}/merge_requests/#{merge_request_id}/discussions/#{discussion_id}")
    end
  end
end
