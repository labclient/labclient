# Top namespace
module LabClient
  # Specifics
  class MergeRequestDiscussions < Common
    doc 'MergeRequests' do
      title 'Show'
      desc 'Returns a single discussion for a given merge request. [Project ID, MergeRequest IID, Discussion ID]'
      example 'client.discussions.merge_requests.show(16, 1, "7ae79")'
      result <<~DOC
        => #<Discussions id: 7ae79>
      DOC
    end

    def show(project_id, merge_request_id, discussion_id)
      project_id = format_id(project_id)
      merge_request_id = format_id(merge_request_id)
      discussion_id = format_id(discussion_id)

      client.request(:get, "projects/#{project_id}/merge_requests/#{merge_request_id}/discussions/#{discussion_id}", Discussion)
    end
  end
end
