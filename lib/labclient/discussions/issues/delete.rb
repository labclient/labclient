# Top namespace
module LabClient
  # Specifics
  class IssueDiscussions < Common
    doc 'Issues' do
      title 'Delete'
      desc 'Get a single project issue. [Project ID, Issue IID, Discussion ID]'
      example 'client.discussions.issues.delete(16, 2, "7ae79")'
    end

    # Delete
    def delete(project_id, issue_id, discussion_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)
      discussion_id = format_id(discussion_id)

      client.request(:delete, "projects/#{project_id}/issues/#{issue_id}/discussions/#{discussion_id}")
    end
  end
end
