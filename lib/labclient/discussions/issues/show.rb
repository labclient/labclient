# Top namespace
module LabClient
  # Specifics
  class IssueDiscussions < Common
    doc 'Issues' do
      title 'Show'
      desc 'Returns a single discussion for a given merge request. [Project ID, Issue IID, Discussion ID]'
      example 'client.discussions.issues.show(16, 1, "7ae79")'
      result <<~DOC
        => #<Discussions id: 7ae79>
      DOC
    end

    def show(project_id, issue_id, discussion_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)
      discussion_id = format_id(discussion_id)

      client.request(:get, "projects/#{project_id}/issues/#{issue_id}/discussions/#{discussion_id}", Discussion)
    end
  end
end
