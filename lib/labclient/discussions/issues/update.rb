# Top namespace
module LabClient
  # Specifics
  class IssueDiscussions < Common
    doc 'Issues' do
      title 'Update'
      desc 'Get a single project issue. [Project ID, Issue IID, Discussion ID]'
      example 'client.discussions.issues.update(16, 1, "2dc2a8d", "Updaaate")'
      result <<~DOC
        => #<Discussions id: 2dc2a8d>
      DOC
    end

    doc 'Issues' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
      DOC
    end

    # Show
    def update(project_id, issue_id, discussion_id, body)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)
      discussion_id = format_id(discussion_id)
      query = { body: body }

      client.request(:put, "projects/#{project_id}/issues/#{issue_id}/discussions/#{discussion_id}", Discussion, query)
    end
  end
end
