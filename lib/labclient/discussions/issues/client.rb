# Top namespace
module LabClient
  # Inspect Helper
  class IssueDiscussions < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Discussions < Common
    def issues
      IssueDiscussions.new(client)
    end
  end
end
