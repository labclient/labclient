# Top namespace
module LabClient
  # Specifics
  class IssueDiscussions < Common
    @group_name = 'Discussions'
    doc 'Issues' do
      title 'List'
      desc 'Gets a list of all discussion items for a single issue. [Project ID, Issue IID]'
      example 'client.discussions.issues.list(16, 1)'
      result <<~DOC
        => [#<Discussions id: 2dc2a8d>, .. ]
      DOC
    end

    def list(project_id, issue_id)
      project_id = format_id(project_id)
      issue_id = format_id(issue_id)

      client.request(:get, "projects/#{project_id}/issues/#{issue_id}/discussions", Discussion)
    end
  end
end
