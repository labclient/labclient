# Top namespace
module LabClient
  # Specifics
  class SnippetDiscussions < Common
    doc 'Snippets' do
      title 'Show'
      desc 'Returns a single discussion for a given merge request. [Project ID, Snippet ID, Discussion ID]'
      example 'client.discussions.snippets.show(16, 1, "7ae79")'
      result <<~DOC
        => #<Discussions id: 7ae79>
      DOC
    end

    def show(project_id, snippet_id, discussion_id)
      project_id = format_id(project_id)
      snippet_id = format_id(snippet_id)
      discussion_id = format_id(discussion_id)

      client.request(:get, "projects/#{project_id}/snippets/#{snippet_id}/discussions/#{discussion_id}", Discussion)
    end
  end
end
