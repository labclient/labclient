# Top namespace
module LabClient
  # Specifics
  class SnippetDiscussions < Common
    @group_name = 'Discussions'
    doc 'Snippets' do
      title 'List'
      desc 'Gets a list of all discussion items for a single snippet. [Project ID, Snippet ID]'
      example 'client.discussions.snippets.list(16, 1)'
      result <<~DOC
        => [#<Discussions id: 2dc2a8d>, .. ]
      DOC
    end

    def list(project_id, snippet_id)
      project_id = format_id(project_id)
      snippet_id = format_id(snippet_id)

      client.request(:get, "projects/#{project_id}/snippets/#{snippet_id}/discussions", Discussion)
    end
  end
end
