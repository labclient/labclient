# Top namespace
module LabClient
  # Specifics
  class SnippetDiscussions < Common
    doc 'Snippets' do
      title 'Update'
      desc 'Get a single project snippet. [Project ID, Snippet ID, Discussion ID]'
      example 'client.discussions.snippets.update(16, 1, "2dc2a8d", "Updaaate")'
      result <<~DOC
        => #<Discussions id: 2dc2a8d>
      DOC
    end

    doc 'Snippets' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
      DOC
    end

    # Show
    def update(project_id, snippet_id, discussion_id, body)
      project_id = format_id(project_id)
      snippet_id = format_id(snippet_id)
      discussion_id = format_id(discussion_id)
      query = { body: body }

      client.request(:put, "projects/#{project_id}/snippets/#{snippet_id}/discussions/#{discussion_id}", Discussion, query)
    end
  end
end
