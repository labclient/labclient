# Top namespace
module LabClient
  # Inspect Helper
  class SnippetDiscussions < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Discussions < Common
    def snippets
      SnippetDiscussions.new(client)
    end
  end
end
