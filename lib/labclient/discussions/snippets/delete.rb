# Top namespace
module LabClient
  # Specifics
  class SnippetDiscussions < Common
    doc 'Snippets' do
      title 'Delete'
      desc 'Get a single project snippet. [Project ID, Snippet ID, Discussion ID]'
      example 'client.discussions.snippets.delete(16, 2, "7ae79")'
    end

    # Delete
    def delete(project_id, snippet_id, discussion_id)
      project_id = format_id(project_id)
      snippet_id = format_id(snippet_id)
      discussion_id = format_id(discussion_id)

      client.request(:delete, "projects/#{project_id}/snippets/#{snippet_id}/discussions/#{discussion_id}")
    end
  end
end
