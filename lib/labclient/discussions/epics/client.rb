# Top namespace
module LabClient
  # Inspect Helper
  class EpicDiscussions < Common
    include ClassHelpers
  end
end

# Top namespace
module LabClient
  # Specifics
  class Discussions < Common
    def epics
      EpicDiscussions.new(client)
    end
  end
end
