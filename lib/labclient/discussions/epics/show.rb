# Top namespace
module LabClient
  # Specifics
  class EpicDiscussions < Common
    doc 'Epics' do
      title 'Show'
      desc 'Returns a single discussion for a given merge request. [Project ID, Epic ID, Discussion ID]'
      example 'client.discussions.epics.show(16, 1, "7ae79")'
      result <<~DOC
        => #<Discussions id: 7ae79>
      DOC
    end

    def show(project_id, epic_id, discussion_id)
      project_id = format_id(project_id)
      epic_id = format_id(epic_id)
      discussion_id = format_id(discussion_id)

      client.request(:get, "projects/#{project_id}/epics/#{epic_id}/discussions/#{discussion_id}", Discussion)
    end
  end
end
