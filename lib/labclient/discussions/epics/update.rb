# Top namespace
module LabClient
  # Specifics
  class EpicDiscussions < Common
    doc 'Epics' do
      title 'Update'
      desc 'Get a single project epic. [Project ID, Epic ID, Discussion ID]'
      example 'client.discussions.epics.update(16, 1, "2dc2a8d", "Updaaate")'
      result <<~DOC
        => #<Discussions id: 2dc2a8d>
      DOC
    end

    doc 'Epics' do
      markdown <<~DOC
        | Attribute  | Type   | Required | Description |
        | ---------  | ----   | -------- | ----------- |
        | body       | string | yes | Name for Badge |
      DOC
    end

    # Show
    def update(project_id, epic_id, discussion_id, body)
      project_id = format_id(project_id)
      epic_id = format_id(epic_id)
      discussion_id = format_id(discussion_id)
      query = { body: body }

      client.request(:put, "projects/#{project_id}/epics/#{epic_id}/discussions/#{discussion_id}", Discussion, query)
    end
  end
end
