# Top namespace
module LabClient
  # Specifics
  class EpicDiscussions < Common
    doc 'Epics' do
      title 'Delete'
      desc 'Get a single project epic. [Project ID, Epic ID, Discussion ID]'
      example 'client.discussions.epics.delete(16, 2, "7ae79")'
    end

    # Delete
    def delete(project_id, epic_id, discussion_id)
      project_id = format_id(project_id)
      epic_id = format_id(epic_id)
      discussion_id = format_id(discussion_id)

      client.request(:delete, "projects/#{project_id}/epics/#{epic_id}/discussions/#{discussion_id}")
    end
  end
end
