# Top namespace
module LabClient
  # Specifics
  class EpicDiscussions < Common
    @group_name = 'Discussions'
    doc 'Epics' do
      title 'List'
      desc 'Gets a list of all discussion items for a single epic. [Project ID, Epic ID]'
      example 'client.discussions.epics.list(16, 1)'
      result <<~DOC
        => [#<Discussions id: 2dc2a8d>, .. ]
      DOC
    end

    def list(project_id, epic_id)
      project_id = format_id(project_id)
      epic_id = format_id(epic_id)

      client.request(:get, "projects/#{project_id}/epics/#{epic_id}/discussions", Discussion)
    end
  end
end
