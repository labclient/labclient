# Top namespace
module LabClient
  # Specifics
  class Wikis < Common
    doc 'Show' do
      desc 'Get a wiki page for a given project. [Project ID, Slug]'
      example 'client.wikis.show(5,"Home")'
      result '#<ImpersonationToken id: 7, name: User!, active: false>'
    end

    doc 'Show' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(5)
        project.wiki(:home)
      DOC
    end

    def show(project_id, slug)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/wikis/#{slug}", Wiki)
    end
  end
end
