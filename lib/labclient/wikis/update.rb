# Top namespace
module LabClient
  # Specifics
  class Wikis < Common
    doc 'Update' do
      desc 'Updates an existing wiki page. [Project ID, Slug, Hash]'
      example 'client.wikis.update(5, "Title!", content: "Updated!")'
      result '#<Wiki slug: Title!>'

      markdown <<~DOC
        | Attribute        | Type    | Required | Description                  |
        | ---------------- | ------- | -------- | ---------------------------- |
        | content          | string  | yes      | The content of the wiki page |
        | title            | string  | yes      | The title of the wiki page        |
        | format | string  | no       | The format of the wiki page. Available formats are: markdown (default), rdoc, asciidoc and org |

      DOC
    end

    doc 'Update' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(5)
        project.wiki_update("Title!", content: "Hello World!")
      DOC
    end

    doc 'Update' do
      desc 'Via Wiki'
      example <<~DOC
        wiki = client.wikis.show(5, "Title!")
        wiki.update(content: "Better Content!")
      DOC
    end

    def update(project_id, slug, query)
      project_id = format_id(project_id)

      client.request(:put, "projects/#{project_id}/wikis/#{slug}", Wiki, query)
    end
  end
end
