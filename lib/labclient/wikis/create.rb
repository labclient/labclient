# Top namespace
module LabClient
  # Specifics
  class Wikis < Common
    doc 'Create' do
      desc 'Creates a new wiki page for the given repository with the given title, slug, and content. [Project ID, Hash]'
      example 'client.wikis.create(5, content: "Lots of Stuff", title: "Title!")'
      result '#<Wiki slug: Title!>'

      markdown <<~DOC
        | Attribute        | Type    | Required | Description                  |
        | ---------------- | ------- | -------- | ---------------------------- |
        | content          | string  | yes      | The content of the wiki page |
        | title            | string  | yes      | The title of the wiki page        |
        | format | string  | no       | The format of the wiki page. Available formats are: markdown (default), rdoc, asciidoc and org |
      DOC
    end

    doc 'Create' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(5)
        project.wiki_create(title: "Special", content: "Hello World!")
      DOC
    end

    def create(project_id, query)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/wikis", Wiki, query)
    end
  end
end
