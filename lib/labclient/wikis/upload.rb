# Top namespace
module LabClient
  # Specifics
  class Wikis < Common
    doc 'Upload' do
      desc 'Uploads a file to the attachment folder inside the wiki’s repository. The attachment folder is the uploads folder. [ Project ID, File Path, Branch(Optional)]'
      example 'client.projects.upload(25, "/tmp/text.txt")'
      result '{:alt=>"text.txt", :url=>"/uploads/123123/text.txt", :markdown=>"[text.txt](/uploads/123123/text.txt)"}'
    end

    doc 'Upload' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(299)
        project.wiki_upload("/tmp/text.txt")
      DOC
    end

    def upload(project_id, path, branch = nil)
      project_id = format_id(project_id)

      file = File.open(path, 'r')

      client.request(:post, "projects/#{project_id}/wikis/attachments", nil, { file: file, branch: branch }, false)
    end
  end
end
