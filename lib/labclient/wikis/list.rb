# Top namespace
module LabClient
  # Specifics
  class Wikis < Common
    @group_name = 'Wiki'

    doc 'List' do
      desc 'Get all wiki pages for a given project. [Project ID, Include Content(Boolean)]'
      example 'client.wikis.list(5)'
      result ' #<Wiki slug: Home>'
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(5)
        project.wikis.list
      DOC
    end

    def list(project_id, with_content = false)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/wikis", Wiki, { with_content: with_content })
    end
  end
end
