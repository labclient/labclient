# Top namespace
module LabClient
  # Specifics
  class Wikis < Common
    doc 'Delete' do
      desc 'Deletes an existing wiki page. [Project ID, Slug]'
      example 'client.wikis.delete(5, "Title!")'
    end

    doc 'Delete' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(5)
        project.wiki_delete("Title!")
      DOC
    end

    doc 'Delete' do
      desc 'Via Wiki'
      example <<~DOC
        wiki = client.wikis.show(5, "Title!")
        wiki.delete
      DOC
    end

    def delete(project_id, slug)
      project_id = format_id(project_id)

      client.request(:delete, "projects/#{project_id}/wikis/#{slug}")
    end
  end
end
