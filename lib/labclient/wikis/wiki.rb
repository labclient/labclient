# Top namespace
module LabClient
  # Inspect Helper
  class Wiki < Klass
    include ClassHelpers

    def inspect
      "#<Wiki slug: #{slug}>"
    end

    def web_url
      "#{project.web_url}/-/wikis/#{slug}"
    end

    def project
      project_id = collect_project_id
      client.projects.show project_id
    end

    def update(query)
      project_id = collect_project_id
      update_self client.wikis.update(project_id, slug, query)
    end

    def delete
      project_id = collect_project_id
      client.wikis.delete(project_id, slug)
    end

    help do
      @group_name = 'Wiki'

      subtitle 'Wiki'
      option 'project', 'Get Project Object'
      option 'web_url', 'Generate Web URL to Wiki Page'
      option 'update', 'Update this wiki page [Hash]'
      option 'delete', 'Delete this wiki page'
    end
  end
end
