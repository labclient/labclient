# Top namespace
module LabClient
  # Specifics
  class ProtectedEnvironments < Common
    doc 'Show' do
      desc 'Gets a single protected environment [Project ID, ProtectedEnvironment]'
      example 'client.protected_environments.show(36, :prod)'
      result '#<ProtectedEnvironment name: prod>'
    end

    doc 'Show' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.protected_environment(:master)
      DOC
    end

    def show(project_id, environment_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/protected_environments/#{environment_id}", ProtectedEnvironment)
    end
  end
end
