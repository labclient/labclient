# Top namespace
module LabClient
  # Specifics
  class ProtectedEnvironments < Common
    doc 'List' do
      desc 'Gets a list of protected environmentes from a project. [Project ID, String]'
      example 'client.protected_environments.list(36)'
      result '[#<ProtectedEnvironment name: prod>]'

      markdown <<~DOC
        Search: You can use ^term and term$ to find environmentes that begin and end with term respectively.
      DOC
    end

    doc 'List' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.protected_environments
      DOC
    end

    def list(project_id)
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/protected_environments", ProtectedEnvironment)
    end
  end
end
