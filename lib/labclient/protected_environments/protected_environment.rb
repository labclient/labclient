# Top namespace
module LabClient
  # Inspect Helper
  class ProtectedEnvironment < Klass
    include ClassHelpers
    def inspect
      "#<ProtectedEnvironment name: #{name}>"
    end

    def unprotect
      project_id = collect_project_id

      client.protected_environments.unprotect(project_id, name)
    end

    help do
      subtitle 'ProjectEnvironment'
      option 'project', 'Show Project'
      option 'unprotect', 'Remove protected environment'
    end
  end
end
