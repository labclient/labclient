# Top namespace
module LabClient
  # Specifics
  class ProtectedEnvironments < Common
    doc 'Unprotect' do
      desc 'Unprotects the given protected environment [Project ID, ProtectedEnvironment]'
      example 'client.protected_environments.unprotect(264, :prod)'
    end

    doc 'Unprotect' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.unprotect_environment(:other)
      DOC
    end

    doc 'Unprotect' do
      desc 'via ProtectedEnvironment'
      example <<~DOC
        protected_env = client.protected_environments.show(36, :prod)
        protected_env.unprotect
      DOC
    end

    def unprotect(project_id, environment_id)
      project_id = format_id(project_id)
      client.request(:delete, "projects/#{project_id}/protected_environments/#{environment_id}")
    end
  end
end
