# Top namespace
module LabClient
  # Specifics
  class ProtectedEnvironments < Common
    doc 'Protect' do
      desc 'Protects a single environment [Project ID, Hash]'
      example <<~DOC
        client.protected_environments.protect(36,
          name: :prod,
          deploy_access_levels: [
            { access_level: :developer }
          ]
        )
      DOC
      result '#<ProtectedEnvironment name: prod>'

      markdown <<~DOC
        | Attribute            | Type           | Required | Description                                                                                                      |
        | -------------------- | -------------- | -------- | ---------------------------------------------------------------------------------------------------------------- |
        | name                 | string         | yes      | The name of the environment.                                                                                     |
        | deploy_access_levels | array          | yes      | Array of access levels allowed to deploy, with each described by a hash.                                         |

        Elements in the deploy_access_levels array should take the form {user_id: integer}, {group_id: integer} or {access_level: integer}. Each user must have access to the project and each group must have this project shared.

        **Valid access levels**

        | Access Level | Values          |
        | ------------ | --------------- |
        | Developer    | 30, :developer  |
        | Maintainer   | 40, :maintainer |
        | Admin        | 60, :admin      |

      DOC
    end

    doc 'Protect' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.protect_environment(name: :other, push_access_level: :admin)
      DOC
    end

    def protect(project_id, query)
      query[:deploy_access_levels].each do |access_level|
        protected_query_access_level(access_level, :access_level)
      end

      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/protected_environments", ProtectedEnvironment, query)
    end
  end
end
