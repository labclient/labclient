# Top namespace
module LabClient
  # Inspect Helper
  class Avatar < Klass
    include ClassHelpers
    def inspect
      "#<Avatar url: #{avatar_url}>"
    end

    def url
      avatar_url
    end
  end
end
