# Top namespace
module LabClient
  # Specifics
  class Avatars < Common
    @group_name = 'Avatar'

    doc 'Show' do
      desc 'Get a single avatar URL for a user with the given email address.'
      example 'client.avatar.show(email: "jeanlucpicard@example", size: 512)'
      result <<~DOC
        =>  #<Avatar url: https://secure.gravatar.com/avatar/1f218446d7b40a24b0eae6b741d97fe6?s=1024&d=identicon>
      DOC

      markdown <<~DOC
        | Attribute | Type    | Required | Description                                                                                                                             |
        |---------- |-------- |--------- |----------------------------------------------------------------------------------------------------------------------------------------|
        | email   | string  | yes      | Public email address of the user.                                                                                                       |
        | size    | integer | no       | Single pixel dimension (since images are squares). Only used for avatar lookups at Gravatar or at the configured Libravatar server. |
      DOC
    end

    # List/Search users
    def show(query = {})
      client.request(:get, 'avatar', Avatar, query)
    end
  end
end
