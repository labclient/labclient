module LabClient
  # Application Specifics
  class ApplicationSettings < Common
    doc 'Update' do
      title 'Update a single setting'
      example <<~DOC
        client.application_settings.update(sign_in_text: "This is a test")
      DOC
      result <<~DOC
        => #<Application Settings - id: 1>
      DOC
    end

    doc 'Update' do
      title 'Update multiple settings'
      example <<~DOC
        client.application_settings.update(sign_in_text: "This is a test", signup_enabled: true)
      DOC
      result <<~DOC
        => #<Application Settings - id: 1>
      DOC
    end

    doc 'Update' do
      title 'Attributes'
      markdown <<~DOC
        Please see [here for a full list](https://docs.gitlab.com/ee/api/settings.html#list-of-settings-that-can-be-accessed-via-api-calls) of all attributes.

        | Attribute | Type | Required |
        | --------- | ---- | :------: |
        | admin_notification_email             | string           | no                                |
        | after_sign_out_path                    | string           | no                             |
        | after_sign_up_text                     | string           | no                            |
        | akismet_api_key                        | string           | required by: akismet_enabled |
        | akismet_enabled                        | boolean          | no           |
        | allow_group_owners_to_manage_ldap      | boolean          | no          |
        | allow_local_requests_from_hooks_and_services | boolean    | no         |
        | allow_local_requests_from_system_hooks | boolean    | no              |
        | allow_local_requests_from_web_hooks_and_services | boolean    | no   |
        | archive_builds_in_human_readable       | string           | no                                    |
        | asset_proxy_enabled                    | boolean          | no                                   |
        | asset_proxy_secret_key                 | string           | no                                   |
        | asset_proxy_url                        | string           | no                                   |
        | asset_proxy_whitelist                  | string or array of strings | no                         |
      DOC
    end

    # Update Settings
    def update(query)
      client.request(:put, 'application/settings', ApplicationSetting, query)
    end
  end
end
