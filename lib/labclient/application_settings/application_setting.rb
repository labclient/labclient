module LabClient
  # Inspect Helper
  class ApplicationSetting < Klass
    include ClassHelpers

    def inspect
      "#<Application Settings id: #{id}>"
    end
  end
end
