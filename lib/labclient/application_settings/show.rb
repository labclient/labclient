module LabClient
  # Apllication Settings Specifics
  class ApplicationSettings < Common
    doc 'Show' do
      title 'Get current settings'
      desc 'List the current application settings of the GitLab instance'
      example 'client.application_settings.show'
      result <<~DOC
        => #<Application Settings - id: 1>
      DOC
    end

    doc 'Show' do
      title 'Show current settings'
      desc 'Uses built in awesome print to show all current settings.'
      example <<~DOC
        settings = client.application_settings.show
        settings.verbose
      DOC
      result <<~DOC
        => LabClient::ApplicationSetting {
                                                             :id => 1,
                               :performance_bar_allowed_group_id => nil,
                                       :admin_notification_email => nil,
                                            :after_sign_out_path => "",
                                             :after_sign_up_text => "",
                                                :akismet_api_key => nil,
                                                :akismet_enabled => false,
               :allow_local_requests_from_web_hooks_and_services => false,
                         :allow_local_requests_from_system_hooks => true,
                               :dns_rebinding_protection_enabled => true,
                               ...
      DOC
    end

    doc 'Show' do
      title 'Show a single setting'
      example <<~DOC
        settings = client.application_settings.show
        settings.sign_in_text
      DOC
      result <<~DOC
        => "I love GitLab!"
      DOC
    end

    # Show Settings
    def show
      client.request(:get, 'application/settings', ApplicationSetting)
    end
  end
end
