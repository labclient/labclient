# Top namespace
module LabClient
  # Specifics
  class Commits < Common
    doc 'List' do
      desc 'List repository commits. [Project ID, Hash]'
      example 'client.commits.list(16)'
      result <<~DOC
        => [#<Commit title: Update README.md, sha: ae5cd273>, #<Commit title: Add README.md, sha: 04d8c6be>]
      DOC

      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | ref_name | string | no | The name of a repository branch, tag or revision range, or if not given the default branch |
        | since | string | no | Only commits after or on this date will be returned in ISO 8601 format YYYY-MM-DDTHH:MM:SSZ |
        | until | string | no | Only commits before or on this date will be returned in ISO 8601 format YYYY-MM-DDTHH:MM:SSZ |
        | path | string | no | The file path |
        | all | boolean | no | Retrieve every commit from the repository |
        | with_stats | boolean | no | Stats about each commit will be added to the response |
        | first_parent | boolean | no | Follow only the first parent commit upon seeing a merge commit |
        | order | string | no | List commits in order. Possible values: default, [topo](https://git-scm.com/docs/git-log#Documentation/git-log.txt---topo-order). Defaults to default, the commits are shown in reverse chronological order. |
      DOC
    end

    doc 'List' do
      desc 'Filter by ref_name'
      example 'client.commits.list(16, ref_name: :master)'
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.commits
      DOC
    end

    doc 'List' do
      desc 'Since with Time Helper'
      example 'client.commits.list(315, since: 1.month.ago)'
    end

    def list(project_id, query = {})
      project_id = format_id(project_id)

      %i[since until].each do |field|
        query[field] = query[field].to_time.iso8601 if format_time?(query[field])
      end

      client.request(:get, "projects/#{project_id}/repository/commits", Commit, query)
    end
  end
end
