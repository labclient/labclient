# Top namespace
module LabClient
  # Inspect Helper
  class CommitComment < Klass
    include ClassHelpers

    def inspect
      "#<CommitComment title: #{note[0..9]}>"
    end

    date_time_attrs %i[created_at]
    user_attrs %i[author]
  end
end
