# Top namespace
module LabClient
  # Specifics
  class Commits < Common
    doc 'Show' do
      title 'Merge Requests'
      desc 'Get a list of Merge Requests related to the specified commit. [Project ID, Commit ID]'
      example <<~DOC
        client.commits.merge_requests(16, 'a0550a65')
      DOC

      result <<~DOC
        => [#<MergeRequest id: 12, title: Update README.md, state: opened>]
      DOC
    end

    doc 'Show' do
      desc 'Via Commit'
      example <<~DOC
        commit = client.commits.show(16, 'a0550a65')
        commit.merge_requests
      DOC
    end

    doc 'Show' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.commit_merge_requests('a0550a65')
      DOC
    end

    def merge_requests(project_id, commit_id)
      project_id = format_id(project_id)
      commit_id = format_id(commit_id)

      client.request(:get, "projects/#{project_id}/repository/commits/#{commit_id}/merge_requests", MergeRequest)
    end
  end
end
