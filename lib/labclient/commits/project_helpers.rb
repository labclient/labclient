# Top namespace
module LabClient
  # Helper Methods for Projects
  module ProjectCommits
    def commits(query = {})
      client.commits.list(id, query)
    end

    def commit(commit_id, query = {})
      client.commits.show(id, commit_id, query)
    end

    def commit_create(query = {})
      client.commits.create(id, query)
    end

    def commit_refs(commit_id, scope = :all)
      client.commits.refs(id, commit_id, scope)
    end

    def commit_cherry_pick(commit_id, branch_name)
      client.commits.cherry_pick(id, commit_id, branch_name)
    end

    def commit_revert(commit_id, branch_name)
      client.commits.revert(id, commit_id, branch_name)
    end

    def commit_diff(commit_id)
      client.commits.diff(id, commit_id)
    end

    def commit_comments(commit_id)
      client.commits.comments(id, commit_id)
    end

    def commit_comment_create(commit_id, query)
      client.commits.comment_create(id, commit_id, query)
    end

    def commit_merge_requests(commit_id)
      client.commits.merge_requests(id, commit_id)
    end

    def commit_status(commit_id, query = {})
      client.commits.status(id, commit_id, query)
    end

    def commit_status_update(commit_id, query = {})
      client.commits.status_update(id, commit_id, query)
    end
  end
end
