# Top namespace
module LabClient
  # Specifics
  class Commits < Common
    doc 'Comments' do
      desc 'Get the comments of a commit in a project. [Project ID, Commit ID]'
      example <<~DOC
        client.commits.comments(16, '92c2b96b')
      DOC

      result <<~DOC
        => [#<CommitComment title: another!>]
      DOC
    end

    doc 'Comments' do
      desc 'Via Commit'
      example <<~DOC
        commit = client.commits.show(16,)
        commit.comments
      DOC
    end

    doc 'Comments' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.commit_comments('a0550a65')
      DOC
    end

    def comments(project_id, commit_id)
      project_id = format_id(project_id)
      commit_id = format_id(commit_id)

      client.request(:get, "projects/#{project_id}/repository/commits/#{commit_id}/comments", CommitComment)
    end
  end
end
