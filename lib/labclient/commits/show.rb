# Top namespace
module LabClient
  # Specifics
  class Commits < Common
    doc 'Show' do
      desc 'Get a specific commit identified by the commit hash or name of a branch or tag.'
      example 'client.commits.show(343, "b539e5d3")'
      result <<~DOC
        => #<Commit title: Update README.md, sha: b539e5d3>
      DOC

      markdown <<~DOC
        | Attribute | Type | Description |
        | --------- | ---- | ----------- |
        | stats | boolean | Include commit stats. Default is true |
      DOC
    end

    doc 'Show' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.commit("b539e5d3")
      DOC
    end

    # Show
    def show(project_id, commit_id, query = {})
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/repository/commits/#{commit_id}", Commit, query)
    end
  end
end
