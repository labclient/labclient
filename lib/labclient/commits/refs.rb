# Top namespace
module LabClient
  # Specifics
  class Commits < Common
    doc 'Show' do
      title 'Refs'
      desc 'Get all references (from branches or tags) a commit is pushed to.  [Project ID, Commit ID, Scope]'
      example <<~DOC
        client.commits.refs(16, 'a0550a65')
      DOC

      result <<~DOC
        => [{:type=>"branch", :name=>"master"}]
      DOC

      markdown <<~DOC
        - Possible Scopes: `branch`, `tag`, and `all`. Default is `all`
      DOC
    end

    doc 'Show' do
      desc 'Via Commit'
      example <<~DOC
        commit = client.commits.show(16, 'a0550a65')
        commit.refs
      DOC
    end

    doc 'Show' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.commit_refs('a0550a65')
      DOC
    end

    def refs(project_id, commit_id, scope = :all)
      project_id = format_id(project_id)
      commit_id = format_id(commit_id)

      client.request(:get, "projects/#{project_id}/repository/commits/#{commit_id}/refs", nil, { scope: scope })
    end
  end
end
