# Top namespace
module LabClient
  # Specifics
  class Commits < Common
    doc 'Status' do
      title 'Update'
      desc 'Adds or updates a build status of a commit. [Project ID, Commit ID, Hash]'
      example <<~DOC
        client.commits.status_update(16, '2a3ae164d43b9f7b32d887d6ac7e125f40fd3958', state: :running)
      DOC

      result <<~DOC
        => #<CommitStatus id: 217, name: default, status: running>
      DOC

      markdown <<~DOC

        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | state   | string  | yes   | The state of the status. Can be one of the following: pending, running, success, failed, canceled
        | ref     | string  | no    | The ref (branch or tag) to which the status refers
        | name or context | string  | no | The label to differentiate this status from the status of other systems. Default value is default
        | target_url |  string  | no  | The target URL to associate with this status
        | description | string  | no  | The short description of the status
        | coverage | float  | no    | The total code coverage
        | pipeline_id |  integer  | no  | The ID of the pipeline to set status. Use in case of several pipeline on same SHA.
      DOC
    end

    doc 'Status' do
      desc 'Via Commit'
      example <<~DOC
        commit = client.commits.status_update(16, '2a3ae164', state: :success)
        commit.status
      DOC
    end

    doc 'Status' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.commit_status_update('a0550a65', state: :failed)
      DOC
    end

    def status_update(project_id, commit_id, query = {})
      project_id = format_id(project_id)
      commit_id = format_id(commit_id)

      client.request(:post, "projects/#{project_id}/statuses/#{commit_id}", CommitStatus, query)
    end
  end
end
