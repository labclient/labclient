# Top namespace
module LabClient
  # Inspect Helper
  class CommitStatus < Klass
    include ClassHelpers

    def inspect
      "#<CommitStatus id: #{id}, name: #{name}, status: #{status}>"
    end

    date_time_attrs %i[created_at started_at finished_at]
    user_attrs %i[author]
  end
end
