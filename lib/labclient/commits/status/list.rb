# Top namespace
module LabClient
  # Specifics
  class Commits < Common
    doc 'Status' do
      title 'List'
      desc 'List the statuses of a commit in a project. [Project ID, Commit ID, Hash]'
      example <<~DOC
        client.commits.status(16, '2a3ae164d43b9f7b32d887d6ac7e125f40fd3958')
      DOC

      result <<~DOC
        => [#<CommitStatus id: 213, status: success>, #<CommitStatus id: 214, status: success>, ... ]
      DOC

      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | ref     | string  | no  | The name of a repository branch or tag or, if not given, the default branch
        | stage   | string  | no  | Filter by build stage. e.g., test
        | name    | string  | no  | Filter by job name, e.g., bundler:audit
        | all     | boolean | no  | Return all statuses, not only the latest ones
      DOC
    end

    doc 'Status' do
      desc 'Via Commit'
      example <<~DOC
        commit = client.commits.show(16, '2a3ae164')
        commit.status
      DOC
    end

    doc 'Status' do
      desc 'Filter by Stage'
      example <<~DOC
        commit = client.commits.show(16, 'a0550a65')
        commit.status(stage: :test)
      DOC
    end

    doc 'Status' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.commit_status('a0550a65')
      DOC
    end

    def status(project_id, commit_id, query = {})
      project_id = format_id(project_id)
      commit_id = format_id(commit_id)

      client.request(:get, "projects/#{project_id}/repository/commits/#{commit_id}/statuses", CommitStatus, query)
    end
  end
end
