# Top namespace
module LabClient
  # Specifics
  class Commits < Common
    doc 'Create' do
      desc 'Create a commit with multiple files and actions. [Project ID, Hash]'
      example <<~DOC
        payload = {
          actions: [
            {
              action: 'create',
              content: 'some content',
              file_path: 'foo/bar'
            }
          ],
          branch: 'master',
          commit_message: 'some commit message'
        }


        client.commits.create(16, payload)
      DOC

      result <<~DOC
        => #<Commit title: some commit message, sha: a0550a65>
      DOC

      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | id | integer/string | yes | The ID or [URL-encoded path of the project|
        | branch | string | yes | Name of the branch to commit into. To create a new branch, also provide either start_branch or start_sha, and optionally start_project. |
        | commit_message | string | yes | Commit message |
        | start_branch | string | no | Name of the branch to start the new branch from |
        | start_sha | string | no | SHA of the commit to start the new branch from |
        | start_project | integer/string | no | The project ID or URL-encoded path of the project to start the new branch from. Defaults to the value of id. |
        | actions[] | array | yes | An array of action hashes to commit as a batch. See the next table for what attributes it can take. |
        | author_email | string | no | Specify the commit author's email address |
        | author_name | string | no | Specify the commit author's name |
        | stats | boolean | no | Include commit stats. Default is true |
        | force | boolean | no | When true overwrites the target branch with a new commit based on the start_branch or start_sha |

        ##### Actions

        | Attribute | Type | Required | Description |
        | --------------------- | ---- | -------- | ----------- |
        | action | string | yes | The action to perform, create, delete, move, update, chmod|
        | file_path | string | yes | Full path to the file. Ex. lib/class.rb |
        | previous_path | string | no | Original full path to the file being moved. Ex. lib/class1.rb. Only considered for move action. |
        | content | string | no | File content, required for all except delete, chmod, and move. Move actions that do not specify content will preserve the existing file content, and any other value of content will overwrite the file content. |
        | encoding | string | no | text or base64. text is default. |
        | last_commit_id | string | no | Last known file commit id. Will be only considered in update, move and delete actions. |
        | execute_filemode | boolean | no | When true/false enables/disables the execute flag on the file. Only considered for chmod action. |
      DOC
    end

    doc 'Create' do
      desc 'Via Project'
      example <<~DOC
        payload = {
          actions: [
            {
              action: 'create',
              content: 'some content',
              file_path: 'foo/bar'
            }
          ],
          branch: 'master',
          commit_message: 'some commit message'
        }

        project = client.projects.show(16)
        project.commit_create(payload)
      DOC
    end

    def create(project_id, query = {})
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/repository/commits", Commit, query)
    end
  end
end
