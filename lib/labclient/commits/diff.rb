# Top namespace
module LabClient
  # Specifics
  class Commits < Common
    doc 'Diff' do
      desc 'Diffs a commit in a given branch. [Project ID, Commit ID]'
      example <<~DOC
        client.commits.diff(16, '92c2b96b')
      DOC

      result <<~DOC
        => #<Commit title: Update bar, sha: 1e64f40c>
      DOC
    end

    doc 'Diff' do
      desc 'Via Commit'
      example <<~DOC
        commit = client.commits.show(16,)
        commit.diff('new_branch')
      DOC
    end

    doc 'Diff' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.commit_diff('a0550a65')
      DOC
    end

    def diff(project_id, commit_id)
      project_id = format_id(project_id)
      commit_id = format_id(commit_id)

      client.request(:get, "projects/#{project_id}/repository/commits/#{commit_id}/diff", CommitDiff)
    end
  end
end
