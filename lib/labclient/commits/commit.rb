# Top namespace
module LabClient
  # Inspect Helper
  class Commit < Klass
    include ClassHelpers

    def inspect
      "#<Commit title: #{title}, sha: #{short_id}>"
    end

    def refs(scope = :all)
      # If from List Project ID isn't stored
      project_id = collect_project_id if project_id.nil?

      client.commits.refs(project_id, id, scope)
    end

    def cherry_pick(branch_name)
      # If from List Project ID isn't stored
      project_id = collect_project_id if project_id.nil?

      client.commits.cherry_pick(project_id, id, branch_name)
    end

    def revert(branch_name)
      # If from List Project ID isn't stored
      project_id = collect_project_id if project_id.nil?

      client.commits.revert(project_id, id, branch_name)
    end

    def diff
      # If from List Project ID isn't stored
      project_id = collect_project_id if project_id.nil?

      client.commits.diff(project_id, id)
    end

    def merge_requests
      # If from List Project ID isn't stored
      project_id = collect_project_id if project_id.nil?

      client.commits.merge_requests(project_id, id)
    end

    def comments
      # If from List Project ID isn't stored
      project_id = collect_project_id if project_id.nil?

      client.commits.comments(project_id, id)
    end

    def comment(query)
      # If from List Project ID isn't stored
      project_id = collect_project_id if project_id.nil?

      client.commits.comment_create(project_id, id, query)
    end

    def project
      # If from List Project ID isn't stored
      project_id = collect_project_id if project_id.nil?

      client.projects.show(project_id)
    end

    def web_url
      "#{project.web_url}/-/commit/#{id}"
    end

    def status(query = {})
      # If from List Project ID isn't stored
      project_id = collect_project_id if project_id.nil?

      client.commits.status(project_id, id, query)
    end

    def status_update(query = {})
      # If from List Project ID isn't stored
      project_id = collect_project_id if project_id.nil?

      client.commits.status_update(project_id, id, query)
    end

    date_time_attrs %i[committed_date authored_date created_at]

    help do
      subtitle 'Commit'
      option 'refs', 'Get all references'
      option 'cherry_pick', 'Cherry picks a commit to a target branch [Target Branch]'
      option 'revert', 'Reverts a commit in a given branch [Target Branch]'
      option 'diff', 'Get the diff'
      option 'project', 'Show Commit Project'
      option 'web_url', 'Generate project/web_url'
      option 'comments', 'Get comments'
      option 'comment', 'Create new comment [Hash]'
      option 'status', 'List the statuses [Hash]'
      option 'status_update', 'Adds or updates a build status [Hash]'
    end
  end
end
