# Top namespace
module LabClient
  # Specifics
  class Commits < Common
    doc 'Comments' do
      title 'Create'
      desc 'Post comment to commit. [Project ID, Commit ID, Hash]'
      example <<~DOC
        client.commits.comment_create(16, '04d8c6be', note: 'hi there!')
      DOC

      result <<~DOC
        => [#<CommitComment title: another!>]
      DOC

      markdown <<~DOC


        In order to post a comment in a particular line of a particular file, you must
        specify the full commit SHA, the `path`, the `line` and `line_type` should be
        `new`.

        The comment will be added at the end of the last commit if at least one of the
        cases below is valid:

        - the `sha` is instead a branch or a tag and the `line` or `path` are invalid
        - the `line` number is invalid (does not exist)
        - the `path` is invalid (does not exist)

        In any of the above cases, the response of `line`, `line_type` and `path` is
        set to `null`.


        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | note      | string  | yes | The text of the comment |
        | path      | string  | no  | The file path relative to the repository |
        | line      | integer | no  | The line number where the comment should be placed |
        | line_type | string  | no  | The line type. Takes new or old as arguments |

      DOC
    end

    doc 'Comments' do
      desc 'Via Commit'
      example <<~DOC
        commit = client.commits.show(16,)
        commit.comments
      DOC
    end

    doc 'Comments' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.commit_comments('a0550a65')
      DOC
    end

    def comment_create(project_id, commit_id, query)
      project_id = format_id(project_id)
      commit_id = format_id(commit_id)

      client.request(:post, "projects/#{project_id}/repository/commits/#{commit_id}/comments", CommitComment, query)
    end
  end
end
