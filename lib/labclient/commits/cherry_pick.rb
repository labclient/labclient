# Top namespace
module LabClient
  # Specifics
  class Commits < Common
    doc 'Cherry Pick' do
      desc 'Cherry picks a commit to a target branch.  [Project ID, Commit ID, Target Branch]'
      example <<~DOC
        client.commits.cherry_pick(16, '92c2b96b', 'master')
      DOC

      result <<~DOC
        => #<Commit title: Update bar, sha: 1e64f40c>
      DOC
    end

    doc 'Cherry Pick' do
      desc 'Via Commit'
      example <<~DOC
        commit = client.commits.show(16, 'a0550a65')
        commit.cherry_pick('new_branch')
      DOC
    end

    doc 'Cherry Pick' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.commit_cherry_pick('a0550a65', 'target_branch)
      DOC
    end

    def cherry_pick(project_id, commit_id, branch_name)
      project_id = format_id(project_id)
      commit_id = format_id(commit_id)
      branch_name = format_id(branch_name)

      client.request(:post, "projects/#{project_id}/repository/commits/#{commit_id}/cherry_pick", Commit, { branch: branch_name })
    end
  end
end
