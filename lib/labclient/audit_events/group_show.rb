# Top namespace
module LabClient
  # Specifics
  class AuditEvents < Common
    doc 'Group' do
      title 'Group Show'
      desc 'Retrieve single group audit event (group, audit)'
      example 'client.audit_events.group_show(15,24)'
      result <<~DOC
        => #<AuditEvent id: 24, type: Group>
      DOC
    end

    doc 'Group' do
      title 'via Group'
      example <<~DOC
        group = client.group.show(5)
        group.audit_events(24)
      DOC
    end

    # Show
    def group_show(group_id = nil, audit_event_id = nil)
      client.request(:get, "groups/#{group_id}/audit_events/#{audit_event_id}", AuditEvent)
    end
  end
end
