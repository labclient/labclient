# Top namespace
module LabClient
  # Specifics
  class AuditEvents < Common
    doc 'Instance' do
      title 'Show'
      desc 'Retrieve single instance audit event'
      example 'client.audit_events.show(50)'
      result <<~DOC
        => #<AuditEvent id: 50, type: Group>
      DOC
    end

    # Show
    def show(audit_event_id = nil)
      client.request(:get, "audit_events/#{audit_event_id}", AuditEvent)
    end
  end
end
