# Top namespace
module LabClient
  # Specifics
  class AuditEvents < Common
    doc 'Group' do
      title 'Group List'
      desc 'Retrive and filter group events '
      example 'client.audit_events.group_list(15)'
      result <<~DOC
        => [#<AuditEvent id: 25, type: Group>]
      DOC

      markdown <<~DOC
        | Attribute | Type | Description |
        | --------- | ---- | | ----------- |
        | created_after | string | Return audit events created on or after the given time. Format: ISO 8601 YYYY-MM-DDTHH:MM:SSZ  |
        | created_before | string | Return audit events created on or before the given time. Format: ISO 8601 YYYY-MM-DDTHH:MM:SSZ |
      DOC
    end

    doc 'Group' do
      markdown <<~DOC
        ##### DateTime Helpers

        Both `:created_after` and `:created_before` will access Date or Time objects. (responds_to: `to_time`)
      DOC

      example <<~DOC
        client.audit_events.group_list(80,created_after: Date.today)
      DOC

      result <<~DOC
        => [#<AuditEvent id: 359, type: Project>]
      DOC
    end

    doc 'Group' do
      example <<~DOC
        client.audit_events.group_list(80, created_before: 1.month.ago )
      DOC

      result <<~DOC
        => [#<AuditEvent id: 24, type: Group>]
      DOC
    end

    doc 'Group' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(5)
        group.audit_events
      DOC
    end

    # List
    def group_list(group_id, query = {})
      query_format_time(query, :created_before)
      query_format_time(query, :created_after)

      client.request(:get, "groups/#{group_id}/audit_events", AuditEvent, query)
    end
  end
end
