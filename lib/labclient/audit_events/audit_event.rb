# Top namespace
module LabClient
  # Inspect Helper
  class AuditEvent < Klass
    include ClassHelpers
    def inspect
      "#<AuditEvent id: #{id}, type: #{entity_type}>"
    end

    date_time_attrs %i[created_at]
  end
end
