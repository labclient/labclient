# Top namespace
module LabClient
  # Specifics
  class AuditEvents < Common
    doc 'Instance' do
      title 'List'
      desc 'Retrive and filter instance events '
      example 'client.audit_events.list'
      result <<~DOC
        => [
            #<AuditEvent id: 372, type: User>,
            #<AuditEvent id: 371, type: User>,
            #<AuditEvent id: 370, type: Project>,
            #<AuditEvent id: 369, type: Group>
        ]
      DOC

      markdown <<~DOC
        | Attribute | Type | Description |
        | --------- | ---- | | ----------- |
        | created_after | string | Return audit events created on or after the given time. Format: ISO 8601 YYYY-MM-DDTHH:MM:SSZ  |
        | created_before | string | Return audit events created on or before the given time. Format: ISO 8601 YYYY-MM-DDTHH:MM:SSZ |
        | entity_type | string | Return audit events for the given entity type. Valid values are: User, Group, or Project.  |
        | entity_id | integer | Return audit events for the given entity ID. Requires entity_type attribute to be present. |
      DOC
    end

    doc 'Instance' do
      desc 'Filter by Type'
      example 'client.audit_events.list(entity_type: "Group")'
      result <<~DOC
        => [
            #<AuditEvent id: 369, type: Group>,
            #<AuditEvent id: 380, type: Group>,
        ]
      DOC
    end

    doc 'Instance' do
      markdown <<~DOC
        ##### DateTime Helpers

        Both `:created_after` and `:created_before` can be used with Date or Time objects. (responds_to: `to_time`)
      DOC

      example <<~DOC
        client.audit_events.list(created_after: Date.today)
      DOC

      result <<~DOC
        => [#<AuditEvent id: 359, type: Project>]
      DOC
    end

    doc 'Instance' do
      example <<~DOC
        client.audit_events.list(created_before: 1.month.ago )
      DOC

      result <<~DOC
        => [#<AuditEvent id: 159, type: User>]
      DOC
    end

    # List
    def list(query = {})
      query[:created_after] = query[:created_after].to_time.iso8601 if format_time?(query[:created_after])
      query[:created_before] = query[:created_before].to_time.iso8601 if format_time?(query[:created_before])

      client.request(:get, 'audit_events', AuditEvent, query)
    end
  end
end
