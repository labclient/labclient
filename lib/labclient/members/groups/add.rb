# Top namespace
module LabClient
  # Specifics
  class GroupMembers < Common
    doc 'Group' do
      title 'Add'
      desc 'Gets a list of group or group members viewable by the authenticated user. [Group ID, User ID, Hash]'
      example 'client.members.groups.add(264, 14, access_level: :developer)'
      result '=> #<Member name: Tristana, access: developer>'

      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | access_level | integer | yes | A valid access level |
        | expires_at | string | no | A date string in the format YEAR-MONTH-DAY |
      DOC
    end

    doc 'Group' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(264)
        group.member_add(14, access_level: :reporter)
      DOC
    end

    def add(group_id, user_id, query)
      query_format_time(query, :expires_at)
      query_access_level(query, :access_level)
      group_id = format_id(group_id)
      user_id = format_id(user_id)
      query[:user_id] = user_id

      client.request(:post, "groups/#{group_id}/members", Member, query)
    end
  end
end
