# Top namespace
module LabClient
  # Specifics
  class GroupMembers < Common
    doc 'Group' do
      title 'Update'
      desc 'Updates a member of a group or group. [Group ID, User ID, Hash]'
      example 'client.members.groups.update(264, 14, access_level: :reporter)'
      result '=> #<Member name: Tristana, access: reporter>'

      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | access_level | integer | yes | A valid access level |
        | expires_at | string | no | A date string in the format YEAR-MONTH-DAY |
      DOC
    end

    doc 'Group' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(264)
        group.member_update(14, access_level: :developer)
      DOC
    end

    def update(group_id, user_id, query)
      query_format_time(query, :expires_at)
      query_access_level(query, :access_level)
      group_id = format_id(group_id)
      user_id = format_id(user_id)

      client.request(:put, "groups/#{group_id}/members/#{user_id}", Member, query)
    end
  end
end
