# Top namespace
module LabClient
  # Specifics
  class GroupMembers < Common
    @group_name = 'Members'

    doc 'Group' do
      title 'List'
      desc 'Gets a list of group or group members viewable by the authenticated user. [Group ID, Hash]'
      example 'client.members.groups.list(264)'
      result '[#<Member name: Administrator, access: owner>, #<Member name: Shyvanna, access: maintainer>, ... ]'
    end

    doc 'Group' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(264,1)
        group.members
      DOC
    end

    doc 'Group' do
      subtitle 'List All'
      desc 'List including inherited members. [Group ID, Hash]'
      example 'client.members.groups.list_all(264)'
    end

    doc 'Group' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(264,1)
        group.members_all
      DOC
    end

    doc 'Examples' do
      subtitle 'Groups'
      desc 'Only Developers'
      example 'client.members.groups.list_all(264).select(&:developer?)'
    end

    doc 'Examples' do
      desc 'Except Owners'
      example 'client.members.groups.list_all(264).reject(&:owner?)'
    end

    doc 'Examples' do
      desc 'Higher Permission than Guest'
      example 'client.members.groups.list_all(264).select {|x| x.greater_than :guest }'
    end

    # Default to False
    def list(group_id, query = nil, all = nil)
      all = '/all' if all # Reset and Use

      format_query_ids(:user_ids, query) if query

      group_id = format_id(group_id)
      client.request(:get, "groups/#{group_id}/members#{all}", Member, query)
    end

    # Default to True
    def list_all(group_id, query = nil)
      list(group_id, query, true)
    end
  end
end
