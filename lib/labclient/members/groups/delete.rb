# Top namespace
module LabClient
  # Specifics
  class GroupMembers < Common
    doc 'Group' do
      title 'Delete'
      desc 'Gets a list of group or group members viewable by the authenticated user. [Group ID, User ID]'
      example 'client.members.groups.delete(264, 14)'
    end

    doc 'Group' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(264)
        group.member_delete(14)
      DOC
    end

    def delete(group_id, user_id)
      client.request(:delete, "groups/#{group_id}/members/#{user_id}")
    end
  end
end
