# Top namespace
module LabClient
  # Specifics
  class GroupMembers < Common
    doc 'Group' do
      title 'Show'
      desc 'Gets a member of a group or group [Group ID, User ID]'
      example 'client.members.groups.show(264, 14)'
      result '#<Member name: Annie, access: developer>'
    end

    doc 'Group' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(264)
        group.member(14)
      DOC
    end

    doc 'Group' do
      subtitle 'Show All'
      desc 'Gets a member of a group or group, including members inherited through ancestor groups [Group ID, User ID]'
      example 'client.members.group.show(264, 14)'
      result '#<Member name: Annie, access: developer>'
    end

    doc 'Group' do
      desc 'via Group'
      example <<~DOC
        group = client.groups.show(264)
        group.member_all(14)
      DOC
    end

    # Default to False
    def show(group_id, user_id, all = nil)
      all = '/all' if all # Reset and Use

      user_id = format_id(user_id)
      group_id = format_id(group_id)
      client.request(:get, "groups/#{group_id}/members#{all}/#{user_id}", Member)
    end

    # Default to True
    def show_all(group_id, user_id)
      show(group_id, user_id, true)
    end
  end
end
