# Top namespace
module LabClient
  # Container
  class Members < Common
    include ClassHelpers

    def groups
      GroupMembers.new(client)
    end

    def projects
      ProjectMembers.new(client)
    end
  end
end
