# Top namespace
module LabClient
  # Specifics
  class ProjectMembers < Common
    doc 'Project' do
      title 'Show'
      desc 'Gets a member of a group or project [Project ID, User ID]'
      example 'client.members.projects.show(264, 14)'
      result '#<Member name: Annie, access: developer>'
    end

    doc 'Project' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.member(14)
      DOC
    end

    doc 'Project' do
      subtitle 'Show All'
      desc 'Gets a member of a group or project, including members inherited through ancestor groups [Project ID, User ID]'
      example 'client.members.project.show(264, 14)'
      result '#<Member name: Annie, access: developer>'
    end

    doc 'Project' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.member_all(14)
      DOC
    end

    # Default to False
    def show(project_id, user_id, all = nil)
      all = '/all' if all # Reset and Use

      user_id = format_id(user_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/members#{all}/#{user_id}", Member)
    end

    # Default to True
    def show_all(project_id, user_id)
      show(project_id, user_id, true)
    end
  end
end
