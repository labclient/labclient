# Top namespace
module LabClient
  # Specifics
  class ProjectMembers < Common
    doc 'Project' do
      title 'Add'
      desc 'Gets a list of group or project members viewable by the authenticated user. [Project ID, User ID, Hash]'
      example 'client.members.projects.add(264, 14, access_level: :developer)'
      result '=> #<Member name: Tristana, access: developer>'

      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | access_level | integer | yes | A valid access level |
        | expires_at | string | no | A date string in the format YEAR-MONTH-DAY |
      DOC
    end

    doc 'Project' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.member_add(14, access_level: :reporter)
      DOC
    end

    def add(project_id, user_id, query)
      query_format_time(query, :expires_at)
      query_access_level(query, :access_level)
      project_id = format_id(project_id)
      user_id = format_id(user_id)
      query[:user_id] = user_id

      client.request(:post, "projects/#{project_id}/members", Member, query)
    end
  end
end
