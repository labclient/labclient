# Top namespace
module LabClient
  # Specifics
  class ProjectMembers < Common
    doc 'Project' do
      title 'Delete'
      desc 'Gets a list of group or project members viewable by the authenticated user. [Project ID, User ID]'
      example 'client.members.projects.delete(264, 14)'
    end

    doc 'Project' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.member_delete(14)
      DOC
    end

    def delete(project_id, user_id)
      client.request(:delete, "projects/#{project_id}/members/#{user_id}")
    end
  end
end
