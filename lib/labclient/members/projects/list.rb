# Top namespace
module LabClient
  # Specifics
  class ProjectMembers < Common
    @group_name = 'Members'
    doc 'Project' do
      title 'List'
      desc 'Gets a list of group or project members viewable by the authenticated user. [Project ID, Hash]'
      example 'client.members.projects.list(264)'
      result '[#<Member name: Administrator, access: owner>, #<Member name: Shyvanna, access: maintainer>, ... ]'
    end

    doc 'Project' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.members
      DOC
    end

    doc 'Project' do
      subtitle 'List All'
      desc 'List including inherited members. [Project ID, Hash]'
      example 'client.members.projects.list_all(264)'
    end

    doc 'Project' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.members_all
      DOC
    end

    doc 'Examples' do
      title 'Filter'
      desc 'Only Developers'
      example 'client.members.projects.list_all(264).select(&:developer?)'
    end

    doc 'Examples' do
      desc 'Except Owners'
      example 'client.members.projects.list_all(264).reject(&:owner?)'
    end

    doc 'Examples' do
      desc 'Higher Permission than Guest'
      example 'client.members.projects.list_all(264).select {|x| x.greater_than :guest }'
    end

    # Default to False
    def list(project_id, query = nil, all = nil)
      all = '/all' if all # Reset and Use

      format_query_ids(:user_ids, query) if query

      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/members#{all}", Member, query)
    end

    # Default to True
    def list_all(project_id, query = nil)
      list(project_id, query, true)
    end
  end
end
