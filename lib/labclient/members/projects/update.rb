# Top namespace
module LabClient
  # Specifics
  class ProjectMembers < Common
    doc 'Project' do
      title 'Update'
      desc 'Updates a member of a group or project. [Project ID, User ID, Hash]'
      example 'client.members.projects.update(264, 14, access_level: :reporter)'
      result '=> #<Member name: Tristana, access: reporter>'

      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | access_level | integer | yes | A valid access level |
        | expires_at | string | no | A date string in the format YEAR-MONTH-DAY |
      DOC
    end

    doc 'Project' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.member_update(14, access_level: :developer)
      DOC
    end

    def update(project_id, user_id, query)
      query_format_time(query, :expires_at)
      query_access_level(query, :access_level)
      project_id = format_id(project_id)
      user_id = format_id(user_id)

      client.request(:put, "projects/#{project_id}/members/#{user_id}", Member, query)
    end
  end
end
