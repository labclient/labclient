# Top namespace
module LabClient
  # Inspect Helper
  class Member < Klass
    include ClassHelpers
    include AccessLevel

    def inspect
      "#<Member name: #{name}, access: #{level}>"
    end

    def level
      human_access_level(access_level)
    end

    def user
      client.users.show(id)
    end

    def guest?
      level == :guest
    end

    def reporter?
      level == :reporter
    end

    def developer?
      level == :developer
    end

    def maintainer?
      level == :maintainer
    end

    def owner?
      level == :owner
    end

    def greater_than(leveler)
      case leveler
      when Symbol
        access_level > machine_access_level(leveler)
      when Integer
        access_level > leveler
      end
    end

    help do
      subtitle 'Member'
      option 'level', 'Humanized symbol of access level. e.g. :developer'
      option 'user', 'Collect user object.'
      option '<permission>?', 'True/False evaluation each guest?, developer? etc'
      option 'greater_than', 'True/False if user has greater than [Permission Name]'
    end
  end
end
