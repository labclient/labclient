# Top namespace
module LabClient
  # Specifics
  class ProtectedBranches < Common
    doc 'Protect' do
      desc 'Protects a single repository branch or several project repository branches using a wildcard protected branch. [Project ID, Hash]'
      example 'client.protected_branches.protect(264, name: :master)'
      result '#<Branch name: master>'

      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | name                          | string         | yes | The name of the branch or wildcard |
        | push_access_level             | string         | no  | Access levels allowed to push (defaults: 40, maintainer access level) |
        | merge_access_level            | string         | no  | Access levels allowed to merge (defaults: 40, maintainer access level) |
        | unprotect_access_level        | string         | no  | Access levels allowed to unprotect (defaults: 40, maintainer access level) |
        | allowed_to_push               | array          | no  | **(STARTER)** Array of access levels allowed to push, with each described by a hash |
        | allowed_to_merge              | array          | no  | **(STARTER)** Array of access levels allowed to merge, with each described by a hash |
        | allowed_to_unprotect          | array          | no  | **(STARTER)** Array of access levels allowed to unprotect, with each described by a hash |
        | code_owner_approval_required  | boolean        | no  | **(PREMIUM)** Prevent pushes to this branch if it matches an item in the CODEOWNERS file (defaults: false) |

        **Valid access levels**

        - 0, :none  => No access
        - 30, :developer => Developer access
        - 40, :maintainer => Maintainer access
        - 60, :admin => Admin access

      DOC
    end

    doc 'Protect' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.protect_branch(name: :other, push_access_level: :admin)
      DOC
    end

    def protect(project_id, query)
      protected_query_access_level(query, :push_access_level)
      protected_query_access_level(query, :merge_access_level)
      protected_query_access_level(query, :unprotect_access_level)
      # POST /projects/:id/protected_branches

      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/protected_branches", Branch, query)
    end
  end
end
