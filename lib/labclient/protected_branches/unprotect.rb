# Top namespace
module LabClient
  # Specifics
  class ProtectedBranches < Common
    doc 'Unprotect' do
      desc 'Unprotects the given protected branch or wildcard protected branch [Project ID, Branch]'
      example 'client.protected_branches.unprotect(264, :other)'
      result '#<Branch name: master>'
    end

    doc 'Unprotect' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.unprotect_branch(:other)
      DOC
    end

    def unprotect(project_id, branch_id)
      project_id = format_id(project_id)
      client.request(:delete, "projects/#{project_id}/protected_branches/#{branch_id}", Branch)
    end
  end
end
