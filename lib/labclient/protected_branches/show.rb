# Top namespace
module LabClient
  # Specifics
  class ProtectedBranches < Common
    doc 'Show' do
      desc 'Gets a single protected branch or wildcard protected branch. [Project ID, Branch]'
      example 'client.protected_branches.show(264, :master)'
      result '#<Branch name: master>'
    end

    doc 'Show' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.protected_branch(:master)
      DOC
    end

    def show(project_id, branch_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/protected_branches/#{branch_id}", Branch)
    end
  end
end
