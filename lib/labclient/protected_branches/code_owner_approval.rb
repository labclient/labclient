# Top namespace
module LabClient
  # Specifics
  class ProtectedBranches < Common
    doc 'Approval' do
      desc 'Update the “code owner approval required” option for the given protected branch protected branch. [Project ID, Branch]'
      example 'client.protected_branches.code_owner_approval(264, :other, true)'
      result '#<Branch name: feature>'
    end

    doc 'Unprotect' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(264)
        project.branch_code_owner_approval(:other, true)
      DOC
    end

    def code_owner_approval(project_id, branch_id, approv_id = true)
      query = { code_owner_approval_required: approv_id }

      project_id = format_id(project_id)
      client.request(:patch, "projects/#{project_id}/protected_branches/#{branch_id}", Branch, query)
    end
  end
end
