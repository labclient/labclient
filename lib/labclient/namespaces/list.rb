# Top namespace
module LabClient
  # Specifics
  class Namespaces < Common
    doc 'List' do
      desc 'Get a list of the namespaces [Search String]'
      example 'client.namespaces.list'
      result <<~DOC
        => [#<Namespace id: 2, kind: user, path: lulu>,#<Namespace id: 4, kind: user, path: kinglotharon>, ... ]
      DOC
    end

    doc 'List' do
      subtitle 'Search'
      example 'client.namespaces.list("scott")'
      result <<~DOC
        => [#<Namespace id: 20, kind: user, path: montgomeryscott>]
      DOC
    end

    # List
    def list(search = nil)
      if search
        client.request(:get, 'namespaces', Namespace, { search: search })
      else
        client.request(:get, 'namespaces', Namespace)
      end
    end
  end
end
