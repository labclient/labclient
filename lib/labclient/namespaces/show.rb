# Top namespace
module LabClient
  # Specifics
  class Namespaces < Common
    doc 'Show' do
      desc 'Get a namespace by ID. [Namespace ID]'
      example 'client.namespaces.show(5)'
      result <<~DOC
        => #<Namespace id: 5, kind: user, path: janna>
      DOC
    end

    def show(namespace_id)
      namespace_id = format_id(namespace_id)
      client.request(:get, "namespaces/#{namespace_id}", Namespace)
    end
  end
end
