# Top namespace
module LabClient
  # Inspect Helper
  class Namespace < Klass
    include ClassHelpers
    def inspect
      "#<Namespace id: #{id}, kind: #{kind}, path: #{path}>"
    end
  end
end
