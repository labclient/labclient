# Top namespace
module LabClient
  # Inspect Helper
  class Tag < Klass
    include ClassHelpers
    date_time_attrs %i[authored_date committed_date created_at]

    def inspect
      "#<Tag name: #{name}>"
    end

    def commit
      Commit.new(@table[:commit], response, client)
    end
  end
end
