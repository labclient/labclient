# Top namespace
module LabClient
  # Specifics
  class Tags < Common
    doc 'Delete' do
      desc 'Deletes a tag of a repository with given name. [Project ID, Tag Name]'
      example 'client.tags.create(298, :other)'
    end

    doc 'Delete' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(298)
        project.tag_delete(:other)
      DOC
    end

    def delete(project_id, tag_id)
      project_id = format_id(project_id)
      tag_id = format_id(tag_id)

      client.request(:delete, "projects/#{project_id}/repository/tags/#{tag_id}", Tag)
    end
  end
end
