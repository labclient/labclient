# Top namespace
module LabClient
  # Specifics
  class Tags < Common
    doc 'Show' do
      desc 'Get a specific repository tag determined by its name. [Project ID, String]'
      example 'client.tags.show(298, :initial)'
      result '#<Tag name: initial>'
    end

    doc 'Show' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(298)
        project.tag(:initial)
      DOC
    end

    def show(project_id, tag_id)
      project_id = format_id(project_id)
      tag_id = format_id(tag_id)

      client.request(:get, "projects/#{project_id}/repository/tags/#{tag_id}", Tag)
    end
  end
end
