# Top namespace
module LabClient
  # Specifics
  class Tags < Common
    doc 'Release' do
      title 'Create'
      desc 'Add release notes to the existing Git tag. [Project ID, Tag Name, Description]'
      example 'client.tags.release(298, :initial, "Special release notes!")'
      result '{:tag_name=>"initial", :description=>"Special release notes!"}'
    end

    doc 'Release' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(298)
        project.tag_release(:initial, 'Special release notes!')
      DOC
    end

    def release(project_id, tag_id, description)
      project_id = format_id(project_id)
      tag_id = format_id(tag_id)

      client.request(:post, "projects/#{project_id}/repository/tags/#{tag_id}/release", nil, description: description)
    end
  end
end
