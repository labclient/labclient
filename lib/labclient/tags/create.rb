# Top namespace
module LabClient
  # Specifics
  class Tags < Common
    doc 'Create' do
      desc 'Creates a new tag in the repository that points to the supplied ref. [Project ID, Hash]'
      example 'client.tags.create(298, tag_name: :new, ref: :master)'
      result '#<Tag name: initial>'

      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | tag_name | string | yes | The name of a tag |
        | ref | string | yes | Create tag using commit SHA, another tag name, or branch name. |
        | message | string | no |Creates annotated tag.|
        | release_description | string | no |Add release notes to the Git tag and store it in the GitLab database.|
      DOC
    end

    doc 'Create' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(298)
        project.tag_create(:initial)
      DOC
    end

    def create(project_id, query)
      project_id = format_id(project_id)

      client.request(:post, "projects/#{project_id}/repository/tags", Tag, query)
    end
  end
end
