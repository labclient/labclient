# Top namespace
module LabClient
  # Specifics
  class Tags < Common
    doc 'Release' do
      title 'Update'
      desc 'Updates the release notes of a given release. [Project ID, Tag Name, Description]'
      example 'client.tags.update(298, :initial, "Special notes!!!")'
      result '{:tag_name=>"initial", :description=>"Special notes!!!"}'
    end

    doc 'Release' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(298)
        project.tag_update(:initial, 'Special release notes!')
      DOC
    end

    def update(project_id, tag_id, description)
      project_id = format_id(project_id)
      tag_id = format_id(tag_id)

      client.request(:put, "projects/#{project_id}/repository/tags/#{tag_id}/release", nil, description: description)
    end
  end
end
