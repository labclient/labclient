# Top namespace
module LabClient
  # Specifics
  class Tags < Common
    doc 'List' do
      desc 'Get a list of repository tags from a project. [Project ID, Hash]'
      example 'client.tags.list(298)'
      result '[#<Tag name: initial>]'

      markdown <<~DOC

        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | order_by | string | no | Return tags ordered by name or updated fields. Default is updated |
        | sort | string | no | Return tags sorted in asc or desc order. Default is desc |
        | search | string | no | Return list of tags matching the search criteria. You can use ^term and term$ to find tags that begin and end with term respectively. |
      DOC
    end

    doc 'List' do
      desc 'via Project'
      example <<~DOC
        project = client.projects.show(298)
        project.tags
      DOC
    end

    def list(project_id, query = {})
      project_id = format_id(project_id)

      client.request(:get, "projects/#{project_id}/repository/tags", Tag, query)
    end
  end
end
