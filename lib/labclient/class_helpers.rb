# Top Namesapce
module LabClient
  # Shared Methods
  module ClassHelpers
    def keys
      to_h.keys.sort
    end

    def has?(key)
      @table.key?(key) && !@table[key].blank?
    end

    def raw
      to_h
    end
  end
end
