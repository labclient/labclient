# The glorious gitlab labclient of labs
module LabClient
  # Helper for Docs
  class Search
    extend Docs

    doc 'Search' do
      title ''
      desc 'Lots! Search Scopes are on each of the individual scope endpoints'

      markdown <<~DOC
        - <a target="_blank" href="/labclient/#Users-Search">Users</a>
        - <a target="_blank" href="/labclient/#Projects-Search">Projects</a>
        - <a target="_blank" href="/labclient/#Groups-Search">Groups</a>
      DOC
    end
  end
end
