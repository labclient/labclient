# Top namespace
module LabClient
  # Specifics
  class DeployKeys < Common
    doc 'Update' do
      desc 'Updates a deploy key for a project.'
      markdown <<~DOC
        | Attribute  | Type | Required | Description |
        | ---------  | ---- | -------- | ----------- |
        | id       | integer/string | yes | The ID or URL-encoded path of the project owned by the authenticated user |
        | title    | string  | no | New deploy key's title |
        | can_push | boolean | no  | Can deploy key push to the project's repository |
      DOC
    end

    doc 'Update' do
      example <<~DOC
        client.deploy_keys.update(330, 2, title: "Very Special Key")
      DOC
      result <<~DOC
        => #<Deploy Key id: 3, title: Very Special Key>
      DOC
    end

    doc 'Update' do
      desc 'Via DeployKey'
      example <<~DOC
        key = client.deploy_keys.show(330,2)
        key.update(title "Moar Special")
      DOC
      result <<~DOC
        => #<Deploy Key id: 2, title: Moar Special>
      DOC
    end

    doc 'Update' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.deploy_key_update(2,
          title: "Special Key",
          key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHTAOsjGAI4ZGW5QZQSXhq...'
        )
      DOC
    end

    # Update
    def update(project_id, deploy_key_id, query = {})
      project_id = format_id(project_id)
      client.request(:put, "projects/#{project_id}/deploy_keys/#{deploy_key_id}", DeployKey, query)
    end
  end
end
