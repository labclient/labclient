# Top namespace
module LabClient
  # Helpers for Projects
  module ProjectDeployKey
    def deploy_keys
      client.deploy_keys.list(id)
    end

    def deploy_key_add(query)
      client.deploy_keys.add(id, query)
    end

    def deploy_key_show(key_id)
      client.deploy_keys.show(id, key_id)
    end

    def deploy_key_update(key_id, query)
      client.deploy_keys.update(id, key_id, query)
    end

    def deploy_key_delete(key_id)
      client.deploy_keys.delete(id, key_id)
    end

    def deploy_key_enable(key_id)
      client.deploy_keys.enable(id, key_id)
    end
  end
end
