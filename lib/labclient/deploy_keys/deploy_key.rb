# Top namespace
module LabClient
  # Inspect Helper
  class DeployKey < Klass
    include ClassHelpers

    def inspect
      "#<Deploy Key id: #{id}, title: #{title}>"
    end

    date_time_attrs %i[created_at]

    def update(query = {})
      project_id = collect_project_id

      update_self client.deploy_keys.update(project_id, id, query)
    end

    def delete
      project_id = collect_project_id
      client.deploy_keys.delete(project_id, id)
    end

    help do
      subtitle 'Deploy Key'
      option 'delete', 'Delete this deploy key.'
      option 'update', 'Update this this deploy key.'
    end
  end
end
