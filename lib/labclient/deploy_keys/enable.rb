# Top namespace
module LabClient
  # Specifics
  class DeployKeys < Common
    doc 'Enable' do
      desc 'Enables a deploy key for a project so this can be used. Returns the enabled key, with a status code 201 when successful.'
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | id      | integer/string | yes | The ID or URL-encoded path of the project owned by the authenticated user |
        | key_id  | integer | yes | The ID of the deploy key |
      DOC
    end

    doc 'Enable' do
      example <<~DOC
        client.deploy_keys.enable(330, 2)
      DOC
      result <<~DOC
        => nil
      DOC
    end

    doc 'Enable' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.deploy_key_enable(2)
      DOC
    end

    # Enable
    def enable(project_id, deploy_key_id)
      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/deploy_keys/#{deploy_key_id}/enable", DeployKey)
    end
  end
end
