# Top namespace
module LabClient
  # Specifics
  class DeployKeys < Common
    doc 'Delete' do
      desc 'Removes a deploy key from the project. If the deploy key is used only for this project, it will be deleted from the system.'
      markdown <<~DOC
        | Attribute | Type | Required | Description |
        | --------- | ---- | -------- | ----------- |
        | id      | integer/string | yes | The ID or URL-encoded path of the project owned by the authenticated user |
        | key_id  | integer | yes | The ID of the deploy key |
      DOC
    end

    doc 'Delete' do
      example <<~DOC
        client.deploy_keys.delete(330, 2)
      DOC
      result <<~DOC
        => nil
      DOC
    end

    doc 'Delete' do
      desc 'Delete through DeployKey object'
      example <<~DOC
        key = client.deploy_keys.show(330,2)
        key.delete
      DOC
    end

    doc 'Delete' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.deploy_key_delete(2)
      DOC
    end

    # Delete
    def delete(project_id, deploy_key_id)
      project_id = format_id(project_id)
      client.request(:delete, "projects/#{project_id}/deploy_keys/#{deploy_key_id}", DeployKey)
    end
  end
end
