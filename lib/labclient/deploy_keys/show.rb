# Top namespace
module LabClient
  # Specifics
  class DeployKeys < Common
    doc 'Show' do
      desc 'Get a single key.'
      example <<~DOC
        client.deploy_keys.show(330, 2)
      DOC

      result <<~DOC
        => #<Deploy Key id: 2, title: Special Key>
      DOC
    end

    doc 'Show' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.deploy_key_show(2)
      DOC
    end

    # Show
    def show(project_id, deploy_key_id)
      project_id = format_id(project_id)
      client.request(:get, "projects/#{project_id}/deploy_keys/#{deploy_key_id}", DeployKey)
    end
  end
end
