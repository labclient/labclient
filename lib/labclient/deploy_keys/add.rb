# Top namespace
module LabClient
  # Specifics
  class DeployKeys < Common
    doc 'Add' do
      desc 'Creates a new deploy key for a project.'
      markdown <<~DOC
        | Attribute  | Type | Required | Description |
        | ---------  | ---- | -------- | ----------- |
        | id       | integer/string | yes | The ID or URL-encoded path of the project owned by the authenticated user |
        | title    | string  | yes | New deploy key's title |
        | key      | string  | yes | New deploy key |
        | can_push | boolean | no  | Can deploy key push to the project's repository |
      DOC
    end

    doc 'Add' do
      example <<~DOC
        client.deploy_keys.add(330,
          title: "Special Key",
          key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHTAOsjGAI4ZGW5QZQSXhq...'
        )
      DOC
      result <<~DOC
        => #<Deploy Key id: 3, title: Special Key>
      DOC
    end

    doc 'Add' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.deploy_key_add(
          title: "Special Key",
          key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHTAOsjGAI4ZGW5QZQSXhq...'
        )
      DOC
    end

    # Add
    def add(project_id, query = {})
      project_id = format_id(project_id)
      client.request(:post, "projects/#{project_id}/deploy_keys", DeployKey, query)
    end
  end
end
