# Top namespace
module LabClient
  # Specifics
  class DeployKeys < Common
    doc 'List' do
      desc 'Get a list of all deploy keys across all projects of the GitLab instance. This endpoint requires admin access.'
      example 'client.deploy_keys.list'
      result <<~DOC
        => [#<DeployKey id: 2, title: Special Title>, #<DeployKey id: 1, title: HereWeGo>]
      DOC
    end

    doc 'List' do
      desc 'Get a list of a project’s deploy keys.'
      example 'client.deploy_keys.list(330)'
      result <<~DOC
        => [#<DeployKey id: 2, title: Special Title>, #<DeployKey id: 1, title: HereWeGo>]
      DOC
    end

    doc 'List' do
      desc 'Via Project'
      example <<~DOC
        project = client.projects.show(16)
        project.deploy_keys(2)
      DOC
    end

    # List
    def list(deploy_key_id = nil)
      if deploy_key_id
        client.request(:get, "projects/#{deploy_key_id}/deploy_keys", DeployKey)
      else
        client.request(:get, 'deploy_keys', DeployKey)
      end
    end
  end
end
