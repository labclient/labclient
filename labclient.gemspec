lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'labclient/version'

Gem::Specification.new do |spec|
  spec.name          = 'labclient'
  spec.version       = LabClient::VERSION
  spec.authors       = ['Davin Walker']
  spec.email         = ['dwalker@gitlab.com']

  spec.summary       = 'Gitlab API Client'
  spec.description   = 'Gitlab API Client.'
  spec.homepage      = 'https://gitlab.com/labclient/labclient'
  spec.license       = 'MIT'

  # Specify which files should be added to the gem when it is released.
  spec.files = Dir['lib/**/*']

  spec.require_paths = ['lib']
  spec.required_ruby_version = '>= 2.5.0'
  spec.add_dependency 'activesupport', '>= 6.0'
  spec.add_dependency 'amazing_print', '>= 1.2'
  spec.add_dependency 'gitlab_chronic_duration', '>= 0.10.6.2'
  spec.add_dependency 'kramdown', '>= 2.1.0'
  spec.add_dependency 'oj', '>= 3.10'
  spec.add_dependency 'ougai', '>= 2.0'
  spec.add_dependency 'typhoeus', '>= 1.4'

  spec.add_development_dependency 'bundler', '>= 2.2'
  spec.add_development_dependency 'faker', '~> 2.16'
  spec.add_development_dependency 'minitest', '~> 5.14'
  spec.add_development_dependency 'minitest-reporters', '~> 1.4'
  spec.add_development_dependency 'pry', '~> 0.12'
  spec.add_development_dependency 'rack-test', '~> 1.1.0'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rdoc', '~> 6.2.0'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-minitest', '~> 0.10'
  spec.add_development_dependency 'rubocop-rake', '~> 0.5'
  spec.add_development_dependency 'simplecov', '~> 0.21'
  spec.add_development_dependency 'webmock', '~> 3.7'
  spec.metadata = {
    'rubygems_mfa_required' => 'true'
  }
end
