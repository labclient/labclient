#!/usr/bin/env ruby
# Helper to generate the docs.json that will be used to generate the Ember site

require 'labclient'

File.write('docs.json', LabClient::Docs.json)
