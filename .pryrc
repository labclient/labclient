require 'labclient'
require 'benchmark'

# rubocop:disable Lint/UselessAssignment
client = LabClient::Client.new unless ENV['LABCLIENT_TESTING']
# rubocop:enable Lint/UselessAssignment
