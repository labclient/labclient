#!/usr/bin/env ash

# User Input
echo "$@" >> /root/start.rb

# Drop to PRY Shell afterwards
echo "binding.pry" >> /root/start.rb
pry /root/start.rb