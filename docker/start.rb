#!/usr/bin/env ruby
require 'labclient'
require 'pry'

# Disable Less
Pry.config.pager = false

client = LabClient::Client.new
